import BootcampData from './bootcamp';
import CoursesData from './courses';
import InstructorData from './instructors';
import StudentsData from './students';

export { BootcampData, CoursesData, InstructorData, StudentsData };
