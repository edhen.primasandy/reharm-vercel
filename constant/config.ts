/* eslint-disable import/no-anonymous-default-export */
const appName = process.env.NEXT_PUBLIC_APP_NAME;
const apiURL = process.env.NEXT_PUBLIC_API_URL;
const assetUrl = process.env.NEXT_PUBLIC_BASE_URL_ASSET;
const googleApiKey = process.env.NEXT_PUBLIC_GOOGLE_API_KEY;
const googleCLientID = process.env.NEXT_PUBLIC_GOOGLE_CLIENTID;
const midtransClientKey = process.env.NEXT_PUBLIC_MIDTRANS_CLIENT_KEY;
const midtransURL = process.env.NEXT_PUBLIC_MIDTRANS_URL;
const recaptchaSiteKey = process.env.NEXT_PUBLIC_RECAPTCHA_SITE_KEY;
const recaptchaSecretKey = process.env.NEXT_PUBLIC_RECAPTCHA_SECRET_KEY;

const firebaseApiKey = process.env.NEXT_PUBLIC_FIREBASE_APIKEY;
const firebaseAuthDomain = process.env.NEXT_PUBLIC_FIREBASE_AUTHDOMAIN;
const firebaseProjectId = process.env.NEXT_PUBLIC_FIREBASE_PROJECTID;
const firebaseStorageBucket = process.env.NEXT_PUBLIC_FIREBASE_STORAGEBUCKET;
const firebaseMessagingSenderId =
  process.env.NEXT_PUBLIC_FIREBASE_MESSAGINGSENDERID;
const firebaseAppId = process.env.NEXT_PUBLIC_FIREBASE_APPID;
const firebaseMeasurementId = process.env.NEXT_PUBLIC_FIREBASE_MEASUREMENTID;

export default {
  appName,
  apiURL,
  assetUrl,
  googleApiKey,
  googleCLientID,
  midtransClientKey,
  midtransURL,
  recaptchaSiteKey,
  recaptchaSecretKey,
  firebaseApiKey,
  firebaseAuthDomain,
  firebaseProjectId,
  firebaseStorageBucket,
  firebaseMessagingSenderId,
  firebaseAppId,
  firebaseMeasurementId,
};
