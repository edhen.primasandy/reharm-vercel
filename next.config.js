/* eslint-disable import/no-extraneous-dependencies */
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

module.exports = withBundleAnalyzer({
  eslint: {
    dirs: ['.'],
  },
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'api-dev.reharm.com',
        port: '',
        pathname: '/storage/**',
      },
    ],
    unoptimized: true,
  },
  poweredByHeader: false,
  trailingSlash: true,
  basePath: '',
  // The starter code load resources from `public` folder with `router.basePath` in React components.
  // So, the source code is "basePath-ready".
  // You can remove `basePath` if you don't need it.
  reactStrictMode: true,
  // output: 'export',
  // distDir: '_static',
  async redirects() {
    return [
      {
        source: '/admin/',
        destination: '/admin/dashboard',
        permanent: true,
      },
    ];
  },
});
