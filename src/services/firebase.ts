// Import the functions you need from the SDKs you need
import { Config } from '@constant/index';
import { initializeApp } from 'firebase/app';

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: Config.firebaseApiKey,
  authDomain: Config.firebaseAuthDomain,
  projectId: Config.firebaseProjectId,
  storageBucket: Config.firebaseStorageBucket,
  messagingSenderId: Config.firebaseMessagingSenderId,
  appId: Config.firebaseAppId,
  measurementId: Config.firebaseMeasurementId,
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const auth = getAuth(app);
// const provider = new GoogleAuthProvider();

export { app };
