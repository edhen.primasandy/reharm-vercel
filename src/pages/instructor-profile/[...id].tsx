/* eslint-disable tailwindcss/no-custom-classname */
import { Rating, Table, Tabs } from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import { BsFacebook, BsLinkedin, BsTwitter } from 'react-icons/bs';
import { HiOutlineMail, HiUserCircle } from 'react-icons/hi';
import { MdOutlineLibraryMusic } from 'react-icons/md';

import Breadscrumb from '@/components/elements/breadscrumb';
import { useActions } from '@/overmind';
import Main from '@/templates/Main';

const Profile = () => {
  const router = useRouter();
  const { getUserInstructor } = useActions();
  const [data, setData] = React.useState({} as any);

  React.useEffect(() => {
    if (router.query.id) {
      getUserInstructor({ id: router.query.id.toString() }).then((res) => {
        setData(res);
      });
    }

    return () => {};
  }, [router.query.id]);
  return (
    <section>
      <div className="container mx-auto mt-20 flex items-center">
        <div className="w-full">
          <Breadscrumb
            homeRoute="/"
            dataItem={[{ label: 'Instructor profile', route: '' }]}
          />
          <h1 className="mb-2 text-4xl font-semibold">
            {data?.first_name} {data?.last_name}
          </h1>
          <div className="mb-5 rounded-2xl bg-white p-10">
            <div className="grid w-full grid-cols-1 gap-6 lg:grid-cols-10">
              <div className="lg:col-span-3">
                <div
                  className="flex items-center justify-center space-x-4"
                  data-testid="flowbite-avatar"
                >
                  <div className="relative">
                    <img
                      alt=""
                      src={
                        data?.photo
                          ? data?.photo
                          : 'https://flowbite.com/docs/images/people/profile-picture-5.jpg'
                      }
                      className="h-36 w-36 !rounded-full object-cover p-1 ring-2 ring-gray-300"
                      data-testid="flowbite-avatar-img"
                    />
                  </div>
                </div>
                <div className="mt-4 flex justify-center space-x-6">
                  <a
                    href={data?.social_links?.facebook}
                    target="_blank"
                    className="text-gray-500 hover:text-blue-600 dark:hover:text-white"
                    rel="noreferrer"
                  >
                    <BsFacebook />
                  </a>
                  <a
                    href={data?.social_links?.twitter}
                    target="_blank"
                    className="text-gray-500 hover:text-sky-400"
                    rel="noreferrer"
                  >
                    <BsTwitter />
                  </a>
                  <a
                    href={data?.social_links?.linkedin}
                    target="_blank"
                    className="text-gray-500 hover:text-blue-700"
                    rel="noreferrer"
                  >
                    <BsLinkedin />
                  </a>
                  <a
                    href={`mailto:${data?.email}`}
                    className="text-gray-500 hover:text-gray-900"
                  >
                    <HiOutlineMail />
                  </a>
                </div>
                <hr className="my-4 border-gray-200 sm:mx-auto" />
                <div className="text-lg text-gray-900">
                  {data?.first_name} {data?.last_name}
                </div>
                <hr className="my-4 border-gray-200 sm:mx-auto" />
                <div className="text-lg text-gray-900">
                  <span className="font-semibold">
                    {data?.instructor?.total_students}
                  </span>{' '}
                  total students
                </div>
                <hr className="my-4 border-gray-200 sm:mx-auto" />
                <div className="text-lg text-gray-900">
                  <span className="font-semibold">
                    {data?.instructor?.total_courses}
                  </span>{' '}
                  total courses
                </div>
                <hr className="my-4 border-gray-200 sm:mx-auto" />
                <div className="text-lg text-gray-900">
                  <span className="font-semibold">
                    {data?.instructor?.total_ratings}
                  </span>{' '}
                  total reviews
                </div>
              </div>
              <div className="lg:col-span-7">
                <Tabs.Group style="underline">
                  <Tabs.Item active title="Biography" icon={HiUserCircle}>
                    <div className="rounded bg-slate-50 p-4">
                      <p>{data?.biography}</p>
                    </div>
                  </Tabs.Item>
                  <Tabs.Item
                    title="Instructor courses"
                    icon={MdOutlineLibraryMusic}
                  >
                    <div className="hidden rounded bg-slate-50 p-4 md:block">
                      <Table>
                        <Table.Head>
                          <Table.HeadCell>Category</Table.HeadCell>
                          <Table.HeadCell>Course name</Table.HeadCell>
                          <Table.HeadCell>Rating</Table.HeadCell>
                        </Table.Head>
                        <Table.Body className="divide-y">
                          {data?.instructor?.courses?.map(
                            (item: any, i: number) => (
                              <Table.Row className="bg-white" key={i}>
                                <Table.Cell>{item.category}</Table.Cell>
                                <Table.Cell>
                                  <button
                                    className="font-semibold text-pink-600 hover:text-pink-800"
                                    onClick={() => {
                                      const href = `/courses/${item.id}`;
                                      router.push(href);
                                    }}
                                  >
                                    {item.course_name}
                                  </button>
                                </Table.Cell>
                                <Table.Cell>
                                  <div className="flex flex-row items-center justify-between">
                                    <div>
                                      <Rating>
                                        <Rating.Star
                                          filled={item?.rating >= 1}
                                        />
                                        <Rating.Star
                                          filled={item?.rating >= 2}
                                        />
                                        <Rating.Star
                                          filled={item?.rating >= 3}
                                        />
                                        <Rating.Star
                                          filled={item?.rating >= 4}
                                        />
                                        <Rating.Star
                                          filled={item?.rating >= 5}
                                        />
                                      </Rating>
                                    </div>
                                  </div>
                                </Table.Cell>
                              </Table.Row>
                            )
                          )}
                        </Table.Body>
                      </Table>
                    </div>
                    <div className="block p-4 md:hidden">
                      {data?.instructor?.courses?.map(
                        (item: any, i: number) => (
                          <div
                            key={i}
                            className="mb-2 block max-w-sm rounded-lg border border-gray-200 bg-white p-6 shadow hover:bg-gray-100"
                          >
                            <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900">
                              {item.course_name}
                            </h5>
                            <p className="font-normal text-gray-700 dark:text-gray-400">
                              {item.category}
                            </p>
                            <div className="flex flex-row items-center justify-between">
                              <div>
                                <Rating>
                                  <Rating.Star filled={item?.rating >= 1} />
                                  <Rating.Star filled={item?.rating >= 2} />
                                  <Rating.Star filled={item?.rating >= 3} />
                                  <Rating.Star filled={item?.rating >= 4} />
                                  <Rating.Star filled={item?.rating >= 5} />
                                </Rating>
                              </div>
                            </div>
                          </div>
                        )
                      )}
                    </div>
                  </Tabs.Item>
                </Tabs.Group>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Profile;

Profile.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
