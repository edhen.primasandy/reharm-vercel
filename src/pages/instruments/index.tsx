import TokopediaIcon from '@images/tokopedia-icon.png';
import { Label, Pagination, Radio } from 'flowbite-react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';
import { RiEqualizerLine } from 'react-icons/ri';
import Lottie from 'react-lottie';

import Breadscrumb from '@/components/elements/breadscrumb';
import { useActions, useState } from '@/overmind';
import * as animationData from '@/public/assets/lottie/no-data-found.json';
import Main from '@/templates/Main';
import helper from '@/utils/helper';

const Layout = () => {
  const router = useRouter();
  const { user } = useState();
  const { getInstrumentCategories, getListInstrument } = useActions();
  const [expandFilter, setExpandFilter] = React.useState(true);
  const [loading, setLoading] = React.useState(false);
  const [dataCategories, setDataCategories] = React.useState([] as any);
  const [dataInstruments, setDataInstruments] = React.useState([] as any);
  const [currPage, setCurrPage] = React.useState(1);
  const [totalPages, setTotalPages] = React.useState(1);
  const [filter, setFilter] = React.useState({
    category_id: 0,
    title: '',
    skip: 0,
    limit: 6,
    status: 'active',
    sort: '',
    sort_type: 'asc',
    min_price: 0,
    max_price: 0,
    tuts: '',
    speaker: '',
    accompaniment: '',
    weight: '',
  });
  const tuts = [
    { value: 'light', label: 'Light' },
    { value: 'heavy', label: 'Heavy' },
  ];
  const speaker = [
    { value: 'none', label: 'None' },
    { value: 'built-in', label: 'Built-in' },
  ];
  const accompaniment = [
    { value: 'none', label: 'None' },
    { value: 'available', label: 'Available' },
  ];
  const weight = [
    { value: '<5kg', label: '<5kg' },
    { value: '>5kg', label: '>5kg' },
  ];

  const initData = () => {
    setLoading(true);
    const payload = { ...filter };
    getListInstrument(payload).then((res) => {
      setDataInstruments(res.items);
      const remainder = res.total % filter.limit;
      let count = res.total / filter.limit;
      if (remainder > 0) {
        count = Math.ceil(count);
      }
      setTotalPages(count);
      setLoading(false);
    });
  };
  React.useEffect(() => {
    getInstrumentCategories({}).then((res) => {
      setDataCategories(res.items);
    });
    return () => {};
  }, []);
  React.useEffect(() => {
    initData();
    return () => {};
  }, [filter, user]);
  const onPageChange = (page: number) => {
    if (currPage !== page) {
      setFilter({ ...filter, skip: (page - 1) * filter.limit });
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
    setCurrPage(page);
  };
  const onFilterByCategory = (value: any) => {
    setCurrPage(1);
    setFilter({ ...filter, category_id: parseInt(value, 10), skip: 0 });
  };

  const onChangeFilterByMinPrice = (value: any) => {
    setCurrPage(1);
    setFilter({ ...filter, min_price: parseFloat(value), skip: 0 });
  };
  const onChangeFilterByMaxPrice = (value: any) => {
    setCurrPage(1);
    setFilter({ ...filter, max_price: parseFloat(value), skip: 0 });
  };

  const Card = ({ item }: any) => {
    return (
      <div className="relative mb-3 flex h-full flex-col rounded-2xl bg-white shadow-md duration-200 ease-in hover:scale-105 hover:shadow-md">
        <div className="h-60">
          {item.image_url ? (
            <img
              src={item.image_url}
              className="h-60 w-full rounded-t-2xl object-cover"
              alt=""
            />
          ) : (
            <img
              src="https://edhenprimasandy.sirv.com/reharm/no-image.jpg"
              className="h-60 w-full rounded-t-2xl object-cover"
              alt=""
            />
          )}
        </div>
        <div className="mt-[-20px] flex flex-col rounded-2xl bg-white p-4">
          <span className="font-semibold text-gray-500">
            <span className="font-normal">
              {
                dataCategories?.find((o: any) => o.id === item.category_id)
                  ?.name
              }
            </span>
          </span>
          <span className="mb-2 text-lg font-semibold">{item.title}</span>
          <div className="mb-2 border-b-2 border-dashed pb-2">
            <div className="mb-5 min-h-[50px] text-xs line-clamp-3">
              {item.description}
            </div>
            <div className="mb-5">
              <div className="mt-4 flex flex-row flex-wrap items-center gap-2">
                {item?.discounted_price > 0 && (
                  <>
                    <span className="rounded-md bg-red-500 px-2 text-sm text-white">
                      {Math.round(
                        ((item.price - item.discounted_price) / item.price) *
                          100
                      )}{' '}
                      %
                    </span>
                    <span className="text-base line-through">
                      {helper.currencyFormat(item?.price)}
                    </span>
                  </>
                )}

                <span className="text-lg font-bold text-red-600">
                  {helper.currencyFormat(
                    item.discounted_price ? item.discounted_price : item.price
                  )}
                </span>
              </div>
            </div>
          </div>
          <div className="grid grid-cols-2 gap-3">
            <button
              type="button"
              onClick={() => {
                const href = `/instruments/${item.id}`;
                router.push(href);
              }}
              className="mb-2 w-full rounded-full bg-orange-500 px-5 py-2.5 text-sm font-medium text-white hover:bg-orange-600 focus:outline-none focus:ring-4 focus:ring-orange-300 dark:bg-orange-600 dark:hover:bg-orange-700 dark:focus:ring-orange-800"
            >
              View detail
            </button>
            <a
              target="_blank"
              href={item.tokopedia_url}
              type="button"
              className="mr-2 mb-2 inline-flex items-center gap-3 rounded-full bg-[#00d451] px-5 py-2.5 text-center text-sm font-medium text-white hover:bg-[#00d451]/90 focus:outline-none focus:ring-4 focus:ring-[#1da1f2]/50"
              rel="noreferrer"
            >
              <Image className="h-6 w-6" src={TokopediaIcon} alt={''} />
              Buy
            </a>
          </div>
        </div>
      </div>
    );
  };
  return (
    <section>
      <div className="container mx-auto mt-20 flex items-center">
        <div className="w-full">
          <Breadscrumb
            homeRoute="/"
            dataItem={[{ label: 'Instruments', route: '' }]}
          />
          <h1 className="mb-2 text-4xl font-semibold">Instruments</h1>
          <div className="mb-5 grid w-full grid-cols-1 gap-6 md:grid-cols-4">
            <div>
              <div className="mb-5 rounded-2xl bg-white px-4 py-5 shadow-md">
                <div className="flex flex-row items-center justify-between">
                  <span className="text-base font-semibold">Filters</span>
                  <button onClick={() => setExpandFilter(!expandFilter)}>
                    <RiEqualizerLine />
                  </button>
                </div>
                <div
                  className={`transition duration-1000 ease-out ${
                    expandFilter ? 'visible' : 'hidden'
                  }`}
                >
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <label className="mb-2 block text-sm font-semibold text-gray-900">
                      Title
                    </label>
                    <input
                      type="text"
                      className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500"
                      placeholder="Title"
                      onChange={(e) => {
                        setCurrPage(1);
                        setFilter({
                          ...filter,
                          title: e.target.value,
                          skip: 0,
                        });
                      }}
                    />
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Categories</span>
                    <div
                      onChange={(e: any) => onFilterByCategory(e.target?.value)}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'all'}
                            name="categories"
                            value={0}
                            defaultChecked={filter.category_id === 0}
                          />
                          <Label htmlFor={'all'}>All Category</Label>
                        </div>
                      </div>
                      {dataCategories.map((item: any, i: number) => (
                        <div key={i} className="mb-4 ml-2">
                          <div className="mb-2 flex items-center gap-2">
                            <Radio
                              id={item.id}
                              name="categories"
                              value={item.id}
                              defaultChecked={filter.category_id === item.id}
                            />
                            <Label htmlFor={item.id}>{item.name}</Label>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Tuts</span>
                    <div
                      onChange={(e: any) => {
                        setCurrPage(1);
                        setFilter({
                          ...filter,
                          tuts: e.target?.value,
                          skip: 0,
                        });
                      }}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'all'}
                            name="tuts"
                            value={''}
                            checked={filter.tuts === ''}
                          />
                          <Label htmlFor={'all'}>All</Label>
                        </div>
                      </div>
                      {tuts.map((item: any, i: number) => (
                        <div key={i} className="mb-4 ml-2">
                          <div className="mb-2 flex items-center gap-2">
                            <Radio
                              id={item.value}
                              name="tuts"
                              value={item.value}
                              defaultChecked={filter.tuts === item.value}
                            />
                            <Label htmlFor={item.value}>{item.label}</Label>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Speaker</span>
                    <div
                      onChange={(e: any) => {
                        setCurrPage(1);
                        setFilter({
                          ...filter,
                          speaker: e.target?.value,
                          skip: 0,
                        });
                      }}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'all'}
                            name="speaker"
                            value={''}
                            defaultChecked={filter.speaker === ''}
                          />
                          <Label htmlFor={'all'}>All</Label>
                        </div>
                      </div>
                      {speaker.map((item: any, i: number) => (
                        <div key={i} className="mb-4 ml-2">
                          <div className="mb-2 flex items-center gap-2">
                            <Radio
                              id={item.value}
                              name="speaker"
                              value={item.value}
                              defaultChecked={filter.speaker === item.value}
                            />
                            <Label htmlFor={item.value}>{item.label}</Label>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Accompaniment</span>
                    <div
                      onChange={(e: any) => {
                        setCurrPage(1);
                        setFilter({
                          ...filter,
                          accompaniment: e.target?.value,
                          skip: 0,
                        });
                      }}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'all'}
                            name="accompaniment"
                            value={''}
                            defaultChecked={filter.accompaniment === ''}
                          />
                          <Label htmlFor={'all'}>All</Label>
                        </div>
                      </div>
                      {accompaniment.map((item: any, i: number) => (
                        <div key={i} className="mb-4 ml-2">
                          <div className="mb-2 flex items-center gap-2">
                            <Radio
                              id={item.value}
                              name="accompaniment"
                              value={item.value}
                              defaultChecked={
                                filter.accompaniment === item.value
                              }
                            />
                            <Label htmlFor={item.value}>{item.label}</Label>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Weight</span>
                    <div
                      onChange={(e: any) => {
                        setCurrPage(1);
                        setFilter({
                          ...filter,
                          weight: e.target?.value,
                          skip: 0,
                        });
                      }}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'all'}
                            name="weight"
                            value={''}
                            defaultChecked={filter.weight === ''}
                          />
                          <Label htmlFor={'all'}>All</Label>
                        </div>
                      </div>
                      {weight.map((item: any, i: number) => (
                        <div key={i} className="mb-4 ml-2">
                          <div className="mb-2 flex items-center gap-2">
                            <Radio
                              id={item.value}
                              name="weight"
                              value={item.value}
                              defaultChecked={filter.weight === item.value}
                            />
                            <Label htmlFor={item.value}>{item.label}</Label>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Price</span>
                    <div className="relative mb-5">
                      <div className="pointer-events-none absolute inset-y-[1px] left-[1px] flex items-center rounded-l-lg bg-gray-200 px-2 text-base font-semibold">
                        Rp
                      </div>
                      <input
                        type="number"
                        id="email-address-icon"
                        className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 pl-10 text-sm text-gray-900 focus:border-pink-500 focus:ring-pink-500"
                        placeholder="Minimum price"
                        // value={filter.min_price}
                        onChange={(e) => {
                          onChangeFilterByMinPrice(
                            e.target.value !== null ? e.target.value : 0
                          );
                        }}
                      />
                    </div>
                    <div className="relative">
                      <div className="pointer-events-none absolute inset-y-[1px] left-[1px] flex items-center rounded-l-lg bg-gray-200 px-2 text-base font-semibold">
                        Rp
                      </div>
                      <input
                        type="number"
                        id="email-address-icon"
                        className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 pl-10 text-sm text-gray-900 focus:border-pink-500 focus:ring-pink-500"
                        placeholder="Maximum price"
                        // value={filter.max_price}
                        onChange={(e) => {
                          onChangeFilterByMaxPrice(
                            e.target.value !== null ? e.target.value : 0
                          );
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-3">
              {!loading && (
                <>
                  <div className="mb-5">
                    <div className="grid grid-cols-1 gap-5 md:grid-cols-3">
                      {dataInstruments.map((item: any, i: number) => (
                        <div key={i} className="mb-2">
                          <Card item={item} />
                        </div>
                      ))}
                    </div>
                    {dataInstruments.length === 0 && (
                      <Lottie
                        options={{
                          loop: true,
                          autoplay: true,
                          animationData,
                          rendererSettings: {
                            preserveAspectRatio: 'xMidYMid slice',
                          },
                        }}
                        width={'100%'}
                      />
                    )}
                  </div>
                  <div className="flex items-center justify-center">
                    <Pagination
                      currentPage={currPage}
                      layout="pagination"
                      onPageChange={onPageChange}
                      showIcons={true}
                      totalPages={totalPages}
                      previousLabel="Prev"
                      nextLabel="Next"
                    />
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Layout;

Layout.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
