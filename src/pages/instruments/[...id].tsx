/* eslint-disable no-unsafe-optional-chaining */
import TokopediaIcon from '@images/tokopedia-icon.png';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';

import Breadscrumb from '@/components/elements/breadscrumb';
import { useActions } from '@/overmind';
import Main from '@/templates/Main';
import helper from '@/utils/helper';

const Layout = () => {
  const router = useRouter();
  const { getInstrument } = useActions();
  const [data, setData] = React.useState({} as any);
  const init = () => {
    if (router.query.id) {
      getInstrument({ id: router.query.id.toString() }).then((res: any) => {
        setData(res.data);
      });
    }
  };
  React.useEffect(() => {
    init();
    return () => {};
  }, [router.query.id]);
  return (
    <section>
      <div className="container mx-auto mt-20 mb-10 flex flex-col">
        <div className="mb-5">
          <Breadscrumb
            homeRoute="/"
            dataItem={[
              { label: 'Instruments', route: '/instruments' },
              { label: data?.title, route: '' },
            ]}
          />
        </div>
        <div className="w-full rounded-2xl bg-white px-4 py-5 shadow-md">
          <div className="grid grid-cols-1 gap-10 md:grid-cols-12">
            <div className="col-span-1 md:col-span-4">
              <div className="">
                {data?.image_url ? (
                  <img
                    src={data.image_url}
                    className="w-full object-cover"
                    alt=""
                  />
                ) : (
                  <img
                    src="https://edhenprimasandy.sirv.com/reharm/no-image.jpg"
                    className="h-60 w-full object-cover"
                    alt=""
                  />
                )}
              </div>
            </div>
            <div className="col-span-1 md:col-span-8">
              <h1 className="mb-2 text-2xl font-semibold">{data?.title}</h1>
              <h1 className="text-3xl font-semibold">
                {helper.currencyFormat(data?.discounted_price)}
              </h1>
              <div className="mb-10 flex gap-3">
                <span className="rounded-md bg-red-500 px-2 text-sm text-white">
                  {Math.round(
                    ((data.price - data?.discounted_price) / data?.price) * 100
                  )}{' '}
                  %
                </span>
                <span className="text-base line-through">
                  {helper.currencyFormat(data?.price)}
                </span>
              </div>
              <div className="mb-5">
                <h1 className="mb-5 text-lg font-semibold">
                  Product Description
                </h1>
                <p className="whitespace-pre-wrap">{data.description}</p>
              </div>
              <div className="flex flex-row gap-3">
                <a
                  target="_blank"
                  href={data.tokopedia_url}
                  type="button"
                  className="mr-2 mb-2 inline-flex items-center gap-3 rounded-full bg-[#00d451] px-5 py-2.5 text-center text-sm font-medium text-white hover:bg-[#00d451]/90 focus:outline-none focus:ring-4 focus:ring-[#1da1f2]/50"
                  rel="noreferrer"
                >
                  <Image className="h-6 w-6" src={TokopediaIcon} alt={''} />
                  Buy @Reharm store
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Layout;
Layout.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
