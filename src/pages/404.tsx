/* eslint-disable @next/next/no-html-link-for-pages */
// pages/404.js

import Link from 'next/link';
import React from 'react';

import { Meta } from '@/layouts/Meta';

const Custom404 = () => (
  <>
    <Meta title="Reharm Music Courses" description="Reharm Music Courses" />
    <div className="flex h-screen w-full flex-col items-center justify-center bg-[#1A2238]">
      <h1 className="text-[200px] font-extrabold tracking-widest text-white">
        404
      </h1>
      <div className="absolute rotate-12 rounded bg-[#FF6A3D] px-2 text-xl">
        Page Not Found
      </div>
      <Link className="mt-5" href="/">
        <div className="group relative inline-block text-sm font-medium text-[#FF6A3D] focus:outline-none focus:ring active:text-orange-500">
          <span className="absolute inset-0 translate-x-0.5 translate-y-0.5 bg-[#FF6A3D] transition-transform group-hover:translate-y-0 group-hover:translate-x-0"></span>

          <span className="relative block border border-current bg-[#1A2238] px-8 py-3">
            Go Home
          </span>
        </div>
      </Link>
    </div>
  </>
);

export default Custom404;
