import Logo from '@images/logo-color.png';
import { Button, Table } from 'flowbite-react';
import moment from 'moment';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { HiOutlinePrinter } from 'react-icons/hi';
import { useReactToPrint } from 'react-to-print';
import Swal from 'sweetalert2';

import Breadscrumb from '@/components/elements/breadscrumb';
import { useActions } from '@/overmind';
import Main from '@/templates/Main';
import helper from '@/utils/helper';

const OrderDetail = () => {
  const router = useRouter();
  const { getOrder } = useActions();
  const [data, setData] = React.useState({} as any);
  const [summary, setSummary] = React.useState({
    count: 0,
    price: 0,
    discount: 0,
    commission: 0,
    totalPrice: 0,
  });
  React.useEffect(() => {
    if (router.query.id) {
      getOrder({ order_id: router.query.id.toString() }).then((res: any) => {
        setData(res);
        const newSum = {
          count: 0,
          price: 0,
          discount: 0,
          commission: 0,
          totalPrice: 0,
        };
        res?.item_details.forEach((item: any) => {
          newSum.count += 1;
          newSum.price += item.price;
          newSum.commission += item.commission;
          newSum.discount +=
            item.discount_flag === 'yes' || item.discount_price > 0
              ? item.price - item.discount_price
              : 0;
          newSum.totalPrice =
            newSum.price - newSum.discount - newSum.commission;
        });
        setSummary({ ...newSum });
        if (
          router.query?.transaction_status === 'settlement' &&
          res.status === 'success'
        ) {
          Swal.fire(
            'Thank You!',
            'Your payment was successful. This is a receipt of your purchase',
            'success'
          );
        }
      });
    }
    return () => {};
  }, [router.query.id]);
  const componentRef = React.useRef(null);
  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });
  return (
    <section>
      <div className="container mx-auto mt-20 flex flex-col">
        <Breadscrumb
          homeRoute="/"
          dataItem={[
            { label: 'Orders', route: '/orders' },
            { label: data?.order_id, route: '' },
          ]}
        />
        <div className="w-full p-5 lg:px-40">
          {/* <h1 className="mb-2 text-4xl font-semibold">Order Detail</h1> */}
          <div className="rounded-2xl bg-white p-4">
            <div ref={componentRef}>
              <div className="mb-5 flex flex-row justify-between">
                <div className="flex flex-col px-4">
                  <Link href="/" className="mb-5">
                    <Image className="w-52" src={Logo} alt={''} />
                  </Link>
                  <span className="text-lg font-bold">Reharm</span>
                  <span className="font-sans text-base text-gray-600">
                    info@reharm.com
                  </span>
                  <span className="font-sans text-base text-gray-600">
                    Jakarta, Indonesia
                  </span>
                  <span className="font-sans text-base text-gray-600">
                    Phone: +62 857 1760 5990{' '}
                  </span>
                </div>
                <div className="mt-2 flex flex-col px-4 text-right">
                  <span className="mb-20 text-2xl font-bold">Order Detail</span>
                  <span className="font-sans text-base text-gray-600">
                    Payment method:{' '}
                    <span className="font-bold text-gray-800">
                      {data?.payment_type}
                    </span>
                  </span>
                  <span className="font-sans text-base text-gray-600">
                    Purchase date:{' '}
                    <span className="font-bold text-gray-800">
                      {moment(data?.updated_at).format('ddd, DD MMM YYYY')}
                    </span>
                  </span>
                </div>
              </div>
              <div className="mb-5 flex flex-col px-4">
                <span className="font-bold text-gray-600">Bill to:</span>
                <span className="text-lg font-bold">
                  {data?.user?.first_name} {data?.user?.last_name}
                </span>
                <span className="font-sans text-base text-gray-600">
                  Email: {data?.user?.email}
                </span>
              </div>
              <div className="mb-5">
                <Table>
                  <Table.Head>
                    <Table.HeadCell>Item name</Table.HeadCell>
                    <Table.HeadCell>Category</Table.HeadCell>
                    <Table.HeadCell>Instructor</Table.HeadCell>
                    <Table.HeadCell>Price</Table.HeadCell>
                  </Table.Head>
                  <Table.Body className="divide-y">
                    {data?.item_details?.map((item: any, i: number) => (
                      <Table.Row
                        className="bg-white dark:border-gray-700 dark:bg-gray-800"
                        key={i}
                      >
                        <Table.Cell className="whitespace-nowrap font-medium text-gray-900 dark:text-white">
                          {data?.is_subscription === 'no'
                            ? item.course_name
                            : item.name}
                        </Table.Cell>
                        <Table.Cell>
                          {data?.is_subscription === 'no'
                            ? item.category_name
                            : 'Subscription'}
                        </Table.Cell>
                        <Table.Cell>{item.instructor_name ?? '-'}</Table.Cell>
                        <Table.Cell>
                          {helper.currencyFormat(item.price)}
                        </Table.Cell>
                      </Table.Row>
                    ))}
                  </Table.Body>
                </Table>
              </div>
              <div className="mb-5 flex justify-end px-4">
                <div className="grid w-1/2 grid-cols-2 gap-2">
                  <span className="text-right text-gray-600">Sub total:</span>
                  <span className="text-right text-base font-bold text-gray-900">
                    {helper.currencyFormat(summary.price)}
                  </span>
                  <span className="text-right text-gray-600">Discount:</span>
                  <span className="text-right text-base font-bold text-gray-900">
                    {helper.currencyFormat(summary.discount)}
                  </span>
                  {summary.commission > 0 && (
                    <>
                      <span className="text-right text-gray-600">
                        Discount referral:
                      </span>
                      <span className="text-right text-base font-bold text-gray-900">
                        {helper.currencyFormat(summary.commission)}
                      </span>
                    </>
                  )}
                  <span className="mt-5 text-right text-gray-600">
                    Grand total:
                  </span>
                  <span className="mt-5 text-right text-base font-bold text-gray-900">
                    {helper.currencyFormat(data?.total_price)}
                  </span>
                </div>
              </div>
            </div>
            <div className="mb-5">
              <Button gradientDuoTone="cyanToBlue" onClick={handlePrint}>
                <HiOutlinePrinter className="mr-2 h-5 w-5" />
                Print
              </Button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default OrderDetail;
OrderDetail.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
