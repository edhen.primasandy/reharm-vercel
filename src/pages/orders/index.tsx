/* eslint-disable import/no-extraneous-dependencies */
import { RadioGroup } from '@headlessui/react';
import classNames from 'classnames';
import { Button, Label, Modal, Pagination, Textarea } from 'flowbite-react';
import lodash from 'lodash';
import moment from 'moment';
import { useRouter } from 'next/router';
import React from 'react';
import { BsStarFill } from 'react-icons/bs';
import { toast } from 'react-toastify';

import Breadscrumb from '@/components/elements/breadscrumb';
import { useActions, useState } from '@/overmind';
import Main from '@/templates/Main';
import helper from '@/utils/helper';

const Orders = () => {
  const router = useRouter();
  const { user } = useState();
  const { getListOrder, getUserProfile, createRating } = useActions();
  const [data, setData] = React.useState([] as any);
  const [loading, setLoading] = React.useState(false);
  const [showModalReview, setShowModalReview] = React.useState(false);
  const [formReview, setFormReview] = React.useState({
    user_id: user?.id,
    course_id: 0,
    rating: 0,
    review: '',
  });
  const [valueRating, setValueRating] = React.useState('');
  const [currPage, setCurrPage] = React.useState(1);
  const [totalPages, setTotalPages] = React.useState(1);
  const [filter, setFilter] = React.useState({
    user_id: user?.id,
    status: '',
    skip: 0,
    limit: 10,
    sort: 'updated_at',
    sort_type: 'desc',
  });
  const init = () => {
    setLoading(true);
    const payload = { ...filter, user_id: user?.id };
    getListOrder(payload)
      .then((res) => {
        setData(res.items);
        const remainder = res.total % filter.limit;
        let count = res.total / filter.limit;
        if (remainder > 0) {
          count = Math.ceil(count);
        }
        setTotalPages(count);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  React.useEffect(() => {
    if (!router.query?.order_id) {
      if (user?.id) {
        init();
      } else {
        getUserProfile().then(() => init());
      }
    }
    return () => {};
  }, [filter, user]);
  React.useEffect(() => {
    if (
      router.query?.order_id &&
      router.query?.transaction_status === 'settlement'
    ) {
      const href = `/orders/${router.query?.order_id}?transaction_status=settlement`;
      router.push(href);
    }
    return () => {};
  }, [router]);

  const onPageChange = (page: number) => {
    if (currPage !== page) {
      setFilter({ ...filter, skip: (page - 1) * filter.limit });
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
    setCurrPage(page);
  };

  const renderStatus = (status: any) => {
    switch (status) {
      case 'success':
        return (
          <div className="rounded bg-green-400 px-2 text-white">{status}</div>
        );
      case 'expire':
        return (
          <div className="rounded bg-red-500 px-2 text-white">{status}</div>
        );
      case 'unpaid':
        return (
          <div className="rounded bg-orange-400 px-2 text-white">{status}</div>
        );
      case 'draft':
        return (
          <div className="rounded bg-gray-400 px-2 text-black">{status}</div>
        );
      default:
        return <></>;
    }
  };

  const onSubmitReview = () => {
    createRating({
      ...formReview,
      user_id: user?.id,
      rating: parseInt(valueRating, 10),
    })
      .then(() => {
        toast.success('data has been saved');
        setFormReview({ ...formReview, course_id: 0, rating: 0, review: '' });
      })
      .catch((e) => toast.error(e.message))
      .finally(() => setShowModalReview(false));
  };
  return (
    <section>
      <div className="container mx-auto mt-20 flex items-center ">
        <div className="w-full">
          <Breadscrumb
            homeRoute="/"
            dataItem={[{ label: 'Orders', route: '' }]}
          />
          <h1 className="mb-2 text-4xl font-semibold">Order List</h1>
          <div className="mb-5 rounded-2xl bg-white p-4">
            {!loading && (
              <div className="mb-5">
                {data.map((order: any, index: number) => {
                  const price = lodash.sumBy(order.item_details, 'price');
                  const discount = lodash.sumBy(
                    order.item_details,
                    'discount_price'
                  );
                  const commission = lodash.sumBy(
                    order.item_details,
                    'commission'
                  );
                  return (
                    <div key={index} className="mb-5 rounded-xl p-5 shadow-md ">
                      <div className="flex flex-col justify-between lg:flex-row">
                        <div>
                          <div className="flex flex-row items-center gap-3">
                            <span className="font-bold">Order</span>
                            <span className="text-base">
                              {moment(order.created_at).format('DD MMM YYYY')}
                            </span>
                            {renderStatus(order.status)}
                            <span className="text-base text-gray-700">
                              #{order.order_id}
                            </span>
                          </div>
                          {order?.item_details?.map((item: any, i: number) => (
                            <div
                              className="mb-2 flex flex-col border-b"
                              key={i}
                            >
                              {/* <span className=" font-bold">Item details: </span> */}
                              <div className="mb-5 ml-5">
                                <div className="flex items-start gap-2">
                                  <div className="flex flex-1 flex-col">
                                    <div className="mb-2 flex flex-col">
                                      <span className="text-lg font-semibold">
                                        {order.is_subscription === 'yes'
                                          ? item.name
                                          : item.course_name}
                                      </span>
                                      <span className="text-sm text-gray-600">
                                        {order.is_subscription === 'yes'
                                          ? 'Subscription'
                                          : item.category_name}
                                      </span>
                                    </div>
                                    <div className="flex flex-row items-center justify-between">
                                      <div className="flex items-center gap-3">
                                        <img
                                          className="h-20 w-20 rounded object-contain"
                                          src={
                                            item.thumbnail ??
                                            'https://edhenprimasandy.sirv.com/reharm/no-image.jpg'
                                          }
                                          alt="Large avatar"
                                        ></img>
                                        <div className="flex flex-col">
                                          <span className="mb-2 text-base">
                                            {item.short_description}
                                          </span>
                                          <span className="mb-2 text-base text-gray-700">
                                            {item.instructor_name}
                                          </span>
                                          <div className="flex items-center gap-2">
                                            {(item.discount_flag === 'yes' ||
                                              item.discount_price > 0) && (
                                              <>
                                                <span className="rounded bg-red-500 px-2 py-1 text-white">
                                                  {Math.round(
                                                    ((item.price -
                                                      item.discount_price) /
                                                      item.price) *
                                                      100
                                                  )}
                                                  %
                                                </span>
                                                <span className="line-through">
                                                  {helper.currencyFormat(
                                                    item.price
                                                  )}
                                                </span>
                                              </>
                                            )}

                                            <span className="font-semibold">
                                              {helper.currencyFormat(
                                                item.discount_flag === 'yes' ||
                                                  item.discount_price > 0
                                                  ? item.discount_price
                                                  : item.price
                                              )}
                                            </span>
                                          </div>
                                        </div>
                                        {order.status === 'success' &&
                                          item?.rating === null && (
                                            <Button
                                              pill
                                              color="warning"
                                              onClick={() => {
                                                setFormReview({
                                                  ...formReview,
                                                  course_id: item.course_id,
                                                });
                                                setShowModalReview(true);
                                              }}
                                            >
                                              Give a review
                                            </Button>
                                          )}
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          ))}
                        </div>
                        <div className="my-5 border-l border-gray-300 px-5">
                          <div className="rounded-2xl bg-white px-4 py-2">
                            <span className="mb-3 text-lg font-bold">
                              Details
                            </span>
                            <div className="flex flex-row items-center justify-between gap-5">
                              <span className="text-base text-gray-500">
                                Total price ({order.item_details.length})
                              </span>
                              <span className="text-base text-gray-500">
                                {helper.currencyFormat(price)}
                              </span>
                            </div>
                            <div className="flex flex-row items-center justify-between gap-5">
                              <span className="text-base text-gray-500">
                                Total discount
                              </span>
                              <span className="text-base text-gray-500">
                                {discount ? '-' : ''}{' '}
                                {helper.currencyFormat(price - discount)}
                              </span>
                            </div>
                            {commission > 0 && (
                              <div className="flex flex-row items-center justify-between gap-5">
                                <span className="text-base text-gray-500">
                                  Total discount referral
                                </span>
                                <span className="text-base text-gray-500">
                                  - {helper.currencyFormat(commission)}
                                </span>
                              </div>
                            )}
                            <div className="my-2 border-b border-gray-200" />
                            <div className="flex flex-row items-center justify-between gap-5">
                              <span className="text-lg font-bold">
                                Total Price
                              </span>
                              <span className="text-lg font-bold">
                                {helper.currencyFormat(order.total_price)}
                              </span>
                            </div>
                            <div className="my-4 flex flex-col gap-3">
                              <Button
                                pill
                                gradientDuoTone="purpleToBlue"
                                onClick={() => {
                                  const href = `/orders/${order.order_id}`;
                                  router.push(href);
                                }}
                              >
                                Detail
                              </Button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            )}

            <div className="flex items-center justify-center">
              <Pagination
                currentPage={currPage}
                layout="pagination"
                onPageChange={onPageChange}
                showIcons={true}
                totalPages={totalPages}
                previousLabel="Prev"
                nextLabel="Next"
              />
            </div>
          </div>
        </div>
      </div>
      <Modal show={showModalReview} onClose={() => setShowModalReview(false)}>
        <Modal.Header>Submit review and rating</Modal.Header>
        <Modal.Body>
          <div className="mb-5">
            <div className="mb-2 block">
              <Label htmlFor="Rating" value="Rating*" />
            </div>
            <RadioGroup
              value={valueRating}
              onChange={setValueRating}
              className="my-1 mb-2"
            >
              <RadioGroup.Label className="sr-only">
                Choose a option
              </RadioGroup.Label>
              <div className="flex flex-row-reverse justify-center gap-1">
                {[1, 2, 3, 4, 5].reverse().map((item) => (
                  <RadioGroup.Option
                    key={item}
                    value={item}
                    className={({ active, checked }) =>
                      classNames(
                        'cursor-pointer text-gray-200',
                        'flex-1 hover:text-yellow-400',
                        'peer',
                        'peer-hover:text-yellow-400',
                        active ? 'text-yellow-500' : '',
                        checked ? 'text-yellow-500' : '',
                        // 👇 Add a compare with selected value here
                        parseInt(valueRating, 10) >= item
                          ? 'text-yellow-500'
                          : ''
                      )
                    }
                  >
                    <RadioGroup.Label as={BsStarFill} className="h-6 w-6" />
                  </RadioGroup.Option>
                ))}
              </div>
            </RadioGroup>
          </div>
          <div>
            <div className="mb-2 block">
              <Label htmlFor="Review" value="Review*" />
            </div>
            <Textarea
              className="eTextarea"
              required
              value={formReview.review}
              rows={4}
              id="Review"
              onChange={(e) => {
                setFormReview({
                  ...formReview,
                  review: e.target.value,
                });
              }}
            />
          </div>
        </Modal.Body>
        <Modal.Footer className="flex justify-end">
          <Button
            color="success"
            onClick={() => {
              onSubmitReview();
            }}
          >
            Submit
          </Button>
          <Button color="gray" onClick={() => setShowModalReview(false)}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </section>
  );
};

export default Orders;

Orders.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
