import { Pagination, Progress, Rating } from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import { FaRegPlayCircle } from 'react-icons/fa';
import { MdAccessTime } from 'react-icons/md';

import { useActions, useState } from '@/overmind';

const MyCourses = () => {
  const router = useRouter();
  const { user } = useState();
  const { getListEnrol, setSelectedCourseFromCourses, getUserProfile } =
    useActions();
  const [data, setData] = React.useState([] as any[]);
  const [loading, setLoading] = React.useState(false);
  const [currPage, setCurrPage] = React.useState(1);
  const [totalPages, setTotalPages] = React.useState(1);
  const [filter, setFilter] = React.useState({
    course_id: 0,
    user_id: user?.id,
    order_id: 0,
    skip: 0,
    limit: 6,
    sort: '',
    sort_type: 'asc',
  });
  const init = (resuser: any) => {
    setLoading(true);
    const payload = { ...filter, user_id: resuser?.id };
    getListEnrol(payload).then((res) => {
      setData(res.items);
      const remainder = res.total % filter.limit;
      let count = res.total / filter.limit;
      if (remainder > 0) {
        count = Math.ceil(count);
      }
      setTotalPages(count);
      setLoading(false);
    });
  };
  React.useEffect(() => {
    if (user?.id) {
      init(user);
    } else {
      getUserProfile().then((res) => init(res));
    }

    return () => {};
  }, [filter]);
  const onPageChange = (page: number) => {
    if (currPage !== page) {
      setFilter({ ...filter, skip: (page - 1) * filter.limit });
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
    setCurrPage(page);
  };
  const Card = ({ item }: any) => {
    const course = item?.course;
    return (
      <div className="relative mb-3 flex h-full flex-col rounded-2xl bg-white shadow-md duration-200 ease-in hover:scale-105 hover:shadow-md">
        <div className="h-60">
          {course.thumbnail ? (
            <img
              src={course.thumbnail}
              className="h-60 w-full rounded-t-2xl object-cover"
              alt=""
            />
          ) : (
            <img
              src="https://edhenprimasandy.sirv.com/reharm/no-image.jpg"
              className="h-60 w-full rounded-t-2xl object-cover"
              alt=""
            />
          )}
        </div>
        <div className="mt-[-20px] flex flex-col rounded-2xl bg-white p-4">
          <span className="font-semibold text-gray-500">
            {course.category.parent.name}:{' '}
            <span className="font-normal">{course.category.name}</span>
          </span>
          <span className="mb-2 text-lg font-semibold">{course.title}</span>
          <div className="mb-2 border-b-2 border-dashed pb-2">
            <div className="mb-5 min-h-[50px] text-xs line-clamp-3">
              {course.short_description}
            </div>
            <div className="mb-5">
              <Progress
                progress={Math.round(
                  (course.completed_lessons / course.total_lessons) * 100
                )}
              />
              <span className="text-gray-700">
                {Math.round(
                  (course.completed_lessons / course.total_lessons) * 100
                )}
                %
              </span>
            </div>
            <div className="mb-5 text-blue-600">
              {course?.user?.first_name} {course?.user?.last_name}
            </div>
            <div className="flex flex-row items-center justify-between">
              <div>
                <Rating>
                  <Rating.Star filled={course?.rating_total_average >= 1} />
                  <Rating.Star filled={course?.rating_total_average >= 2} />
                  <Rating.Star filled={course?.rating_total_average >= 3} />
                  <Rating.Star filled={course?.rating_total_average >= 4} />
                  <Rating.Star filled={course?.rating_total_average >= 5} />
                </Rating>
              </div>
              <div className="flex flex-row gap-3">
                <div className="flex flex-row items-center gap-2">
                  <FaRegPlayCircle size={15} />
                  <span>{course.total_lessons} lessons</span>
                </div>
                <div className="flex flex-row items-center gap-2">
                  <MdAccessTime size={16} />
                  <span>{course.total_hours}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-row gap-3">
            <button
              type="button"
              onClick={() => {
                const href = `/courses/${course.id}`;
                setSelectedCourseFromCourses(course);
                router.push(href);
              }}
              className="mb-2 w-full rounded-full bg-orange-500 px-5 py-2.5 text-sm font-medium text-white hover:bg-orange-600 focus:outline-none focus:ring-4 focus:ring-orange-300 dark:bg-orange-600 dark:hover:bg-orange-700 dark:focus:ring-orange-800"
            >
              View detail
            </button>
            <button
              onClick={() => {
                const href = `/my-courses/${item.id}`;
                router.push(href);
              }}
              className="group relative mb-2 mr-2 inline-flex w-full items-center justify-center overflow-hidden rounded-full bg-gradient-to-br from-purple-600 to-blue-500 p-0.5 text-sm font-medium text-gray-900 hover:text-white focus:outline-none focus:ring-4 focus:ring-blue-300 group-hover:from-purple-600 group-hover:to-blue-500 dark:text-white dark:focus:ring-blue-800"
            >
              <span className="relative w-full rounded-full bg-white px-5 py-2.5 transition-all duration-75 ease-in group-hover:bg-opacity-[0]">
                Start lesson
              </span>
            </button>
          </div>
        </div>
      </div>
    );
  };

  return (
    <section>
      <div className="container mx-auto mt-20 flex items-center p-5 md:px-0">
        <div className="w-full">
          <h1 className="mb-2 text-4xl font-semibold">My Courses</h1>
          <div>
            <div>
              {!loading && (
                <>
                  <div className="mb-5">
                    <div className="grid grid-cols-1 gap-5 lg:grid-cols-3">
                      {data.map((item: any, i: number) => (
                        <div key={i} className="mb-2">
                          <Card item={item} />
                        </div>
                      ))}
                    </div>
                  </div>
                </>
              )}
              <div className="flex items-center justify-center">
                <Pagination
                  currentPage={currPage}
                  layout="pagination"
                  onPageChange={onPageChange}
                  showIcons={true}
                  totalPages={totalPages}
                  previousLabel="Prev"
                  nextLabel="Next"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default MyCourses;
