import Loading from '@components/elements/skeleton-loading/card-course';
import { Label, Pagination, Progress, Radio, Rating } from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import { FaRegPlayCircle } from 'react-icons/fa';
import { MdAccessTime } from 'react-icons/md';
import { RiEqualizerLine } from 'react-icons/ri';
import Lottie from 'react-lottie';

import Breadscrumb from '@/components/elements/breadscrumb';
import { useActions, useState } from '@/overmind';
import * as animationData from '@/public/assets/lottie/no-data-found.json';
import Main from '@/templates/Main';

const MyCourses = () => {
  const router = useRouter();
  const { user } = useState();
  const {
    getsCourses,
    setSelectedCourseFromCourses,
    getUserProfile,
    getsCategories,
  } = useActions();
  const [data, setData] = React.useState([] as any[]);
  const [loading, setLoading] = React.useState(false);
  const [currPage, setCurrPage] = React.useState(1);
  const [totalPages, setTotalPages] = React.useState(1);
  const [activeTab, setActiveTab] = React.useState('purchase');
  const [dataCategories, setDataCategories] = React.useState([] as any);

  const filterObj = {
    title: '',
    category_id: 0,
    skip: 0,
    limit: 6,
    sort: '',
    is_free_course: '',
    level: '',
    rating: 0,
    language: '',
    min_price: 0,
    max_price: 0,
    sort_type: 'asc',
    status: 'active',
  };
  const [filter, setFilter] = React.useState(filterObj);
  const [expandFilter, setExpandFilter] = React.useState(true);

  const ratings = [
    { id: 1, value: 1 },
    { id: 2, value: 2 },
    { id: 3, value: 3 },
    { id: 4, value: 4 },
    { id: 5, value: 5 },
  ];

  const init = () => {
    setLoading(true);
    let payload = {};
    if (activeTab === 'purchase') {
      payload = { ...filter, is_purchased: 'yes' };
    } else {
      payload = { ...filter, is_subscription: 'yes' };
    }
    getsCourses(payload).then((res) => {
      setData(res.items);
      const remainder = res.total % filter.limit;
      let count = res.total / filter.limit;
      if (remainder > 0) {
        count = Math.ceil(count);
      }
      setTotalPages(count);
      setLoading(false);
    });
  };
  React.useEffect(() => {
    getsCategories().then((res) => {
      setDataCategories(res.items);
    });
    return () => {};
  }, []);
  React.useEffect(() => {
    if (user?.id) {
      init();
    } else {
      getUserProfile().then(() => init());
    }

    return () => {};
  }, [filter]);
  const onPageChange = (page: number) => {
    if (currPage !== page) {
      setFilter({ ...filter, skip: (page - 1) * filter.limit });
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
    setCurrPage(page);
  };
  const ontTabChange = (tab: string) => {
    setActiveTab(tab);
    setFilter(filterObj);
    // if (tab === 'purchase') {
    //   setFilter({ ...filter, is_purchased: 'yes', is_subscription: 'no' });
    // } else {
    //   setFilter({ ...filter, is_purchased: 'no', is_subscription: 'yes' });
    // }
  };
  const onFilterByCategory = (value: any) => {
    setFilter({ ...filter, category_id: parseInt(value, 10) });
  };
  const onFilterByLanguage = (value: any) => {
    setFilter({ ...filter, language: value });
  };
  const onFilterByLevel = (value: any) => {
    setFilter({ ...filter, level: value });
  };
  const onFilterByRatings = (value: any) => {
    setFilter({ ...filter, rating: parseInt(value, 10) });
  };
  const Card = ({ item }: any) => {
    const course = item;
    return (
      <div className="relative mb-3 flex h-full flex-col rounded-2xl bg-white shadow-md duration-200 ease-in hover:scale-105 hover:shadow-md">
        <div className="h-60">
          {course.thumbnail ? (
            <img
              src={course.thumbnail}
              className="h-60 w-full rounded-t-2xl object-cover"
              alt=""
            />
          ) : (
            <img
              src="https://edhenprimasandy.sirv.com/reharm/no-image.jpg"
              className="h-60 w-full rounded-t-2xl object-cover"
              alt=""
            />
          )}
        </div>
        <div className="mt-[-20px] flex flex-col rounded-2xl bg-white p-4">
          <span className="font-semibold text-gray-500">
            {course.category.parent.name}:{' '}
            <span className="font-normal">{course.category.name}</span>
          </span>
          <span className="mb-2 text-lg font-semibold">{course.title}</span>
          <div className="mb-2 border-b-2 border-dashed pb-2">
            <div className="mb-5 min-h-[50px] text-xs line-clamp-3">
              {course.short_description}
            </div>
            <div className="mb-5">
              <Progress
                progress={Math.round(
                  (course.completed_lessons / course.total_lessons) * 100
                )}
              />
              <span className="text-gray-700">
                {Math.round(
                  (course.completed_lessons / course.total_lessons) * 100
                )}
                %
              </span>
            </div>
            <div className="mb-5 text-blue-600">
              {course?.user?.first_name} {course?.user?.last_name}
            </div>
            <div className="flex flex-row items-center justify-between">
              <div>
                <Rating>
                  <Rating.Star filled={course?.rating_total_average >= 1} />
                  <Rating.Star filled={course?.rating_total_average >= 2} />
                  <Rating.Star filled={course?.rating_total_average >= 3} />
                  <Rating.Star filled={course?.rating_total_average >= 4} />
                  <Rating.Star filled={course?.rating_total_average >= 5} />
                </Rating>
              </div>
              <div className="flex flex-row gap-3">
                <div className="flex flex-row items-center gap-2">
                  <FaRegPlayCircle size={15} />
                  <span>{course.total_lessons} lessons</span>
                </div>
                <div className="flex flex-row items-center gap-2">
                  <MdAccessTime size={16} />
                  <span>{course.total_hours}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-row gap-3">
            <button
              type="button"
              onClick={() => {
                const href = `/courses/${course.id}`;
                setSelectedCourseFromCourses(course);
                router.push(href);
              }}
              className="mb-2 w-full rounded-full bg-orange-500 px-5 py-2.5 text-sm font-medium text-white hover:bg-orange-600 focus:outline-none focus:ring-4 focus:ring-orange-300 dark:bg-orange-600 dark:hover:bg-orange-700 dark:focus:ring-orange-800"
            >
              View detail
            </button>
            <button
              onClick={() => {
                const href = `/my-courses/${item.id}`;
                router.push(href);
              }}
              className="group relative mb-2 mr-2 inline-flex w-full items-center justify-center overflow-hidden rounded-full bg-gradient-to-br from-purple-600 to-blue-500 p-0.5 text-sm font-medium text-gray-900 hover:text-white focus:outline-none focus:ring-4 focus:ring-blue-300 group-hover:from-purple-600 group-hover:to-blue-500 dark:text-white dark:focus:ring-blue-800"
            >
              <span className="relative w-full rounded-full bg-white px-5 py-2.5 transition-all duration-75 ease-in group-hover:bg-opacity-[0]">
                Start lesson
              </span>
            </button>
          </div>
        </div>
      </div>
    );
  };

  return (
    <section>
      <div className="container mx-auto mt-20 flex items-center px-5 md:px-0">
        <div className="w-full">
          <Breadscrumb
            homeRoute="/"
            dataItem={[{ label: 'My courses', route: '' }]}
          />
          <div className="mb-2 flex flex-row items-center justify-between">
            <h1 className="mb-2 text-4xl font-semibold">My Courses</h1>
            <div>
              <ul className="flex flex-wrap text-center text-sm font-medium text-gray-500 dark:text-gray-400">
                <li className="mr-4">
                  <button
                    onClick={() => ontTabChange('purchase')}
                    // className="inline-block rounded-full bg-gradient-to-r from-purple-500 to-pink-500 px-10 py-2 text-white outline-none ring-4 ring-purple-200 hover:bg-gray-100 hover:bg-gradient-to-l"
                    className={`${
                      activeTab === 'purchase'
                        ? 'bg-gradient-to-r from-purple-500 to-pink-500 text-white'
                        : 'bg-white '
                    } inline-block rounded-full px-10 py-2 outline-none ring-4 ring-purple-200 hover:bg-gray-100 hover:bg-gradient-to-l`}
                    aria-current="page"
                  >
                    Purchased
                  </button>
                </li>
                <li className="mr-2">
                  <button
                    onClick={() => ontTabChange('subscribe')}
                    className={`${
                      activeTab === 'subscribe'
                        ? 'bg-gradient-to-r from-purple-500 to-pink-500 text-white'
                        : 'bg-white '
                    } inline-block rounded-full px-10 py-2 outline-none ring-4 ring-purple-200 hover:bg-gray-100 hover:bg-gradient-to-l`}
                    // className="inline-block rounded-full bg-white px-10 py-2 text-gray-900 outline-none ring-4 ring-purple-200 hover:bg-gray-100 hover:bg-gradient-to-l"
                  >
                    Subscribed
                  </button>
                </li>
              </ul>
            </div>
          </div>
          <div className="mb-5 grid w-full grid-cols-1 gap-6 md:grid-cols-4">
            <div>
              <div className="mb-5 rounded-2xl bg-white px-4 py-5 shadow-md">
                <div className="flex flex-row items-center justify-between">
                  <span className="text-base font-semibold">Filters</span>
                  <button onClick={() => setExpandFilter(!expandFilter)}>
                    <RiEqualizerLine />
                  </button>
                </div>
                <div
                  className={`transition duration-1000 ease-out ${
                    expandFilter ? 'visible' : 'hidden'
                  }`}
                >
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <label className="mb-2 block text-sm font-semibold text-gray-900">
                      Title
                    </label>
                    <input
                      type="text"
                      className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500"
                      placeholder="Title"
                      onChange={(e) => {
                        setFilter({ ...filter, title: e.target.value });
                      }}
                    />
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Categories</span>
                    <div
                      onChange={(e: any) => onFilterByCategory(e.target?.value)}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'all'}
                            name="categories"
                            value={0}
                            checked={filter.category_id === 0}
                          />
                          <Label htmlFor={'all'}>All Category</Label>
                        </div>
                      </div>
                      {dataCategories.map((item: any, i: number) => (
                        <div key={i} className="mb-4 ml-2">
                          <div className="mb-2 flex items-center gap-2">
                            <Radio
                              id={item.id}
                              name="categories"
                              value={item.id}
                              checked={filter.category_id === item.id}
                            />
                            <Label htmlFor={item.id}>{item.name}</Label>
                          </div>
                          {item.childs.map((ch: any, ii: number) => (
                            <div
                              key={ii}
                              className="mb-2 ml-4 flex items-center gap-2"
                            >
                              <Radio
                                id={ch.id}
                                name="categories"
                                value={ch.id}
                                checked={filter.category_id === ch.id}
                              />
                              <Label htmlFor={ch.id}>{ch.name}</Label>
                            </div>
                          ))}
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Level</span>
                    <div
                      onChange={(e: any) => onFilterByLevel(e.target?.value)}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'alllevel'}
                            name="level"
                            value={''}
                            defaultChecked={true}
                          />
                          <Label htmlFor={'alllevel'}>All</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'Beginner'}
                            name="level"
                            value={'beginner'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'Beginner'}>Beginner</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'Advanced'}
                            name="level"
                            value={'advanced'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'Advanced'}>Advanced</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'Intermediate'}
                            name="level"
                            value={'Intermediate'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'intermediate'}>Intermediate</Label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Language</span>
                    <div
                      onChange={(e: any) => onFilterByLanguage(e.target?.value)}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'alllang'}
                            name="language"
                            value={''}
                            defaultChecked={true}
                          />
                          <Label htmlFor={'alllang'}>All</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'English'}
                            name="language"
                            value={'English'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'english'}>English</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'Indonesia'}
                            name="language"
                            value={'indonesia'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'Indonesia'}>Indonesia</Label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Ratings</span>
                    <div
                      onChange={(e: any) => onFilterByRatings(e.target?.value)}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'allratings'}
                            name="ratings"
                            value={0}
                            defaultChecked={true}
                          />
                          <Label htmlFor={'allratings'}>All</Label>
                        </div>
                      </div>
                      {ratings.map((item) => (
                        <div className="mb-4 ml-2" key={item.id}>
                          <div className="mb-2 flex items-center gap-2">
                            <Radio
                              id={`${item.value}star`}
                              name="ratings"
                              value={item.value}
                              defaultChecked={false}
                            />
                            <Label htmlFor={`${item.value}star`}>
                              <div>
                                <Rating>
                                  <Rating.Star filled={item.id >= 1} />
                                  <Rating.Star filled={item.id >= 2} />
                                  <Rating.Star filled={item.id >= 3} />
                                  <Rating.Star filled={item.id >= 4} />
                                  <Rating.Star filled={item.id >= 5} />
                                </Rating>
                              </div>
                            </Label>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-3">
              {!loading && (
                <>
                  <div className="mb-5">
                    <div className="grid grid-cols-1 gap-5 lg:grid-cols-2">
                      {data.map((item: any, i: number) => (
                        <div key={i} className="mb-2">
                          <Card item={item} />
                        </div>
                      ))}
                    </div>
                    {data.length === 0 && (
                      <Lottie
                        options={{
                          loop: true,
                          autoplay: true,
                          animationData,
                          rendererSettings: {
                            preserveAspectRatio: 'xMidYMid slice',
                          },
                        }}
                        width={'100%'}
                      />
                    )}
                  </div>
                </>
              )}
              {loading && <Loading count={3} />}
              <div className="flex items-center justify-center">
                <Pagination
                  currentPage={currPage}
                  layout="pagination"
                  onPageChange={onPageChange}
                  showIcons={true}
                  totalPages={totalPages}
                  previousLabel="Prev"
                  nextLabel="Next"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default MyCourses;

MyCourses.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
