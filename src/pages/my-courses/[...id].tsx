/* eslint-disable no-nested-ternary */
/* eslint-disable @next/next/no-sync-scripts */
/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable tailwindcss/no-custom-classname */
import 'react-circular-progressbar/dist/styles.css';

import LogoSM from '@images/logo-light-sm.png';
import {
  Tab,
  TabPanel,
  Tabs,
  TabsBody,
  TabsHeader,
} from '@material-tailwind/react';
// import { saveAs } from 'file-saver';
import { Accordion, Button, Checkbox, Modal } from 'flowbite-react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';
import { Fade } from 'react-awesome-reveal';
import { buildStyles, CircularProgressbar } from 'react-circular-progressbar';
import { BsDownload, BsFileText } from 'react-icons/bs';
import { FaLock, FaLockOpen } from 'react-icons/fa';
import {
  HiChevronLeft,
  HiChevronRight,
  HiOutlineExclamationCircle,
} from 'react-icons/hi';
import { MdOndemandVideo } from 'react-icons/md';
import ReactPlayer from 'react-player';

import Breadscrumb from '@/components/elements/breadscrumb';
import ButtonComp from '@/components/elements/button';
import { Meta } from '@/layouts/Meta';
import { useActions, useState } from '@/overmind';

import Footer from '../../templates/MainComponents/footer';

const RenderVideo = ({ selectedLesson, onCheckLesson, updateWatch }: any) => {
  const { user } = useState();
  const playerRef = React.useRef() as any;
  // const iFrameRef = React.useRef(null);

  const [isReady, setIsReady] = React.useState(false);
  // const [isLoaded, setIsLoaded] = React.useState(false);
  const [statePer10, setStatePer10] = React.useState(10);
  const [lastPlay, setLastPlay] = React.useState({} as any);

  React.useEffect(() => {
    setIsReady(false);
    setLastPlay(
      user.watch_history?.find(
        (o: any) => selectedLesson.id === parseInt(o.lesson_id, 10)
      )
    );
    return () => {};
  }, [selectedLesson]);

  const onReady = React.useCallback(() => {
    if (!isReady && lastPlay) {
      const timeToStart = parseFloat(
        lastPlay.progress !== '1' ? lastPlay.progress : 0
      );
      playerRef.current.seekTo(
        Number.isNaN(timeToStart) ? 0 : timeToStart,
        'seconds'
      );
      setIsReady(true);
    }
  }, [isReady]);

  const onEndVideo = () => {
    if (!selectedLesson.is_completed) {
      onCheckLesson(selectedLesson.id, '1');
    }
  };
  const onProgress = (state: any) => {
    if (!selectedLesson.is_completed && state.played !== 1) {
      if (state.playedSeconds >= statePer10) {
        setStatePer10(statePer10 + 10);
        updateWatch(selectedLesson.id, state.playedSeconds.toString());
      }
    }
  };

  return (
    <ReactPlayer
      ref={playerRef}
      url={selectedLesson?.video_url}
      controls
      onEnded={onEndVideo}
      onProgress={onProgress}
      height={600}
      width={'100%'}
      onReady={onReady}
      playing={true}
    />
  );
};
const EnrolDetail = () => {
  const router = useRouter();
  const { user } = useState();
  const {
    getsCourseSections,
    getMyCourse,
    // getsCourseLesson,
    updatetEnrolSubsciption,
    updateLessonProgress,
    getCertificate,
  } = useActions();
  const [loading, setLoading] = React.useState(false);
  const [isSubscribed, setIsSubscribed] = React.useState(false);
  const [data, setData] = React.useState({} as any);
  const [valueProgress, setValueProgress] = React.useState(0);
  const [selectedLesson, setSelectedLesson] = React.useState({} as any);
  const [selectedLessonTmp, setSelectedLessonTmp] = React.useState({} as any);
  // const [dataLesson, setDataLesson] = React.useState([] as any);
  const [dataSection, setDataSection] = React.useState([] as any);
  const [openPlayConfirm, setOpenPlayConfirm] = React.useState(false);

  const init = async (id: string) => {
    const payload = { id: id.toString() };
    getMyCourse(payload).then((res) => {
      const item = res.items[0];
      setValueProgress(
        Math.round((item.completed_lessons / item.total_lessons) * 100)
      );
      setIsSubscribed(item.is_subscription);
      setData(item);
      getsCourseSections({
        course_id: parseInt(`${item.id}`, 10),
      }).then((ress: any) => {
        setDataSection(ress.items);
      });
      // getsCourseLesson({
      //   course_id: parseInt(`${res.course_id}`, 10),
      // }).then((ress: any) => {
      //   setDataLesson(ress.items);
      //   console.log(ress.items[0]);
      //   setSelectedLesson(ress.items[0]);
      // });
    });
  };
  React.useEffect(() => {
    if (router.query.id) {
      setLoading(true);
      init(router.query.id.toString()).finally(() => setLoading(false));
    }

    return () => {};
  }, [user]);

  const onCheckLesson = (lesson_id: string, val: any) => {
    updateLessonProgress({ id: user.id, lesson_id, progress: val }).then(() => {
      init(data.id.toString()).finally(() => setLoading(false));
    });
  };
  const updateWatch = (lesson_id: string, val: any) => {
    updateLessonProgress({ id: user.id, lesson_id, progress: val });
  };
  const getCertificateAction = () => {
    getCertificate({
      user_id: user.id,
      course_id: parseInt(`${data.id}`, 10),
    }).then(async (res) => {
      // saveAs(
      //   'https://timesofindia.indiatimes.com/thumb/msid-70238371,imgsize-89579,width-400,resizemode-4/70238371.jpg',
      //   'image.jpg'
      // );

      const anchorElement = document.createElement('a');
      anchorElement.href = res.certificate_url;
      anchorElement.download = 'image.jpg';
      anchorElement.target = '_blank';

      document.body.appendChild(anchorElement);
      anchorElement.click();
      document.body.removeChild(anchorElement);
    });
  };
  const onSelectLessonSubscription = (lesson: any) => {
    updatetEnrolSubsciption({
      id: data.enrol_subscription_id,
      lesson_id: lesson ? lesson.id : selectedLessonTmp.id,
    })
      .then(() => {
        setSelectedLesson(lesson ?? selectedLessonTmp);
        init(data.id.toString()).finally(() => setLoading(false));
      })
      .finally(() => {
        setOpenPlayConfirm(false);
      });
  };
  return (
    <>
      <Meta title="Reharm Music Courses" description="Reharm Music Courses" />
      <div
        id="mycourselesson"
        className="flex min-h-screen flex-col"
        onContextMenu={(event) => event.preventDefault()}
      >
        <header className={`main-header header 'on-top' fixed z-10 w-full`}>
          <nav className="mx-4 flex items-center justify-between py-4 lg:mx-20">
            <Fade duration={500} cascade direction="up">
              <div className="flex flex-row items-center gap-2">
                <div className="logosm">
                  <ButtonComp href="/">
                    <Image className="logosm" src={LogoSM} alt={''} />
                  </ButtonComp>
                </div>
                <span className="text-xl font-semibold text-white lg:text-2xl">
                  {' '}
                  | {data?.title}
                </span>
              </div>
            </Fade>
            <div className="flex flex-row items-center gap-2 lg:gap-5">
              <div className="hidden md:block">
                <div className="flex flex-row items-center gap-2 lg:gap-5">
                  <Button
                    color="gray"
                    className="gap-2"
                    onClick={() => {
                      router.back();
                    }}
                  >
                    <HiChevronLeft size={20} />
                    <span>My courses</span>
                  </Button>
                  <Button
                    color="gray"
                    className="gap-2"
                    onClick={() => {
                      const href = `/courses/${data.id}`;
                      router.push(href);
                    }}
                  >
                    <span>Course detail</span>
                    <HiChevronRight size={20} />
                  </Button>
                </div>
              </div>
            </div>
          </nav>
        </header>
        {!loading && (
          <div className="content-body flex-1 bg-slate-100">
            <div className="mx-4 mt-[5.5rem] lg:mx-20">
              <Breadscrumb
                homeRoute="/"
                dataItem={[
                  { label: 'My courses', route: '/my-courses' },
                  { label: data?.title, route: '' },
                ]}
              />
              <div className="grid grid-cols-1 gap-5 py-5 lg:grid-cols-12">
                <div className="lg:col-span-8">
                  <div className="is-sticky ">
                    <div className="mb-5 w-full">
                      {selectedLesson?.attachment_type === 'pdf' &&
                        selectedLesson?.lesson_type === 'other' && (
                          <iframe
                            width={'100%'}
                            height="500px"
                            src={selectedLesson?.attachment}
                          />
                        )}
                      {selectedLesson?.lesson_type === 'video' && (
                        <>
                          <RenderVideo
                            selectedLesson={selectedLesson}
                            onCheckLesson={onCheckLesson}
                            updateWatch={updateWatch}
                          />
                        </>
                      )}
                    </div>
                    <div className="rounded bg-slate-300 p-3">
                      <h1>Note:</h1>
                      <span>{selectedLesson?.summary}</span>
                    </div>
                  </div>
                </div>
                <div className="rounded bg-white p-3 lg:col-span-4">
                  <h1 className="mb-3 text-xl">Course content</h1>
                  <Tabs value="Lesson">
                    <TabsHeader className="bg-slate-300">
                      <Tab value={'Lesson'}>Lesson</Tab>
                      <Tab value={'Certificate'}>Certificate</Tab>
                    </TabsHeader>
                    <TabsBody>
                      <TabPanel value={'Lesson'}>
                        <div>
                          <Accordion className="p-0">
                            {dataSection?.map((item: any, i: number) => {
                              const lessons = data?.lessons?.filter(
                                (o: any) => o.section_id === item.id
                              );
                              return (
                                <Accordion.Panel key={i}>
                                  <Accordion.Title className="text-black">
                                    <span>
                                      <span className="text-lg font-thin">
                                        Section {i + 1}:{' '}
                                      </span>
                                      {item.title}
                                    </span>
                                  </Accordion.Title>
                                  <Accordion.Content>
                                    {lessons?.map((lesson: any, ii: number) => (
                                      <div
                                        className={`flex flex-row items-center justify-between border-b-[1px] py-1 px-2 ${
                                          selectedLesson?.id === lesson.id
                                            ? 'bg-indigo-500'
                                            : isSubscribed && lesson.is_locked
                                            ? 'bg-gray-200'
                                            : ''
                                        }`}
                                        key={ii}
                                      >
                                        <div className="flex flex-row items-center gap-2">
                                          {isSubscribed && (
                                            <>
                                              {lesson.is_locked ? (
                                                <FaLock color={'#ca156b'} />
                                              ) : (
                                                <FaLockOpen color={'#ca156b'} />
                                              )}
                                            </>
                                          )}
                                          <Checkbox
                                            id={`${i}-${ii}`}
                                            defaultChecked={lesson.is_completed}
                                            disabled={
                                              isSubscribed && lesson.is_locked
                                            }
                                            value={lesson.is_completed}
                                            onClick={(e) =>
                                              onCheckLesson(
                                                lesson.id,
                                                e.currentTarget.checked
                                                  ? '1'
                                                  : '0'
                                              )
                                            }
                                          />
                                          {lesson?.lesson_type === 'other' && (
                                            <BsFileText
                                              color={
                                                selectedLesson?.id === lesson.id
                                                  ? 'white'
                                                  : 'black'
                                              }
                                            />
                                          )}
                                          {lesson?.lesson_type === 'video' && (
                                            <MdOndemandVideo
                                              color={
                                                selectedLesson?.id === lesson.id
                                                  ? 'white'
                                                  : 'black'
                                              }
                                            />
                                          )}
                                          <button
                                            className="text-left"
                                            disabled={
                                              isSubscribed && lesson.is_locked
                                            }
                                            onClick={() => {
                                              if (isSubscribed) {
                                                setSelectedLessonTmp(lesson);
                                                if (lesson.is_opened) {
                                                  onSelectLessonSubscription(
                                                    lesson
                                                  );
                                                } else {
                                                  setOpenPlayConfirm(true);
                                                }
                                              } else {
                                                setSelectedLesson(lesson);
                                              }
                                            }}
                                          >
                                            <span
                                              className={`text-sm ${
                                                (isSubscribed &&
                                                  !lesson.is_locked) ||
                                                !isSubscribed
                                                  ? 'hover:text-sky-600'
                                                  : ''
                                              } ${
                                                selectedLesson?.id === lesson.id
                                                  ? 'text-white hover:text-black'
                                                  : ''
                                              }`}
                                            >
                                              {lesson?.title}
                                            </span>
                                          </button>
                                        </div>
                                        <span className="text-sm">
                                          {lesson?.duration}
                                        </span>
                                      </div>
                                    ))}
                                  </Accordion.Content>
                                </Accordion.Panel>
                              );
                            })}
                          </Accordion>
                        </div>
                      </TabPanel>
                      <TabPanel value={'Certificate'}>
                        <div className="flex flex-col items-center justify-center">
                          <div
                            style={{ width: 200, height: 200 }}
                            className="mb-5"
                          >
                            <CircularProgressbar
                              value={valueProgress}
                              text={`${valueProgress}%`}
                              styles={buildStyles({
                                trailColor: '#d6d6d6',
                                pathColor:
                                  valueProgress === 100
                                    ? `rgb(14 159 110)`
                                    : `rgb(109, 40, 217)`,
                              })}
                            />
                          </div>
                          {valueProgress !== 100 && (
                            <>
                              <div className="max-w-sm rounded-lg border border-gray-200 bg-white p-6 text-center shadow">
                                <a href="#">
                                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                    Notice
                                  </h5>
                                </a>
                                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                                  You have completed {valueProgress}% of the
                                  course. You can download the course completion
                                  certificate after completing the course
                                </p>
                              </div>
                            </>
                          )}
                          {valueProgress === 100 && (
                            <>
                              <div className="mb-10 max-w-sm rounded-lg border border-green-200 bg-green-100 p-6 text-center shadow">
                                <a href="#">
                                  <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                    Notice
                                  </h5>
                                </a>
                                <p className="mb-3 font-semibold text-gray-700 dark:text-gray-400">
                                  Congratulations!
                                </p>
                                <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
                                  You are now eligible to donwload the course
                                  completion certificate
                                </p>
                              </div>
                              <Button
                                gradientDuoTone="purpleToBlue"
                                onClick={() => getCertificateAction()}
                              >
                                <BsDownload className="mr-2 h-5 w-5" />
                                Get Certificate
                              </Button>
                            </>
                          )}
                        </div>
                      </TabPanel>
                    </TabsBody>
                  </Tabs>
                </div>
              </div>
            </div>
          </div>
        )}
        <Footer />
      </div>
      <Modal
        show={openPlayConfirm}
        size="lg"
        popup
        onClose={() => setOpenPlayConfirm(false)}
      >
        <Modal.Header />
        <Modal.Body>
          <div className="text-center">
            <HiOutlineExclamationCircle className="mx-auto mb-4 h-14 w-14 text-red-600" />
            <h3 className="mb-2 text-xl font-semibold text-gray-900">
              Are you sure to choose this lesson?
            </h3>
            <h4 className="mb-5 text-base font-normal text-gray-600">
              You can only watch one lesson per day
            </h4>
            <div className="flex justify-center gap-4">
              <Button
                color="failure"
                onClick={() => onSelectLessonSubscription(undefined)}
              >
                {`Yes, I'm sure`}
              </Button>
              <Button color="gray" onClick={() => setOpenPlayConfirm(false)}>
                No, cancel
              </Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default EnrolDetail;
