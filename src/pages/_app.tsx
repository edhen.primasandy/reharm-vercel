import '../styles/global.scss';
import '@styles/index.scss';

import { Toast } from '@components/elements';
import config from '@constant/config';
import { GoogleOAuthProvider } from '@react-oauth/google';
import type { NextPage } from 'next';
import type { AppProps } from 'next/app';
import { Provider } from 'overmind-react';
import type { ReactElement, ReactNode } from 'react';
import { CookiesProvider } from 'react-cookie';

import AuthProvider from '@/context/auth';
import { store } from '@/overmind/index';

export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
  getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout;
};
const App = ({ Component, pageProps }: AppPropsWithLayout) => {
  const getLayout = Component.getLayout ?? ((page) => page);
  return (
    <Provider value={store}>
      <CookiesProvider>
        <AuthProvider>
          <GoogleOAuthProvider
            clientId={config.googleCLientID?.toString() ?? ''}
          >
            <Toast />
            {getLayout(<Component {...pageProps} />)}
          </GoogleOAuthProvider>
        </AuthProvider>
      </CookiesProvider>
    </Provider>
  );
};

export default App;
