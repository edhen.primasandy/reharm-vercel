import Button from '@components/elements/button';
import { Config } from '@constant/index';
import Logo from '@images/logo-color.png';
import Image from 'next/image';
import Link from 'next/link';
import Router from 'next/router';
import React from 'react';
import { Fade } from 'react-awesome-reveal';
import ReCAPTCHA from 'react-google-recaptcha';

import { useActions } from '@/overmind/index';
import AuthLayout from '@/templates/Auth';

const Register = () => {
  const [newUser, setNewUser] = React.useState({
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    role: 'user',
    is_gmail: 'no',
  });
  const [captcha, setCaptcha] = React.useState(null as any);

  const { signUp } = useActions();
  const onSignUp = () => {
    signUp(newUser).then(() => Router.push('/login'));
  };
  return (
    <div className="relative z-10 m-5 flex items-center justify-center rounded-3xl bg-white shadow-lg">
      <Fade duration={600} cascade>
        <div className="flex flex-col items-center justify-center py-5 px-10">
          <div className="mb-1">
            <Image className="w-52" src={Logo} alt={''} />
          </div>
          <span className="mb-5 px-5 text-center text-sm text-gray-500">
            {'The Biggest Music Family with \n Community'}
          </span>
          <div className="mb-3 w-full">
            <label className="mb-2 block text-sm font-medium text-gray-900 dark:text-white">
              First name
            </label>
            <input
              type="text"
              id="firstname"
              className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500  "
              placeholder="First name"
              value={newUser.first_name}
              onChange={(event) =>
                setNewUser({ ...newUser, first_name: event.target.value })
              }
            />
          </div>
          <div className="mb-3 w-full">
            <label className="mb-2 block text-sm font-medium text-gray-900 dark:text-white">
              Last name
            </label>
            <input
              type="text"
              id="lastname"
              className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500  "
              placeholder="Last name"
              value={newUser.last_name}
              onChange={(event) =>
                setNewUser({ ...newUser, last_name: event.target.value })
              }
            />
          </div>
          <div className="mb-3 w-full">
            <label className="mb-2 block text-sm font-medium text-gray-900 dark:text-white">
              Email
            </label>
            <input
              type="email"
              id="email-address-icon"
              className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500  "
              placeholder="Email"
              value={newUser.email}
              onChange={(event) =>
                setNewUser({ ...newUser, email: event.target.value })
              }
            />
          </div>
          <div className="mb-10 w-full">
            <label className="mb-2 block text-sm font-medium text-gray-900 dark:text-white">
              Password
            </label>
            <input
              type="password"
              id="password"
              className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500  "
              placeholder="Password"
              value={newUser.password}
              onChange={(event) =>
                setNewUser({ ...newUser, password: event.target.value })
              }
            />
            {/* <p className="mt-2 text-sm text-red-600 dark:text-red-500">
            <span className="font-medium">Oops!</span> Username already taken!
          </p> */}
          </div>
          <div className="mb-5">
            {Config.recaptchaSiteKey && (
              <ReCAPTCHA
                sitekey={Config.recaptchaSiteKey ?? ''}
                onChange={(token) => setCaptcha(token)}
              />
            )}
          </div>
          <div className="mb-10 w-full justify-center">
            <Button
              onClick={onSignUp}
              isDisabled={captcha === null}
              className="group relative inline-block w-full items-center rounded-full px-5 py-2.5 text-center font-medium text-white disabled:opacity-50"
            >
              <span className="absolute top-0 left-0 h-full w-full rounded-full bg-gradient-to-l from-reharmpink to-reharmpurple opacity-50 blur-sm"></span>
              <span className="absolute inset-0 mt-0.5 ml-0.5 h-full w-full rounded-full bg-gradient-to-l from-reharmpink to-reharmpurple opacity-50 group-active:opacity-0"></span>
              <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-l from-reharmpink to-reharmpurple shadow-xl transition-all duration-200 ease-out group-hover:blur-sm group-active:opacity-0"></span>
              <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-l from-reharmpurple to-reharmpink transition duration-200 ease-out"></span>
              <span className="relative">Sign Up</span>
            </Button>
          </div>
          <div className="mb-5">
            <span className="text-sm">
              Already have an account?{' '}
              <Link href="/login">
                <span className="text-base font-semibold text-reharmpink hover:text-reharmpurple">
                  Sign in here
                </span>
              </Link>
            </span>
          </div>
          <div className="">
            <span className="text-xs text-gray-500">&copy; Reharm 2023</span>
          </div>
        </div>
      </Fade>
    </div>
  );
};

export default Register;

Register.getLayout = (page: any) => {
  return <AuthLayout>{page}</AuthLayout>;
};
