/* eslint-disable tailwindcss/no-custom-classname */
import { Button, Label, Tabs, Textarea, TextInput } from 'flowbite-react';
import React from 'react';
import { FaHistory } from 'react-icons/fa';
import {
  HiAdjustments,
  HiRefresh,
  HiSave,
  HiUpload,
  HiUserCircle,
} from 'react-icons/hi';
import { MdLock } from 'react-icons/md';
import { toast } from 'react-toastify';

import Breadscrumb from '@/components/elements/breadscrumb';
import ImageDropzone from '@/components/elements/image-dropzone';
import { useActions, useState } from '@/overmind';
import Main from '@/templates/Main';

import ReferralHistoryTable from './history-referral';
import ReferralLink from './referal-code';

const Profile = () => {
  const { user } = useState();
  const { updateUser, getUserProfile, changePassword, uploadPhotoUser } =
    useActions();
  const [data, setData] = React.useState(user);
  const [isBase64, setIsBase64] = React.useState(false);
  const [authData, setAuthData] = React.useState({
    old_password: '',
    password: '',
    password_confirmation: '',
  });

  React.useEffect(() => {
    if (user === null) {
      getUserProfile().then((res) => {
        setData(res);
      });
    }

    return () => {};
  }, [user]);

  const updateProfile = () => {
    const payload = {
      id: data.id,
      first_name: data.first_name,
      last_name: data.last_name,
      email: data.email,
      biography: data.biography,
      province_id: 0,
      regency_id: 0,
      district_id: 0,
      village_id: 0,
      address: data.address,
      wishlist: data.wishlist,
    };
    updateUser(payload).then(() => {
      getUserProfile().then((res) => {
        setData(res);
      });
      toast.success('data has been updated');
    });
  };
  const onChangePassword = () => {
    changePassword(authData)
      .then(() => {
        toast.success('Password has been changed');
      })
      .catch((e) => toast.error(e.message));
  };
  const onChangePhoto = () => {
    uploadPhotoUser({
      photo: data?.photo,
      id: data.id,
    })
      .then(() => {
        getUserProfile().then((res) => {
          setData(res);
        });
        toast.success('User photo hase been changed');
      })
      .catch((e) => toast.error(e.message));
  };

  return (
    <section>
      <div className="container mx-auto mt-20 flex items-center">
        <div className="w-full">
          <Breadscrumb
            homeRoute="/"
            dataItem={[{ label: 'Profile', route: '' }]}
          />
          <h1 className="mb-2 text-4xl font-semibold">User Profile</h1>
          <div className="mb-5 rounded-2xl bg-white p-10">
            <div className="grid w-full grid-cols-1 gap-6 lg:grid-cols-10">
              <div className="mb-5 lg:col-span-3">
                <div
                  className="flex items-center justify-center space-x-4"
                  data-testid="flowbite-avatar"
                >
                  <div className="relative">
                    <img
                      alt=""
                      src={
                        data?.photo
                          ? data?.photo
                          : 'https://flowbite.com/docs/images/people/profile-picture-5.jpg'
                      }
                      className="h-36 w-36 !rounded-full object-cover p-1 ring-2 ring-gray-300"
                      data-testid="flowbite-avatar-img"
                    />
                  </div>
                </div>
                <div className="mt-5 grid grid-cols-3 gap-5 rounded-md bg-slate-100 p-2">
                  <div className="text-center">
                    <p className="font-semibold">{user?.levels?.name}</p>
                    <p>Class</p>
                  </div>
                  <div className="text-center">
                    <p className="font-semibold">{user?.levels?.level}</p>
                    <p>Level</p>
                  </div>
                  <div className="text-center">
                    <p className="font-semibold">{user?.levels?.point}</p>
                    <p>Total point</p>
                  </div>
                </div>
                <ReferralLink />
              </div>
              <div className="lg:col-span-7">
                <Tabs.Group style="pills">
                  <Tabs.Item active title="Basic Info" icon={HiUserCircle}>
                    <div className="rounded bg-slate-50 p-4">
                      <form className="grid grid-cols-2 gap-3">
                        <div className="mb-2">
                          <div className="mb-2 block">
                            <Label htmlFor="first_name" value="First name" />
                          </div>
                          <TextInput
                            className="eTextinput"
                            value={data?.first_name}
                            id="first_name"
                            type="text"
                            sizing="md"
                            onChange={(e) =>
                              setData({ ...data, first_name: e.target.value })
                            }
                          />
                        </div>
                        <div className="mb-2">
                          <div className="mb-2 block">
                            <Label htmlFor="last_name" value="Last name" />
                          </div>
                          <TextInput
                            className="eTextinput"
                            value={data?.last_name}
                            id="last_name"
                            type="text"
                            sizing="md"
                            onChange={(e) =>
                              setData({ ...data, last_name: e.target.value })
                            }
                          />
                        </div>
                        <div className="mb-2">
                          <div className="mb-2 block">
                            <Label htmlFor="facebook" value="Facebook link" />
                          </div>
                          <TextInput
                            className="eTextinput"
                            value={data?.social_links?.facebook}
                            id="facebook"
                            type="text"
                            sizing="md"
                            onChange={(e) => {
                              const soslinks = {
                                ...data.social_links,
                                facebok: e.target.value,
                              };
                              setData({
                                ...data,
                                social_links: { ...soslinks },
                              });
                            }}
                          />
                        </div>
                        <div className="mb-2">
                          <div className="mb-2 block">
                            <Label htmlFor="twitter" value="Twiter link" />
                          </div>
                          <TextInput
                            className="eTextinput"
                            value={data?.social_links?.twitter}
                            id="twitter"
                            type="text"
                            sizing="md"
                            onChange={(e) => {
                              const soslinks = {
                                ...data.social_links,
                                twitter: e.target.value,
                              };
                              setData({
                                ...data,
                                social_links: { ...soslinks },
                              });
                            }}
                          />
                        </div>
                        <div className="mb-2">
                          <div className="mb-2 block">
                            <Label htmlFor="linkedin" value="Linkedin link" />
                          </div>
                          <TextInput
                            className="eTextinput"
                            value={data?.social_links?.linkedin}
                            id="linkedin"
                            type="text"
                            sizing="md"
                            onChange={(e) => {
                              const soslinks = {
                                ...data.social_links,
                                linkedin: e.target.value,
                              };
                              setData({
                                ...data,
                                social_links: { ...soslinks },
                              });
                            }}
                          />
                        </div>
                        <div className="col-span-2 mb-2">
                          <div className="mb-2 block">
                            <Label htmlFor="biography" value="Biography" />
                            <Textarea
                              className="eTextarea"
                              value={data?.biography}
                              id="biography"
                              placeholder="Biography"
                              required={true}
                              rows={4}
                              onChange={(e) =>
                                setData({ ...data, biography: e.target.value })
                              }
                            />
                          </div>
                        </div>
                      </form>
                      <div className="flex justify-end">
                        <Button
                          gradientDuoTone="cyanToBlue"
                          onClick={updateProfile}
                        >
                          <HiSave className="mr-2 h-5 w-5" />
                          Update profile
                        </Button>
                      </div>
                    </div>
                  </Tabs.Item>
                  <Tabs.Item title="Login Credentials" icon={MdLock}>
                    <div className="rounded bg-slate-50 p-4">
                      <form className="mb-2 grid grid-cols-2 gap-3">
                        <div className="mb-2">
                          <div className="mb-2 block">
                            <Label htmlFor="email" value="Email" />
                          </div>
                          <TextInput
                            className="eTextinput"
                            value={data?.email}
                            id="email"
                            type="text"
                            sizing="md"
                            disabled
                          />
                        </div>
                        <div className="mb-2">
                          <div className="mb-2 block">
                            <Label htmlFor="old_password" value="Password" />
                          </div>
                          <TextInput
                            className="eTextinput"
                            value={authData.old_password}
                            id="old_password"
                            type="password"
                            sizing="md"
                            onChange={(e) =>
                              setAuthData({
                                ...authData,
                                old_password: e.target.value,
                              })
                            }
                          />
                        </div>
                        <div className="mb-2">
                          <div className="mb-2 block">
                            <Label htmlFor="password" value="New Password" />
                          </div>
                          <TextInput
                            className="eTextinput"
                            value={authData.password}
                            id="old_password"
                            type="password"
                            sizing="md"
                            onChange={(e) =>
                              setAuthData({
                                ...authData,
                                password: e.target.value,
                              })
                            }
                          />
                        </div>
                        <div className="mb-2">
                          <div className="mb-2 block">
                            <Label
                              htmlFor="password_confirmation"
                              value="New Password Confirmation"
                            />
                          </div>
                          <TextInput
                            className="eTextinput"
                            value={authData.password_confirmation}
                            id="password_confirmation"
                            type="password"
                            sizing="md"
                            onChange={(e) =>
                              setAuthData({
                                ...authData,
                                password_confirmation: e.target.value,
                              })
                            }
                          />
                        </div>
                      </form>
                      <div className="flex justify-end">
                        <Button
                          gradientDuoTone="cyanToBlue"
                          onClick={onChangePassword}
                        >
                          <HiRefresh className="mr-2 h-5 w-5" />
                          Change password
                        </Button>
                      </div>
                    </div>
                  </Tabs.Item>
                  <Tabs.Item title="User Photo" icon={HiAdjustments}>
                    <div className="rounded bg-slate-50 p-4">
                      <div id="fileUpload" className="mb-2">
                        <div className="mb-2 block">
                          <Label htmlFor="photo" value="Upload photo" />
                        </div>
                        <ImageDropzone
                          imgSource={isBase64 ? '' : data?.photo}
                          onChange={(file) => {
                            if (file) {
                              setData({ ...data, photo: file });
                              setIsBase64(true);
                            } else {
                              setData({ ...data, photo: '' });
                            }
                          }}
                        />
                      </div>
                      <div className="flex justify-end">
                        <Button
                          gradientDuoTone="cyanToBlue"
                          onClick={onChangePhoto}
                        >
                          <HiUpload className="mr-2 h-5 w-5" />
                          Upload photo
                        </Button>
                      </div>
                    </div>
                  </Tabs.Item>
                  <Tabs.Item title="Referral History" icon={FaHistory}>
                    <ReferralHistoryTable />
                  </Tabs.Item>
                </Tabs.Group>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Profile;

Profile.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
