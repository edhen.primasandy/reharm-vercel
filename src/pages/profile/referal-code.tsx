import React from 'react';
import { toast } from 'react-toastify';

import { useState } from '@/overmind';

const ReferralLink = () => {
  const { user } = useState();
  const copyToClipboard = () => {
    navigator.clipboard.writeText(user?.referral_code);
    toast.success('referral copied to clipboard!');
  };

  return (
    <div className="mx-auto mt-4 max-w-md rounded-lg bg-gray-100 p-4">
      <label className="mb-2 block text-sm font-medium text-gray-800">
        YOUR REFERRAL CODE
      </label>
      <div className="relative">
        <input
          type="text"
          value={user?.referral_code}
          readOnly
          className="w-full rounded-lg bg-gray-100 py-2 pl-3 pr-20 text-gray-800"
        />
        <button
          onClick={copyToClipboard}
          className="absolute right-2 top-1/2 -translate-y-1/2 rounded-md bg-gray-700 px-3 py-1 text-gray-200 hover:bg-gray-600"
        >
          Copy
        </button>
      </div>
    </div>
  );
};

export default ReferralLink;
