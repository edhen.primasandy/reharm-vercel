import { Table } from 'flowbite-react';
import moment from 'moment';
import React, { useEffect } from 'react';

import { useActions, useState } from '@/overmind/index';
import helper from '@/utils/helper';

const ReferralHistoryTable = () => {
  const { getsReferral } = useActions();
  const { user } = useState();
  const [data, setData] = React.useState([] as any[]);

  useEffect(() => {
    getsReferral({ user_id: user.id }).then((r) => {
      setData(r.data.items);
    });
  }, []);

  return (
    <div className="mx-auto max-w-full p-4">
      <Table>
        <Table.Head>
          <Table.HeadCell>Date</Table.HeadCell>
          <Table.HeadCell>Course Name</Table.HeadCell>
          <Table.HeadCell>Commission</Table.HeadCell>
          <Table.HeadCell>Status</Table.HeadCell>
        </Table.Head>
        <Table.Body className="divide-y">
          {data.map((item, index) => (
            <Table.Row
              key={index}
              className="bg-white dark:border-gray-700 dark:bg-gray-800"
            >
              <Table.Cell>
                {moment(item.created_at).format('DD MMM YYYY')}
              </Table.Cell>
              <Table.Cell>{item.course.title}</Table.Cell>
              <Table.Cell>
                {helper.currencyFormat(parseFloat(item.commission))}
              </Table.Cell>
              <Table.Cell>
                <span
                  className={`rounded-full px-2 py-1 text-xs ${
                    item.is_paid === 1
                      ? 'bg-green-100 text-green-700'
                      : 'bg-yellow-100 text-yellow-700'
                  }`}
                >
                  {item.is_paid === 1 ? 'Paid' : 'Pending'}
                </span>
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    </div>
  );
};

export default ReferralHistoryTable;
