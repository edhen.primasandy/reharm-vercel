import { useActions, useState } from '@overmind/index';
import { Breadcrumb, Button, Table } from 'flowbite-react';
import moment from 'moment';
import router from 'next/router';
import React from 'react';
import { FaFileDownload } from 'react-icons/fa';
import { HiHome } from 'react-icons/hi';
import Datepicker from 'react-tailwindcss-datepicker';

import { ETable } from '@/components/elements';
import AdminLayout from '@/templates/Admin';
import helper from '@/utils/helper';

const Layout = () => {
  const { getsReportRevenue, resetFilter } = useActions();
  const { filter } = useState();
  const [loading, setLoading] = React.useState(false);
  const [isProcessingExport, setIsProcessingExport] = React.useState(false);
  const [ready, setReady] = React.useState(false);
  const [data, setData] = React.useState({} as any);
  const [dateFilter, setDateFilter] = React.useState({
    startDate: moment().startOf('month').toISOString(),
    endDate: moment().endOf('month').toISOString(),
  });
  const handleValueChange = (newValue: any) => {
    setDateFilter(newValue);
  };
  const initData = () => {
    setLoading(true);
    getsReportRevenue({
      ...filter,
      start_date: dateFilter.startDate,
      end_date: dateFilter.endDate,
    })
      .then((res) => {
        setData(res.data);
      })
      .finally(() => setLoading(false));
  };

  React.useEffect(() => {
    if (ready) {
      initData();
    }
    return () => {};
  }, [dateFilter, filter, ready]);
  React.useEffect(() => {
    resetFilter().then(() => setReady(true));
    return () => {};
  }, []);
  const exportData = () => {
    setIsProcessingExport(true);
    getsReportRevenue({
      format: 'export',
      start_date: dateFilter.startDate,
      end_date: dateFilter.endDate,
    })
      .then((res) => {
        const link = document.createElement('a');
        link.href = res.data;
        link.setAttribute('download', 'revenue-report.xlsx');
        document.body.appendChild(link);
        link.click();
      })
      .finally(() => setIsProcessingExport(false));
  };
  const header = [
    { title: '#', field: 'no' },
    { title: 'User name', field: 'user_name' },
    { title: 'Enrolled course', field: 'course_name' },
    { title: 'Instructor', field: 'instructor_name' },
    { title: 'Order id', field: 'order_id' },
    { title: 'Total amount', field: 'total_amount' },
    { title: 'Instructor revenue', field: 'instructor_revenue' },
    { title: 'Enrolment date', field: 'enrolment_date' },
    { title: 'Status', field: 'status' },
  ];
  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Report</Breadcrumb.Item>
        <Breadcrumb.Item>Instructor Revenue</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Instructor Revenue</h1>
        </div>
      </div>
      <div className="mx-5 rounded bg-white p-5 shadow-md">
        <div className="mb-5 flex flex-row justify-between gap-4">
          <div className="w-1/4">
            <Datepicker
              primaryColor={'fuchsia'}
              value={dateFilter}
              onChange={handleValueChange}
              showShortcuts={true}
              displayFormat={'DD MMM YYYY'}
              showFooter={true}
            />
          </div>
          <Button
            isProcessing={isProcessingExport}
            disabled={isProcessingExport}
            gradientMonochrome="success"
            onClick={() => exportData()}
          >
            <FaFileDownload className="mr-2 h-5 w-5" />
            Export data
          </Button>
        </div>
        <div>
          <ETable
            header={header}
            data={data.items}
            totalData={data.total ?? 0}
            pageCount={10}
            searchText=""
            customTableBody
            renderTableBody={
              <>
                <Table.Body className="divide-y">
                  {loading && (
                    <Table.Row className="bg-white">
                      <Table.Cell colSpan={8}>
                        <div className="flex h-10 w-full items-center justify-center">
                          <div className="animate-pulse rounded-full bg-fuchsia-200 px-3 py-1 text-center text-xs font-medium leading-none text-fuchsia-800">
                            loading...
                          </div>
                        </div>
                      </Table.Cell>
                    </Table.Row>
                  )}
                  {!loading &&
                    data?.items?.map((item: any, i: number) => {
                      return (
                        <Table.Row className="bg-white" key={i}>
                          <Table.Cell> {filter.skip + i + 1}</Table.Cell>
                          <Table.Cell>
                            <p className="text-sm">{item.user_name}</p>
                          </Table.Cell>
                          <Table.Cell>
                            <p className="font-semibold text-purple-700">
                              {item.course_name}
                            </p>
                          </Table.Cell>
                          <Table.Cell>
                            <p className="text-sm">{item.instructor_name}</p>
                          </Table.Cell>
                          <Table.Cell>
                            <p className="text-sm">{item.order_id}</p>
                          </Table.Cell>
                          <Table.Cell>
                            <span className="rounded bg-gray-200 px-1 text-gray-700">
                              {helper.currencyFormat(item.total_amount)}
                            </span>
                          </Table.Cell>
                          <Table.Cell>
                            <span className="rounded bg-gray-200 px-1 text-gray-700">
                              {helper.currencyFormat(item.admin_revenue)}
                            </span>
                          </Table.Cell>
                          <Table.Cell>
                            <p className="text-sm">
                              {moment(item.enrolment_date).format(
                                'DD MMM YYYY'
                              )}
                            </p>
                          </Table.Cell>
                          <Table.Cell>
                            <p className="text-sm">{item.status}</p>
                          </Table.Cell>
                        </Table.Row>
                      );
                    })}
                </Table.Body>
              </>
            }
          />
        </div>
      </div>
    </div>
  );
};

export default Layout;

Layout.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
