import { Breadcrumb, Button, Label, Select, Table } from 'flowbite-react';
import moment from 'moment';
import router from 'next/router';
import React from 'react';
import { HiHome, HiOutlineFilter } from 'react-icons/hi';
import Swal from 'sweetalert2';

import { ETable } from '@/components/elements';
import { useActions, useState } from '@/overmind';
import AdminLayout from '@/templates/Admin';
import helper from '@/utils/helper';

const Layout = () => {
  const { resetFilter, getsReferral, updateReferral } = useActions();
  const { filter } = useState();
  const [status, setStatus] = React.useState(-1);
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState([] as any);
  const initData = () => {
    setLoading(true);
    let payload: any = { ...filter };
    if (status > -1) {
      payload = { ...payload, is_paid: status };
    }
    getsReferral(payload)
      .then((r) => {
        setData(r.data);
      })
      .finally(() => setLoading(false));
  };
  const handleChange = (event: any) => {
    setStatus(event.target.value);
    // Handle the selected status here (e.g., update state or send to API)
  };
  const header = [
    { title: '#', field: 'no' },
    { title: 'User name', field: 'user' },
    { title: 'Date', field: 'date' },
    { title: 'Course name', field: 'course_name' },
    { title: 'Referral Code', field: 'referral_code' },
    { title: 'Commission', field: 'commission' },
    { title: 'Status', field: 'status' },
  ];
  React.useEffect(() => {
    resetFilter();
    return () => {};
  }, []);

  React.useEffect(() => {
    initData();
    return () => {};
  }, [filter, status]);

  const handleUpdateStatus = (item: any) => {
    Swal.fire({
      title: 'Are you sure?',
      text: 'Do you want to update the status of this commission?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, update it!',
      cancelButtonText: 'No, cancel!',
    }).then((result) => {
      if (result.isConfirmed) {
        console.log(item);
        updateReferral({ ...item, is_paid: 1 }).then(() => {
          initData();
          Swal.fire('Updated!', 'The status has been updated.', 'success');
        });
        // const newData: any = [...data.items];
        // if (newData[index]) {
        //   newData[index].status = 'paid';
        //   setData({ ...data, items: newData });
        //   Swal.fire('Updated!', 'The status has been updated.', 'success');
        // } else {
        //   console.error('Attempted to access an undefined item in newData.');
        // }
      }
    });
  };
  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Referral History</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Referral history</h1>
        </div>
      </div>
      <div className="mx-5 rounded bg-white p-5 shadow-md">
        <div className="mb-5 flex flex-row gap-4">
          <div className="flex-1">
            <div className="grid grid-cols-4 gap-3">
              <div>
                <div className="mb-2 block">
                  <Label value="Status" />
                </div>
                <Select value={status} onChange={handleChange}>
                  <option value={-1}>Select Status</option>
                  <option value={0}>Pending</option>
                  <option value={1}>Paid</option>
                </Select>
              </div>
            </div>
          </div>
          <div className="flex items-end">
            <Button
              gradientMonochrome="pink"
              onClick={() => {
                initData();
              }}
            >
              <HiOutlineFilter className="mr-2 h-5 w-5" />
              Filter
            </Button>
          </div>
        </div>
        <ETable
          header={header}
          data={data.items}
          totalData={data.total ?? 0}
          pageCount={10}
          searchText=""
          haveAction
          customTableBody
          renderTableBody={
            <Table.Body className="divide-y">
              {loading && (
                <Table.Row className="bg-white">
                  <Table.Cell colSpan={8}>
                    <div className="flex h-10 w-full items-center justify-center">
                      <div className="animate-pulse rounded-full bg-fuchsia-200 px-3 py-1 text-center text-xs font-medium leading-none text-fuchsia-800">
                        loading...
                      </div>
                    </div>
                  </Table.Cell>
                </Table.Row>
              )}
              {!loading &&
                data?.items?.map((item: any, i: number) => {
                  return (
                    <Table.Row className="bg-white" key={i}>
                      <Table.Cell> {filter.skip + i + 1}</Table.Cell>
                      <Table.Cell>
                        <p className="text-sm text-gray-800">
                          {item.user.first_name} {item.user.last_name}
                        </p>
                        <p className="text-sm">Email: {item.user.email}</p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">
                          {moment(item.created_at).format('ddd, DD MMM YYYY')}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm font-semibold text-violet-600">
                          {item.course.title}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm font-semibold">
                          {item.referred_user.referral_code}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm font-semibold">
                          {helper.currencyFormat(parseFloat(item.commission))}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <span
                          className={`rounded-full px-2 py-1 text-xs ${
                            item.is_paid === 1
                              ? 'bg-green-100 text-green-700'
                              : 'bg-yellow-100 text-yellow-700'
                          }`}
                        >
                          {item.is_paid === 1 ? 'Paid' : 'Pending'}
                        </span>
                      </Table.Cell>
                      <Table.Cell width={100}>
                        {/* <button
                          disabled
                          onClick={() => handleUpdateStatus(i)}
                          className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-1 px-3 rounded"
                        >
                          Update Status
                        </button> */}
                        <Button
                          disabled={item.is_paid === 1}
                          onClick={() => handleUpdateStatus(item)}
                          size="xs"
                        >
                          Update
                        </Button>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
            </Table.Body>
          }
        />
      </div>
    </div>
  );
};

export default Layout;

Layout.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
