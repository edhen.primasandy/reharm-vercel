import ButtonComp from '@components/elements/button';
import ETable from '@components/elements/table';
import helper from '@utils/helper';
import { Breadcrumb, Button, Table } from 'flowbite-react';
import router from 'next/router';
import React from 'react';
import {
  HiHome,
  HiOutlineDocumentAdd,
  HiPencil,
  HiTrash,
} from 'react-icons/hi';
import { toast } from 'react-toastify';
import Swal from 'sweetalert2';

import { useActions, useState } from '@/overmind/index';
import AdminLayout from '@/templates/Admin';

const Courses = () => {
  const { getListSubscription, deleteSubscription, resetFilter } = useActions();
  const { filter } = useState();
  const [loading, setLoading] = React.useState(false);
  const [ready, setReady] = React.useState(false);
  const [data, setData] = React.useState({} as any);
  const header = [
    { title: '#', field: 'no' },
    { title: 'Name', field: 'name' },
    { title: 'Period', field: 'period' },
    { title: 'Total Feature', field: 'features' },
    { title: 'Status', field: 'status' },
    { title: 'Price', field: 'price' },
  ];
  const initData = () => {
    setLoading(true);
    getListSubscription({ ...filter })
      .then((res: any) => {
        setData(res);
      })
      .finally(() => setLoading(false));
  };
  React.useEffect(() => {
    if (ready) {
      initData();
    }
    return () => {};
  }, [filter, ready]);
  React.useEffect(() => {
    resetFilter().then(() => setReady(true));
    return () => {};
  }, []);

  const onDelete = (id: any) => {
    Swal.fire({
      title: 'Are you sure? ',
      text: 'You want to delete this plan',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        deleteSubscription({ id }).then((res) => {
          toast.success(res.message);
          initData();
        });
      }
    });
  };
  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Subscription Plan</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Subscription Plan</h1>
          <Button
            gradientMonochrome="info"
            onClick={() => {
              router.push({
                pathname: '/admin/subscription-plan/[...id]',
                query: { id: 'add' },
              });
            }}
          >
            <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
            Add new subscription
          </Button>
        </div>
      </div>
      <div className="mx-5 rounded bg-white p-5 shadow-md">
        <ETable
          header={header}
          data={data.items}
          totalData={data.total ?? 0}
          pageCount={10}
          searchText=""
          haveAction
          customTableBody
          renderTableBody={
            <Table.Body className="divide-y">
              {loading && (
                <Table.Row className="bg-white">
                  <Table.Cell colSpan={8}>
                    <div className="flex h-10 w-full items-center justify-center">
                      <div className="animate-pulse rounded-full bg-fuchsia-200 px-3 py-1 text-center text-xs font-medium leading-none text-fuchsia-800">
                        loading...
                      </div>
                    </div>
                  </Table.Cell>
                </Table.Row>
              )}
              {!loading &&
                data?.items?.map((item: any, i: number) => {
                  return (
                    <Table.Row className="bg-white" key={i}>
                      <Table.Cell> {filter.skip + i + 1}</Table.Cell>
                      <Table.Cell>
                        <p className="font-semibold text-purple-700">
                          {item.name}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">{item.period}</p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">
                          Total Feature:{' '}
                          <span className="font-semibold">
                            {item.features.length}
                          </span>
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        {item.status === 'active' && (
                          <span className="rounded bg-emerald-200 px-1 text-emerald-700">
                            {item.status}
                          </span>
                        )}
                        {item.status === 'pending' && (
                          <span className="rounded bg-red-200 px-1 text-red-700">
                            {item.status}
                          </span>
                        )}
                      </Table.Cell>
                      <Table.Cell>
                        {item.price === 0 && (
                          <span className="rounded bg-emerald-200 px-1 text-emerald-700">
                            Free
                          </span>
                        )}
                        {item.price > 0 && (
                          <span className="rounded bg-gray-200 px-1 text-gray-700">
                            {helper.currencyFormat(item.price)}
                          </span>
                        )}
                      </Table.Cell>
                      <Table.Cell width={100}>
                        <ButtonComp
                          onClick={() => {
                            router.push({
                              pathname: '/admin/subscription-plan/[...id]',
                              query: { id: item.id },
                            });
                          }}
                        >
                          <HiPencil color="#2b6cb0" size={20} />
                        </ButtonComp>
                        <ButtonComp
                          className="ml-3"
                          onClick={() => onDelete(item.id)}
                        >
                          <HiTrash color="gray" size={20} />
                        </ButtonComp>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
            </Table.Body>
          }
        />
      </div>
    </div>
  );
};

export default Courses;

Courses.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
