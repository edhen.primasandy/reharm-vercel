/* eslint-disable tailwindcss/no-custom-classname */
import {
  Breadcrumb,
  Button,
  Card,
  Checkbox,
  Label,
  Textarea,
  TextInput,
} from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import { HiHome, HiOutlineMinus, HiOutlinePlus } from 'react-icons/hi';
import { IoIosSave } from 'react-icons/io';
import { MdOutlineCancel } from 'react-icons/md';
import { toast } from 'react-toastify';

import { InputSelect } from '@/components/elements';
import { useActions } from '@/overmind';
import AdminLayout from '@/templates/Admin';

const Form = () => {
  const router = useRouter();
  const { updateSubscription, createSubscription, getSubscription } =
    useActions();
  const [title, setTitle] = React.useState('Form');
  const [selectedStatus, setSelectedStatus] = React.useState(null);
  const [data, setData] = React.useState({
    name: '',
    description: '',
    price: 0,
    discounted_price: 0,
    features: [''],
    period: 0,
    status: '',
    is_top_subscription: 'no',
  } as any);

  const dataStatus = [
    { value: 'active', label: 'Active' },
    { value: 'pending', label: 'Pending' },
  ];

  React.useEffect(() => {
    if (router.query.id && !router.query.id?.includes('add')) {
      setTitle(`Edit ${router.query.id}`);
      getSubscription({ id: router.query.id.toString() }).then((res: any) => {
        const mappingData = res;
        if (mappingData?.features.length === 0) {
          mappingData.features.push('');
        }
        setData(mappingData);
        const fetchStatus: any = dataStatus.find((o) => o.value === res.status);
        if (fetchStatus) {
          setSelectedStatus(fetchStatus);
        }
      });
    }
    return () => {};
  }, [router.query.id]);

  const saveData = () => {
    const payload = { ...data };
    if (payload.id) {
      updateSubscription({ ...payload })
        .then(() => {
          toast.success('data has been saved');
          router.back();
        })
        .catch((e) => toast.error(e.message));
    } else {
      createSubscription({ ...payload })
        .then(() => {
          toast.success('data has been saved');
          router.back();
        })
        .catch((e) => toast.error(e.message));
    }
  };
  return (
    <div className="w-full">
      <Breadcrumb>
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/subscription-plan')}
        >
          Subscription Plan
        </Breadcrumb.Item>
        <Breadcrumb.Item>Form</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">{title}</h1>
          <div className="flex flex-row gap-3">
            <Button
              color="gray"
              onClick={() => {
                router.back();
              }}
            >
              <MdOutlineCancel className="mr-2 h-5 w-5" />
              Cancel
            </Button>
            <Button
              gradientMonochrome="info"
              onClick={() => {
                saveData();
              }}
            >
              <IoIosSave className="mr-2 h-5 w-5" />
              Save
            </Button>
          </div>
        </div>
        <Card className="mt-2">
          <div>
            <form className="grid grid-cols-2 gap-3">
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label htmlFor="name" value="Name*" />
                </div>
                <TextInput
                  className="eTextinput"
                  required
                  value={data.name}
                  id="name"
                  type="text"
                  sizing="md"
                  onChange={(e) => {
                    const newdata = { ...data, name: e.target.value };
                    setData({ ...newdata });
                  }}
                />
              </div>
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label htmlFor="period" value="Period*" />
                </div>
                <TextInput
                  className="eTextinput"
                  required
                  value={data.period}
                  id="period"
                  type="number"
                  sizing="md"
                  onChange={(e) => {
                    const newdata = { ...data, period: e.target.value };
                    setData({ ...newdata });
                  }}
                />
              </div>
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label htmlFor="description" value="Description" />
                </div>
                <Textarea
                  className="eTextarea"
                  required
                  value={data.description}
                  id="description"
                  onChange={(e) => {
                    const newdata = { ...data, description: e.target.value };
                    setData({ ...newdata });
                  }}
                />
              </div>
              <div className="mb-2">
                <InputSelect
                  isClearable
                  label="Status"
                  data={dataStatus}
                  value={selectedStatus}
                  placeholder="Select Status"
                  onChange={(e: any) => {
                    const newdata = {
                      ...data,
                      status: e !== null ? e.value : '',
                    };
                    setSelectedStatus(e);
                    setData({ ...newdata });
                  }}
                />
              </div>
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label htmlFor="price" value="Price*" />
                </div>
                <TextInput
                  className="eTextinput"
                  required
                  value={data.price}
                  id="price"
                  type="number"
                  sizing="md"
                  onChange={(e) => {
                    const newdata = { ...data, price: e.target.value };
                    setData({ ...newdata });
                  }}
                />
              </div>
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label htmlFor="discounted_price" value="Discounted price" />
                </div>
                <TextInput
                  className="eTextinput"
                  required
                  value={data.discounted_price}
                  id="discounted_price"
                  type="number"
                  sizing="md"
                  onChange={(e) => {
                    const newdata = {
                      ...data,
                      discounted_price: e.target.value,
                    };
                    setData({ ...newdata });
                  }}
                />
              </div>
              <div className="mb-2">
                <div className="flex flex-row items-center gap-2">
                  <Checkbox
                    id="topcourse"
                    checked={data.is_top_subscription === 'yes'}
                    value={data.is_top_subscription}
                    onChange={() => {
                      const newdata = {
                        ...data,
                        is_top_subscription:
                          data.is_top_subscription === 'no' ? 'yes' : 'no',
                      };
                      setData({ ...newdata });
                    }}
                  />
                  <Label htmlFor="topcourse">
                    Check if this top subscription
                  </Label>
                </div>
              </div>
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label htmlFor="features" value="Features" />
                </div>
                {data.features.map((item: string, i: number) => (
                  <div className="mb-2 flex flex-row gap-3" key={i}>
                    <TextInput
                      className="eTextinput basis-full"
                      placeholder="Features"
                      required
                      value={item}
                      id="req"
                      type="text"
                      sizing="md"
                      onChange={(e) => {
                        const reqs: any[] = [...data.features];
                        reqs[i] = e.target.value;
                        const newdata = { ...data, features: reqs };
                        setData({ ...newdata });
                      }}
                    />
                    {i === 0 ? (
                      <Button
                        gradientMonochrome="success"
                        onClick={() => {
                          const reqs: any[] = [...data.features];
                          reqs.push('');
                          const newdata = { ...data, features: reqs };
                          setData({ ...newdata });
                        }}
                      >
                        <HiOutlinePlus className="h-6 w-6" />
                      </Button>
                    ) : (
                      <Button
                        gradientMonochrome="failure"
                        onClick={() => {
                          const reqs: any[] = [...data.features];
                          reqs.splice(i, 1);
                          const newdata = { ...data, features: reqs };
                          setData({ ...newdata });
                        }}
                      >
                        <HiOutlineMinus className="h-6 w-6" />
                      </Button>
                    )}
                  </div>
                ))}
              </div>
            </form>
          </div>
        </Card>
      </div>
    </div>
  );
};

export default Form;
Form.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
