import EButton from '@components/elements/button';
import { Breadcrumb, Button, Table } from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import {
  HiHome,
  HiOutlineDocumentAdd,
  HiPencil,
  HiTrash,
} from 'react-icons/hi';
import { toast } from 'react-toastify';
import Swal from 'sweetalert2';

import { ETable } from '@/components/elements';
import { useActions, useState } from '@/overmind';
import AdminLayout from '@/templates/Admin';

const Blogs = () => {
  const router = useRouter();
  const { getListBlogs, deleteBlog, resetFilter } = useActions();
  const { filter } = useState();
  const [ready, setReady] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [data, setData] = React.useState({} as any);
  const header = [
    { title: '#', field: 'no' },
    { title: 'Title', field: 'title' },
    { title: 'Content', field: 'content' },
    { title: 'Status', field: 'status' },
  ];
  const initData = () => {
    setLoading(true);
    getListBlogs({ ...filter })
      .then((res: any) => {
        setData(res);
      })
      .finally(() => setLoading(false));
  };
  React.useEffect(() => {
    if (ready) {
      initData();
    }
    return () => {};
  }, [filter, ready]);
  React.useEffect(() => {
    resetFilter().then(() => setReady(true));
    return () => {};
  }, []);

  const onDelete = (id: any) => {
    Swal.fire({
      title: 'Are you sure? ',
      text: 'You want to delete this article',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        deleteBlog({ id }).then((res) => {
          toast.success(res.message);
          initData();
        });
      }
    });
  };
  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Blogs</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Blogs</h1>
          <Button
            gradientMonochrome="info"
            onClick={() => {
              router.push({
                pathname: '/admin/blogs/[...id]',
                query: { id: 'add' },
              });
            }}
          >
            <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
            Add new acticle
          </Button>
        </div>
      </div>
      <div className="mx-5 rounded bg-white p-5 shadow-md">
        <ETable
          header={header}
          data={data.items}
          totalData={data.total ?? 0}
          pageCount={10}
          searchText=""
          haveAction
          customTableBody
          renderTableBody={
            <Table.Body className="divide-y">
              {loading && (
                <Table.Row className="bg-white">
                  <Table.Cell colSpan={8}>
                    <div className="flex h-10 w-full items-center justify-center">
                      <div className="animate-pulse rounded-full bg-fuchsia-200 px-3 py-1 text-center text-xs font-medium leading-none text-fuchsia-800">
                        loading...
                      </div>
                    </div>
                  </Table.Cell>
                </Table.Row>
              )}
              {!loading &&
                data?.items?.map((item: any, i: number) => {
                  return (
                    <Table.Row className="bg-white" key={i}>
                      <Table.Cell> {filter.skip + i + 1}</Table.Cell>
                      <Table.Cell>
                        <p className="font-semibold text-purple-700">
                          {item.title}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="">{item.short_content}</p>
                      </Table.Cell>
                      <Table.Cell>
                        {item.status === 'active' && (
                          <span className="rounded bg-emerald-200 px-1 text-emerald-700">
                            {item.status}
                          </span>
                        )}
                        {item.status === 'inactive' && (
                          <span className="rounded bg-red-200 px-1 text-red-700">
                            {item.status}
                          </span>
                        )}
                      </Table.Cell>
                      <Table.Cell width={100}>
                        <EButton
                          onClick={() => {
                            router.push({
                              pathname: '/admin/blogs/[...id]',
                              query: { id: item.slug },
                            });
                          }}
                        >
                          <HiPencil color="#2b6cb0" size={20} />
                        </EButton>
                        <EButton
                          className="ml-3"
                          onClick={() => onDelete(item.id)}
                        >
                          <HiTrash color="gray" size={20} />
                        </EButton>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
            </Table.Body>
          }
        />
      </div>
    </div>
  );
};

export default Blogs;

Blogs.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
