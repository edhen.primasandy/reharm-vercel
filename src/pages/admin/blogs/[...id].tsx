import { Breadcrumb, Button, Card, Label, TextInput } from 'flowbite-react';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import React from 'react';
import { HiHome } from 'react-icons/hi';
import { IoIosSave } from 'react-icons/io';
import { MdOutlineCancel } from 'react-icons/md';
import { toast } from 'react-toastify';

import { InputSelect } from '@/components/elements';
import ImageDropzone from '@/components/elements/image-dropzone';
import { useActions, useState } from '@/overmind';
import AdminLayout from '@/templates/Admin';

const Editor = dynamic(() => import('@components/elements/wysiwyg-editor'), {
  ssr: false,
});

const Blogs = () => {
  const router = useRouter();
  const { getBlog, createBlog, updateBlog } = useActions();
  const { user } = useState();
  const [isBase64, setIsBase64] = React.useState(false);
  const [title, setTitle] = React.useState('Form');
  const [data, setData] = React.useState({
    id: '',
    author: 0,
    title: '',
    content: '',
    media_type: 'image',
    media_url: '',
    status: '',
  });
  const [selectedStatus, setSelectedStatus] = React.useState(null);
  const dataStatus = [
    { value: 'active', label: 'Active' },
    { value: 'inactive', label: 'Inactive' },
  ];
  const [selectedMediaType, setSelectedMediaType] = React.useState({
    value: 'image',
    label: 'Image',
  });
  const dataMediaType = [
    { value: 'image', label: 'Image' },
    { value: 'video', label: 'Video' },
  ];
  React.useEffect(() => {
    if (router.query.id && !router.query.id?.includes('add')) {
      setTitle(`Edit ${router.query.id}`);
      getBlog({ slug: router.query.id.toString() }).then((res) => {
        setData(res);
        const fetchStatus: any = dataStatus.find((o) => o.value === res.status);
        if (fetchStatus) {
          setSelectedStatus(fetchStatus);
        }
        const fetchMediaType: any = dataMediaType.find(
          (o) => o.value === res.media_type
        );
        if (fetchMediaType) {
          setSelectedMediaType(fetchMediaType);
        }
      });
    }

    return () => {};
  }, [router.query.id]);

  const saveData = () => {
    if (data.id) {
      updateBlog({ ...data }).then(() => {
        toast.success('data has been updated');
        router.back();
      });
    } else {
      createBlog({ ...data, author: user.id }).then(() => {
        toast.success('data has been saved');
        router.back();
      });
    }
  };
  return (
    <div className="w-full">
      <Breadcrumb>
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/categories')}
        >
          Categories
        </Breadcrumb.Item>
        <Breadcrumb.Item>Form</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">{title}</h1>
          <div className="flex flex-row gap-3">
            <Button
              color="gray"
              onClick={() => {
                router.back();
              }}
            >
              <MdOutlineCancel className="mr-2 h-5 w-5" />
              Cancel
            </Button>
            <Button
              gradientMonochrome="info"
              onClick={() => {
                saveData();
              }}
            >
              <IoIosSave className="mr-2 h-5 w-5" />
              Save
            </Button>
          </div>
        </div>
        <div className="my-5">
          <Card>
            <form className="grid grid-cols-2 gap-3">
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label htmlFor="title" value="Title*" />
                </div>
                <TextInput
                  className="eTextinput"
                  required
                  value={data.title}
                  id="title"
                  type="text"
                  sizing="md"
                  onChange={(e) => {
                    const newdata = { ...data, title: e.target.value };
                    setData({ ...newdata });
                  }}
                />
              </div>
              <div className="mb-2">
                <InputSelect
                  isClearable
                  label="Status"
                  data={dataStatus}
                  value={selectedStatus}
                  placeholder="Select Status"
                  onChange={(e: any) => {
                    const newdata = {
                      ...data,
                      status: e !== null ? e.value : '',
                    };
                    setSelectedStatus(e);
                    setData({ ...newdata });
                  }}
                />
              </div>
              <div className="mb-2">
                <InputSelect
                  isClearable
                  label="Media Type"
                  data={dataMediaType}
                  value={selectedMediaType}
                  placeholder="Select Status"
                  onChange={(e: any) => {
                    const newdata = {
                      ...data,
                      media_type: e !== null ? e.value : '',
                    };
                    setSelectedMediaType(e);
                    setData({ ...newdata });
                  }}
                />
              </div>
              {data.media_type === 'image' ? (
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="media" value="Media" />
                  </div>
                  <ImageDropzone
                    imgSource={isBase64 ? '' : data.media_url}
                    onChange={(file) => {
                      setData({ ...data, media_url: file });
                      if (file) {
                        setIsBase64(true);
                      }
                    }}
                  />
                </div>
              ) : (
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="media" value="Media" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    required
                    value={data.media_url}
                    id="media"
                    type="text"
                    sizing="md"
                    onChange={(e) => {
                      const newdata = { ...data, media_url: e.target.value };
                      setData({ ...newdata });
                    }}
                  />
                </div>
              )}
              <div className="col-span-2 mb-2">
                <div className="mb-2 block">
                  <Label htmlFor="content" value="Content" />
                  <Editor
                    content={data.content}
                    setContent={(newcontent: any) => {
                      const newdata = { ...data, content: newcontent };
                      setData(newdata);
                    }}
                  />
                </div>
              </div>
            </form>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default Blogs;

Blogs.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
