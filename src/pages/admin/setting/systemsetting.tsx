/* eslint-disable tailwindcss/no-custom-classname */

import ETags from '@components/elements/tags-input';
import ETextInput from '@components/elements/text-input';
import ETextarea from '@components/elements/textarea-input';
// React-wrapper file
import { Breadcrumb, Button, Card, Label } from 'flowbite-react';
import dynamic from 'next/dynamic';
import router from 'next/router';
import React from 'react';
import { HiHome } from 'react-icons/hi';
import { IoIosSave } from 'react-icons/io';
import { toast } from 'react-toastify';

import { useActions } from '@/overmind/index';
import AdminLayout from '@/templates/Admin';

const Editor = dynamic(() => import('@components/elements/wysiwyg-editor'), {
  ssr: false,
});
const Setting = () => {
  const { getsSetting, updateSetting } = useActions();
  const [data, setData] = React.useState({
    system_name: '',
    system_title: '',
    system_email: '',
    website_keywords: '',
    website_description: '',
    slogan: '',
    address: '',
    phone: '',
    allow_instructor: '',
    instructor_revenue: '',
    system_currency: '',
    protocol: '',
    about_us: '',
    terms_and_condition: '',
    privacy_policy: '',
    smtp_host: '',
    smtp_port: '',
    smtp_user: '',
    smtp_pass: '',
    certificate_template: '',
    link_play_store: '',
    link_app_store: '',
    youtube_api_key: '',
  });

  const initData = () => {
    getsSetting({}).then((res) => {
      const datas: any = {};
      res.items.forEach((o: any) => {
        datas[o.key] = o.value;
      });
      setData({ ...datas });
    });
  };
  React.useEffect(() => {
    initData();
    return () => {};
  }, []);
  const saveData = () => {
    updateSetting({ ...data }).then(() =>
      toast.success('data setting is updated')
    );
  };

  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>System setting</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">System setting</h1>
          <div className="flex flex-row gap-3">
            <Button
              gradientMonochrome="info"
              onClick={() => {
                saveData();
              }}
            >
              <IoIosSave className="mr-2 h-5 w-5" />
              Save
            </Button>
          </div>
        </div>
        <Card className="mt-2">
          <form className="grid grid-cols-2 gap-3">
            <ETextInput
              id="name"
              label="Website name"
              value={data.system_name}
              onChange={(e) =>
                setData({ ...data, system_name: e.target.value })
              }
            />
            <ETextInput
              id="title"
              label="Website title"
              value={data.system_title}
              onChange={(e) =>
                setData({ ...data, system_title: e.target.value })
              }
            />
            <div className="mb-2">
              <div className="mb-2 block">
                <Label htmlFor="keywords" value="Website keywords" />
              </div>
              <ETags
                keywords={data.website_keywords}
                changeKeyword={(values) => {
                  setData({ ...data, website_keywords: values });
                }}
              />
            </div>
            <ETextarea
              id="website_description"
              label="Website descrition"
              value={data.website_description}
              onChange={(e) =>
                setData({ ...data, website_description: e.target.value })
              }
            />
            <ETextInput
              id="slogan"
              label="Slogan*"
              value={data.slogan}
              onChange={(e) => setData({ ...data, slogan: e.target.value })}
            />
            <ETextInput
              id="system_email"
              label="System email*"
              value={data.system_email}
              onChange={(e) =>
                setData({ ...data, system_email: e.target.value })
              }
            />
            <ETextarea
              id="address"
              label="Address"
              value={data.address}
              onChange={(e) => setData({ ...data, address: e.target.value })}
            />
            <ETextInput
              id="phone"
              label="Phone"
              value={data.phone}
              onChange={(e) => setData({ ...data, phone: e.target.value })}
            />
            <ETextInput
              id="appstore"
              label="Link app store"
              value={data.link_app_store}
              onChange={(e) =>
                setData({ ...data, link_app_store: e.target.value })
              }
            />
            <ETextInput
              id="playstore"
              label="Link play store"
              value={data.link_play_store}
              onChange={(e) =>
                setData({ ...data, link_play_store: e.target.value })
              }
            />
            <ETextInput
              id="youtube_api_key"
              label="Youtube api key"
              value={data.youtube_api_key}
              onChange={(e) =>
                setData({ ...data, youtube_api_key: e.target.value })
              }
            />
            <div className="col-span-2 mb-2">
              <div className="mb-2 block">
                <Label htmlFor="about_us" value="About us" />
              </div>
              <Editor
                content={data.about_us}
                setContent={(newcontent: any) => {
                  setData({ ...data, about_us: newcontent });
                }}
              />
            </div>
            <div className="col-span-2 mb-2">
              <div className="mb-2 block">
                <Label htmlFor="privacy_policy" value="Privacy & policy" />
              </div>
              <Editor
                content={data.privacy_policy}
                setContent={(newcontent: any) => {
                  setData({ ...data, privacy_policy: newcontent });
                }}
              />
            </div>
            <div className="col-span-2 mb-2">
              <div className="mb-2 block">
                <Label
                  htmlFor="terms_and_condition"
                  value="Terms & condition"
                />
              </div>
              <Editor
                content={data.terms_and_condition}
                setContent={(newcontent: any) => {
                  setData({ ...data, terms_and_condition: newcontent });
                }}
              />
            </div>
          </form>
        </Card>
      </div>
    </div>
  );
};

export default Setting;

Setting.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
