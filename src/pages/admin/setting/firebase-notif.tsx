/* eslint-disable tailwindcss/no-custom-classname */

import ETextInput from '@components/elements/text-input';
import ETextarea from '@components/elements/textarea-input';
// React-wrapper file
import { Breadcrumb, Button, Card } from 'flowbite-react';
import router from 'next/router';
import React from 'react';
import { HiHome } from 'react-icons/hi';
import { IoIosSave } from 'react-icons/io';
import { toast } from 'react-toastify';

import { useActions, useState } from '@/overmind/index';
import AdminLayout from '@/templates/Admin';

const Courses = () => {
  const { user } = useState();
  const { sendNotification } = useActions();
  const [data, setData] = React.useState({
    title: '',
    body: '',
  });
  const submit = () => {
    sendNotification({
      ...data,
      user_id: user.id,
    })
      .then(() => {
        toast.success('notification is sended');
      })
      .catch((e) => toast.error(e.message));
  };

  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Firebase notification</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Firebase notification</h1>
          <div className="flex flex-row gap-3">
            <Button
              gradientMonochrome="info"
              onClick={() => {
                submit();
              }}
            >
              <IoIosSave className="mr-2 h-5 w-5" />
              Submit
            </Button>
          </div>
        </div>
        <Card className="mt-2">
          <form className="grid grid-cols-2 gap-3">
            <ETextInput
              id="title"
              label="Title"
              value={data.title}
              onChange={(e) => setData({ ...data, title: e.target.value })}
            />
            <ETextarea
              id="body"
              label="Body"
              value={data.body}
              onChange={(e) => setData({ ...data, body: e.target.value })}
            />
          </form>
        </Card>
      </div>
    </div>
  );
};

export default Courses;

Courses.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
