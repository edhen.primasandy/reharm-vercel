import ButtonComp from '@components/elements/button';
import ETable from '@components/elements/table';
import helper from '@utils/helper';
import { Breadcrumb, Button, Modal, Table } from 'flowbite-react';
import router from 'next/router';
import React from 'react';
import { FaFileDownload, FaFileUpload } from 'react-icons/fa';
import { HiHome, HiTrash } from 'react-icons/hi';
import { toast } from 'react-toastify';
import Swal from 'sweetalert2';

import { useActions, useState } from '@/overmind/index';
import AdminLayout from '@/templates/Admin';

const Layout = () => {
  const {
    getListInstrument,
    deleteInstrument,
    getInstrumentCategories,
    importInstrument,
    downloadTemplateInstrument,
    resetFilter,
  } = useActions();

  const { filter } = useState();
  const [loading, setLoading] = React.useState(false);
  const [ready, setReady] = React.useState(false);
  const [dataCategory, setDataCategory] = React.useState([] as any);
  const [data, setData] = React.useState({} as any);
  const [showModal, setShowModal] = React.useState(false);
  const [file, setFile] = React.useState(null as any);

  const header = [
    { title: '#', field: 'no' },
    { title: 'Title', field: 'title' },
    { title: 'Category', field: 'category_id' },
    { title: 'Type', field: 'type' },
    { title: 'Link', field: 'tokopedia_url' },
    { title: 'Price', field: 'price' },
    { title: 'Discount', field: 'discounted_price' },
  ];
  const initData = () => {
    getInstrumentCategories({}).then((res: any) => {
      const cats = res.items.reduce((map: any, obj: any) => {
        // eslint-disable-next-line no-param-reassign
        map[obj.id] = obj.name;
        return map;
      }, {});
      setDataCategory(cats);
    });
    setLoading(true);
    getListInstrument({ ...filter })
      .then((res: any) => {
        setData(res);
      })
      .finally(() => setLoading(false));
  };
  React.useEffect(() => {
    if (ready) {
      initData();
    }
    return () => {};
  }, [filter, ready]);
  React.useEffect(() => {
    resetFilter().then(() => setReady(true));
    return () => {};
  }, []);

  const onDelete = (id: any) => {
    Swal.fire({
      title: 'Are you sure? ',
      text: 'You want to delete this data',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        deleteInstrument({ id }).then((res) => {
          toast.success(res.message);
          initData();
        });
      }
    });
  };

  const handleFileChange = (e: any) => {
    e.preventDefault();
    if (e.target.files) {
      setFile(e.target.files[0]);
    }
  };
  const inputRef = React.useRef(null as any);
  const resetFile = () => {
    setFile(null);
    inputRef.current.value = null;
  };
  const [dragActive, setDragActive] = React.useState(false);
  const handleDrag = (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    if (e.type === 'dragenter' || e.type === 'dragover') {
      setDragActive(true);
    } else if (e.type === 'dragleave') {
      setDragActive(false);
    }
  };

  // triggers when file is dropped
  const handleDrop = (e: any) => {
    e.preventDefault();
    e.stopPropagation();
    setDragActive(false);
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      setFile(e.target.files[0]);
    }
  };
  const onSubmitUpload = () => {
    if (file !== null) {
      const formData = new FormData();
      formData.append('file', file);
      importInstrument(formData)
        .then((res: any) => {
          console.log(res);
          toast.success(res.message);
        })
        .catch((e) => {
          toast.error(e.message);
        })
        .finally(() => {
          resetFile();
          setShowModal(false);
        });
    } else {
      toast.error('No file uploaded');
    }
  };
  const downloadTemplate = () => {
    downloadTemplateInstrument().then((res) => {
      // const url = window.URL.createObjectURL(new Blob([res]));
      const link = document.createElement('a');
      link.href = res.data;
      link.setAttribute('download', 'Import-Instruments-Template.xlsx');
      document.body.appendChild(link);
      link.click();
    });
  };
  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Instruments</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Instruments</h1>
          <div className="flex flex-row gap-3">
            <Button
              gradientMonochrome="success"
              onClick={() => setShowModal(true)}
            >
              <FaFileUpload className="mr-2 h-5 w-5" />
              Import data
            </Button>
            <Button
              gradientMonochrome="pink"
              onClick={() => downloadTemplate()}
            >
              <FaFileDownload className="mr-2 h-5 w-5" />
              Download template
            </Button>
          </div>
        </div>
      </div>
      <div className="mx-5 rounded bg-white p-5 shadow-md">
        <ETable
          header={header}
          data={data.items}
          totalData={data.total ?? 0}
          pageCount={10}
          searchText=""
          haveAction
          customTableBody
          renderTableBody={
            <Table.Body className="divide-y">
              {loading && (
                <Table.Row className="bg-white">
                  <Table.Cell colSpan={8}>
                    <div className="flex h-10 w-full items-center justify-center">
                      <div className="animate-pulse rounded-full bg-fuchsia-200 px-3 py-1 text-center text-xs font-medium leading-none text-fuchsia-800">
                        loading...
                      </div>
                    </div>
                  </Table.Cell>
                </Table.Row>
              )}
              {!loading &&
                data?.items?.map((item: any, i: number) => {
                  return (
                    <Table.Row className="bg-white" key={i}>
                      <Table.Cell> {filter.skip + i + 1}</Table.Cell>
                      <Table.Cell>
                        <p className="font-semibold text-purple-700">
                          {item.title}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">
                          {dataCategory[item.category_id]}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">{item.type}</p>
                      </Table.Cell>
                      <Table.Cell>
                        <a
                          target="_blank"
                          href={item.tokopedia_url}
                          rel="noreferrer"
                          className="break-all"
                        >
                          {item.tokopedia_url}
                        </a>
                      </Table.Cell>
                      <Table.Cell>
                        <span className="rounded bg-gray-200 px-1 text-gray-700">
                          {helper.currencyFormat(item.price)}
                        </span>
                      </Table.Cell>
                      <Table.Cell>
                        <span className="rounded bg-gray-200 px-1 text-gray-700">
                          {helper.currencyFormat(item.discounted_price)}
                        </span>
                      </Table.Cell>
                      <Table.Cell width={100}>
                        <ButtonComp
                          className="ml-3"
                          onClick={() => onDelete(item.id)}
                        >
                          <HiTrash color="gray" size={20} />
                        </ButtonComp>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
            </Table.Body>
          }
        />
      </div>
      <Modal
        onClose={() => {
          resetFile();
          setShowModal(false);
        }}
        popup
        size="md"
        show={showModal}
      >
        <Modal.Header />
        <Modal.Body>
          <div className="text-center">
            <div className="mb-5 flex w-full items-center justify-center">
              <label className="flex h-64 w-full cursor-pointer flex-col items-center justify-center rounded-lg border-2 border-dashed border-gray-300 bg-gray-50 hover:bg-gray-100 dark:border-gray-600 dark:bg-gray-700 dark:hover:border-gray-500 dark:hover:bg-gray-600">
                <div className="flex flex-col items-center justify-center pt-5 pb-6">
                  <svg
                    aria-hidden="true"
                    className="mb-3 h-10 w-10 text-gray-400"
                    fill="none"
                    stroke="currentColor"
                    viewBox="0 0 24 24"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      strokeWidth="2"
                      d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12"
                    ></path>
                  </svg>
                  <p className="mb-2 text-sm text-gray-500">
                    <span className="font-semibold">Click to upload</span>
                  </p>
                </div>
                <input
                  ref={inputRef}
                  id="dropzone-file"
                  type="file"
                  className="hidden"
                  accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
                  onChange={handleFileChange}
                />
                {file !== null && (
                  <p className="text-xs text-violet-600">{file?.name}</p>
                )}
              </label>
              {dragActive && (
                <div
                  id="drag-file-element"
                  onDragEnter={handleDrag}
                  onDragLeave={handleDrag}
                  onDragOver={handleDrag}
                  onDrop={handleDrop}
                ></div>
              )}
            </div>

            <div className="flex justify-center gap-4">
              <Button
                color="info"
                onClick={() => {
                  onSubmitUpload();
                }}
              >
                Submit
              </Button>
              <Button
                color="gray"
                onClick={() => {
                  resetFile();
                  setShowModal(false);
                }}
              >
                No, cancel
              </Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default Layout;

Layout.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
