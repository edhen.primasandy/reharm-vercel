import ButtonComp from '@components/elements/button';
import ETable from '@components/elements/table';
import UserImage from '@images/user-img-default.png';
import { Breadcrumb, Button, Label, Table, TextInput } from 'flowbite-react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';
import {
  HiHome,
  HiOutlineDocumentAdd,
  HiOutlineFilter,
  HiPencil,
  HiTrash,
} from 'react-icons/hi';
import { toast } from 'react-toastify';
import Swal from 'sweetalert2';

import { InputSelect } from '@/components/elements';
import { useActions, useState } from '@/overmind';
import AdminLayout from '@/templates/Admin';

const Categories = () => {
  const router = useRouter();
  const { getsUsers, deleteUser, resetFilter } = useActions();
  const { filter } = useState();
  const [loading, setLoading] = React.useState(false);
  const [ready, setReady] = React.useState(false);
  const [data, setData] = React.useState({} as any);
  const [keyword, setKeyword] = React.useState('');
  const [filterRole, setFilterRole] = React.useState('all');
  const dataRoles = [
    { value: 'all', label: 'All' },
    { value: 'user', label: 'User' },
    { value: 'admin', label: 'Admin' },
    { value: 'instructor', label: 'Instructor' },
    { value: 'editor', label: 'Editor' },
  ];
  const header = [
    { title: '#', field: 'no' },
    { title: 'Photo', field: 'photo' },
    { title: 'Name', field: 'name' },
    { title: 'Email', field: 'email' },
    { title: 'Role', field: 'role' },
    { title: 'Status', field: 'status' },
  ];
  const initData = () => {
    setLoading(true);
    getsUsers({
      skip: filter.skip,
      limit: filter.limit,
      role: filterRole !== 'all' ? filterRole : '',
      keyword,
    })
      .then((res) => setData(res))
      .finally(() => setLoading(false));
  };
  React.useEffect(() => {
    if (ready) {
      initData();
    }
    return () => {};
  }, [filter, ready]);
  React.useEffect(() => {
    resetFilter().then(() => setReady(true));
    return () => {};
  }, []);
  const onDelete = (id: any) => {
    Swal.fire({
      title: 'Are you sure? ',
      text: 'You want to delete this user',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        deleteUser({ id }).then(() => {
          initData();
          toast.success('User is deleted');
        });
      }
    });
  };
  const onClickFilter = () => {
    resetFilter();
  };
  return (
    <div className="w-full">
      <Breadcrumb aria-label="Default breadcrumb example">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Users</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Users</h1>
          <Button
            gradientMonochrome="info"
            onClick={() => {
              router.push({
                pathname: '/admin/users/[...id]',
                query: { id: 'add' },
              });
            }}
          >
            <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
            Add Users
          </Button>
        </div>
      </div>
      <div className="mx-5">
        <div className="mb-5 flex flex-row justify-end gap-4">
          <div>
            <div className="mb-2 block">
              <Label htmlFor="first_name" value="Search" />
            </div>
            <TextInput
              required
              className="eTextinput"
              value={keyword}
              placeholder="Search"
              id="search"
              type="text"
              sizing="md"
              onChange={(e) => setKeyword(e.target.value)}
            />
          </div>
          <div className="w-52">
            <InputSelect
              isClearable
              label="Role"
              data={dataRoles}
              value={filterRole}
              placeholder="Select role"
              onChange={(e: any) => {
                setFilterRole(e?.value ?? '');
              }}
            />
          </div>
          <div className="flex items-end">
            <Button
              gradientMonochrome="pink"
              onClick={() => {
                onClickFilter();
              }}
            >
              <HiOutlineFilter className="mr-2 h-5 w-5" />
              Filter
            </Button>
          </div>
        </div>
        <ETable
          header={header}
          data={data.items}
          totalData={data.total ?? 0}
          pageCount={10}
          searchText=""
          haveAction
          customTableBody
          renderTableBody={
            <Table.Body className="divide-y">
              {loading && (
                <Table.Row className="bg-white">
                  <Table.Cell colSpan={8}>
                    <div className="flex h-10 w-full items-center justify-center">
                      <div className="animate-pulse rounded-full bg-fuchsia-200 px-3 py-1 text-center text-xs font-medium leading-none text-fuchsia-800">
                        loading...
                      </div>
                    </div>
                  </Table.Cell>
                </Table.Row>
              )}
              {!loading &&
                data?.items?.map((item: any, i: number) => {
                  return (
                    <Table.Row className="bg-white" key={i}>
                      <Table.Cell> {filter.skip + i + 1}</Table.Cell>
                      <Table.Cell>
                        <div>
                          {item.photo ? (
                            <img
                              className="h-6 w-6 rounded-full shadow-lg"
                              src={item.photo}
                              alt="user photo"
                            />
                          ) : (
                            <Image
                              className="h-6 w-6 rounded-full shadow-lg"
                              src={UserImage}
                              alt={''}
                            />
                          )}
                        </div>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">
                          {item.first_name} {item.last_name}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">{item.email}</p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">{item.role}</p>
                      </Table.Cell>
                      <Table.Cell>
                        {item.status === 'active' && (
                          <span className="rounded bg-emerald-200 px-1 text-emerald-700">
                            {item.status}
                          </span>
                        )}
                        {item.status === 'inactive' && (
                          <span className="rounded bg-red-200 px-1 text-red-700">
                            {item.status}
                          </span>
                        )}
                      </Table.Cell>
                      <Table.Cell width={100}>
                        <ButtonComp
                          onClick={() => {
                            router.push({
                              pathname: '/admin/users/[...id]',
                              query: { id: item.id },
                            });
                          }}
                        >
                          <HiPencil color="#2b6cb0" size={20} />
                        </ButtonComp>
                        <ButtonComp
                          className="ml-3"
                          onClick={() => onDelete(item.id)}
                        >
                          <HiTrash color="gray" size={20} />
                        </ButtonComp>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
            </Table.Body>
          }
        />
      </div>
    </div>
  );
};

export default Categories;

Categories.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
