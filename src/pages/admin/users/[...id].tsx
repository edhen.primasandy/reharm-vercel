/* eslint-disable tailwindcss/no-custom-classname */
import {
  Breadcrumb,
  Button,
  Card,
  Label,
  Tabs,
  Textarea,
  TextInput,
} from 'flowbite-react';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import React from 'react';
import { HiHome, HiUserCircle } from 'react-icons/hi';
import { IoIosSave, IoMdUnlock } from 'react-icons/io';
import { MdOutlineCancel } from 'react-icons/md';
import { TbSocial } from 'react-icons/tb';
import { toast } from 'react-toastify';

import { InputSelect } from '@/components/elements';
import ImageDropzone from '@/components/elements/image-dropzone';
import { useActions } from '@/overmind';
import AdminLayout from '@/templates/Admin';

const Editor = dynamic(() => import('@components/elements/wysiwyg-editor'), {
  ssr: false,
});
const Form = () => {
  const router = useRouter();
  const dataRoles = [
    { value: 'user', label: 'User' },
    { value: 'admin', label: 'Admin' },
    { value: 'instructor', label: 'Instructor' },
    { value: 'editor', label: 'Editor' },
  ];
  const {
    getUser,
    createUser,
    updateUser,
    uploadPhotoUser,
    getListProvince,
    getListRegency,
    getListDistrict,
    getListVilage,
  } = useActions();
  const [title, setTitle] = React.useState('Form');
  const [dataProvince, setDataProvince] = React.useState([] as any);
  const [dataRegency, setDataRegency] = React.useState([] as any);
  const [dataDistrict, setDataDistrict] = React.useState([] as any);
  const [dataVilage, setDataVilage] = React.useState([] as any);
  const [isUpdatedPhoto, setIsUpdatedPhoto] = React.useState(false);

  const [data, setData] = React.useState({
    id: '',
    first_name: '',
    last_name: '',
    biography: '',
    photo: '',
    email: '',
    social_links: {
      facebook: '',
      twitter: '',
      linkedin: '',
    },
    role: '',
    password: '',
    address: '',
    province_id: 0,
    regency_id: 0,
    district_id: 0,
    village_id: 0,
  });
  const [isBase64, setIsBase64] = React.useState(false);
  const init = () => {
    getListProvince({}).then((res) => {
      const datas: any[] = [];
      res.items.forEach((o: any) => {
        datas.push({
          value: o.id,
          label: o.name,
        });
      });
      setDataProvince(datas);
    });
    getListRegency({}).then((res) => {
      const datas: any[] = [];
      res.items.forEach((o: any) => {
        datas.push({
          value: o.id,
          label: o.name,
        });
      });
      setDataRegency(datas);
    });
  };
  const getDistrict = () => {
    getListDistrict({ regencies_id: data.regency_id }).then((res) => {
      const datas: any[] = [];
      res.items.forEach((o: any) => {
        datas.push({
          value: o.id,
          label: o.name,
        });
      });
      setDataDistrict(datas);
    });
  };
  const getVilage = () => {
    getListVilage({ districts_id: data.district_id }).then((res) => {
      const datas: any[] = [];
      res.items.forEach((o: any) => {
        datas.push({
          value: o.id,
          label: o.name,
        });
      });
      setDataVilage(datas);
    });
  };
  React.useEffect(() => {
    init();
    if (router.query?.id && !router.query.id?.includes('add')) {
      getUser({ id: router.query.id.toString() }).then((res) => {
        setTitle(`Edit ${res.first_name} ${res.last_name}`);
        setData(res);
      });
    }

    return () => {};
  }, [router.query?.id]);
  const matchDataRegency = (prov_id: any) => {
    getListRegency({ provinces_id: prov_id }).then((res) => {
      const datas: any[] = [];
      res.items.forEach((o: any) => {
        datas.push({
          value: o.id,
          label: o.name,
        });
      });
      setDataRegency(datas);
    });
  };
  const saveData = () => {
    if (data.id) {
      updateUser({ ...data }).then(() => {
        toast.success('data has been updated');
        router.back();
      });
    } else {
      createUser(data).then((res: any) => {
        if (isUpdatedPhoto) {
          uploadPhotoUser({
            photo: data?.photo,
            id: res.id,
          })
            .then(() => {
              toast.success('data has been saved');
              router.back();
            })
            .catch((e) => toast.error(e.message));
        } else {
          toast.success('data has been saved');
          router.back();
        }
      });
    }
  };
  return (
    <div className="w-full">
      <Breadcrumb>
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item href="#" onClick={() => router.push('/admin/users')}>
          User
        </Breadcrumb.Item>
        <Breadcrumb.Item>Form</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">{title}</h1>
          <div className="flex flex-row gap-3">
            <Button
              color="gray"
              onClick={() => {
                router.back();
              }}
            >
              <MdOutlineCancel className="mr-2 h-5 w-5" />
              Cancel
            </Button>
            <Button
              gradientMonochrome="info"
              onClick={() => {
                saveData();
              }}
            >
              <IoIosSave className="mr-2 h-5 w-5" />
              Save
            </Button>
          </div>
        </div>
        <Card className="mt-2">
          <Tabs.Group aria-label="Tabs with icons" style="underline">
            <Tabs.Item title="Basic Info" icon={HiUserCircle}>
              <form className="grid grid-cols-2 gap-3">
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="first_name" value="First name*" />
                  </div>
                  <TextInput
                    required
                    className="eTextinput"
                    value={data.first_name}
                    placeholder="First name"
                    id="first_name"
                    type="text"
                    sizing="md"
                    onChange={(e) =>
                      setData({ ...data, first_name: e.target.value })
                    }
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="last_name" value="Last name*" />
                  </div>
                  <TextInput
                    required
                    className="eTextinput"
                    placeholder="Last name"
                    value={data.last_name}
                    id="last_name"
                    type="text"
                    sizing="md"
                    onChange={(e) =>
                      setData({ ...data, last_name: e.target.value })
                    }
                  />
                </div>
                <div className="mb-2">
                  <InputSelect
                    isClearable
                    required
                    label="Provinsi"
                    data={dataProvince}
                    value={data.province_id}
                    placeholder="Select Provinsi"
                    onChange={(e: any) => {
                      const newdata = {
                        ...data,
                        province_id: e !== null ? e.value : 0,
                      };
                      if (e === null) {
                        newdata.regency_id = 0;
                        newdata.district_id = 0;
                        newdata.village_id = 0;
                      }
                      matchDataRegency(e !== null ? e.value : 0);
                      setData(newdata);
                    }}
                  />
                </div>
                <div className="mb-2">
                  <InputSelect
                    isClearable
                    required
                    label="Kabupaten"
                    data={dataRegency}
                    value={data.regency_id}
                    placeholder="Select Kabupaten"
                    onChange={(e: any) => {
                      const newdata = {
                        ...data,
                        regency_id: e !== null ? e.value : 0,
                      };
                      setData(newdata);
                    }}
                  />
                </div>
                <div className="mb-2">
                  <InputSelect
                    isClearable
                    onFocus={() => {
                      if (data.regency_id !== 0 || data.regency_id !== null) {
                        getDistrict();
                      }
                    }}
                    required
                    label="Kecamatan"
                    data={dataDistrict}
                    value={data.district_id}
                    placeholder="Select Kecamatan"
                    onChange={(e: any) => {
                      const newdata = {
                        ...data,
                        district_id: e !== null ? e.value : 0,
                      };
                      setData(newdata);
                    }}
                  />
                </div>
                <div className="mb-2">
                  <InputSelect
                    isClearable
                    onFocus={() => {
                      if (data.district_id !== 0 || data.district_id !== null) {
                        getVilage();
                      }
                    }}
                    required
                    label="Kelurahan"
                    data={dataVilage}
                    value={data.village_id}
                    placeholder="Select Kelurahan"
                    onChange={(e: any) => {
                      const newdata = {
                        ...data,
                        village_id: e !== null ? e.value : 0,
                      };
                      setData(newdata);
                    }}
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="address" value="Address" />
                  </div>
                  <Textarea
                    className="eTextarea"
                    value={data.address}
                    placeholder="Addresss..."
                    rows={5}
                    id="address"
                    onChange={(e) =>
                      setData({ ...data, address: e.target.value })
                    }
                  />
                </div>
                <div className="mb-2">
                  <InputSelect
                    isClearable
                    required
                    label="Role*"
                    data={dataRoles}
                    value={data.role}
                    placeholder="Select role"
                    onChange={(e: any) => {
                      const newdata = {
                        ...data,
                        role: e !== null ? e.value : 0,
                      };
                      setData(newdata);
                    }}
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <div className="mb-2 block">
                      <Label htmlFor="biography" value="Biography" />
                    </div>
                    <Editor
                      content={data.biography}
                      setContent={(newcontent: any) => {
                        const newdata = { ...data, biography: newcontent };
                        setData(newdata);
                      }}
                    />
                  </div>
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="photo" value="Upload photo" />
                  </div>
                  <ImageDropzone
                    imgSource={isBase64 ? '' : data?.photo}
                    onChange={(file) => {
                      if (file) {
                        setData({ ...data, photo: file });
                        setIsBase64(true);
                        setIsUpdatedPhoto(true);
                      } else {
                        setData({ ...data, photo: '' });
                      }
                    }}
                  />
                </div>
              </form>
            </Tabs.Item>
            <Tabs.Item title="Login credential" icon={IoMdUnlock}>
              <form className="grid grid-cols-2 gap-3">
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="email" value="Email*" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.email}
                    id="email"
                    type="email"
                    sizing="md"
                    onChange={(e) =>
                      setData({ ...data, email: e.target.value })
                    }
                  />
                </div>
                {/* <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="password" value="Password*" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.password}
                    id="password"
                    type="password"
                    sizing="md"
                    onChange={(e) =>
                      setData({ ...data, password: e.target.value })
                    }
                  />
                </div> */}
              </form>
            </Tabs.Item>
            <Tabs.Item title="Social information" icon={TbSocial}>
              <form className="grid grid-cols-2 gap-3">
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="facebook" value="Facebook" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.social_links?.facebook}
                    id="facebook"
                    type="text"
                    sizing="md"
                    onChange={(e) =>
                      setData({
                        ...data,
                        social_links: {
                          ...data.social_links,
                          facebook: e.target.value,
                        },
                      })
                    }
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="twiter" value="Twiter" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.social_links?.twitter}
                    id="twiter"
                    type="text"
                    sizing="md"
                    onChange={(e) =>
                      setData({
                        ...data,
                        social_links: {
                          ...data.social_links,
                          twitter: e.target.value,
                        },
                      })
                    }
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="linkedin" value="Linked In" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.social_links?.linkedin}
                    id="linkedin"
                    type="text"
                    sizing="md"
                    onChange={(e) =>
                      setData({
                        ...data,
                        social_links: {
                          ...data.social_links,
                          linkedin: e.target.value,
                        },
                      })
                    }
                  />
                </div>
              </form>
            </Tabs.Item>
          </Tabs.Group>
        </Card>
      </div>
    </div>
  );
};

export default Form;
Form.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
