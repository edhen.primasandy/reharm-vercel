/* eslint-disable no-plusplus */
import {
  CategoryScale,
  Chart as ChartJS,
  Filler,
  Legend,
  LinearScale,
  LineElement,
  PointElement,
  Title,
  Tooltip,
} from 'chart.js';
import { Breadcrumb } from 'flowbite-react';
import router from 'next/router';
import React, { useRef } from 'react';
import { Line } from 'react-chartjs-2';
import { BiArchive } from 'react-icons/bi';
import { FaNetworkWired, FaUsers, FaVideo } from 'react-icons/fa';
import { HiHome } from 'react-icons/hi';

import { InputSelect } from '@/components/elements';
import { useActions, useState } from '@/overmind/index';
import AdminLayout from '@/templates/Admin';

const AdminDashboad = () => {
  const { getsDashboard } = useActions();
  const { sidebar } = useState();
  const [optionYears, setOptionYears] = React.useState(
    [] as { label: number; value: number }[]
  );
  const [selectedYear, setSelectedYear] = React.useState({
    label: new Date().getFullYear(),
    value: new Date().getFullYear(),
  });
  const [data, setData] = React.useState({
    chart: {},
    number_courses: 0,
    number_lessons: 0,
    number_enrols: 0,
    number_users: 0,
  });
  const [loading, setLoading] = React.useState({} as any);
  const [dataChart, setDataChart] = React.useState({
    labels: [],
    datasets: [] as any,
  });
  const chartRef = useRef(null as any);

  const resizeChart = () => {
    if (chartRef.current && chartRef.current.chart) {
      chartRef.current.chart.update();
    }
  };

  React.useEffect(() => {
    resizeChart();
  }, [sidebar]);

  let width: any;
  let height: any;
  let gradient: any;
  const getGradient = (ctx: any, chartArea: any) => {
    const chartWidth = chartArea.right - chartArea.left;
    const chartHeight = chartArea.bottom - chartArea.top;
    if (!gradient || width !== chartWidth || height !== chartHeight) {
      // Create the gradient because this is either the first render
      // or the size of the chart has changed
      width = chartWidth;
      height = chartHeight;
      gradient = ctx.createLinearGradient(
        0,
        chartArea.bottom,
        0,
        chartArea.top
      );
      gradient.addColorStop(1, 'rgba(149, 76, 233, 0.5)');
      gradient.addColorStop(0.35, 'rgba(149, 76, 233, 0.25)');
      gradient.addColorStop(0, 'rgba(149, 76, 233, 0)');
    }

    return gradient;
  };
  const generateYears = () => {
    const currentYear = new Date().getFullYear();
    const years: { label: number; value: number }[] = [];
    const startYear = 2020;
    for (let i = currentYear; i >= startYear; i--) {
      years.push({
        label: i,
        value: i,
      });
    }
    // while (startYear <= currentYear) {
    //   years.push({
    //     label: startYear + 1,
    //     value: startYear + 1,
    //   });
    //   startYear++;
    // }
    setOptionYears(years);
  };
  React.useEffect(() => {
    generateYears();
    return () => {};
  }, []);
  React.useEffect(() => {
    setLoading(true);

    getsDashboard({ year: selectedYear.value })
      .then((res) => {
        setData(res.data);
        setDataChart({
          labels: res.data.chart.categories,
          datasets: [
            {
              fill: true,
              label: 'Revenue',
              data: res.data.chart.series,
              borderColor: 'rgba(149, 76, 233, 1)',
              pointBackgroundColor: 'rgba(149, 76, 233, 1)',
              backgroundColor: (context: any) => {
                const { chart } = context;
                const { ctx, chartArea } = chart;
                if (!chartArea) {
                  // This case happens on initial chart load
                  return;
                }
                // eslint-disable-next-line consistent-return
                return getGradient(ctx, chartArea);
              },
              lineTension: 0.2,
            },
          ],
        });
      })
      .finally(() => setLoading(false));

    return () => {};
  }, [selectedYear]);

  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Filler,
    Legend
  );
  const options = {
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      legend: {
        position: 'bottom' as const,
      },
      title: {
        display: false,
      },
      tooltip: {
        callbacks: {
          label(context: any) {
            let label = context.dataset.label || '';
            if (label) {
              label += ': ';
            }
            if (context.parsed.y !== null) {
              label += new Intl.NumberFormat('id-ID', {
                style: 'currency',
                currency: 'IDR',
              }).format(context.parsed.y);
            }
            return label;
          },
        },
      },
    },
    scales: {
      y: {
        ticks: {
          // Include a dollar sign in the ticks
          callback(value: any) {
            const newValue = new Intl.NumberFormat('id-ID', {
              style: 'currency',
              currency: 'IDR',
            }).format(value);
            return newValue;
          },
        },
      },
    },
  };
  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Dashboard</h1>
          <InputSelect
            label=""
            data={optionYears}
            value={selectedYear}
            placeholder="Select..."
            onChange={(e: any) => {
              setSelectedYear(e);
            }}
          />
        </div>
      </div>
      <div className="mx-5 mb-5">
        <div className="-mx-3 flex flex-wrap">
          <div className="mb-6 w-full max-w-full px-3 sm:w-1/2 sm:flex-none xl:mb-0 xl:w-1/4">
            <div className="relative flex min-w-0 flex-col break-words rounded-xl bg-white bg-clip-border shadow-md">
              <div className="flex-auto p-4">
                <div className="-mx-3 flex flex-row">
                  <div className="w-2/3 max-w-full flex-none px-3">
                    <div>
                      <p className="mb-0 font-sans text-sm font-semibold leading-normal">
                        Total courses
                      </p>
                      <h5 className="mb-0 text-2xl font-bold">
                        {data.number_courses}
                      </h5>
                    </div>
                  </div>
                  <div className="basis-1/3 px-3 text-right">
                    <div className="inline-block h-12 w-12 rounded-lg bg-gradient-to-tl from-purple-700 to-pink-500 text-center">
                      <div className="relative flex h-full items-center justify-center leading-none text-white">
                        <BiArchive size={30} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="mb-6 w-full max-w-full px-3 sm:w-1/2 sm:flex-none xl:mb-0 xl:w-1/4">
            <div className="relative flex min-w-0 flex-col break-words rounded-xl bg-white bg-clip-border shadow-md">
              <div className="flex-auto p-4">
                <div className="-mx-3 flex flex-row">
                  <div className="w-2/3 max-w-full flex-none px-3">
                    <div>
                      <p className="mb-0 font-sans text-sm font-semibold leading-normal">
                        Total lesson
                      </p>
                      <h5 className="mb-0 text-2xl font-bold">
                        {data.number_lessons}
                      </h5>
                    </div>
                  </div>
                  <div className="basis-1/3 px-3 text-right">
                    <div className="inline-block h-12 w-12 rounded-lg bg-gradient-to-tl from-purple-700 to-pink-500 text-center">
                      <div className="relative flex h-full items-center justify-center leading-none text-white">
                        <FaVideo size={30} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="mb-6 w-full max-w-full px-3 sm:w-1/2 sm:flex-none xl:mb-0 xl:w-1/4">
            <div className="relative flex min-w-0 flex-col break-words rounded-xl bg-white bg-clip-border shadow-md">
              <div className="flex-auto p-4">
                <div className="-mx-3 flex flex-row">
                  <div className="w-2/3 max-w-full flex-none px-3">
                    <div>
                      <p className="mb-0 font-sans text-sm font-semibold leading-normal">
                        Total enrolment
                      </p>
                      <h5 className="mb-0 text-2xl font-bold">
                        {data.number_enrols}
                      </h5>
                    </div>
                  </div>
                  <div className="basis-1/3 px-3 text-right">
                    <div className="inline-block h-12 w-12 rounded-lg bg-gradient-to-tl from-purple-700 to-pink-500 text-center">
                      <div className="relative flex h-full items-center justify-center leading-none text-white">
                        <FaNetworkWired size={30} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="w-full max-w-full px-3 sm:w-1/2 sm:flex-none xl:w-1/4">
            <div className="relative flex min-w-0 flex-col break-words rounded-xl bg-white bg-clip-border shadow-md">
              <div className="flex-auto p-4">
                <div className="-mx-3 flex flex-row">
                  <div className="w-2/3 max-w-full flex-none px-3">
                    <div>
                      <p className="mb-0 font-sans text-sm font-semibold leading-normal">
                        Total student
                      </p>
                      <h5 className="mb-0 text-2xl font-bold">
                        {data.number_users}
                      </h5>
                    </div>
                  </div>
                  <div className="basis-1/3 px-3 text-right">
                    <div className="inline-block h-12 w-12 rounded-lg bg-gradient-to-tl from-purple-700 to-pink-500 text-center">
                      <div className="relative flex h-full items-center justify-center leading-none text-white">
                        <FaUsers size={30} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mx-5 rounded-xl bg-white p-5 shadow-md">
        <div className="mb-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Transaction Volume</h1>
        </div>
        {!loading && (
          <div className="h-[400px]">
            <Line
              ref={chartRef}
              options={options}
              data={dataChart}
              height={'100%'}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default AdminDashboad;

AdminDashboad.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
