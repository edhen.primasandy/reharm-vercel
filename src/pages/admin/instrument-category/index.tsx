import EButton from '@components/elements/button';
import {
  Breadcrumb,
  Button,
  Label,
  Modal,
  Table,
  TextInput,
} from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import {
  HiHome,
  HiOutlineDocumentAdd,
  HiPencil,
  HiTrash,
} from 'react-icons/hi';
import { toast } from 'react-toastify';
import Swal from 'sweetalert2';

import { ETable } from '@/components/elements';
import { useActions, useState } from '@/overmind';
import AdminLayout from '@/templates/Admin';

const Layout = () => {
  const router = useRouter();
  const {
    getInstrumentCategories,
    deleteInstrumentCategory,
    createInstrumentCategory,
    updateInstrumentCategory,
    resetFilter,
  } = useActions();
  const { filter } = useState();
  const [loading, setLoading] = React.useState(false);
  const [ready, setReady] = React.useState(false);
  const [data, setData] = React.useState({} as any);
  const [dataForm, setDataForm] = React.useState({
    name: '',
  } as any);
  const [showForm, setShowForm] = React.useState(false);
  const [formType, setFormType] = React.useState('Add');
  const header = [
    { title: '#', field: 'no' },
    { title: 'Name', field: 'name' },
    { title: 'Slug', field: 'slug' },
  ];
  const initData = () => {
    setLoading(true);
    getInstrumentCategories({ ...filter })
      .then((res: any) => {
        setData(res);
      })
      .finally(() => setLoading(false));
  };
  React.useEffect(() => {
    if (ready) {
      initData();
    }
    return () => {};
  }, [filter, ready]);
  React.useEffect(() => {
    resetFilter().then(() => setReady(true));
    return () => {};
  }, []);
  const onDelete = (id: any) => {
    Swal.fire({
      title: 'Are you sure? ',
      text: 'You want to delete this data',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        deleteInstrumentCategory({ id }).then((res) => {
          toast.success(res.message);
          initData();
        });
      }
    });
  };
  const onEdit = (item: any) => {
    setDataForm(item);
    setShowForm(true);
  };
  const onSave = () => {
    if (!dataForm.name) {
      toast.error('form input can not empty');
    } else if (dataForm?.id) {
      updateInstrumentCategory(dataForm)
        .then(() => {
          toast.success('data has been saved');
          initData();
        })
        .finally(() => {
          setShowForm(false);
          setDataForm({ name: '' });
        });
    } else {
      createInstrumentCategory({ name: dataForm.name })
        .then(() => {
          toast.success('data has been updated');
          initData();
        })
        .finally(() => {
          setShowForm(false);
          setDataForm({ name: '' });
        });
    }
  };
  const rootRef = React.useRef<HTMLDivElement>(null);
  return (
    <div className="w-full" ref={rootRef}>
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Instrument</Breadcrumb.Item>
        <Breadcrumb.Item>Category</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Instrument Category</h1>
          <Button
            gradientMonochrome="info"
            onClick={() => {
              setFormType('Add');
              setShowForm(true);
            }}
          >
            <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
            Add new category
          </Button>
        </div>
      </div>
      <div className="mx-5 rounded bg-white p-5 shadow-md">
        <ETable
          header={header}
          data={data.items}
          totalData={data.total ?? 0}
          pageCount={10}
          searchText=""
          haveAction
          customTableBody
          renderTableBody={
            <Table.Body className="divide-y">
              {loading && (
                <Table.Row className="bg-white">
                  <Table.Cell colSpan={8}>
                    <div className="flex h-10 w-full items-center justify-center">
                      <div className="animate-pulse rounded-full bg-fuchsia-200 px-3 py-1 text-center text-xs font-medium leading-none text-fuchsia-800">
                        loading...
                      </div>
                    </div>
                  </Table.Cell>
                </Table.Row>
              )}
              {!loading &&
                data?.items?.map((item: any, i: number) => {
                  return (
                    <Table.Row className="bg-white" key={i}>
                      <Table.Cell> {filter.skip + i + 1}</Table.Cell>
                      <Table.Cell>
                        <p className="font-semibold text-purple-700">
                          {item.name}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="">{item.slug}</p>
                      </Table.Cell>
                      <Table.Cell width={100}>
                        <EButton
                          onClick={() => {
                            onEdit(item);
                          }}
                        >
                          <HiPencil color="#2b6cb0" size={20} />
                        </EButton>
                        <EButton
                          className="ml-3"
                          onClick={() => onDelete(item.id)}
                        >
                          <HiTrash color="gray" size={20} />
                        </EButton>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
            </Table.Body>
          }
        />
      </div>
      <Modal
        root={rootRef.current ?? undefined}
        show={showForm}
        size="md"
        popup
        onClose={() => {
          setShowForm(false);
          setDataForm({ name: '' });
        }}
      >
        <Modal.Header />
        <Modal.Body>
          <div className="space-y-6">
            <h3 className="text-xl font-medium text-gray-900 dark:text-white">
              {formType === 'Add' ? 'Add new' : 'Edit'} category
            </h3>
            <div>
              <div className="mb-2 block">
                <Label htmlFor="name" value="Name" />
              </div>
              <TextInput
                className="eTextinput"
                required
                value={dataForm.name}
                id="name"
                type="text"
                sizing="md"
                onChange={(e) => {
                  const newdata = { ...dataForm, name: e.target.value };
                  setDataForm({ ...newdata });
                }}
              />
            </div>
            <div className="flex justify-end">
              <Button onClick={() => onSave()}>Save</Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default Layout;

Layout.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
