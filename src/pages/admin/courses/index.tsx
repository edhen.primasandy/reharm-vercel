import ButtonComp from '@components/elements/button';
import InputSelect from '@components/elements/input-select';
import ETable from '@components/elements/table';
import helper from '@utils/helper';
import { Breadcrumb, Button, Table, TextInput } from 'flowbite-react';
import router from 'next/router';
import React from 'react';
import {
  HiHome,
  HiOutlineDocumentAdd,
  HiOutlineFilter,
  HiOutlineStar,
  HiPencil,
  HiTrash,
} from 'react-icons/hi';
import { MdOutlinePaid, MdPendingActions } from 'react-icons/md';
import { VscLayersActive } from 'react-icons/vsc';
import { toast } from 'react-toastify';
import Swal from 'sweetalert2';

import { useActions, useState } from '@/overmind/index';
import AdminLayout from '@/templates/Admin';

const Courses = () => {
  const {
    getsAdminCourses,
    getSummaryCourses,
    deleteCourse,
    getsCategories,
    getsUsers,
    resetFilter,
  } = useActions();
  const { filter, user } = useState();
  const [loading, setLoading] = React.useState(false);
  const [ready, setReady] = React.useState(false);
  const [summary, setSummary] = React.useState({} as any);
  const [categories, setCategories] = React.useState([] as any);
  const [instructors, setInstructors] = React.useState([] as any);
  const [data, setData] = React.useState([] as any);
  const [search, setSearch] = React.useState('');
  const [selectedCategory, setSelectedCategory] = React.useState(null as any);
  const [selectedPricing, setSelectedPricing] = React.useState(null as any);
  const [selectedInstructor, setSelectedInstructor] = React.useState(
    null as any
  );
  const [selectedStatus, setSelectedStatus] = React.useState(null as any);
  const header = [
    { title: '#', field: 'no' },
    { title: 'Title', field: 'title' },
    { title: 'Category', field: 'category' },
    { title: 'Lesson and Section', field: 'email' },
    { title: 'Rating', field: 'rating_total_average' },
    { title: 'Status', field: 'status' },
    { title: 'Price', field: 'price' },
  ];
  const initData = () => {
    if (user?.role === 'instructor') {
      setSelectedInstructor({ value: user.id, label: user.first_name });
    }
    getsCategories().then((res: any) => {
      const Newcategories: any[] = [];
      res.items.forEach((o: any) => {
        o.childs.forEach((c: any) => {
          Newcategories.push({
            value: c.id,
            label: `[${o.name}] - ${c.name}`,
          });
        });
      });
      setCategories(Newcategories);
    });

    getSummaryCourses().then((res) => {
      setSummary(res.data);
    });
    getsUsers({ role: 'instructor' }).then((res) => {
      const newinstructors: any[] = [];
      res.items.forEach((o: any) => {
        newinstructors.push({
          value: o.id,
          label: `${o.first_name} ${o.last_name}`,
        });
      });
      setInstructors(newinstructors);
    });
  };
  React.useEffect(() => {
    initData();
    return () => {};
  }, []);

  const onFilter = () => {
    let payload: any = { ...filter };
    if (selectedCategory !== null) {
      payload = {
        ...payload,
        category_id: selectedCategory.value,
      };
    }
    if (selectedInstructor !== null) {
      payload = {
        ...payload,
        user_id: selectedInstructor.value,
      };
    }
    if (selectedStatus !== null) {
      payload = {
        ...payload,
        status: selectedStatus.value,
      };
    }
    if (selectedPricing !== null) {
      payload = {
        ...payload,
        is_free_course: selectedPricing.value === 'free' ? 'yes' : 'no',
      };
    }
    setLoading(true);
    getsAdminCourses({ ...payload })
      .then((res: any) => {
        setData(res);
      })
      .finally(() => setLoading(false));
  };
  React.useEffect(() => {
    if (ready) {
      onFilter();
    }
    return () => {};
  }, [filter, ready]);

  React.useEffect(() => {
    resetFilter().then(() => setReady(true));
    return () => {};
  }, []);
  const statusData = [
    { value: '', label: 'All' },
    { value: 'active', label: 'Active' },
    { value: 'pending', label: 'Pending' },
  ];
  const pricingData = [
    { value: '', label: 'All' },
    { value: 'free', label: 'Free' },
    { value: 'paid', label: 'Paid' },
  ];
  const onDelete = (id: any) => {
    Swal.fire({
      title: 'Are you sure? ',
      text: 'You want to delete this course',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        deleteCourse({ id }).then((res) => {
          toast.success(res.message);
          initData();
        });
      }
    });
  };
  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Courses</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Courses</h1>
          <Button
            gradientMonochrome="info"
            onClick={() => {
              router.push({
                pathname: '/admin/courses/[...id]',
                query: { id: 'add' },
              });
            }}
          >
            <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
            Add new course
          </Button>
        </div>
      </div>
      <div className="mx-5 mb-5">
        <div className="-mx-3 flex flex-wrap">
          <div className="mb-6 w-full max-w-full px-3 sm:w-1/2 sm:flex-none xl:mb-0 xl:w-1/4">
            <div className="relative flex min-w-0 flex-col break-words rounded-xl bg-white bg-clip-border shadow-md">
              <div className="flex-auto p-4">
                <div className="-mx-3 flex flex-row">
                  <div className="w-2/3 max-w-full flex-none px-3">
                    <div>
                      <p className="mb-0 font-sans text-sm font-semibold leading-normal">
                        Active courses
                      </p>
                      <h5 className="mb-0 text-2xl font-bold">
                        {summary?.total_active_course}
                      </h5>
                    </div>
                  </div>
                  <div className="basis-1/3 px-3 text-right">
                    <div className="inline-block h-12 w-12 rounded-lg bg-gradient-to-tl from-purple-700 to-pink-500 text-center">
                      <div className="relative flex h-full items-center justify-center leading-none text-white">
                        <VscLayersActive size={30} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="mb-6 w-full max-w-full px-3 sm:w-1/2 sm:flex-none xl:mb-0 xl:w-1/4">
            <div className="relative flex min-w-0 flex-col break-words rounded-xl bg-white bg-clip-border shadow-md">
              <div className="flex-auto p-4">
                <div className="-mx-3 flex flex-row">
                  <div className="w-2/3 max-w-full flex-none px-3">
                    <div>
                      <p className="mb-0 font-sans text-sm font-semibold leading-normal">
                        Pending courses
                      </p>
                      <h5 className="mb-0 text-2xl font-bold">
                        {summary?.total_pending_course}
                      </h5>
                    </div>
                  </div>
                  <div className="basis-1/3 px-3 text-right">
                    <div className="inline-block h-12 w-12 rounded-lg bg-gradient-to-tl from-purple-700 to-pink-500 text-center">
                      <div className="relative flex h-full items-center justify-center leading-none text-white">
                        <MdPendingActions size={30} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="mb-6 w-full max-w-full px-3 sm:w-1/2 sm:flex-none xl:mb-0 xl:w-1/4">
            <div className="relative flex min-w-0 flex-col break-words rounded-xl bg-white bg-clip-border shadow-md">
              <div className="flex-auto p-4">
                <div className="-mx-3 flex flex-row">
                  <div className="w-2/3 max-w-full flex-none px-3">
                    <div>
                      <p className="mb-0 font-sans text-sm font-semibold leading-normal">
                        Free courses
                      </p>
                      <h5 className="mb-0 text-2xl font-bold">
                        {summary?.total_free_course}
                      </h5>
                    </div>
                  </div>
                  <div className="basis-1/3 px-3 text-right">
                    <div className="inline-block h-12 w-12 rounded-lg bg-gradient-to-tl from-purple-700 to-pink-500 text-center">
                      <div className="relative flex h-full items-center justify-center leading-none text-white">
                        <HiOutlineStar size={30} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="w-full max-w-full px-3 sm:w-1/2 sm:flex-none xl:w-1/4">
            <div className="relative flex min-w-0 flex-col break-words rounded-xl bg-white bg-clip-border shadow-md">
              <div className="flex-auto p-4">
                <div className="-mx-3 flex flex-row">
                  <div className="w-2/3 max-w-full flex-none px-3">
                    <div>
                      <p className="mb-0 font-sans text-sm font-semibold leading-normal">
                        Paid courses
                      </p>
                      <h5 className="mb-0 text-2xl font-bold">
                        {summary?.total_paid_course}
                      </h5>
                    </div>
                  </div>
                  <div className="basis-1/3 px-3 text-right">
                    <div className="inline-block h-12 w-12 rounded-lg bg-gradient-to-tl from-purple-700 to-pink-500 text-center">
                      <div className="relative flex h-full items-center justify-center leading-none text-white">
                        <MdOutlinePaid size={30} />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="mx-5 rounded bg-white p-5 shadow-md">
        <div className="mb-5 flex flex-row gap-4">
          <div className="flex-1">
            <div className="grid grid-cols-4 gap-3">
              <InputSelect
                isClearable
                label="Category"
                data={categories}
                value={selectedCategory}
                placeholder="Select category"
                onChange={(e: any) => {
                  setSelectedCategory(e);
                }}
              />
              <InputSelect
                isClearable
                label="Status"
                data={statusData}
                value={selectedStatus}
                placeholder="Select status"
                onChange={(e: any) => setSelectedStatus(e)}
              />
              {user?.role === 'admin' && (
                <InputSelect
                  isClearable
                  label="Instructor"
                  data={instructors}
                  value={selectedInstructor}
                  placeholder="Select instructor"
                  onChange={(e: any) => setSelectedInstructor(e)}
                />
              )}
              <InputSelect
                isClearable
                label="Pricing"
                data={pricingData}
                value={selectedPricing}
                placeholder="Select pricing"
                onChange={(e: any) => setSelectedPricing(e)}
              />
            </div>
          </div>
          <div className="flex items-end">
            <Button
              gradientMonochrome="pink"
              onClick={() => {
                onFilter();
              }}
            >
              <HiOutlineFilter className="mr-2 h-5 w-5" />
              Filter
            </Button>
          </div>
        </div>
        <div className="mb-5 flex justify-end">
          <TextInput
            value={search}
            id="base"
            type="text"
            placeholder="search..."
            sizing="md"
            className="eTextinput basis-1/4"
            onChange={(e) => setSearch(e.target.value)}
          />
        </div>
        <ETable
          header={header}
          data={data.items}
          totalData={data.total ?? 0}
          pageCount={10}
          searchText=""
          haveAction
          customTableBody
          renderTableBody={
            <Table.Body className="divide-y">
              {loading && (
                <Table.Row className="bg-white">
                  <Table.Cell colSpan={8}>
                    <div className="flex h-10 w-full items-center justify-center">
                      <div className="animate-pulse rounded-full bg-fuchsia-200 px-3 py-1 text-center text-xs font-medium leading-none text-fuchsia-800">
                        loading...
                      </div>
                    </div>
                  </Table.Cell>
                </Table.Row>
              )}
              {!loading &&
                data?.items?.map((item: any, i: number) => {
                  return (
                    <Table.Row className="bg-white" key={i}>
                      <Table.Cell> {filter.skip + i + 1}</Table.Cell>
                      <Table.Cell>
                        <p className="font-semibold text-purple-700">
                          {item.title}
                        </p>
                        <p className="text-sm">
                          {`Instructor : ${item.user.first_name} ${item.user.last_name}`}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">{item.category.name}</p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">
                          Total section:{' '}
                          <span className="font-semibold">
                            {item.total_sections}
                          </span>
                        </p>
                        <p className="text-sm">
                          Total lesson:{' '}
                          <span className="font-semibold">
                            {item.total_lessons}
                          </span>
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">
                          <span className="font-semibold">
                            {item.rating_total_average}
                          </span>
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        {item.status === 'active' && (
                          <span className="rounded bg-emerald-200 px-1 text-emerald-700">
                            {item.status}
                          </span>
                        )}
                        {item.status === 'pending' && (
                          <span className="rounded bg-red-200 px-1 text-red-700">
                            {item.status}
                          </span>
                        )}
                      </Table.Cell>
                      <Table.Cell>
                        {item.price === 0 && (
                          <span className="rounded bg-emerald-200 px-1 text-emerald-700">
                            Free
                          </span>
                        )}
                        {item.price > 0 && (
                          <span className="rounded bg-gray-200 px-1 text-gray-700">
                            {helper.currencyFormat(item.price)}
                          </span>
                        )}
                      </Table.Cell>
                      <Table.Cell width={100}>
                        <ButtonComp
                          onClick={() => {
                            router.push({
                              pathname: '/admin/courses/[...id]',
                              query: { id: item.id },
                            });
                          }}
                        >
                          <HiPencil color="#2b6cb0" size={20} />
                        </ButtonComp>
                        <ButtonComp
                          className="ml-3"
                          onClick={() => onDelete(item.id)}
                        >
                          <HiTrash color="gray" size={20} />
                        </ButtonComp>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
            </Table.Body>
          }
        />
      </div>
    </div>
  );
};

export default Courses;

Courses.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
