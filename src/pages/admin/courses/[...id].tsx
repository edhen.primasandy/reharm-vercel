/* eslint-disable no-restricted-globals */
/* eslint-disable tailwindcss/no-custom-classname */
import Formbasic from '@components/modules/admin/courses/form-basic';
import Formrcurirculum from '@components/modules/admin/courses/form-curirculum';
import Formmedia from '@components/modules/admin/courses/form-media';
import Formoutcomes from '@components/modules/admin/courses/form-outcomes';
import Formpricing from '@components/modules/admin/courses/form-pricing';
import Formrequirement from '@components/modules/admin/courses/form-requirement';
import {
  Tab,
  TabPanel,
  Tabs,
  TabsBody,
  TabsHeader,
} from '@material-tailwind/react';
import {
  Breadcrumb,
  Button,
  Card,
  Label,
  Textarea,
  TextInput,
} from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import { BsFillCollectionPlayFill } from 'react-icons/bs';
import { FaTags, FaUserCircle } from 'react-icons/fa';
import { GiReceiveMoney } from 'react-icons/gi';
import { HiHome, HiUserCircle } from 'react-icons/hi';
import { IoIosSave } from 'react-icons/io';
import {
  MdNotificationImportant,
  MdOutlineCancel,
  MdPayment,
} from 'react-icons/md';
import { TiArrowMaximiseOutline } from 'react-icons/ti';
import { toast } from 'react-toastify';

import InputSelect from '@/components/elements/input-select';
import { useActions, useState } from '@/overmind';
import AdminLayout from '@/templates/Admin';

const CommissionType = [
  { value: 'percentage', label: 'Percentage' },
  { value: 'value', label: 'Value' },
];
const Form = () => {
  const router = useRouter();
  const { getCourse, createCourse, updateCourse } = useActions();
  const { user } = useState();
  const [title, setTitle] = React.useState('Form');
  const [selectedCommissionType, setSelectedCommissionType] = React.useState({
    value: 'percentage',
    label: 'Percentage',
  });
  const [data, setData] = React.useState({
    title: '',
    short_description: '',
    description: '',
    language: '',
    category_id: 0,
    level: '',
    is_top_course: 'no',
    requirements: [''],
    outcomes: [''],
    sections: [],
    is_free_course: 'no',
    discount_flag: 'no',
    price: 0,
    discounted_price: 0,
    status: '',
    meta_keywords: '',
    meta_description: '',
    video_provider: '',
    video_url: '',
    thumbnail: null,
    user_id: user?.id,
    admin_revenue: 0,
    instructor_revenue: 0,
    commission_percentage: null,
    commission_value: null,
    commission_type: 'percentage',
  } as any);
  React.useEffect(() => {
    if (router.query.id && !router.query.id?.includes('add')) {
      getCourse({ id: router.query.id.toString() }).then((res: any) => {
        const mappingData = res.data;
        setTitle(`Edit ${mappingData.title}`);

        if (mappingData.requirements.length === 0) {
          mappingData.requirements.push('');
        }
        if (mappingData.outcomes.length === 0) {
          mappingData.outcomes.push('');
        }
        setData(mappingData);
        const fetchComType = CommissionType.find(
          (o) => o.value === data.language
        );
        if (fetchComType) {
          setSelectedCommissionType(fetchComType);
        }
      });
    }
    return () => {};
  }, [router.query.id]);
  // React.useEffect(() => {
  //   // console.log(data);
  //   return () => {};
  // }, [data]);
  const saveData = () => {
    const payload = { ...data };
    payload.requirements = data.requirements.filter((o: any) => o !== '');
    payload.outcomes = data.outcomes.filter((o: any) => o !== '');
    if (payload.id) {
      if (!payload.thumbnail.includes('base64')) {
        delete payload.thumbnail;
      }
      updateCourse({ ...payload })
        .then(() => {
          toast.success('data has been saved');
          router.back();
        })
        .catch((e) => toast.error(e.message));
    } else {
      createCourse({ ...payload })
        .then(() => {
          toast.success('data has been saved');
          router.back();
        })
        .catch((e) => toast.error(e.message));
    }
  };
  const tabAdd = [
    {
      label: 'Basic',
      value: 'Basic',
      icon: HiUserCircle,
      role: ['admin', 'instructor'],
      body: (
        <Formbasic data={data} onChangeData={(item: any) => setData(item)} />
      ),
    },
    {
      label: 'Requirement',
      value: 'Requirement',
      icon: MdNotificationImportant,
      role: ['admin', 'instructor'],
      body: (
        <Formrequirement
          data={data}
          onChangeData={(item: any) => setData(item)}
        />
      ),
    },
    {
      label: 'Outcomes',
      value: 'Outcomes',
      icon: TiArrowMaximiseOutline,
      role: ['admin', 'instructor'],
      body: (
        <Formoutcomes data={data} onChangeData={(item: any) => setData(item)} />
      ),
    },
    {
      label: 'Pricing',
      value: 'Pricing',
      icon: MdPayment,
      role: ['admin', 'instructor'],
      body: (
        <Formpricing data={data} onChangeData={(item: any) => setData(item)} />
      ),
    },
    {
      label: 'Media',
      value: 'Media',
      icon: BsFillCollectionPlayFill,
      role: ['admin', 'instructor'],
      body: (
        <Formmedia data={data} onChangeData={(item: any) => setData(item)} />
      ),
    },
    {
      label: 'Seo',
      value: 'Seo',
      icon: FaTags,
      role: ['admin', 'instructor'],
      body: (
        <div>
          <form className="grid grid-cols-2 gap-3">
            <div className="mb-2">
              <div className="mb-2 block">
                <Label htmlFor="meta_keywords" value="Meta keyword" />
              </div>
              <TextInput
                className="eTextinput"
                required
                value={data.meta_keywords}
                id="meta_keywords"
                type="text"
                sizing="md"
                onChange={(e) => {
                  setData({ ...data, meta_keywords: e.target.value });
                }}
              />
            </div>
            <div className="mb-2">
              <div className="mb-2 block">
                <Label htmlFor="meta_description" value="Meta description" />
              </div>
              <Textarea
                className="eTextarea"
                required
                value={data.meta_description}
                rows={4}
                id="meta_description"
                onChange={(e) => {
                  setData({
                    ...data,
                    meta_description: e.target.value,
                  });
                }}
              />
            </div>
          </form>
        </div>
      ),
    },
    {
      label: 'Revenue setting',
      value: 'Revenue setting',
      icon: GiReceiveMoney,
      role: ['admin'],
      body: (
        <div>
          <form className="grid grid-cols-2 gap-3">
            <div className="mb-2">
              <div className="mb-2 block">
                <Label htmlFor="admin_revenue" value="Admin revenue" />
              </div>
              <TextInput
                className="eTextinput"
                required
                value={data.admin_revenue}
                id="admin_revenue"
                type="number"
                sizing="md"
                onChange={(e) => {
                  setData({
                    ...data,
                    admin_revenue: parseInt(e.target.value, 10),
                    instructor_revenue: 100 - parseInt(e.target.value, 10),
                  });
                }}
              />
            </div>
            <div className="mb-2">
              <div className="mb-2 block">
                <Label
                  htmlFor="instructor_revenue"
                  value="Instructor revenue"
                />
              </div>
              <TextInput
                className="eTextinput"
                required
                disabled
                value={data.instructor_revenue}
                id="instructor_revenue"
                type="number"
                sizing="md"
                onChange={(e) => {
                  setData({
                    ...data,
                    instructor_revenue: e.target.value,
                  });
                }}
              />
            </div>
          </form>
          <div className="my-4 w-full border-t border-gray-300"></div>
          <h1 className="text-xl">Commission Setting</h1>
          <form className="grid grid-cols-2 gap-3">
            <div className="mb-2">
              <InputSelect
                label="Commission type"
                data={CommissionType}
                value={selectedCommissionType}
                placeholder="Select commission type"
                onChange={(e: any) => {
                  setData({
                    ...data,
                    commission_type: e !== null ? e.value : '',
                  });
                  setSelectedCommissionType(e);
                }}
              />
            </div>
            {data.commission_type === 'percentage' ? (
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label
                    htmlFor="commission_percentage"
                    value="Commission percentage"
                  />
                </div>
                <TextInput
                  className="eTextinput"
                  required
                  value={data.commission_percentage}
                  id="commission_percentage"
                  type="number"
                  sizing="md"
                  onChange={(e) => {
                    setData({
                      ...data,
                      commission_percentage: parseFloat(e.target.value),
                    });
                  }}
                />
              </div>
            ) : (
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label htmlFor="commission_value" value="Commission value" />
                </div>
                <TextInput
                  className="eTextinput"
                  required
                  value={data.commission_value}
                  id="commission_value"
                  type="number"
                  sizing="md"
                  onChange={(e) => {
                    setData({
                      ...data,
                      commission_value: parseFloat(e.target.value),
                    });
                  }}
                />
              </div>
            )}
          </form>
        </div>
      ),
    },
  ];
  const tabEdit = [
    {
      label: 'Basic',
      value: 'Basic',
      icon: HiUserCircle,
      role: ['admin', 'instructor'],
      body: (
        <Formbasic data={data} onChangeData={(item: any) => setData(item)} />
      ),
    },
    {
      label: 'Requirement',
      value: 'Requirement',
      icon: MdNotificationImportant,
      role: ['admin', 'instructor'],
      body: (
        <Formrequirement
          data={data}
          onChangeData={(item: any) => setData(item)}
        />
      ),
    },
    {
      label: 'Outcomes',
      value: 'Outcomes',
      icon: TiArrowMaximiseOutline,
      role: ['admin', 'instructor'],
      body: (
        <Formoutcomes data={data} onChangeData={(item: any) => setData(item)} />
      ),
    },
    {
      label: 'Pricing',
      value: 'Pricing',
      icon: MdPayment,
      role: ['admin', 'instructor'],
      body: (
        <Formpricing data={data} onChangeData={(item: any) => setData(item)} />
      ),
    },
    {
      label: 'Media',
      value: 'Media',
      icon: BsFillCollectionPlayFill,
      role: ['admin', 'instructor'],
      body: (
        <Formmedia data={data} onChangeData={(item: any) => setData(item)} />
      ),
    },
    {
      label: 'Seo',
      value: 'Seo',
      icon: FaTags,
      role: ['admin', 'instructor'],
      body: (
        <div>
          <form className="grid grid-cols-2 gap-3">
            <div className="mb-2">
              <div className="mb-2 block">
                <Label htmlFor="meta_keywords" value="Meta keyword" />
              </div>
              <TextInput
                className="eTextinput"
                required
                value={data.meta_keywords}
                id="meta_keywords"
                type="text"
                sizing="md"
                onChange={(e) => {
                  setData({ ...data, meta_keywords: e.target.value });
                }}
              />
            </div>
            <div className="mb-2">
              <div className="mb-2 block">
                <Label htmlFor="meta_description" value="Meta description" />
              </div>
              <Textarea
                className="eTextarea"
                required
                value={data.meta_description}
                rows={4}
                id="meta_description"
                onChange={(e) => {
                  setData({
                    ...data,
                    meta_description: e.target.value,
                  });
                }}
              />
            </div>
          </form>
        </div>
      ),
    },
    {
      label: 'Revenue setting',
      value: 'Revenue setting',
      icon: GiReceiveMoney,
      role: ['admin'],
      body: (
        <div>
          <form className="grid grid-cols-2 gap-3">
            <div className="mb-2">
              <div className="mb-2 block">
                <Label htmlFor="admin_revenue" value="Admin revenue" />
              </div>
              <TextInput
                className="eTextinput"
                required
                value={data.admin_revenue}
                id="admin_revenue"
                type="number"
                max={100}
                maxLength={3}
                sizing="md"
                onChange={(e) => {
                  let value = parseInt(e.target.value, 10);
                  if (!isNaN(value) && value > 100) {
                    value = 100;
                  }
                  setData({
                    ...data,
                    admin_revenue: e.target.value ? value : '',
                    instructor_revenue: 100 - (e.target.value ? value : 0),
                  });
                }}
              />
            </div>
            <div className="mb-2">
              <div className="mb-2 block">
                <Label
                  htmlFor="instructor_revenue"
                  value="Instructor revenue"
                />
              </div>
              <TextInput
                className="eTextinput"
                required
                disabled
                value={data.instructor_revenue}
                id="instructor_revenue"
                type="number"
                sizing="md"
                onChange={(e) => {
                  setData({
                    ...data,
                    instructor_revenue: e.target.value,
                  });
                }}
              />
            </div>
          </form>
          <div className="my-4 w-full border-t border-gray-300"></div>
          <h1 className="text-xl">Commission Setting</h1>
          <form className="grid grid-cols-2 gap-3">
            <div className="mb-2">
              <InputSelect
                label="Commission type"
                data={CommissionType}
                value={selectedCommissionType}
                placeholder="Select commission type"
                onChange={(e: any) => {
                  setData({
                    ...data,
                    commission_type: e !== null ? e.value : '',
                  });
                  setSelectedCommissionType(e);
                }}
              />
            </div>
            {data.commission_type === 'percentage' ? (
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label
                    htmlFor="commission_percentage"
                    value="Commission percentage"
                  />
                </div>
                <TextInput
                  className="eTextinput"
                  required
                  value={data.commission_percentage}
                  id="commission_percentage"
                  type="number"
                  sizing="md"
                  onChange={(e) => {
                    setData({
                      ...data,
                      commission_percentage: parseFloat(e.target.value),
                    });
                  }}
                />
              </div>
            ) : (
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label htmlFor="commission_value" value="Commission value" />
                </div>
                <TextInput
                  className="eTextinput"
                  required
                  value={data.commission_value}
                  id="commission_value"
                  type="number"
                  sizing="md"
                  onChange={(e) => {
                    setData({
                      ...data,
                      commission_value: parseFloat(e.target.value),
                    });
                  }}
                />
              </div>
            )}
          </form>
        </div>
      ),
    },
    {
      label: 'Curriculum',
      value: 'Curriculum',
      icon: FaUserCircle,
      role: ['admin', 'instructor'],
      body: (
        <Formrcurirculum
          courseId={data?.id}
          data={data}
          onChangeData={(item: any) => setData(item)}
        />
      ),
    },
  ];
  return (
    <div className="w-full">
      <Breadcrumb>
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item href="#" onClick={() => router.push('/admin/courses')}>
          Courses
        </Breadcrumb.Item>
        <Breadcrumb.Item>Form</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">{title}</h1>
          <div className="flex flex-row gap-3">
            <Button
              color="gray"
              onClick={() => {
                router.back();
              }}
            >
              <MdOutlineCancel className="mr-2 h-5 w-5" />
              Cancel
            </Button>
            <Button
              gradientMonochrome="info"
              onClick={() => {
                saveData();
              }}
            >
              <IoIosSave className="mr-2 h-5 w-5" />
              Save
            </Button>
          </div>
        </div>
        <Card className="mt-2">
          {data.id ? (
            <Tabs value="Basic">
              <TabsHeader
                className="border-blue-gray-50 rounded-none border-b bg-transparent p-0"
                indicatorProps={{
                  className:
                    'bg-transparent border-b-2 border-pink-600 shadow-none rounded-none',
                }}
              >
                {tabEdit
                  .filter((o) => o.role.includes(user?.role))
                  .map(({ label, value, icon }) => (
                    <Tab key={value} value={value} hidden={true}>
                      <div className="flex items-center gap-2 py-2">
                        {React.createElement(icon, { className: 'w-5 h-5' })}
                        {label}
                      </div>
                    </Tab>
                  ))}
              </TabsHeader>
              <TabsBody>
                {tabEdit.map(({ value, body }) => (
                  <TabPanel key={value} value={value}>
                    {body}
                  </TabPanel>
                ))}
              </TabsBody>
            </Tabs>
          ) : (
            <Tabs value="Basic">
              <TabsHeader
                className="border-blue-gray-50 rounded-none border-b bg-transparent p-0"
                indicatorProps={{
                  className:
                    'bg-transparent border-b-2 border-pink-600 shadow-none rounded-none',
                }}
              >
                {tabAdd
                  .filter((o) => o.role.includes(user?.role))
                  .map(({ label, value, icon }) => (
                    <Tab key={value} value={value}>
                      <div className="flex items-center gap-2 py-2">
                        {React.createElement(icon, { className: 'w-5 h-5' })}
                        {label}
                      </div>
                    </Tab>
                  ))}
              </TabsHeader>
              <TabsBody>
                {tabAdd.map(({ value, body }) => (
                  <TabPanel key={value} value={value}>
                    {body}
                  </TabPanel>
                ))}
              </TabsBody>
            </Tabs>
          )}
        </Card>
      </div>
    </div>
  );
};

export default Form;
Form.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
