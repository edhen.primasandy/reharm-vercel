import { Config } from '@constant/index';
import { Breadcrumb, Button, Card, Label, TextInput } from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import { HiHome, HiOutlineDocumentAdd, HiTrash } from 'react-icons/hi';
import { IoIosSave } from 'react-icons/io';
import { MdOutlineCancel } from 'react-icons/md';
import { toast } from 'react-toastify';

import ImageDropzone from '@/components/elements/image-dropzone';
import { useActions } from '@/overmind';
import AdminLayout from '@/templates/Admin';

const Form = () => {
  const router = useRouter();
  const { getCategory, createCategory, updateCategory } = useActions();
  const [title, setTitle] = React.useState('Form');
  const [data, setData] = React.useState({
    id: '',
    parent_id: 0,
    name: '',
    childs: [] as any[],
    thumbnail: '',
  });
  const [isBase64, setIsBase64] = React.useState(false);

  React.useEffect(() => {
    if (router.query.id && !router.query.id?.includes('add')) {
      setTitle(`Edit ${router.query.id}`);
      getCategory({ id: router.query.id.toString() }).then((res) =>
        setData(res.items[0])
      );
    }

    return () => {};
  }, [router.query.id]);

  const saveData = () => {
    if (data.id) {
      updateCategory(data).then(() => {
        toast.success('data has been updated');
        router.back();
      });
    } else {
      createCategory(data).then(() => {
        toast.success('data has been saved');
        router.back();
      });
    }
  };
  const deleteSub = (index: number) => {
    const subs = data.childs;
    subs.splice(index, 1);
    console.log(index, subs);
    setData({ ...data, childs: subs });
  };
  const addSub = () => {
    const subs = data.childs;
    subs.push({ id: 0, name: '' });
    setData({ ...data, childs: subs });
  };
  return (
    <div className="w-full">
      <Breadcrumb>
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/categories')}
        >
          Categories
        </Breadcrumb.Item>
        <Breadcrumb.Item>Form</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">{title}</h1>
          <div className="flex flex-row gap-3">
            <Button
              color="gray"
              onClick={() => {
                router.back();
              }}
            >
              <MdOutlineCancel className="mr-2 h-5 w-5" />
              Cancel
            </Button>
            <Button
              gradientMonochrome="info"
              onClick={() => {
                saveData();
              }}
            >
              <IoIosSave className="mr-2 h-5 w-5" />
              Save
            </Button>
          </div>
        </div>
        <div className="my-5">
          <Card>
            <form className="grid grid-cols-2 gap-3">
              <div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="categorytitle" value="Category title*" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.name}
                    id="categorytitle"
                    type="text"
                    sizing="md"
                    onChange={(e) => setData({ ...data, name: e.target.value })}
                  />
                </div>
                <div className="mb-4">
                  <div className="mb-2 block">
                    <Label htmlFor="subcategory" value="Sub category" />
                  </div>
                  {data.childs.map((item, i) => {
                    return (
                      <div className="mb-2 flex items-center gap-2" key={i}>
                        <TextInput
                          value={item.name}
                          className="eTextinput flex-1"
                          id="subcategory"
                          type="text"
                          sizing="md"
                          onChange={(e) => {
                            const subs = data.childs;
                            subs[i].name = e.target.value;
                            setData({ ...data, childs: subs });
                          }}
                        />
                        <Button
                          outline={true}
                          gradientDuoTone="pinkToOrange"
                          onClick={() => deleteSub(i)}
                        >
                          <HiTrash className="h-6 w-5" />
                        </Button>
                      </div>
                    );
                  })}
                </div>
                <div>
                  <Button color="dark" onClick={() => addSub()}>
                    <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
                    Add sub category
                  </Button>
                </div>
              </div>
              <div>
                <div className="mb-2 block">
                  <Label htmlFor="thumbnail" value="Category thumbnail" />
                </div>
                <ImageDropzone
                  imgSource={isBase64 ? '' : data.thumbnail}
                  baseUrl={`${Config.assetUrl}/categories/`}
                  onChange={(file) => {
                    setData({ ...data, thumbnail: file });
                    if (file) {
                      setIsBase64(true);
                    }
                  }}
                />
              </div>
            </form>
          </Card>
        </div>
      </div>
    </div>
  );
};

export default Form;
Form.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
