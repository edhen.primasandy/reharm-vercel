// import { Config } from '@constant/index';
import { Breadcrumb, Button, Card, Dropdown } from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import { BsPlayFill } from 'react-icons/bs';
import {
  HiHome,
  HiOutlineDocumentAdd,
  HiPencilAlt,
  HiTrash,
} from 'react-icons/hi';
import { toast } from 'react-toastify';

import { useActions } from '@/overmind';
import AdminLayout from '@/templates/Admin';

const Categories = () => {
  const router = useRouter();
  const { getsCategories, deleteCategory } = useActions();
  const [data, setData] = React.useState([] as any[]);
  const initData = () => {
    getsCategories().then((res: any) => {
      setData(res.items);
    });
  };
  const deleteData = (id: any) => {
    deleteCategory({ id }).then((res) => {
      toast.success(res.message);
      initData();
    });
  };
  React.useEffect(() => {
    initData();
    return () => {};
  }, []);

  // const getImageUrl = (id: string) => {
  //   return `${Config.assetUrl}${id}`;
  // };
  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Categories</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-2 md:mx-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Categories</h1>
          <Button
            gradientMonochrome="info"
            onClick={() => {
              router.push({
                pathname: '/admin/categories/[...id]',
                query: { id: 'add' },
              });
            }}
          >
            <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
            Add new category
          </Button>
        </div>
        <div className="my-5">
          <div className="grid gap-4 md:grid-cols-3">
            {data.map((item: any, i: number) => {
              return (
                <Card key={i} imgSrc={item.thumbnail}>
                  <div className="flex h-full flex-col justify-start gap-4">
                    <div>
                      <div className="flex flex-row items-center justify-between">
                        <div className="flex flex-row items-center gap-1">
                          <BsPlayFill size={30} />
                          <h5 className="text-2xl font-bold text-gray-900 dark:text-white">
                            {item.name}
                          </h5>
                        </div>
                        <Dropdown label="" inline={true}>
                          <Dropdown.Item
                            onClick={() => {
                              router.push({
                                pathname: '/admin/categories/[...id]',
                                query: { id: item.id },
                              });
                            }}
                            icon={HiPencilAlt}
                          >
                            Edit
                          </Dropdown.Item>
                          <Dropdown.Item
                            onClick={() => {
                              deleteData(item.id);
                            }}
                            icon={HiTrash}
                          >
                            Delete
                          </Dropdown.Item>
                        </Dropdown>
                      </div>
                      <span className="text-sm text-gray-600">
                        {item.childs.length} sub categories
                      </span>
                    </div>
                    <div className="flex flex-col">
                      {item.childs.map((sub: any, ii: number) => (
                        <div
                          className="mb-2 rounded bg-gray-50 py-2 px-1"
                          key={ii}
                        >
                          {sub.name}
                        </div>
                      ))}
                    </div>
                  </div>
                </Card>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Categories;

Categories.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
