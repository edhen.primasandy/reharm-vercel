import ButtonComp from '@components/elements/button';
import ETable from '@components/elements/table';
import { Breadcrumb, Button, Table } from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import {
  HiHome,
  HiOutlineDocumentAdd,
  HiPencil,
  HiTrash,
} from 'react-icons/hi';
import { toast } from 'react-toastify';

import { useActions, useState } from '@/overmind';
import AdminLayout from '@/templates/Admin';

const Students = () => {
  const router = useRouter();
  const { getsUsers, deleteUser, resetFilter } = useActions();
  const { filter } = useState();
  const [loading, setLoading] = React.useState(false);
  const [ready, setReady] = React.useState(false);
  const [data, setData] = React.useState({} as any);

  const header = [
    { title: '#', field: 'no' },
    { title: 'Photo', field: 'photo' },
    { title: 'Name', field: 'name' },
    { title: 'Email', field: 'email' },
    { title: 'Enrolled Courses', field: 'enrolled_courses', type: 'list' },
    { title: 'Attempt', field: 'attempt' },
  ];
  const initData = () => {
    setLoading(true);
    getsUsers({ skip: filter.skip, limit: filter.limit, role: 'user' })
      .then((res) => setData(res))
      .finally(() => setLoading(false));
  };
  React.useEffect(() => {
    if (ready) {
      initData();
    }
    return () => {};
  }, [filter, ready]);
  React.useEffect(() => {
    resetFilter().then(() => setReady(true));
    return () => {};
  }, []);
  const onDelete = (id: any) => {
    deleteUser({ id }).then(() => {
      toast.success('User is deleted');
    });
  };
  return (
    <div className="w-full">
      <Breadcrumb aria-label="Default breadcrumb example">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Students</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Students</h1>
          <Button
            gradientMonochrome="info"
            onClick={() => {
              router.push({
                pathname: '/admin/students/[...id]',
                query: { id: 'add' },
              });
            }}
          >
            <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
            Add Student
          </Button>
        </div>
      </div>
      <div className="mx-5">
        <ETable
          header={header}
          data={data.items}
          totalData={data.total ?? 0}
          pageCount={10}
          searchText=""
          haveAction
          customTableBody
          renderTableBody={
            <Table.Body className="divide-y">
              {loading && (
                <Table.Row className="bg-white">
                  <Table.Cell colSpan={8}>
                    <div className="flex h-10 w-full items-center justify-center">
                      <div className="animate-pulse rounded-full bg-fuchsia-200 px-3 py-1 text-center text-xs font-medium leading-none text-fuchsia-800">
                        loading...
                      </div>
                    </div>
                  </Table.Cell>
                </Table.Row>
              )}
              {!loading &&
                data?.items?.map((item: any, i: number) => {
                  return (
                    <Table.Row className="bg-white" key={i}>
                      <Table.Cell> {filter.skip + i + 1}</Table.Cell>
                      <Table.Cell>
                        <div>
                          <img
                            className="h-6 w-6 rounded-full shadow-lg"
                            src={item.photo}
                            alt="user photo"
                          />
                        </div>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">
                          {item.first_name} {item.last_name}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">{item.email}</p>
                      </Table.Cell>
                      <Table.Cell>-</Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">
                          {item?.watch_history?.length ?? 0}
                        </p>
                      </Table.Cell>
                      <Table.Cell width={100}>
                        <ButtonComp
                          onClick={() => {
                            router.push({
                              pathname: '/admin/students/[...id]',
                              query: { id: item.id },
                            });
                          }}
                        >
                          <HiPencil color="#2b6cb0" size={20} />
                        </ButtonComp>
                        <ButtonComp
                          className="ml-3"
                          onClick={() => onDelete(item.id)}
                        >
                          <HiTrash color="gray" size={20} />
                        </ButtonComp>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
            </Table.Body>
          }
        />
      </div>
    </div>
  );
};

export default Students;

Students.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
