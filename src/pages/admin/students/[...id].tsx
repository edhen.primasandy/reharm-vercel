import {
  Breadcrumb,
  Button,
  Card,
  FileInput,
  Label,
  Tabs,
  TextInput,
} from 'flowbite-react';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';
import React from 'react';
import { HiHome, HiUserCircle } from 'react-icons/hi';
import { IoIosSave, IoMdUnlock } from 'react-icons/io';
import { MdOutlineCancel, MdPayment } from 'react-icons/md';
import { TbSocial } from 'react-icons/tb';

import { useActions } from '@/overmind';
import AdminLayout from '@/templates/Admin';

const Editor = dynamic(() => import('@components/elements/wysiwyg-editor'), {
  ssr: false,
});
const Form = () => {
  const router = useRouter();
  const { getUser } = useActions();
  const [title, setTitle] = React.useState('Form');
  const [data, setData] = React.useState({
    first_name: '',
    last_name: '',
    biography: '',
    user_image: '',
    user_image_file: null,
    email: '',
    social_links: {
      facebook: '',
      twitter: '',
      linkedin: '',
    },
    bank: '',
    bank_account_name: '',
    bank_account_no: '',
  });
  // React.useEffect(() => {
  //   setEditorState(EditorState.createEmpty());
  //   return () => {};
  // }, []);
  React.useEffect(() => {
    if (router.query.id && !router.query.id?.includes('add')) {
      setTitle(`Edit ${router.query.id}`);
      getUser({ id: router.query.id.toString() }).then((res) => {
        setData(res);
      });
    }

    return () => {};
  }, [router.query.id]);
  const saveData = () => {};
  return (
    <div className="w-full">
      <Breadcrumb>
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/instructors')}
        >
          Instructors
        </Breadcrumb.Item>
        <Breadcrumb.Item>Form</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">{title}</h1>
          <div className="flex flex-row gap-3">
            <Button
              color="gray"
              onClick={() => {
                router.back();
              }}
            >
              <MdOutlineCancel className="mr-2 h-5 w-5" />
              Cancel
            </Button>
            <Button
              gradientMonochrome="info"
              onClick={() => {
                saveData();
              }}
            >
              <IoIosSave className="mr-2 h-5 w-5" />
              Save
            </Button>
          </div>
        </div>
        <Card className="mt-2">
          <Tabs.Group aria-label="Tabs with icons" style="underline">
            <Tabs.Item title="Basic Info" icon={HiUserCircle}>
              <form className="grid grid-cols-2 gap-3">
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="first_name" value="First name*" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.first_name}
                    id="first_name"
                    type="text"
                    sizing="md"
                    onChange={(e) =>
                      setData({ ...data, first_name: e.target.value })
                    }
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="last_name" value="Last name*" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.last_name}
                    id="last_name"
                    type="text"
                    sizing="md"
                    onChange={(e) =>
                      setData({ ...data, last_name: e.target.value })
                    }
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="biography" value="Biography" />
                    <Editor
                      content={data.biography}
                      setContent={(newcontent: any) => {
                        const newdata = { ...data, biography: newcontent };
                        setData(newdata);
                      }}
                    />
                  </div>
                  <div className="z-10 rounded-md border-[1px] p-1">
                    {/* <Editor /> */}
                  </div>
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="file" value="User image" />
                  </div>
                  <FileInput id="file" color="info" />
                </div>
              </form>
            </Tabs.Item>
            <Tabs.Item title="Login credential" icon={IoMdUnlock}>
              <form className="grid grid-cols-2 gap-3">
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="email" value="Email*" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.email}
                    id="email"
                    type="email"
                    sizing="md"
                    onChange={(e) =>
                      setData({ ...data, email: e.target.value })
                    }
                  />
                </div>
              </form>
            </Tabs.Item>
            <Tabs.Item title="Social information" icon={TbSocial}>
              <form className="grid grid-cols-2 gap-3">
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="facebook" value="Facebook" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.social_links?.facebook}
                    id="facebook"
                    type="text"
                    sizing="md"
                    onChange={(e) =>
                      setData({
                        ...data,
                        social_links: {
                          ...data.social_links,
                          facebook: e.target.value,
                        },
                      })
                    }
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="twiter" value="Twiter" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.social_links?.twitter}
                    id="twiter"
                    type="text"
                    sizing="md"
                    onChange={(e) =>
                      setData({
                        ...data,
                        social_links: {
                          ...data.social_links,
                          twitter: e.target.value,
                        },
                      })
                    }
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="linkedin" value="Linked In" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.social_links?.linkedin}
                    id="linkedin"
                    type="text"
                    sizing="md"
                    onChange={(e) =>
                      setData({
                        ...data,
                        social_links: {
                          ...data.social_links,
                          linkedin: e.target.value,
                        },
                      })
                    }
                  />
                </div>
              </form>
            </Tabs.Item>
            <Tabs.Item title="Payment info" icon={MdPayment}>
              <form className="grid grid-cols-2 gap-3">
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="bank" value="Bank" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.bank}
                    id="bank"
                    type="text"
                    sizing="md"
                    onChange={(e) => setData({ ...data, bank: e.target.value })}
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label
                      htmlFor="bank_account_name"
                      value="Bank account name"
                    />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.bank_account_name}
                    id="bank_account_name"
                    type="text"
                    sizing="md"
                    onChange={(e) =>
                      setData({ ...data, bank_account_name: e.target.value })
                    }
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label
                      htmlFor="bank_account_no"
                      value="Bank account number"
                    />
                  </div>
                  <TextInput
                    className="eTextinput"
                    value={data.bank_account_no}
                    id="bank_account_no"
                    type="number"
                    sizing="md"
                    onChange={(e) =>
                      setData({ ...data, bank_account_no: e.target.value })
                    }
                  />
                </div>
              </form>
            </Tabs.Item>
          </Tabs.Group>
        </Card>
      </div>
    </div>
  );
};

export default Form;
Form.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
