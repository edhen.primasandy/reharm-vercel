import {
  Tab,
  TabPanel,
  Tabs,
  TabsBody,
  TabsHeader,
} from '@material-tailwind/react';
import { Breadcrumb, Button, Card, Label, Modal } from 'flowbite-react';
import router from 'next/router';
import React, { useEffect } from 'react';
import { HiHome, HiOutlineDocumentAdd, HiTrash } from 'react-icons/hi';
import { toast } from 'react-toastify';
import Swal from 'sweetalert2';

import { InputSelect } from '@/components/elements';
import { useActions } from '@/overmind/index';
import AdminLayout from '@/templates/Admin';

const Layout = () => {
  const rootRef = React.useRef<HTMLDivElement>(null);
  const { createPortofolio, getsPortofolio, deletePortofolio } = useActions();
  // const [dataEvent, setDataEvent] = React.useState([]);
  const [dataGallery, setDataGallery] = React.useState([]);
  const [dataPartner, setDataPartner] = React.useState([]);
  const [showForm, setShowForm] = React.useState(false);
  const [formType, setFormType] = React.useState('All');
  const [loading, setLoading] = React.useState(false);

  const [form, setForm] = React.useState({
    name: '',
    type: '',
    files: [] as any[],
  });
  const options = [
    // { value: 'event', label: 'Event' },
    { value: 'partner', label: 'Partner' },
    { value: 'gallery', label: 'Gallery' },
  ];

  const init = () => {
    setLoading(true);
    Promise.all([
      // getsPortofolio({ type: 'event' }),
      getsPortofolio({ type: 'partner' }),
      getsPortofolio({ type: 'gallery' }),
    ])
      .then((res) => {
        const [resPartner, resGallery] = res;
        // setDataEvent(resEvent.data.items);
        setDataPartner(resPartner.data.items);
        setDataGallery(resGallery.data.items);
      })
      .finally(() => setLoading(false));
  };
  useEffect(() => {
    init();

    return () => {};
  }, []);

  const handleFileChange = (e: any) => {
    const files = Array.from(e.target.files);
    console.log(files);
    setForm({ ...form, files });
  };

  const onSubmit = () => {
    // Upload each file to the server one by one
    const uploadPromises = form.files.map((file) => {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          const base64String = reader.result;
          resolve(
            createPortofolio({
              name: file.name,
              type: form.type,
              image_url: base64String,
            })
          );
        };
        reader.onerror = (error) => reject(error);
      });
    });
    setLoading(true);
    setShowForm(false);
    // Reset selectedFiles state after upload
    Promise.all(uploadPromises)
      .then(() => {
        setForm({ files: [], name: '', type: '' });
        init();
      })
      .catch((error) => console.error('Upload error:', error))
      .finally(() => setLoading(false));
  };
  const onDetele = (item: any) => {
    Swal.fire({
      title: 'Are you sure? ',
      text: 'You want to delete this image',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        deletePortofolio({ id: item.id }).then((res) => {
          init();
          toast.success(res.message);
        });
      }
    });
  };
  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Portfolio</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Portfolio</h1>
          <Button
            gradientMonochrome="info"
            onClick={() => {
              setFormType('Add');
              setShowForm(true);
            }}
          >
            <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
            Add new
          </Button>
        </div>
      </div>
      {loading ? (
        <div className="flex h-10 w-full items-center justify-center">
          <div className="animate-pulse rounded-full bg-fuchsia-200 px-3 py-1 text-center text-xs font-medium leading-none text-fuchsia-800">
            loading...
          </div>
        </div>
      ) : (
        <div className="mx-5 mb-5">
          <Card className="mt-2">
            <Tabs value="partner">
              <TabsHeader
                className="border-blue-gray-50 rounded-none border-b bg-transparent p-0"
                indicatorProps={{
                  className:
                    'bg-transparent border-b-2 border-pink-600 shadow-none rounded-none',
                }}
              >
                {options.map(({ label, value }) => (
                  <Tab key={value} value={value}>
                    {label}
                  </Tab>
                ))}
              </TabsHeader>
              <TabsBody>
                {options.map(({ value }) => (
                  <TabPanel key={value} value={value}>
                    {/* {value === 'event' && (
                      <div className="grid grid-cols-4 gap-4">
                        {dataEvent?.map((item: any, i: number) => (
                          <div className="rounded-lg" key={i}>
                            <div
                              style={{
                                backgroundImage: `url(${item?.image_url})`,
                              }}
                              className="h-44 rounded-lg bg-cover duration-200 ease-in hover:scale-105"
                            >
                              <div className="flex h-full flex-1 flex-col justify-end rounded-lg bg-black bg-opacity-20 px-4 py-2">
                                <div className="flex flex-row items-center gap-2">
                                  <span className="text-sm text-slate-100">
                                    {item.type}
                                  </span>
                                </div>
                                <div className="flex items-center justify-between">
                                  <span className="truncate text-base text-white">
                                    {item.name}
                                  </span>
                                  <button onClick={() => onDetele(item)}>
                                    <HiTrash size={20} color="#fff" />
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    )} */}
                    {value === 'partner' && (
                      <div className="grid grid-cols-4 gap-4">
                        {dataPartner?.map((item: any, i: number) => (
                          <div
                            className="relative my-3 flex h-auto flex-col rounded-2xl bg-white shadow-md duration-200 ease-in hover:scale-105 hover:shadow-md"
                            key={i}
                          >
                            <div
                              // style={{
                              //   backgroundImage: `url(${item?.image_url})`,
                              // }}
                              className="h-44"
                            >
                              <div className="h-20">
                                <img
                                  src={item.image_url}
                                  className="h-20 w-full object-contain"
                                  alt=""
                                />
                              </div>
                              <div className="lex-1 flex flex-col justify-end px-4 py-2">
                                <div className="flex flex-row items-center gap-2">
                                  <span className="text-sm text-gray-700">
                                    {item.type}
                                  </span>
                                </div>
                                <div className="flex items-center justify-between">
                                  <span className="truncate text-base text-black">
                                    {item.name}
                                  </span>
                                  <button onClick={() => onDetele(item)}>
                                    <HiTrash size={20} color="black" />
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    )}
                    {value === 'gallery' && (
                      <div className="grid grid-cols-4 gap-4">
                        {dataGallery?.map((item: any, i: number) => (
                          <div className="rounded-lg" key={i}>
                            <div
                              style={{
                                backgroundImage: `url(${item?.image_url})`,
                              }}
                              className="h-44 rounded-lg bg-cover duration-200 ease-in hover:scale-105"
                            >
                              <div className="flex h-full flex-1 flex-col justify-end rounded-lg bg-black bg-opacity-20 px-4 py-2">
                                <div className="flex flex-row items-center gap-2">
                                  <span className="truncate text-sm text-slate-100">
                                    {item.type}
                                  </span>
                                </div>
                                <div className="flex items-center justify-between">
                                  <span className="truncate text-base text-white">
                                    {item.name}
                                  </span>
                                  <button onClick={() => onDetele(item)}>
                                    <HiTrash size={20} color="#fff" />
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    )}
                  </TabPanel>
                ))}
              </TabsBody>
            </Tabs>
          </Card>
        </div>
      )}

      <Modal
        root={rootRef.current ?? undefined}
        show={showForm}
        size="md"
        popup
        onClose={() => {
          setShowForm(false);
        }}
      >
        <Modal.Header />
        <Modal.Body>
          <div className="space-y-6">
            <h3 className="text-xl font-medium text-gray-900 dark:text-white">
              {formType === 'Add' ? 'Add new' : 'Edit'} Portfolio
            </h3>
            {/* <div>
              <div className="mb-2 block">
                <Label htmlFor="name" value="Name" />
              </div>
              <TextInput
                className="eTextinput"
                required
                value={form.name}
                id="name"
                type="text"
                sizing="md"
                onChange={(e) => {
                  const newdata = { ...form, name: e.target.value };
                  setForm({ ...newdata });
                }}
              />
            </div> */}
            <div>
              <InputSelect
                label="Type"
                data={options}
                value={form.type}
                placeholder="Select type"
                onChange={(e: any) => {
                  const newdata = { ...form, type: e.value };
                  setForm({ ...newdata });
                }}
              />
            </div>
            <div className="mb-6">
              <div className="mb-2 block">
                <Label htmlFor="name" value="Upload files" />
              </div>
              <div className="mb-8">
                <input
                  type="file"
                  name="file"
                  id="file"
                  className="sr-only"
                  multiple
                  onChange={handleFileChange}
                />
                <label
                  htmlFor="file"
                  className="relative flex min-h-[100px] items-center justify-center rounded-md border border-dashed border-[#e0e0e0] p-4 text-center"
                >
                  <div>
                    <span className="mb-2 block text-xl font-semibold text-[#07074D]">
                      Drop files here
                    </span>
                    <span className="mb-2 block text-base font-medium text-[#6B7280]">
                      Or
                    </span>
                    <span className="inline-flex rounded border border-[#e0e0e0] py-2 px-7 text-base font-medium text-[#07074D]">
                      Browse
                    </span>
                  </div>
                </label>
              </div>
            </div>
            <div>
              <h3>Selected Images:</h3>
              <ul>
                {form.files?.map((file, index) => (
                  <li key={index}>{file.name}</li>
                ))}
              </ul>
            </div>
            <div className="flex justify-end">
              <Button onClick={() => onSubmit()}>Submit</Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default Layout;

Layout.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
