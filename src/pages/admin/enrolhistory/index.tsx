import ButtonComp from '@components/elements/button';
import ETable from '@components/elements/table';
import UserImage from '@images/user-img-default.png';
import { Breadcrumb, Button, Label, Modal, Table } from 'flowbite-react';
import debounce from 'lodash/debounce';
import moment from 'moment';
import Image from 'next/image';
import router from 'next/router';
import React from 'react';
import {
  HiHome,
  HiOutlineDocumentAdd,
  HiOutlineFilter,
  HiTrash,
} from 'react-icons/hi';
import AsyncSelect from 'react-select/async';
import { toast } from 'react-toastify';
import Swal from 'sweetalert2';

import InputSelect from '@/components/elements/input-select';
import { useActions, useState } from '@/overmind/index';
import AdminLayout from '@/templates/Admin';

const Courses = () => {
  const {
    getListEnrol,
    deleteEnrol,
    resetFilter,
    getsAdminCourses,
    getsUsers,
    createEnrol,
  } = useActions();

  const { filter } = useState();
  const [loading, setLoading] = React.useState(false);
  const [ready, setReady] = React.useState(false);
  const [data, setData] = React.useState({} as any);
  const [courses, setCourses] = React.useState([] as any);
  const [selectedCourse, setSelectedCourse] = React.useState(null as any);
  const [selectedUser, setSelectedUser] = React.useState(null as any);
  const [selectedCourseForm, setSelectedCourseForm] = React.useState(
    null as any
  );
  const [selectedUserForm, setSelectedUserForm] = React.useState(null as any);
  const [showForm, setShowForm] = React.useState(false);
  const [dataForm, setDataForm] = React.useState({
    user_id: '',
    course_id: '',
  });
  const header = [
    { title: '#', field: 'no' },
    { title: 'Photo', field: 'photo' },
    { title: 'User name', field: 'user' },
    { title: 'Enrolled course', field: 'course' },
    { title: 'Enrolled date', field: 'date' },
    { title: 'Status', field: 'status' },
  ];
  const initData = () => {
    setLoading(true);
    let payload: any = { ...filter };
    if (selectedCourse !== null) {
      payload = {
        ...payload,
        course_id: selectedCourse.value,
      };
    }
    if (selectedUser !== null) {
      payload = {
        ...payload,
        user_id: selectedUser.value,
      };
    }
    getListEnrol(payload)
      .then((res: any) => {
        setData(res);
      })
      .finally(() => setLoading(false));
  };
  React.useEffect(() => {
    getsAdminCourses({ status: 'active', sort_type: 'asc' }).then((res) => {
      const newcourses: any = [];
      res.items.forEach((o: any) => {
        newcourses.push({
          value: o.id,
          label: o.title,
        });
      });
      setCourses(newcourses);
    });
    return () => {};
  }, []);
  React.useEffect(() => {
    if (ready) {
      initData();
    }
    return () => {};
  }, [filter, ready]);
  React.useEffect(() => {
    resetFilter().then(() => setReady(true));
    return () => {};
  }, []);

  const onDelete = (id: any) => {
    Swal.fire({
      title: 'Are you sure? ',
      text: 'You want to delete this data',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        deleteEnrol({ id }).then((res) => {
          toast.success(res.message);
          initData();
        });
      }
    });
  };
  const getAsyncOptions = (inputText: string) => {
    return getsUsers({ keyword: inputText.toLowerCase() }).then((res) => {
      return res.items.map((o: any) => ({
        value: o.id,
        label: `${o.first_name} ${o.last_name}`,
      }));
    });
  };
  const loadOptions = React.useCallback(
    debounce((inputText, callback) => {
      getAsyncOptions(inputText).then((options) => callback(options));
    }, 500),
    []
  );
  const rootRef = React.useRef<HTMLDivElement>(null);
  const onSave = () => {
    if (selectedUserForm === null || selectedCourseForm === null) {
      toast.error('form input can not empty');
    } else {
      createEnrol(dataForm)
        .then(() => {
          toast.success('data has been updated');
          initData();
        })
        .catch((e) => toast.error(e.message))
        .finally(() => {
          setShowForm(false);
          setDataForm({ user_id: '', course_id: '' });
        });
    }
  };
  return (
    <div className="w-full">
      <Breadcrumb aria-label="breadcrumb">
        <Breadcrumb.Item
          href="#"
          onClick={() => router.push('/admin/dashboard')}
          icon={HiHome}
        >
          Home
        </Breadcrumb.Item>
        <Breadcrumb.Item>Enrol history</Breadcrumb.Item>
      </Breadcrumb>
      <div className="mx-5 mb-5">
        <div className="mt-10 flex w-full flex-row items-center justify-between">
          <h1 className="text-2xl font-semibold">Enrol history</h1>
          <Button
            gradientMonochrome="info"
            onClick={() => {
              setShowForm(true);
            }}
          >
            <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
            Add new enrol
          </Button>
        </div>
      </div>
      <div className="mx-5 rounded bg-white p-5 shadow-md">
        <div className="mb-5 flex flex-row gap-4">
          <div className="flex-1">
            <div className="grid grid-cols-4 gap-3">
              <InputSelect
                isClearable
                label="Course"
                data={courses}
                value={selectedCourse}
                placeholder="Select course"
                onChange={(e: any) => {
                  setSelectedCourse(e);
                }}
              />
              <div>
                <div className="mb-2 block">
                  <Label value={'User'} />
                </div>
                <AsyncSelect
                  // cacheOptions
                  classNamePrefix={'eSelect'}
                  loadOptions={loadOptions}
                  placeholder="Select user"
                  defaultValue={[]}
                  isClearable
                  defaultOptions={[]}
                  value={selectedUser}
                  onChange={(e: any) => {
                    setSelectedUser(e);
                  }}
                />
              </div>
            </div>
          </div>
          <div className="flex items-end">
            <Button
              gradientMonochrome="pink"
              onClick={() => {
                initData();
              }}
            >
              <HiOutlineFilter className="mr-2 h-5 w-5" />
              Filter
            </Button>
          </div>
        </div>
        <ETable
          header={header}
          data={data.items}
          totalData={data.total ?? 0}
          pageCount={10}
          searchText=""
          haveAction
          customTableBody
          renderTableBody={
            <Table.Body className="divide-y">
              {loading && (
                <Table.Row className="bg-white">
                  <Table.Cell colSpan={8}>
                    <div className="flex h-10 w-full items-center justify-center">
                      <div className="animate-pulse rounded-full bg-fuchsia-200 px-3 py-1 text-center text-xs font-medium leading-none text-fuchsia-800">
                        loading...
                      </div>
                    </div>
                  </Table.Cell>
                </Table.Row>
              )}
              {!loading &&
                data?.items?.map((item: any, i: number) => {
                  return (
                    <Table.Row className="bg-white" key={i}>
                      <Table.Cell> {filter.skip + i + 1}</Table.Cell>
                      <Table.Cell>
                        <div>
                          {item.user.photo ? (
                            <img
                              className="h-6 w-6 rounded-full shadow-lg"
                              src={item.user.photo}
                              alt="user photo"
                            />
                          ) : (
                            <Image
                              className="h-6 w-6 rounded-full shadow-lg"
                              src={UserImage}
                              alt={''}
                            />
                          )}
                        </div>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm text-gray-800">
                          {item.user.first_name} {item.user.last_name}
                        </p>
                        <p className="text-sm">Email: {item.user.email}</p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm font-semibold text-violet-600">
                          {item.course.title}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">
                          {moment(item.created_at).format('ddd, DD MMM YYYY')}
                        </p>
                      </Table.Cell>
                      <Table.Cell>
                        <p className="text-sm">{item.status}</p>
                      </Table.Cell>
                      <Table.Cell width={100}>
                        <ButtonComp
                          className="ml-3"
                          onClick={() => onDelete(item.id)}
                        >
                          <HiTrash color="gray" size={20} />
                        </ButtonComp>
                      </Table.Cell>
                    </Table.Row>
                  );
                })}
            </Table.Body>
          }
        />
      </div>
      <Modal
        root={rootRef.current ?? undefined}
        show={showForm}
        size="lg"
        popup
        onClose={() => {
          setShowForm(false);
          setDataForm({ user_id: '', course_id: '' });
          setSelectedCourseForm(null);
          setSelectedUserForm(null);
        }}
      >
        <Modal.Header />
        <Modal.Body>
          <div className="space-y-6">
            <h3 className="text-xl font-medium text-gray-900 dark:text-white">
              Add new enrol
            </h3>
            <div>
              <InputSelect
                isClearable
                label="Course"
                data={courses}
                value={selectedCourseForm}
                placeholder="Select course"
                onChange={(e: any) => {
                  setSelectedCourseForm(e);
                  setDataForm({ ...dataForm, course_id: e.value });
                }}
              />
            </div>
            <div>
              <div className="mb-2 block">
                <Label value={'User'} />
              </div>
              <AsyncSelect
                // cacheOptions
                classNamePrefix={'eSelect'}
                loadOptions={loadOptions}
                placeholder="Select user"
                defaultValue={[]}
                isClearable
                defaultOptions={[]}
                value={selectedUserForm}
                onChange={(e: any) => {
                  setSelectedUserForm(e);
                  setDataForm({ ...dataForm, user_id: e.value });
                }}
              />
            </div>
            <div className="flex justify-end">
              <Button onClick={() => onSave()}>Save</Button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
};

export default Courses;

Courses.getLayout = (page: any) => {
  return <AdminLayout>{page}</AdminLayout>;
};
