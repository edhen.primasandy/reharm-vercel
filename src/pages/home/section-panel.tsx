import styles from '@styles/pages/home/sectionHeader.module.scss';
import React from 'react';
import { Fade } from 'react-awesome-reveal';
import { GiDrowning, GiDrumKit, GiMusicalScore } from 'react-icons/gi';

function SectionPanel() {
  return (
    <Fade duration={500} cascade direction="up">
      <section className={styles.stickyComponent}>
        <div className="container mx-auto flex items-center justify-center">
          <div className="flex flex-row justify-center">
            <div className="flex w-3/4 flex-col gap-4 rounded-3xl bg-white p-4 shadow-lg md:grid md:grid-cols-3">
              <div>
                <div
                  className={`flex flex-row items-center gap-4 rounded-2xl py-2 px-4 shadow-sm `}
                >
                  <div>
                    <GiMusicalScore size={50} color={'#378ad3'} />
                  </div>

                  <div className="flex flex-col justify-start text-left">
                    <span className="font-bold">Materi</span>
                    <span className="text-sm text-gray-600">
                      Kita punya materi lengkap dari pemula sampai advance, dari
                      tidak bisa sampai jago
                    </span>
                  </div>
                </div>
              </div>
              <div>
                <div
                  className={`flex flex-row items-center gap-4 rounded-2xl py-2 px-4 shadow-sm `}
                >
                  <div>
                    <GiDrumKit size={50} color={'#d3082e'} />
                  </div>

                  <div className="flex flex-col justify-start text-left">
                    <span className="font-bold">Instrument</span>
                    <span className="text-sm text-gray-600">
                      Kita punya berbagai macam instrument yang bisa dipelajari
                      tapi tentu saja PIANO adalah utamanya
                    </span>
                  </div>
                </div>
              </div>
              <div>
                <div
                  className={`flex flex-row items-center gap-4 rounded-2xl py-2 px-4 shadow-sm `}
                >
                  <div>
                    <GiDrowning size={50} color={'#a76105'} />
                  </div>

                  <div className="flex flex-col justify-start text-left">
                    <span className="font-bold">Mentor</span>
                    <span className="text-sm text-gray-600">
                      Mentor utama kita adalah bang David Josade beserta
                      teman-temannya
                    </span>
                  </div>
                </div>
              </div>
              {/* {BootcampData.map((item, i) => {
              return (
                <div
                  key={i}
                  // style={{
                  //   backgroundImage: `url(${item.image})`,
                  //   backgroundSize: 'cover',
                  // }}
                >
                  <Button
                    className={`flex flex-col rounded-2xl py-2 px-4 shadow-sm hover:shadow-lg`}
                  >
                    <span className="font-bold">{item.name}</span>
                    <span className="text-gray-600">{item.instrument}</span>
                  </Button>
                </div>
              );
            })} */}
            </div>
          </div>
        </div>
      </section>
    </Fade>
  );
}

export default SectionPanel;
