import ImgInstrument from '@images/img-instrument.jpeg';
import ImgMateri from '@images/img-materi.jpeg';
import ImgMentor from '@images/img-mentor.jpeg';
import Image from 'next/image';
import React from 'react';
import { Fade } from 'react-awesome-reveal';

const Layout = () => {
  const datas = [
    {
      title: 'Materi',
      description:
        'Kurikulum sangatlah penting dalam belajar, terstruktur dan rapi adalah kunci untuk bisa mengatur jadwal latihan dan bermain musik dengan benar. Di Reharm, materinya sudah tersusun sesuai dengan langkah-langkah mentor dalam mempelajari musik.',
      imageUrl: ImgMateri,
    },
    {
      title: 'Instrument',
      description:
        'Musik adalah bahasa yang unik karena disampaikan melalui melodi dan harmoni, Ada berbagai macam instrument yang bisa dipelajari untuk bermain musik dan Piano adalah salah satunya yang wajib diketahui, karena dalam Piano kita bisa memainkan Melodi, Rhythm dan Chord sekaligus',
      imageUrl: ImgInstrument,
    },
    {
      title: 'Mentor',
      description:
        'Pengalaman adalah guru yang berharga, dan guru yang berpengalaman adalah masternya! David Josade, Musisi, Pengajar, Konten Kreator yang terkenal dengan slogannya "Stay Jazz and Gospel" telah membantu banyak musisi dalam menemukan jati dirinya.',
      imageUrl: ImgMentor,
    },
  ];
  const bg = 'https://edhenprimasandy.sirv.com/reharm/abstract-1.png';
  const bg2 = 'https://edhenprimasandy.sirv.com/reharm/graph_1.png';
  return (
    <section className="container relative mx-auto mt-10 items-center p-4">
      <img src={bg} alt="" className="absolute w-3/4 opacity-20" />
      <img
        src={bg2}
        alt=""
        className="absolute bottom-5 right-0 w-1/4 opacity-20"
      />

      {datas.map((item, i) => (
        <Fade
          key={i}
          duration={700}
          cascade
          direction={i % 2 !== 0 ? 'left' : 'right'}
        >
          <div
            key={i}
            className={`flex items-center justify-between rounded-lg p-6 ${
              i % 2 !== 0 ? 'flex-row-reverse' : ''
            }`}
          >
            <div className="flex w-1/2 justify-center">
              <Image
                className="z-10 h-auto w-[500px] rounded-lg"
                src={item.imageUrl}
                alt={''}
              />
            </div>
            <div
              className={`w-1/2 ${i % 2 !== 0 ? 'pr-2 text-right' : 'pl-2'}`}
            >
              <h2 className="mb-5 text-3xl font-bold">{item.title}</h2>
              <p className="text-lg text-gray-700">{item.description}</p>
            </div>
          </div>
        </Fade>
      ))}
    </section>
  );
};

export default Layout;
