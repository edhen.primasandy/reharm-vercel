import React, { useEffect } from 'react';
import Carousel, { Modal, ModalGateway } from 'react-images';
import Gallery from 'react-photo-gallery';

import { useActions } from '@/overmind';

const GalleryComponent = () => {
  const { getsPortofolio } = useActions();
  const [photos, setPhotos] = React.useState([] as any[]);

  const [currentImage, setCurrentImage] = React.useState(0);
  const [viewerIsOpen, setViewerIsOpen] = React.useState(false);

  const openLightbox = React.useCallback((_event: any, { index }: any) => {
    setCurrentImage(index);
    setViewerIsOpen(true);
  }, []);

  const closeLightbox = () => {
    setCurrentImage(0);
    setViewerIsOpen(false);
  };

  const loadImages = async (result: any) => {
    const imagePromises = result.map((item: any) => {
      return new Promise((resolve) => {
        const img = new Image();
        img.src = item.image_url;
        img.onload = () =>
          resolve({
            src: item.image_url,
            width: img.width,
            height: img.height,
          });
      });
    });
    await Promise.all(imagePromises).then((r) => {
      setPhotos([...r]);
    });
  };
  useEffect(() => {
    getsPortofolio({ type: 'gallery' }).then((res: any) => {
      loadImages(res.data.items);
      // setPhotos(
      //   res.data.items.map((o: any) => {
      //     return {
      //       src: o.image_url,
      //       width: 1,
      //       height: 1,
      //     };
      //   })
      // );
    });

    return () => {};
  }, []);
  // const photos = imageData.images.map((image) => ({
  //   src: image.src,
  //   width: image.width,
  //   height: image.height,
  // }));

  return (
    <section className="container mx-auto items-center p-4">
      <h2 className="mb-10 text-center text-3xl font-semibold">
        Our Photo Gallery
      </h2>
      <Gallery photos={photos} onClick={openLightbox} />
      {/* @ts-ignore */}
      <ModalGateway>
        {viewerIsOpen ? (
          <Modal onClose={closeLightbox}>
            <Carousel
              currentIndex={currentImage}
              views={photos.map((x: any) => ({
                ...x,
                srcset: x.srcSet,
                caption: x.title,
              }))}
            />
          </Modal>
        ) : null}
      </ModalGateway>
    </section>
  );
};

export default GalleryComponent;
