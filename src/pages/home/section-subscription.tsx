import { Button } from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import { Fade } from 'react-awesome-reveal';
import { BsCheck2Circle } from 'react-icons/bs';
import { FaCrown } from 'react-icons/fa';
import { HiOutlineArrowRight } from 'react-icons/hi';
import { toast } from 'react-toastify';

import { useActions, useState } from '@/overmind';
import helper from '@/utils/helper';

const Subscription = () => {
  const router = useRouter();
  const { getListSubscription, createOrderSubscription, getListOrder } =
    useActions();
  const { user } = useState();
  const [data, setData] = React.useState([]);
  // const [buttonLabel, setButtonLabel] = React.useState('CHOOSE PLAN');
  const [disabled, setDisabled] = React.useState(false);
  React.useEffect(() => {
    if (user?.id) {
      getListSubscription({ user_id: user.id }).then((res) => {
        setDisabled(
          !!res.items.find((o: any) => o.is_purchased || o.is_in_cart)
        );
        setData(res.items);
      });
    } else {
      getListSubscription({}).then((res) => {
        setData(res.items);
      });
    }

    return () => {};
  }, [user]);
  const choosePlan = (item: any) => {
    if (user === null) {
      router.push('/login');
    } else {
      const payload = {
        user_id: user.id,
        item_details: [
          {
            subscription_id: item.id,
            price: item.price,
            name: item.name,
            quantity: 1,
            discount_flag: item.discounted_price ? 'yes' : 'no',
            discount_price: item.discounted_price ?? 0,
          },
        ],
        total_price: item.discounted_price ? item.discounted_price : item.price,
        created_at: new Date().toISOString(),
        updated_at: new Date().toISOString(),
      };
      createOrderSubscription(payload).then(() => {
        getListOrder({ user_id: user.id });
        toast.success('Success order, Please check your cart');
      });
    }
  };
  return (
    <Fade duration={500} cascade direction="left">
      <section className="container mx-auto items-center p-4">
        <h2 className="mb-2 text-3xl font-semibold">Choose your plan</h2>
        <div className="grid gap-5 sm:grid-cols-1 md:grid-cols-3">
          {data.map((item: any, i: number) => (
            <div key={i} className="relative rounded-2xl bg-white shadow-md">
              {item.is_top_subscription === 'yes' && (
                <div className="absolute right-0 flex items-center gap-5 rounded-bl-[3rem] rounded-tr-2xl bg-green-400 px-7 py-3">
                  <FaCrown color="#ffff" size={30} />
                  <span className="text-lg font-semibold text-white">
                    BEST VALUE
                  </span>
                </div>
              )}
              <div className="flex h-auto flex-col p-7 ">
                <div className="my-5 flex flex-col items-center justify-center">
                  <div className="my-5 rounded-full bg-blue-200 px-5 py-3">
                    <h2 className="text-xl font-semibold text-blue-700">
                      {item.name}
                    </h2>
                  </div>
                  <div className="text-center text-gray-500">
                    <p>START FROM</p>
                    <p className="mt-2 text-lg font-bold text-gray-600 line-through">
                      {helper.currencyFormat(item.price)}
                    </p>
                    <span className="mb-2 text-2xl font-bold text-blue-600">
                      {helper.currencyFormat(item.discounted_price)}
                    </span>
                    <p>
                      PER {item.period} {item.period < 12 ? 'MONTH' : 'YEAR'}
                    </p>
                  </div>
                </div>
                <div className="mb-5 border-t border-gray-200 py-3">
                  <h2 className="mb-2 font-bold">What you get</h2>
                  {item?.features?.map((feature: string) => (
                    <div
                      key={feature}
                      className="mb-1 flex flex-row items-center gap-3"
                    >
                      <BsCheck2Circle color="#26bf0a" size={30} />
                      <span className="text-sm">{feature}</span>
                    </div>
                  ))}
                </div>
                <div className="flex h-full items-end justify-center">
                  <Button
                    disabled={disabled}
                    onClick={() => {
                      choosePlan(item);
                    }}
                    gradientDuoTone="cyanToBlue"
                  >
                    {disabled ? 'PURCHASED' : 'CHOOSE PLAN'}
                    <HiOutlineArrowRight className="ml-2 h-5 w-5" />
                  </Button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </section>
    </Fade>
  );
};

export default Subscription;
