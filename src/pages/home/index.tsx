import styles from '@styles/pages/home/sectionHeader.module.scss';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { Fade } from 'react-awesome-reveal';

import SectionAbout from './section-about';
import SectionBlog from './section-blog';
import SectionCategories from './section-categories';
import SectionCourses from './section-courses';
import SectionInstruments from './section-instruments';
import SectionPanel from './section-panel';
import SectionPartnerSlide from './section-partner-slide';
import SectionPhotoGallery from './section-photos-galery';
import SectionSubscription from './section-subscription';
import Slider from './slider-recomended';

// type Props = {};

const Home = () => {
  const router = useRouter();

  React.useEffect(() => {
    const path = window.location.pathname;

    // Redirect based on path
    if (path.startsWith('/courses/')) {
      const courseId = path.split('/')[2]; // Get the dynamic part
      router.replace(`/courses/${courseId}`);
    }
  }, [router]);

  return (
    <div>
      {/* first element */}
      <section className={`${styles.setionHeader} mx-[-20px]`}>
        <Fade duration={700} cascade direction="up">
          <div className="container mx-auto py-5">
            <div className="grid grid-cols-1 items-center gap-3 md:grid-cols-2">
              <div className="flex flex-1 flex-col p-9">
                <div className="font-semibold text-white drop-shadow-[0_1.2px_1.2px_rgba(0,0,0,0.8)]">
                  Mau belajar JAZZ dan GOSPEL?
                </div>
                <div className="text-3xl font-semibold text-white drop-shadow-lg md:text-6xl">
                  REHARM tempatnya!
                </div>
                <div className="mt-2">
                  <Link
                    href="/register"
                    className="group relative inline-block rounded-full px-5 py-2.5 font-medium text-white"
                  >
                    <span className="absolute top-0 left-0 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 opacity-50 blur-sm"></span>
                    <span className="absolute inset-0 mt-0.5 ml-0.5 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 opacity-50 group-active:opacity-0"></span>
                    <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 shadow-xl transition-all duration-200 ease-out group-hover:blur-sm group-active:opacity-0"></span>
                    <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-br from-red-500 to-orange-400 transition duration-200 ease-out"></span>
                    <span className="relative font-sans text-lg">Join Us</span>
                  </Link>
                </div>
              </div>
              <div className="mb-5 flex flex-1 justify-center">
                <Slider />
              </div>
            </div>
          </div>
        </Fade>
      </section>
      <SectionPanel />
      <SectionAbout />
      <SectionPartnerSlide />
      <SectionPhotoGallery />
      <SectionCourses />
      <SectionInstruments />
      <SectionSubscription />
      <SectionBlog />
      <SectionCategories />
      {/* <SectionInstructors /> */}
    </div>
  );
};

export default Home;
