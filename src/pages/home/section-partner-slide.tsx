/* eslint-disable tailwindcss/no-custom-classname */
/* eslint-disable no-empty-pattern */
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import React, { useEffect, useState } from 'react';
import Slider from 'react-slick';

import { useActions } from '@/overmind';

type Props = {};

const Layout = (props: Props) => {
  const {} = props;
  const { getsPortofolio } = useActions();
  const [data, setData] = useState([] as any[]);
  const settings = {
    dots: false,
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    speed: 5000,
    autoplaySpeed: 100,
    cssEase: 'linear',
  };
  useEffect(() => {
    getsPortofolio({ type: 'partner' }).then((res) => {
      setData(res.data.items.map((o: any) => o.image_url));
    });

    return () => {};
  }, []);

  return (
    <section className="container mx-auto mb-10 items-center p-4">
      <h2 className="mb-10 text-center text-3xl font-semibold">
        Trusted by well known companies
      </h2>
      <div className="slider-container">
        <div className="slider-container-overlay left"></div>
        <div className="slider-container-overlay right"></div>
        <Slider {...settings}>
          {data.map((item) => (
            <div key={item} className="h-20 w-20">
              <img className="h-20 w-20" src={item} alt="" />
            </div>
          ))}
        </Slider>
      </div>
    </section>
  );
};

export default Layout;
