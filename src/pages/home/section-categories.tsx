/* eslint-disable tailwindcss/migration-from-tailwind-2 */
/* eslint-disable tailwindcss/no-custom-classname */
import React from 'react';
import { Slide } from 'react-awesome-reveal';
import { FiCopy } from 'react-icons/fi';

import { useActions } from '@/overmind';

const SectionCategories = () => {
  const { getsCategories } = useActions();
  const [data, setData] = React.useState([] as any[]);

  React.useEffect(() => {
    getsCategories().then((res) => setData(res.items));

    return () => {};
  }, []);

  return (
    <Slide duration={300}>
      <section className="flex bg-slate-800">
        <div className=" container mx-auto items-center px-4 py-20">
          <h2 className="mb-2 text-3xl font-semibold text-white">Categories</h2>
          <div className="grid grid-cols-2 gap-4 md:grid-cols-4">
            {data.map((item, i) => (
              <div className="rounded-lg" key={i}>
                <div
                  style={{ backgroundImage: `url(${item?.thumbnail})` }}
                  className="h-44 rounded-lg duration-200 ease-in hover:scale-105"
                >
                  <div className="flex h-full flex-1 flex-col justify-end bg-black bg-opacity-20 px-4 py-2">
                    <div className="flex flex-row items-center gap-2">
                      <FiCopy size={20} color="#fff" />
                      <span className="text-xl text-white">
                        {item.childs.length} Courses type
                      </span>
                    </div>
                    <span className="text-2xl font-bold text-white">
                      {item.name}
                    </span>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
    </Slide>
  );
};

export default SectionCategories;
