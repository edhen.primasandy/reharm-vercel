import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

import Styles from '@styles/pages/home/sectionCourses.module.scss';
import { Tooltip } from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import { Fade } from 'react-awesome-reveal';
import { BsHeart, BsHeartFill } from 'react-icons/bs';
import { FaChevronCircleLeft, FaChevronCircleRight } from 'react-icons/fa';
import { toast } from 'react-toastify';
import { Navigation, Pagination, Scrollbar } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

import { useActions, useState } from '@/overmind';
import helper from '@/utils/helper';

const Card = ({ item, onReload }: any) => {
  const router = useRouter();
  const { setSelectedCourseFromCourses, updateUser, getUserProfile } =
    useActions();
  const { user } = useState();
  const [onHover, setOnHover] = React.useState(false);
  const [isOnWishlist, setIsOnWishlist] = React.useState(false);
  React.useEffect(() => {
    if (user !== null) {
      if (item?.is_wishlist) {
        setIsOnWishlist(true);
      }
      // if (user?.wishlist.includes(item.id.toString())) {
      //   setIsOnWishlist(true);
      // }
    }
    return () => {};
  }, [user]);
  const updateWishlist = () => {
    if (user === null) {
      router.push('/login');
    } else {
      const newWishlist: any[] = [...user.wishlist];
      if (isOnWishlist) {
        newWishlist.splice(user.wishlist.indexOf(item.id.toString()), 1);
      } else {
        newWishlist.push(item.id.toString());
      }
      const payloadUser = {
        id: user.id,
        first_name: user.first_name,
        last_name: user.last_name,
        email: user.email,
        biography: user.biography,
        province_id: 0,
        regency_id: 0,
        district_id: 0,
        village_id: 0,
        address: user.address,
        wishlist: newWishlist,
      };
      updateUser({ ...payloadUser }).then(() => {
        getUserProfile();
        onReload();
        toast.success('Please check your wishlist');
      });
    }
  };

  return (
    <div className="relative my-3 flex h-auto flex-col rounded-2xl bg-white shadow-md duration-200 ease-in hover:scale-105 hover:shadow-md">
      <div className="h-60">
        {item.thumbnail ? (
          <img
            src={item.thumbnail}
            className="h-60 w-full rounded-t-2xl object-cover"
            alt=""
          />
        ) : (
          <img
            src="https://edhenprimasandy.sirv.com/reharm/no-image.jpg"
            className="h-60 w-full rounded-t-2xl object-cover"
            alt=""
          />
        )}
      </div>
      <div className="absolute right-2 top-2 rounded-lg bg-black bg-opacity-40">
        <Tooltip
          content={isOnWishlist ? 'Remove from wishlist' : 'Add to wishlist'}
          className="text-sm"
          placement="left"
        >
          <button
            className="mx-2 mt-2"
            onClick={() => updateWishlist()}
            type="button"
            onMouseEnter={() => setOnHover(true)}
            onMouseLeave={() => setOnHover(false)}
          >
            {onHover ? (
              <>
                {isOnWishlist ? (
                  <BsHeart size={20} color={'#ffff'} />
                ) : (
                  <BsHeartFill size={20} color={'#ed0485'} />
                )}
              </>
            ) : (
              <>
                {isOnWishlist ? (
                  <BsHeartFill size={20} color={'#ed0485'} />
                ) : (
                  <BsHeart size={20} color={'#ffff'} />
                )}
              </>
            )}
          </button>
        </Tooltip>
      </div>
      <div className="mt-[-20px] rounded-2xl bg-white p-4">
        <div className="border-dashed">
          <span className="mb-2 text-base font-semibold line-clamp-1">
            {item.title}
          </span>
          <div className="mb-2 h-20 border-b-2 pb-2">
            <div className="text-xs line-clamp-3">{item.short_description}</div>
          </div>
          <div className="mb-2 h-20">
            {item?.is_free_course === 'yes' ? (
              <div className="mt-4 flex flex-row flex-wrap items-center gap-2">
                <span className="text-xl font-bold text-orange-600">FREE</span>
              </div>
            ) : (
              <div className="mt-4 flex flex-row flex-wrap items-center gap-2">
                {item?.discounted_price > 0 && (
                  <>
                    <span className="rounded-md bg-red-500 px-2 text-sm text-white">
                      {Math.round(
                        ((item.price - item.discounted_price) / item.price) *
                          100
                      )}{' '}
                      %
                    </span>
                    <span className="text-base line-through">
                      {helper.currencyFormat(item?.price)}
                    </span>
                  </>
                )}

                <span className="text-lg font-bold text-red-600">
                  {helper.currencyFormat(
                    item.discounted_price ? item.discounted_price : item.price
                  )}
                </span>
              </div>
            )}
          </div>
        </div>
        <button
          type="button"
          onClick={() => {
            const href = `/courses/${item.id}`;
            setSelectedCourseFromCourses(item);
            router.push(href);
          }}
          className=" mr-2 mb-2 w-full rounded-full bg-orange-500 px-5 py-2.5 text-sm font-medium text-white hover:bg-orange-600 focus:outline-none focus:ring-4 focus:ring-orange-300 dark:bg-orange-600 dark:hover:bg-orange-700 dark:focus:ring-orange-800"
        >
          View detail
        </button>
      </div>
    </div>
  );
};

const SectionCorses = () => {
  const router = useRouter();
  const [prevEl, setPrevEl] = React.useState<HTMLElement | null>(null);
  const [nextEl, setNextEl] = React.useState<HTMLElement | null>(null);
  const { getsCourses } = useActions();
  const { user } = useState();
  const [data, setData] = React.useState([] as any[]);
  React.useEffect(() => {
    getsCourses({ skip: 0, limit: 5, status: 'active' }).then((res) => {
      setData(res.items);
    });
    return () => {};
  }, [user]);
  const onReload = () => {
    getsCourses({ skip: 0, limit: 5, status: 'active' }).then((res) => {
      setData(res.items);
    });
  };
  return (
    <Fade duration={500} cascade direction="left">
      <section className="container mx-auto items-center p-4">
        <h2 className="mb-2 text-3xl font-semibold">Courses</h2>
        <div className="hidden md:block">
          <Swiper
            pagination={{
              clickable: true,
              bulletActiveClass: `${Styles.swiperpaginationbulletactive}`,
            }}
            modules={[Navigation, Pagination, Scrollbar]}
            navigation={{
              prevEl,
              nextEl,
              enabled: true,
            }}
            spaceBetween={20}
            slidesPerView={5}
            onSlideChange={() => console.log('slide change')}
            onSwiper={(swiper) => console.log(swiper)}
            className={Styles.swiperWrapperCourse}
          >
            {data?.map((item: any, i: number) => {
              return (
                <SwiperSlide key={i}>
                  {' '}
                  <Card item={item} onReload={() => onReload()} />
                </SwiperSlide>
              );
            })}
            <SwiperSlide>
              <div className="my-3 flex h-[32.4rem] w-48 flex-1 flex-col items-center justify-center rounded-2xl bg-indigo-800 shadow-md hover:shadow-lg">
                <span className="text-white">View all courses</span>

                <button
                  className="block h-16 w-16 rounded-full shadow transition-all hover:rotate-90 hover:scale-110 hover:shadow-lg"
                  onClick={() => {
                    const href = `/courses`;
                    router.push(href);
                  }}
                >
                  <div className="flex h-full w-full items-center justify-center rounded-full object-cover">
                    <FaChevronCircleRight size={50} color="#fff" />
                  </div>
                </button>
              </div>
            </SwiperSlide>
            <div
              className={`flex flex-row gap-2 ${Styles.paginationContainer}`}
            >
              <div
                ref={(node) => setPrevEl(node)}
                className="cursor-pointer text-orange-600 hover:text-orange-700"
              >
                <button>
                  <FaChevronCircleLeft size={25} />
                </button>
              </div>
              <div
                ref={(node) => setNextEl(node)}
                className="cursor-pointer text-orange-600 hover:text-orange-700"
              >
                <button>
                  <FaChevronCircleRight size={25} />
                </button>
              </div>
            </div>
          </Swiper>
        </div>
        <div className="grid grid-cols-2 gap-2 md:hidden">
          {data.map((item, i) => {
            return (
              <div className="" key={i}>
                {' '}
                <Card item={item} />
              </div>
            );
          })}
          <div className="my-3">
            <div className="flex h-full w-full flex-col items-center justify-center rounded-2xl bg-indigo-800 shadow-md hover:shadow-lg">
              <span className="text-white">View all courses</span>

              <button
                className="block h-16 w-16 rounded-full shadow transition-all hover:rotate-90 hover:scale-110 hover:shadow-lg"
                onClick={() => {
                  const href = `/courses`;
                  router.push(href);
                }}
              >
                <div className="flex h-full w-full items-center justify-center rounded-full object-cover">
                  <FaChevronCircleRight size={50} color="#fff" />
                </div>
              </button>
            </div>
          </div>
        </div>
      </section>
    </Fade>
  );
};
export default SectionCorses;
