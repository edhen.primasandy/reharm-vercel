/* eslint-disable tailwindcss/no-custom-classname */
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/autoplay';
import 'swiper/css/effect-cards';
import '@styles/pages/home/sectionHeader.module.scss';

import { useRouter } from 'next/router';
import React from 'react';
import { Cookies } from 'react-cookie';
// import required modules
import { EffectCoverflow } from 'swiper';
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

import { useActions } from '@/overmind';
import helper from '@/utils/helper';

const Card = ({ item }: any) => {
  const router = useRouter();
  const { setSelectedCourseFromCourses } = useActions();
  return (
    <div className="flex h-auto flex-col rounded-2xl bg-white text-white shadow-md duration-200 ease-in">
      <div className="h-52">
        {item.thumbnail ? (
          <img
            src={item.thumbnail}
            className="h-52 w-full rounded-t-2xl object-cover"
            alt=""
          />
        ) : (
          <img
            src="https://edhenprimasandy.sirv.com/reharm/no-image.jpg"
            className="h-52 w-full rounded-t-2xl object-cover"
            alt=""
          />
        )}
      </div>
      <div className="mt-[-20px] rounded-2xl bg-sky-900 p-4">
        <div className="border-dashed">
          <span className="mb-2 text-base font-semibold line-clamp-1">
            {item.title}
          </span>
          <div className="mb-2 h-20 border-b-2 pb-2">
            <div className="text-xs  line-clamp-3">
              {item.short_description}
            </div>
          </div>
          <div className="mb-2 h-20">
            {item?.is_free_course === 'yes' ? (
              <div className="mt-4 flex flex-row flex-wrap items-center gap-2">
                <span className="text-xl font-bold text-orange-600">FREE</span>
              </div>
            ) : (
              <div className="mt-4 flex flex-row flex-wrap items-center gap-2">
                {item?.discounted_price > 0 && (
                  <>
                    <span className="rounded-md bg-red-500 px-2 text-sm text-white">
                      {Math.round(
                        ((item.price - item.discounted_price) / item.price) *
                          100
                      )}{' '}
                      %
                    </span>
                    <span className="text-base line-through">
                      {helper.currencyFormat(item?.price)}
                    </span>
                  </>
                )}

                <span className="text-lg font-bold text-red-600">
                  {helper.currencyFormat(
                    item.discounted_price ? item.discounted_price : item.price
                  )}
                </span>
              </div>
            )}
          </div>
        </div>
        <button
          type="button"
          onClick={() => {
            const href = `/courses/${item.id}`;
            setSelectedCourseFromCourses(item);
            router.push(href);
          }}
          className=" mr-2 mb-2 w-full rounded-full bg-orange-500 px-5 py-2.5 text-sm font-medium text-white hover:bg-orange-600 focus:outline-none focus:ring-4 focus:ring-orange-300 dark:bg-orange-600 dark:hover:bg-orange-700 dark:focus:ring-orange-800"
        >
          View detail
        </button>
      </div>
    </div>
  );
};
const Layout = () => {
  const cookies = new Cookies();
  const jwtToken = cookies.get('_reharm_token');
  const { getsPopularCourses } = useActions();
  const [data, setData] = React.useState([] as any);
  React.useEffect(() => {
    if (jwtToken) {
      getsPopularCourses().then((res) => setData(res.items));
    }

    return () => {};
  }, [jwtToken]);
  return (
    <div className="my-10 w-full">
      <>
        <Swiper
          effect={'coverflow'}
          grabCursor={true}
          centeredSlides={true}
          autoplay={true}
          slidesPerView={'auto'}
          coverflowEffect={{
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true,
          }}
          modules={[EffectCoverflow]}
          className="header-slider"
        >
          {data.map((item: any, i: number) => {
            return (
              <SwiperSlide key={i}>
                <Card item={item} />
              </SwiperSlide>
            );
          })}
        </Swiper>
      </>
    </div>
  );
};
export default Layout;
