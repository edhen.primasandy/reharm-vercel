/* eslint-disable no-console */
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

import EButton from '@components/elements/button';
import Styles from '@styles/pages/home/sectionCourses.module.scss';
import { Button } from 'flowbite-react';
import moment from 'moment';
import { useRouter } from 'next/router';
import React from 'react';
import { Fade } from 'react-awesome-reveal';
import { FaChevronCircleLeft, FaChevronCircleRight } from 'react-icons/fa';
import { HiOutlineArrowRight } from 'react-icons/hi';
import { Navigation, Pagination, Scrollbar } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';

import { useActions } from '@/overmind';
import helper from '@/utils/helper';

const Card = ({ item }: any) => {
  const router = useRouter();

  const getUrlThubnail = () => {
    const idV: any = helper.geIdVideoFromUrlYoutube(item.media_url);
    return `https://img.youtube.com/vi/${idV}/0.jpg`;
  };

  return (
    <div className="relative my-3 flex h-auto flex-col rounded-2xl bg-white shadow-md duration-200 ease-in hover:scale-105 hover:shadow-md">
      <div className="h-40">
        {item.media_url ? (
          <img
            src={
              item.media_type === 'image' ? item.media_url : getUrlThubnail()
            }
            className="h-40 w-full rounded-t-2xl object-cover"
            alt=""
          />
        ) : (
          <img
            src="https://edhenprimasandy.sirv.com/reharm/no-image.jpg"
            className="h-40 w-full rounded-t-2xl object-cover"
            alt=""
          />
        )}
      </div>
      <div className="mt-[-20px] rounded-2xl bg-white p-4">
        <div className="border-dashed">
          <span className="text-sm text-gray-700">
            {moment(item.created_at).format('DD MMM YYYY')}
          </span>
          <div>
            <EButton
              onClick={() =>
                router.push({
                  pathname: 'blogs/[...id]',
                  query: { id: item.slug },
                })
              }
            >
              <span className="mb-2 text-left text-base font-semibold line-clamp-1 hover:text-blue-600">
                {item.title}
              </span>
            </EButton>
          </div>
          <div className="mb-2 h-20 pb-2">
            <div className="text-xs line-clamp-3">{item.short_content}</div>
          </div>
        </div>
      </div>
    </div>
  );
};
const SectionBlog = () => {
  const router = useRouter();
  const [prevEl, setPrevEl] = React.useState<HTMLElement | null>(null);
  const [nextEl, setNextEl] = React.useState<HTMLElement | null>(null);
  const { getListBlogs } = useActions();
  const [data, setData] = React.useState([] as any[]);
  const init = () => {
    getListBlogs({ status: 'active', limit: 10 }).then((res) => {
      setData(res.items);
    });
  };
  React.useEffect(() => {
    init();
    return () => {};
  }, []);
  return (
    <Fade duration={500} cascade direction="left">
      <section className="container mx-auto my-5 items-center p-4">
        {/* <h2 className="mb-2 text-3xl font-semibold">Blog</h2> */}
        <div className="hidden md:block">
          <div className="grid grid-cols-12 gap-5">
            <div className="col-span-3">
              <h1 className="whitespace-pre text-4xl font-semibold">{`What's up with\nReharm`}</h1>
              <div className="mb-10 whitespace-pre text-base">
                {`Story, tips and new article,\nread all here.`}
              </div>
              <Button
                gradientDuoTone="pinkToOrange"
                pill
                onClick={() => router.push('blogs')}
              >
                Read more{' '}
                <div className="ml-3">
                  <HiOutlineArrowRight className="h-4 w-4" />
                </div>
              </Button>
            </div>
            <div className="col-span-9">
              <Swiper
                pagination={{
                  clickable: true,
                  bulletActiveClass: `${Styles.swiperpaginationbulletactive}`,
                }}
                modules={[Navigation, Pagination, Scrollbar]}
                navigation={{
                  prevEl,
                  nextEl,
                  enabled: true,
                }}
                spaceBetween={20}
                slidesPerView={3}
                onSlideChange={() => console.log('slide change')}
                onSwiper={(swiper) => console.log(swiper)}
                className={Styles.swiperWrapperCourse}
              >
                {data?.map((item: any, i: number) => {
                  return (
                    <SwiperSlide key={i}>
                      {' '}
                      <Card item={item} />
                    </SwiperSlide>
                  );
                })}
                <div
                  className={`flex flex-row gap-2 ${Styles.paginationContainer}`}
                >
                  <div
                    ref={(node) => setPrevEl(node)}
                    className="cursor-pointer text-orange-600 hover:text-orange-700"
                  >
                    <button>
                      <FaChevronCircleLeft size={25} />
                    </button>
                  </div>
                  <div
                    ref={(node) => setNextEl(node)}
                    className="cursor-pointer text-orange-600 hover:text-orange-700"
                  >
                    <button>
                      <FaChevronCircleRight size={25} />
                    </button>
                  </div>
                </div>
              </Swiper>
            </div>
          </div>
        </div>
        <div className="md:hidden">
          <div>
            <h1 className="whitespace-pre text-4xl font-semibold">{`What's up with\nReharm`}</h1>
            <div className="mb-10 whitespace-pre text-base">
              {`Story, tips and new article,\nread all here.`}
            </div>
            <Button gradientDuoTone="pinkToOrange" pill>
              Read more{' '}
              <div className="ml-3">
                <HiOutlineArrowRight className="h-4 w-4" />
              </div>
            </Button>
          </div>
          <div className="grid grid-cols-2 gap-2 ">
            {data.map((item, i) => {
              return (
                <div className="" key={i}>
                  {' '}
                  <Card item={item} />
                </div>
              );
            })}
          </div>
        </div>
      </section>
    </Fade>
  );
};

export default SectionBlog;
