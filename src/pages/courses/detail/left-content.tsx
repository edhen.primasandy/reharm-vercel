/* eslint-disable no-plusplus */
/* eslint-disable consistent-return */
import { Accordion, Button, Pagination, Rating } from 'flowbite-react';
import parse from 'html-react-parser';
import moment from 'moment';
import router from 'next/router';
import React from 'react';
import { BsCheck2Circle, BsFileText } from 'react-icons/bs';
import { CgToolbarRight } from 'react-icons/cg';
import { MdOndemandVideo } from 'react-icons/md';

import { useActions } from '@/overmind';
// import Sticky from 'react-sticky-el';
import styles from '@/styles/pages/courses/sectionDetailCourses.module.scss';

type Props = {
  data: any;
  dataLesson: any;
  dataSection: any;
};
const LeftContent = (props: Props) => {
  const { data, dataLesson, dataSection } = props;
  const { getListRatings } = useActions();
  const [ratingSummary, setRatingSummary] = React.useState([] as any);
  const [currPage, setCurrPage] = React.useState(1);
  const [totalPages, setTotalPages] = React.useState(1);
  const [filter, setFilter] = React.useState({
    skip: 0,
    limit: 5,
    sort: '',
    sort_type: 'desc',
  });
  const [listReview, setListreview] = React.useState([] as any);
  const handleScrollElement = (id: string) => {
    const yOffset = -200;
    const element = document.getElementById(id);
    if (element) {
      const y =
        element.getBoundingClientRect().top + window.pageYOffset + yOffset;

      window.scrollTo({ top: y, behavior: 'smooth' });
    }
  };
  React.useEffect(() => {
    if (data !== null) {
      const remainder = data.rating_total % filter.limit;
      let count = data.rating_total / filter.limit;
      if (remainder > 0) {
        count = Math.ceil(count);
      }
      if (count) {
        setTotalPages(count);
      }
      const ratings = [];
      for (let star = 1; star <= 5; star++) {
        const obj: any = {
          rating: star,
          percentage: 0,
          total: 0,
        };
        const findRating = data?.rating_group_by?.find(
          (o: any) => o.rating === star
        );
        if (findRating) {
          obj.percentage = (findRating.total / data.rating_total) * 100;
          obj.total = findRating.total;
        }
        ratings.push(obj);
      }
      setRatingSummary([...ratings.reverse()]);
    }
    return () => {};
  }, [data]);
  React.useEffect(() => {
    if (data?.id) {
      getListRatings({
        ...filter,
        course_id: data?.id,
        sort_type: 'desc',
      }).then((res) => {
        setListreview(res.items);
      });
    }

    return () => {};
  }, [data?.id, filter]);
  const onPageChange = (page: number) => {
    if (currPage !== page) {
      setFilter({ ...filter, skip: (page - 1) * filter.limit });
    }
    setCurrPage(page);
  };

  return (
    <>
      <div>
        <div
          className={`mb-5 grid grid-cols-2 gap-4 rounded-2xl bg-white px-4 py-5 shadow-md md:grid-cols-4 ${styles.isStickyNav} z-10`}
        >
          <button
            type="button"
            onClick={() => handleScrollElement('description')}
            className="rounded-full border border-gray-300 bg-white px-5 py-2.5 text-sm font-medium text-gray-900 hover:bg-pink-100 focus:border-pink-300 focus:bg-pink-200 focus:font-semibold focus:outline-none focus:ring-4 focus:ring-pink-300"
          >
            Description
          </button>
          <button
            onClick={() => handleScrollElement('lesson')}
            type="button"
            className="rounded-full border border-gray-300 bg-white px-5 py-2.5 text-sm font-medium text-gray-900 hover:bg-pink-100 focus:border-pink-300 focus:bg-pink-200 focus:font-semibold focus:outline-none focus:ring-4 focus:ring-pink-300"
          >
            Lesson
          </button>
          <button
            onClick={() => handleScrollElement('instructor')}
            type="button"
            className="rounded-full border border-gray-300 bg-white px-5 py-2.5 text-sm font-medium text-gray-900 hover:bg-pink-100 focus:border-pink-300 focus:bg-pink-200 focus:font-semibold focus:outline-none focus:ring-4 focus:ring-pink-300"
          >
            Instructor
          </button>
          <button
            onClick={() => handleScrollElement('review')}
            type="button"
            className="rounded-full border border-gray-300 bg-white px-5 py-2.5 text-sm font-medium text-gray-900 hover:bg-pink-100 focus:border-pink-300 focus:bg-pink-200 focus:font-semibold focus:outline-none focus:ring-4 focus:ring-pink-300"
          >
            Review
          </button>
        </div>
        {/* Description */}
        <div
          className="mb-5 rounded-2xl bg-white px-4 py-5 shadow-md"
          id="description"
        >
          <div className="mb-4">
            <div className="mb-2 text-lg font-semibold">Description</div>
            <div className="text-sm">
              {data?.description && parse(data?.description)}
            </div>
          </div>
          <div className="mb-4">
            <div className="mb-2 text-sm font-semibold">
              What will you learn
            </div>
            {data?.outcomes?.map((item: any, i: number) => (
              <div key={i} className="mb-1 flex flex-row items-center gap-3">
                <BsCheck2Circle color="#26bf0a" size={30} />
                <span className="text-sm">{item}</span>
              </div>
            ))}
          </div>
          <div className="mb-4">
            <div className="mb-2 text-sm font-semibold">Requirements</div>
            {data?.requirements?.map((item: any, i: number) => (
              <div key={i} className="mb-1 flex flex-row items-center gap-3">
                <CgToolbarRight color="#718096" size={25} />
                <span className="text-sm">{item}</span>
              </div>
            ))}
          </div>
        </div>
        {/* Lesson */}
        <div
          className="mb-5 rounded-2xl bg-white px-4 py-5 shadow-md"
          id="lesson"
        >
          <div className="mb-4">
            <div className="flex flex-row items-center justify-between">
              <div className="mb-2 text-lg font-semibold">Lesson</div>
              <div className="flex gap-5">
                <span>{data?.total_lessons} lesson</span>
                <span>{data?.total_hours}</span>
              </div>
            </div>
            <Accordion>
              {dataSection?.map((item: any, i: number) => {
                const lessons = dataLesson?.filter(
                  (o: any) => o.section_id === item.id
                );
                return (
                  <Accordion.Panel key={i}>
                    <Accordion.Title className="text-black">
                      {item.title}
                    </Accordion.Title>
                    <Accordion.Content>
                      {lessons?.map((lesson: any, ii: number) => (
                        <div
                          className="flex flex-row items-center justify-between border-b-[1px] py-1"
                          key={ii}
                        >
                          <div className="flex flex-row items-center gap-2">
                            {lesson?.lesson_type === 'other' && <BsFileText />}
                            {lesson?.lesson_type === 'video' && (
                              <MdOndemandVideo />
                            )}
                            <div
                            // onClick={() => {
                            //   if (lesson?.attachment_type === 'pdf') {
                            //     window.open(
                            //       lesson?.attachment,
                            //       '_blank',
                            //       'noreferrer'
                            //     );
                            //   }
                            // }}
                            >
                              <span className="text-sm hover:text-sky-600">
                                {lesson?.title}
                              </span>
                            </div>
                          </div>
                          <span className="text-sm">{lesson?.duration}</span>
                        </div>
                      ))}
                    </Accordion.Content>
                  </Accordion.Panel>
                );
              })}
            </Accordion>
          </div>
        </div>
        {/* Instructor */}
        <div
          className="mb-5 rounded-2xl bg-white px-4 py-5 shadow-md"
          id="instructor"
        >
          <div className="mb-4">
            <div className="mb-2 text-lg font-semibold">Instructor</div>
            <div className="flex flex-col items-start gap-10 px-10 md:flex-row">
              <div className="z-0">
                <img
                  className="h-52 w-52 rounded-full object-cover ring-2 ring-gray-300"
                  alt=""
                  src={
                    data?.user?.photo !== null
                      ? data?.user?.photo
                      : 'https://flowbite.com/docs/images/people/profile-picture-5.jpg'
                  }
                />
              </div>
              <div className="flex w-full flex-col md:flex-1">
                <form className="w-full">
                  <div className="mb-2 flex items-center">
                    <div className="w-1/3">
                      <label className="mb-1 block pr-4 font-semibold text-black md:mb-0 md:text-left">
                        Name
                      </label>
                    </div>
                    <div className="">
                      : {data?.user?.first_name} {data?.user?.last_name}
                    </div>
                  </div>
                  <div className="mb-2 flex items-center">
                    <div className="w-1/3">
                      <label className="mb-1 block pr-4 font-semibold text-black md:mb-0 md:text-left">
                        Reviews
                      </label>
                    </div>
                    <div className="md:w-2/3">
                      : {data?.instructor?.total_ratings} Reviews
                    </div>
                  </div>
                  <div className="mb-2 flex items-center">
                    <div className="w-1/3">
                      <label className="mb-1 block pr-4 font-semibold text-black md:mb-0 md:text-left">
                        Student
                      </label>
                    </div>
                    <div className="md:w-2/3">
                      : {data?.instructor?.total_students} Students
                    </div>
                  </div>
                  <div className="mb-2 flex items-center">
                    <div className="w-1/3">
                      <label className="mb-1 block pr-4 font-semibold text-black md:mb-0 md:text-left">
                        Courses
                      </label>
                    </div>
                    <div className="md:w-2/3">
                      : {data?.instructor?.total_courses} Courses
                    </div>
                  </div>
                </form>
                <div className="mt-2">
                  <Button
                    pill={true}
                    gradientMonochrome="pink"
                    onClick={() => {
                      router.push({
                        pathname: '/instructor-profile/[...id]',
                        query: { id: data?.user?.id },
                      });
                    }}
                  >
                    View profile
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Review */}
        <div
          className="mb-5 rounded-2xl bg-white px-4 py-5 shadow-md"
          id="review"
        >
          <div className="mb-10">
            <div className="mb-2 text-lg font-semibold">Review</div>
            <div className="flex flex-row gap-5">
              <div className="flex w-52 flex-col items-center justify-center rounded-lg bg-fuchsia-900 p-3">
                <span className="text-5xl font-medium text-white">
                  {data?.rating_total_average}
                </span>
                <Rating>
                  <Rating.Star filled={data?.rating_total_average >= 1} />
                  <Rating.Star filled={data?.rating_total_average >= 2} />
                  <Rating.Star filled={data?.rating_total_average >= 3} />
                  <Rating.Star filled={data?.rating_total_average >= 4} />
                  <Rating.Star filled={data?.rating_total_average >= 5} />
                </Rating>
                <span className="text-center text-sm font-medium text-white">
                  {data?.rating_total} global ratings
                </span>
              </div>
              <div className="w-full">
                {ratingSummary.map((item: any, i: number) => (
                  <div className="mt-4 flex items-center" key={i}>
                    <div className="mx-4 h-5 w-2/4 rounded bg-gray-200 dark:bg-gray-700">
                      <div
                        className="h-5 rounded bg-fuchsia-900"
                        style={{ width: `${item.percentage}%` }}
                      ></div>
                    </div>
                    <span className="text-sm font-medium text-blue-600 dark:text-blue-500">
                      {item.rating} star
                    </span>
                  </div>
                ))}
              </div>
            </div>
          </div>
          <div className="mb-4 px-4">
            {listReview?.map((item: any, i: number) => (
              <div className="mb-2 border-b border-b-gray-400" key={i}>
                <article>
                  <div className="mb-4 flex items-center space-x-4">
                    <img
                      className="h-10 w-10 rounded-full"
                      alt=""
                      src={
                        item?.user?.photo !== null
                          ? item?.user?.photo
                          : 'https://flowbite.com/docs/images/people/profile-picture-5.jpg'
                      }
                    />
                    <div className="space-y-1 font-medium dark:text-white">
                      <p>
                        {item?.user?.first_name} {item?.user?.last_name}
                        <time
                          dateTime="2014-08-16 19:00"
                          className="block text-sm text-gray-500 dark:text-gray-400"
                        >
                          {moment(item.created_at).format('ddd, DD MMMM YYYY')}
                        </time>
                      </p>
                    </div>
                  </div>
                  <div className="mb-1 flex items-center">
                    <Rating>
                      <Rating.Star filled={item.rating >= 1} />
                      <Rating.Star filled={item.rating >= 2} />
                      <Rating.Star filled={item.rating >= 3} />
                      <Rating.Star filled={item.rating >= 4} />
                      <Rating.Star filled={item.rating >= 5} />
                    </Rating>
                  </div>
                  <p className="mb-2 font-light text-gray-500 dark:text-gray-400">
                    {item.review}
                  </p>
                </article>
              </div>
            ))}
            <div className="flex items-center justify-center">
              <Pagination
                currentPage={currPage}
                layout="pagination"
                onPageChange={onPageChange}
                showIcons={true}
                totalPages={totalPages}
                previousLabel="Prev"
                nextLabel="Next"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LeftContent;
