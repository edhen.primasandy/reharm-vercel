/* eslint-disable no-unsafe-optional-chaining */
import { useRouter } from 'next/router';
import React from 'react';
import { BsHeart, BsShare } from 'react-icons/bs';
import { toast } from 'react-toastify';

import { useActions, useState } from '@/overmind';
import styles from '@/styles/pages/courses/sectionDetailCourses.module.scss';
import helper from '@/utils/helper';

type Props = {
  data: any;
  dataLesson: any;
  onReloadData: any;
};
const LeftContent = (props: Props) => {
  const router = useRouter();
  const { data } = props;
  const {
    createOrder,
    getListOrder,
    updateOrder,
    updateUser,
    getUserProfile,
    // getListEnrolSubsciption,
    // getEnrolSubsciption,
    createEnrol,
  } = useActions();
  const { user, orders } = useState();
  const [isOnWishlist, setIsOnWishlist] = React.useState(false);

  React.useEffect(() => {
    if (user !== null) {
      if (user?.wishlist?.includes(data?.id?.toString())) {
        setIsOnWishlist(true);
      }
      // getListEnrolSubsciption({ user_id: user.id }).then((res) => {
      //   getEnrolSubsciption({ id: res.items[0].id });
      // });
    }

    return () => {};
  }, [user]);

  const onCreate = async (item: any) => {
    const payload = {
      user_id: user.id,
      item_details: [
        {
          course_id: item.id,
          course_name: item.title,
          category_name: item.category.name,
          instructor_name: `${item.user.first_name} ${item.user.last_name}`,
          thumbnail: item.thumbnail,
          price: item.price,
          quantity: 1,
          discount_flag: item.discount_flag,
          discount_price:
            item.discount_flag === 'yes'
              ? item.price - item.discounted_price
              : 0,
        },
      ],
      referral_code: item.referral_code ?? null,
      total_price: item.discounted_price ? item.discounted_price : item.price,
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString(),
    };
    createOrder(payload).then(() => {
      getListOrder({ user_id: user.id });
      toast.success('Success order, Please check your cart');
    });
  };
  const onUpdate = async (dataDraft: any, item: any) => {
    const payload = {
      ...dataDraft,
      item_details: [
        ...dataDraft.item_details,
        {
          course_id: item.id,
          course_name: item.title,
          category_name: item.category.name,
          instructor_name: `${item.user.first_name} ${item.user.last_name}`,
          thumbnail: item.thumbnail,
          price: item.price,
          quantity: 1,
          discount_flag: item.discount_flag,
          discount_price:
            item.discount_flag === 'yes' ? item.discounted_price : 0,
        },
      ],
      referral_code: item.referral_code ?? null,
      total_price:
        dataDraft.total_price +
        (item.discounted_price ? item.discounted_price : item.price),
      updated_at: new Date().toISOString(),
    };
    updateOrder(payload).then(() => {
      getListOrder({ user_id: user.id });
      toast.success('Success, Please check your cart');
    });
  };

  const addToCart = async (item: any) => {
    let payload = item;
    const { ref } = router.query;
    if (ref) {
      payload = { ...payload, referral_code: ref };
    }
    console.log(payload);

    if (orders === null) {
      await onCreate(payload);
    } else {
      const findDraft = orders.items.find(
        (o: any) =>
          o.status.toLowerCase() === 'draft' && o.is_subscription === 'no'
      );
      if (findDraft) {
        await onUpdate(findDraft, payload);
      } else {
        await onCreate(payload);
      }
    }
  };

  const buyNow = (item: any) => {
    addToCart(item).then(() => {
      router.push('/cart');
    });
  };

  const updateWishlist = () => {
    if (user === null) {
      router.push('/login');
    } else {
      const newWishlist: any[] = [...user.wishlist];
      if (isOnWishlist) {
        newWishlist.splice(user.wishlist.indexOf(data.id.toString()), 1);
      } else {
        newWishlist.push(data.id.toString());
      }
      const payloadUser = {
        id: user.id,
        first_name: user.first_name,
        last_name: user.last_name,
        email: user.email,
        biography: user.biography,
        province_id: 0,
        regency_id: 0,
        district_id: 0,
        village_id: 0,
        address: user.address,
        wishlist: newWishlist,
      };
      updateUser({ ...payloadUser }).then(() => {
        getUserProfile();
      });
    }
  };
  const onCreateEnrol = (item: any) => {
    createEnrol({ user_id: user.id, course_id: item.id, order_id: '' }).then(
      () => {
        toast.success('Success enrol course, please check your course');
        props.onReloadData();
      }
    );
  };
  const handleShare = () => {
    let currentUrl = window.location.href;

    // Add query parameter if logged in
    if (user !== null) {
      const url = new URL(currentUrl);
      url.searchParams.set('ref', user.referral_code);
      currentUrl = url.toString();
    }

    navigator.clipboard
      .writeText(currentUrl)
      .then(() => {
        toast.success('link copied to clipboard!');
      })
      .catch((err) => {
        console.error('Failed to copy: ', err);
      });
  };
  const buttonAction = () => {
    return (
      <>
        {data?.is_free_course === 'yes' ? (
          <button
            onClick={() => onCreateEnrol(data)}
            type="button"
            className="rounded-full border border-gray-300 bg-blue-500 px-5 py-2.5 text-sm font-medium  text-white hover:bg-blue-600 focus:outline-none focus:ring-4 focus:ring-gray-200"
          >
            Enroll
          </button>
        ) : (
          <>
            <button
              onClick={() => addToCart(data)}
              type="button"
              className="rounded-full border border-gray-300 bg-white px-5 py-2.5 text-sm font-medium text-gray-900 hover:bg-gray-100 focus:outline-none focus:ring-4 focus:ring-gray-200"
            >
              Add to cart
            </button>
            <button
              onClick={() => buyNow(data)}
              type="button"
              className="rounded-full bg-pink-700 px-5 py-2.5 text-sm font-medium text-white hover:bg-pink-800 focus:outline-none focus:ring-4 focus:ring-pink-300"
            >
              Buy now
            </button>
          </>
        )}
      </>
    );
  };
  return (
    <div className={styles.isSticky}>
      <div className="rounded-2xl bg-white py-5 px-4 shadow-md">
        <span className="font-semibold">{data?.title}</span>
        <div className="mt-2 flex flex-col rounded-lg border-[1px] border-gray-300 py-5 px-4">
          <span className="mb-2 text-sm">What is included</span>
          <span className="text-sm text-gray-600">
            - {data?.total_hours} on demand videos
          </span>
          <span className="text-sm text-gray-600">
            - {data?.total_lessons} Lessons
          </span>
          <span className="text-sm text-gray-600">- Full lifetime access</span>
          <span className="text-sm text-gray-600">
            - Access on mobile and tv
          </span>
          {data?.is_free_course === 'yes' ? (
            <div className="mt-4 flex flex-row flex-wrap items-center gap-2">
              <span className="text-xl font-bold text-orange-600">FREE</span>
            </div>
          ) : (
            <div className="mt-4 flex flex-row flex-wrap items-center gap-2">
              {data?.discounted_price > 0 && (
                <>
                  <span className="rounded-md bg-red-500 px-2 text-sm text-white">
                    {Math.round(
                      ((data?.price - data?.discounted_price) / data?.price) *
                        100
                    )}{' '}
                    %
                  </span>
                  <span className="text-base line-through">
                    {helper.currencyFormat(data?.price)}
                  </span>
                </>
              )}

              <span className="text-lg font-bold text-red-600">
                {helper.currencyFormat(
                  data?.discounted_price ? data?.discounted_price : data?.price
                )}
              </span>
            </div>
          )}
        </div>
        <div className="my-4 flex flex-col gap-3">
          <button
            onClick={() => handleShare()}
            type="button"
            className="flex items-center justify-center gap-2 rounded-full border border-gray-300 bg-white px-5 py-2.5 text-sm font-medium text-gray-900 hover:bg-gray-100 focus:outline-none focus:ring-4 focus:ring-gray-200"
          >
            <BsShare size={16} color={'#000'} />
            {'Share'}
          </button>
          <button
            onClick={() => updateWishlist()}
            type="button"
            className="flex items-center justify-center gap-2 rounded-full border border-gray-300 bg-white px-5 py-2.5 text-sm font-medium text-gray-900 hover:bg-gray-100 focus:outline-none focus:ring-4 focus:ring-gray-200"
          >
            <BsHeart size={16} color={'#000'} />
            {isOnWishlist ? 'Remove from wishlist' : 'Add to wishlist'}
          </button>
          {user !== null ? (
            <>{!data?.is_purchased && buttonAction()}</>
          ) : (
            <>{buttonAction()}</>
          )}
        </div>
      </div>
    </div>
  );
};

export default LeftContent;
