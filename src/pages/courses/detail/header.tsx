/* eslint-disable react/jsx-no-duplicate-props */
/* eslint-disable tailwindcss/no-custom-classname */
import React from 'react';
import { Fade } from 'react-awesome-reveal';
import { BsFillPlayCircleFill } from 'react-icons/bs';
import { IoCloseCircleSharp } from 'react-icons/io5';
import Modal from 'react-modal';

import Breadscrumb from '@/components/elements/breadscrumb';
import styles from '@/styles/pages/courses/sectionDetailCourses.module.scss';
import helper from '@/utils/helper';

type Props = {
  data: any;
};
const Header = (props: Props) => {
  const { data } = props;
  const [showVideo, setShowVideo] = React.useState(false);
  const [videoLoading, setVideoLoading] = React.useState(true);
  const [idVideo, setIdVideo] = React.useState('');

  React.useEffect(() => {
    if (data?.video_url) {
      const idV: any = helper.geIdVideoFromUrlYoutube(data?.video_url);
      setIdVideo(idV);
    }
    return () => {};
  }, [data]);
  const spinner = () => {
    setVideoLoading(!videoLoading);
  };
  return (
    <section className={`${styles.setionHeader} mx-[-20px]`}>
      <div className="container mx-auto flex flex-wrap items-center justify-between md:pt-5">
        <Breadscrumb
          homeRoute="/"
          dataItem={[
            { label: 'Courses', route: '/courses' },
            { label: data?.title, route: '' },
          ]}
        />
      </div>
      <Fade duration={700} cascade direction="left">
        <div className="container mx-auto flex flex-wrap items-center justify-between py-7">
          <div className="flex flex-1 flex-col p-9">
            <div className="text-lg font-semibold text-slate-900">
              {data?.category?.name}
            </div>
            <div className="text-4xl font-semibold text-slate-900">
              {data?.title}
            </div>
            <div className="text-lg font-semibold text-slate-900">
              by {data?.user?.first_name} {data?.user?.last_name}
            </div>
          </div>
          <div className="flex flex-1 flex-col items-center justify-center md:items-end md:justify-end">
            <button
              onClick={() => setShowVideo(true)}
              className="relative w-96 rounded-3xl ring-4 ring-white drop-shadow-lg md:w-[30rem]"
            >
              {data?.thumbnail ? (
                <img
                  src={data?.thumbnail}
                  alt=""
                  className="h-60 w-96 rounded-3xl object-cover md:w-[30rem]"
                />
              ) : (
                <img
                  src="https://edhenprimasandy.sirv.com/reharm/no-image.jpg"
                  alt=""
                  className="h-60 w-96 rounded-3xl object-cover md:w-[30rem]"
                />
              )}

              <div className="absolute	top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 rounded-full bg-white p-1 drop-shadow-lg">
                <BsFillPlayCircleFill size={70} color="#db1039" />
              </div>
            </button>
          </div>
        </div>
      </Fade>
      <Modal
        isOpen={showVideo}
        onRequestClose={() => setShowVideo(false)}
        className="modal-style z-20"
        overlayClassName="modal-overlay-style z-20"
      >
        <div className="flex flex-col justify-center">
          <button
            className="flex items-center justify-center text-center"
            onClick={() => setShowVideo(false)}
          >
            <IoCloseCircleSharp color="#fff" size={50} />
          </button>
          <div className="modal-body-video">
            {data?.video_provider === 'youtube' && (
              <iframe
                className={styles.modalVideo}
                onLoad={spinner}
                loading="lazy"
                width="100%"
                height="100%"
                src={`https://www.youtube.com/embed/${idVideo}`}
                title="YouTube video player"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              ></iframe>
            )}
            {data?.video_provider !== 'youtube' && (
              <iframe
                className={styles.modalVideo}
                onLoad={spinner}
                loading="lazy"
                width="100%"
                height="100%"
                src={data?.video_url}
                title="YouTube video player"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              ></iframe>
            )}
          </div>
        </div>
      </Modal>
    </section>
  );
};

export default Header;
