import React from 'react';

import LeftContent from './left-content';
import RightContent from './right-content';
// import styles from '@/styles/pages/courses/sectionDetailCourses.module.scss';
type Props = {
  data: any;
  dataLesson: any;
  dataSection: any;
  onReloadData: any;
};
const Detail = (props: Props) => {
  const { data, dataLesson, dataSection, onReloadData } = props;

  return (
    <section>
      <div className="container mx-auto flex items-center py-5">
        <div className="grid w-full grid-cols-1 gap-4 md:grid-cols-3">
          <div className="col-span-2">
            <LeftContent
              data={data}
              dataLesson={dataLesson}
              dataSection={dataSection}
            />
          </div>
          <div className="">
            <RightContent
              data={data}
              dataLesson={dataLesson}
              onReloadData={onReloadData}
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Detail;
