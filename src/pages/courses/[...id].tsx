import { useRouter } from 'next/router';
import React from 'react';
import { Cookies } from 'react-cookie';

import { useActions, useState } from '@/overmind';
import Main from '@/templates/Main';

import SectionDetail from './detail/detail';
import SectionHeader from './detail/header';

const cookies = new Cookies();
const CourseDetail = () => {
  const router = useRouter();
  const { getCourse, getMyCourse, getsCourseLesson, getsCourseSections } =
    useActions();
  const { user } = useState();
  const [data, setData] = React.useState({});
  const [dataLesson, setDataLesson] = React.useState([] as any);
  const [dataSection, setDataSection] = React.useState([] as any);
  const init = () => {
    if (router.query.id) {
      const jwtToken = cookies.get('_reharm_token');
      if (jwtToken && user !== null) {
        getMyCourse({ id: router.query.id.toString() }).then((res: any) => {
          setData(res.items[0]);
        });
      } else {
        getCourse({ id: router.query.id.toString() }).then((res: any) => {
          setData(res.data);
        });
      }
      getsCourseSections({
        course_id: parseInt(`${router.query.id}`, 10),
      }).then((res: any) => {
        setDataSection(res.items);
      });
      getsCourseLesson({
        course_id: parseInt(`${router.query.id}`, 10),
      }).then((res: any) => {
        setDataLesson(res.items);
      });
    }
  };
  React.useEffect(() => {
    init();
    return () => {};
  }, [router.query.id]);
  const onReloadData = () => {
    init();
  };
  return (
    <div>
      <SectionHeader data={data} />
      <SectionDetail
        data={data}
        dataLesson={dataLesson}
        dataSection={dataSection}
        onReloadData={onReloadData}
      />
    </div>
  );
};

export default CourseDetail;

CourseDetail.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
