import Loading from '@components/elements/skeleton-loading/card-course';
import { Label, Pagination, Radio, Rating, Tooltip } from 'flowbite-react';
import { useRouter } from 'next/router';
import React from 'react';
import { BsHeart, BsHeartFill } from 'react-icons/bs';
import { FaRegPlayCircle } from 'react-icons/fa';
import { MdAccessTime } from 'react-icons/md';
import { RiEqualizerLine } from 'react-icons/ri';
import Lottie from 'react-lottie';
import { toast } from 'react-toastify';

import Breadscrumb from '@/components/elements/breadscrumb';
import { useActions, useState } from '@/overmind';
import * as animationData from '@/public/assets/lottie/no-data-found.json';
import Main from '@/templates/Main';
import helper from '@/utils/helper';

const Courses = () => {
  const router = useRouter();
  const {
    getsCategories,
    getsCourses,
    createOrder,
    getListOrder,
    updateOrder,
    updateUser,
    getUserProfile,
    createEnrol,
  } = useActions();
  const { user, orders } = useState();
  const [expandFilter, setExpandFilter] = React.useState(true);
  const [loading, setLoading] = React.useState(false);
  const [dataCategories, setDataCategories] = React.useState([] as any);
  const [dataCourses, setDataCourses] = React.useState([] as any);
  const [currPage, setCurrPage] = React.useState(1);
  const [totalPages, setTotalPages] = React.useState(1);
  const [filter, setFilter] = React.useState({
    title: '',
    category_id: 0,
    skip: 0,
    limit: 6,
    sort: '',
    is_free_course: '',
    level: '',
    rating: 0,
    language: '',
    min_price: 0,
    max_price: 0,
    sort_type: 'asc',
    status: 'active',
  });
  const ratings = [
    { id: 1, value: 1 },
    { id: 2, value: 2 },
    { id: 3, value: 3 },
    { id: 4, value: 4 },
    { id: 5, value: 5 },
  ];
  const initData = () => {
    setLoading(true);
    const payload = { ...filter };
    getsCourses(payload).then((res) => {
      setDataCourses(res.items);
      const remainder = res.total % filter.limit;
      let count = res.total / filter.limit;
      if (remainder > 0) {
        count = Math.ceil(count);
      }
      setTotalPages(count);
      setLoading(false);
    });
  };
  React.useEffect(() => {
    if (router.query?.category) {
      setFilter({
        ...filter,
        category_id: parseInt(router.query?.category.toString(), 10),
      });
    } else {
      setFilter({
        ...filter,
        category_id: 0,
      });
    }
    return () => {};
  }, [router.query]);
  React.useEffect(() => {
    getsCategories().then((res) => {
      setDataCategories(res.items);
    });
    return () => {};
  }, []);
  React.useEffect(() => {
    initData();
    return () => {};
  }, [filter, user]);
  const onPageChange = (page: number) => {
    if (currPage !== page) {
      setFilter({ ...filter, skip: (page - 1) * filter.limit });
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
    setCurrPage(page);
  };

  const onFilterByCategory = (value: any) => {
    setFilter({ ...filter, category_id: parseInt(value, 10) });
  };
  const onFilterByPrice = (value: any) => {
    setFilter({ ...filter, is_free_course: value });
  };
  const onFilterByLanguage = (value: any) => {
    setFilter({ ...filter, language: value });
  };
  const onFilterByLevel = (value: any) => {
    setFilter({ ...filter, level: value });
  };
  const onFilterByRatings = (value: any) => {
    setFilter({ ...filter, rating: parseInt(value, 10) });
  };
  const onChangeFilterByMinPrice = (value: any) => {
    setFilter({ ...filter, min_price: parseFloat(value) });
  };
  const onChangeFilterByMaxPrice = (value: any) => {
    setFilter({ ...filter, max_price: parseFloat(value) });
  };

  const onCreate = (item: any) => {
    const payload = {
      user_id: user.id,
      item_details: [
        {
          course_id: item.id,
          course_name: item.title,
          category_name: item.category.name,
          instructor_name: `${item.user.first_name} ${item.user.last_name}`,
          thumbnail: item.thumbnail,
          price: item.price,
          quantity: 1,
          discount_flag: item.discount_flag,
          discount_price:
            item.discount_flag === 'yes' ? item.discounted_price : 0,
        },
      ],
      total_price: item.discounted_price ? item.discounted_price : item.price,
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString(),
    };
    createOrder(payload).then(() => {
      getListOrder({ user_id: user.id });
      initData();
      toast.success('Success, Please check your cart');
    });
  };
  const onUpdate = (dataDraft: any, item: any) => {
    const payload = {
      ...dataDraft,
      item_details: [
        ...dataDraft.item_details,
        {
          course_id: item.id,
          course_name: item.title,
          category_name: item.category.name,
          instructor_name: `${item.user.first_name} ${item.user.last_name}`,
          thumbnail: item.thumbnail,
          price: item.price,
          quantity: 1,
          discount_flag: item.discount_flag,
          discount_price:
            item.discount_flag === 'yes' ? item.discounted_price : 0,
        },
      ],
      total_price:
        (item.discounted_price ? item.discounted_price : item.price) +
        dataDraft.total_price,
      updated_at: new Date().toISOString(),
    };
    updateOrder(payload).then(() => {
      getListOrder({ user_id: user.id });
      initData();
      toast.success('Success, Please check your cart');
    });
  };

  const addToCart = (item: any) => {
    if (orders === null) {
      onCreate(item);
    } else {
      const findDraft = orders.items.find(
        (o: any) =>
          o.status.toLowerCase() === 'draft' && o.is_subscription === 'no'
      );
      if (findDraft) {
        onUpdate(findDraft, item);
      } else {
        onCreate(item);
      }
    }
  };
  const updateWishlist = (id: any, isOnWishlist: boolean) => {
    if (user === null) {
      router.push('/login');
    } else {
      const newWishlist: any[] = [...user.wishlist];
      if (isOnWishlist) {
        newWishlist.splice(user.wishlist.indexOf(id.toString()), 1);
      } else {
        newWishlist.push(id.toString());
      }
      const payloadUser = {
        id: user.id,
        first_name: user.first_name,
        last_name: user.last_name,
        email: user.email,
        biography: user.biography,
        province_id: 0,
        regency_id: 0,
        district_id: 0,
        village_id: 0,
        address: user.address,
        wishlist: newWishlist,
      };
      updateUser({ ...payloadUser }).then(() => {
        getUserProfile();
        setLoading(true);
        const payload = { ...filter };
        getsCourses(payload).then((res) => {
          setDataCourses(res.items);
          const remainder = res.total % filter.limit;
          let count = res.total / filter.limit;
          if (remainder > 0) {
            count = Math.ceil(count);
          }
          setTotalPages(count);
          setLoading(false);
        });
        toast.success('Please check your wishlist');
      });
    }
  };
  const onCreateEnrol = (item: any) => {
    createEnrol({ user_id: user.id, course_id: item.id, order_id: '' }).then(
      () => {
        toast.success('Success enrol course, please check your course');
        initData();
      }
    );
  };
  // const [onHover, setOnHover] = React.useState(false);

  const Card = ({ item }: any) => {
    const { setSelectedCourseFromCourses } = useActions();
    let isOnWishlist = false;
    let isInCartOrPuchased = false;
    // let onHover = false;
    const Category = dataCategories.find(
      (o: any) => o.id === item.category.parent_id
    );
    if (user !== null) {
      if (item?.is_wishlist) {
        isOnWishlist = true;
      }
      if (item?.is_purchased || item?.is_in_cart) {
        isInCartOrPuchased = true;
      }
    }
    return (
      <div className="relative mb-3 flex h-full flex-col rounded-2xl bg-white shadow-md duration-200 ease-in hover:scale-105 hover:shadow-md">
        <div className="h-60">
          {item.thumbnail ? (
            <img
              src={item.thumbnail}
              className="h-60 w-full rounded-t-2xl object-cover"
              alt=""
            />
          ) : (
            <img
              src="https://edhenprimasandy.sirv.com/reharm/no-image.jpg"
              className="h-60 w-full rounded-t-2xl object-cover"
              alt=""
            />
          )}
        </div>
        <div className="absolute right-2 top-2 rounded-lg bg-black bg-opacity-[0.5]">
          <Tooltip
            content={isOnWishlist ? 'Remove from wishlist' : 'Add to wishlist'}
            className="text-sm"
            placement="left"
          >
            <button
              className="mx-2 mt-2"
              onClick={() => updateWishlist(item.id, isOnWishlist)}
              type="button"
              // onMouseEnter={() => {
              //   onHover = true;
              // }}
              // onMouseLeave={() => {
              //   onHover = false;
              // }}
            >
              <>
                {isOnWishlist ? (
                  <BsHeartFill size={20} color={'#ed0485'} />
                ) : (
                  <BsHeart size={20} color={'#ffff'} />
                )}
              </>
            </button>
          </Tooltip>
        </div>
        <div className="mt-[-20px] flex flex-col rounded-2xl bg-white p-4">
          <span className="font-semibold text-gray-500">
            {Category?.name}:{' '}
            <span className="font-normal">{item.category.name}</span>
          </span>
          <span className="mb-2 text-lg font-semibold">{item.title}</span>
          <div className="mb-2 border-b-2 border-dashed pb-2">
            <div className="mb-5 min-h-[50px] text-xs line-clamp-3">
              {item.short_description}
            </div>
            <div className="mb-5">
              {item?.is_free_course === 'yes' ? (
                <div className="mt-4 flex flex-row flex-wrap items-center gap-2">
                  <span className="text-xl font-bold text-orange-600">
                    FREE
                  </span>
                </div>
              ) : (
                <div className="mt-4 flex flex-row flex-wrap items-center gap-2">
                  {item?.discounted_price > 0 && (
                    <>
                      <span className="rounded-md bg-red-500 px-2 text-sm text-white">
                        {Math.round(
                          ((item.price - item.discounted_price) / item.price) *
                            100
                        )}{' '}
                        %
                      </span>
                      <span className="text-base line-through">
                        {helper.currencyFormat(item?.price)}
                      </span>
                    </>
                  )}

                  <span className="text-lg font-bold text-red-600">
                    {helper.currencyFormat(
                      item.discounted_price ? item.discounted_price : item.price
                    )}
                  </span>
                </div>
              )}
            </div>
            <div className="mb-5 text-blue-600">
              {item?.user?.first_name} {item?.user?.last_name}
            </div>
            <div className="flex flex-row items-center justify-between">
              <div>
                <Rating>
                  <Rating.Star filled={item?.rating_total_average >= 1} />
                  <Rating.Star filled={item?.rating_total_average >= 2} />
                  <Rating.Star filled={item?.rating_total_average >= 3} />
                  <Rating.Star filled={item?.rating_total_average >= 4} />
                  <Rating.Star filled={item?.rating_total_average >= 5} />
                </Rating>
              </div>
              <div className="flex flex-row gap-3">
                <div className="flex flex-row items-center gap-2">
                  <FaRegPlayCircle size={15} />
                  <span>{item.total_lessons} lessons</span>
                </div>
                <div className="flex flex-row items-center gap-2">
                  <MdAccessTime size={16} />
                  <span>{item.total_hours}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="flex flex-row gap-3">
            <button
              type="button"
              onClick={() => {
                const href = `/courses/${item.id}`;
                setSelectedCourseFromCourses(item);
                router.push(href);
              }}
              className="mb-2 w-full rounded-full bg-orange-500 px-5 py-2.5 text-sm font-medium text-white hover:bg-orange-600 focus:outline-none focus:ring-4 focus:ring-orange-300 dark:bg-orange-600 dark:hover:bg-orange-700 dark:focus:ring-orange-800"
            >
              View detail
            </button>
            <button
              onClick={() => {
                if (user !== null) {
                  if (item.is_free_course === 'no') {
                    addToCart(item);
                  } else {
                    onCreateEnrol(item);
                  }
                } else {
                  router.push('/login');
                }
              }}
              disabled={isInCartOrPuchased}
              className={`group relative mb-2 mr-2 inline-flex w-full items-center justify-center overflow-hidden rounded-full ${
                !isInCartOrPuchased
                  ? 'bg-gradient-to-br from-purple-600 to-blue-500 p-0.5 text-sm font-medium text-gray-900 hover:text-white focus:outline-none focus:ring-4 focus:ring-blue-300 group-hover:from-purple-600 group-hover:to-blue-500'
                  : 'p-0.5 text-sm font-medium text-gray-900 ring-2'
              }`}
            >
              <span className="relative w-full rounded-full bg-white px-5 py-2.5 transition-all duration-75 ease-in group-hover:bg-opacity-[0]">
                {isInCartOrPuchased && item?.is_purchased && 'Purchased'}
                {isInCartOrPuchased &&
                  item?.is_in_cart &&
                  !item?.is_purchased &&
                  'Already in cart'}
                {!isInCartOrPuchased &&
                  item.is_free_course === 'yes' &&
                  'Enrol'}
                {!isInCartOrPuchased &&
                  item.is_free_course === 'no' &&
                  'Add to cart'}
              </span>
            </button>
          </div>
        </div>
      </div>
    );
  };
  return (
    <section>
      <div className="container mx-auto mt-20 flex items-center">
        <div className="w-full">
          <Breadscrumb
            homeRoute="/"
            dataItem={[{ label: 'Courses', route: '' }]}
          />
          <h1 className="mb-2 text-4xl font-semibold">Courses</h1>
          <div className="mb-5 grid w-full grid-cols-1 gap-6 md:grid-cols-4">
            <div>
              <div className="mb-5 rounded-2xl bg-white px-4 py-5 shadow-md">
                <div className="flex flex-row items-center justify-between">
                  <span className="text-base font-semibold">Filters</span>
                  <button onClick={() => setExpandFilter(!expandFilter)}>
                    <RiEqualizerLine />
                  </button>
                </div>
                <div
                  className={`transition duration-1000 ease-out ${
                    expandFilter ? 'visible' : 'hidden'
                  }`}
                >
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <label className="mb-2 block text-sm font-semibold text-gray-900">
                      Title
                    </label>
                    <input
                      type="text"
                      className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500"
                      placeholder="Title"
                      onChange={(e) => {
                        setFilter({ ...filter, title: e.target.value });
                      }}
                    />
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Categories</span>
                    <div
                      onChange={(e: any) => onFilterByCategory(e.target?.value)}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'all'}
                            name="categories"
                            value={0}
                            checked={filter.category_id === 0}
                          />
                          <Label htmlFor={'all'}>All Category</Label>
                        </div>
                      </div>
                      {dataCategories.map((item: any, i: number) => (
                        <div key={i} className="mb-4 ml-2">
                          <div className="mb-2 flex items-center gap-2">
                            <Radio
                              id={item.id}
                              name="categories"
                              value={item.id}
                              checked={filter.category_id === item.id}
                            />
                            <Label htmlFor={item.id}>{item.name}</Label>
                          </div>
                          {item.childs.map((ch: any, ii: number) => (
                            <div
                              key={ii}
                              className="mb-2 ml-4 flex items-center gap-2"
                            >
                              <Radio
                                id={ch.id}
                                name="categories"
                                value={ch.id}
                                checked={filter.category_id === ch.id}
                              />
                              <Label htmlFor={ch.id}>{ch.name}</Label>
                            </div>
                          ))}
                        </div>
                      ))}
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Price</span>
                    <div
                      onChange={(e: any) => onFilterByPrice(e.target?.value)}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'allprice'}
                            name="price"
                            value={''}
                            defaultChecked={true}
                          />
                          <Label htmlFor={'allprice'}>All</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'free'}
                            name="price"
                            value={'yes'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'free'}>Free</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'paid'}
                            name="price"
                            value={'no'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'paid'}>Paid</Label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Range Price</span>
                    <div className="relative mb-5">
                      <div className="pointer-events-none absolute inset-y-[1px] left-[1px] flex items-center rounded-l-lg bg-gray-200 px-2 text-base font-semibold">
                        Rp
                      </div>
                      <input
                        type="number"
                        id="email-address-icon"
                        className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 pl-10 text-sm text-gray-900 focus:border-pink-500 focus:ring-pink-500"
                        placeholder="Minimum price"
                        // value={filter.min_price}
                        onChange={(e) => {
                          onChangeFilterByMinPrice(
                            e.target.value !== null ? e.target.value : 0
                          );
                        }}
                      />
                    </div>
                    <div className="relative">
                      <div className="pointer-events-none absolute inset-y-[1px] left-[1px] flex items-center rounded-l-lg bg-gray-200 px-2 text-base font-semibold">
                        Rp
                      </div>
                      <input
                        type="number"
                        id="email-address-icon"
                        className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 pl-10 text-sm text-gray-900 focus:border-pink-500 focus:ring-pink-500"
                        placeholder="Maximum price"
                        // value={filter.max_price}
                        onChange={(e) => {
                          onChangeFilterByMaxPrice(
                            e.target.value !== null ? e.target.value : 0
                          );
                        }}
                      />
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Level</span>
                    <div
                      onChange={(e: any) => onFilterByLevel(e.target?.value)}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'alllevel'}
                            name="level"
                            value={''}
                            defaultChecked={true}
                          />
                          <Label htmlFor={'alllevel'}>All</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'Beginner'}
                            name="level"
                            value={'beginner'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'Beginner'}>Beginner</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'Advanced'}
                            name="level"
                            value={'advanced'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'Advanced'}>Advanced</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'Intermediate'}
                            name="level"
                            value={'Intermediate'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'intermediate'}>Intermediate</Label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Language</span>
                    <div
                      onChange={(e: any) => onFilterByLanguage(e.target?.value)}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'alllang'}
                            name="language"
                            value={''}
                            defaultChecked={true}
                          />
                          <Label htmlFor={'alllang'}>All</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'English'}
                            name="language"
                            value={'English'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'english'}>English</Label>
                        </div>
                      </div>
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'Indonesia'}
                            name="language"
                            value={'indonesia'}
                            defaultChecked={false}
                          />
                          <Label htmlFor={'Indonesia'}>Indonesia</Label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="my-4 border-b border-b-gray-200"></div>
                  <div className="flex flex-col">
                    <span className="mb-2 font-semibold">Ratings</span>
                    <div
                      onChange={(e: any) => onFilterByRatings(e.target?.value)}
                    >
                      <div className="mb-4 ml-2">
                        <div className="mb-2 flex items-center gap-2">
                          <Radio
                            id={'allratings'}
                            name="ratings"
                            value={0}
                            defaultChecked={true}
                          />
                          <Label htmlFor={'allratings'}>All</Label>
                        </div>
                      </div>
                      {ratings.map((item) => (
                        <div className="mb-4 ml-2" key={item.id}>
                          <div className="mb-2 flex items-center gap-2">
                            <Radio
                              id={`${item.value}star`}
                              name="ratings"
                              value={item.value}
                              defaultChecked={false}
                            />
                            <Label htmlFor={`${item.value}star`}>
                              <div>
                                <Rating>
                                  <Rating.Star filled={item.id >= 1} />
                                  <Rating.Star filled={item.id >= 2} />
                                  <Rating.Star filled={item.id >= 3} />
                                  <Rating.Star filled={item.id >= 4} />
                                  <Rating.Star filled={item.id >= 5} />
                                </Rating>
                              </div>
                            </Label>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-span-3">
              {!loading && (
                <>
                  <div className="mb-5">
                    <div className="grid grid-cols-1 gap-5 md:grid-cols-2">
                      {dataCourses.map((item: any, i: number) => (
                        <div key={i} className="mb-2">
                          <Card item={item} />
                        </div>
                      ))}
                    </div>
                    {dataCourses.length === 0 && (
                      <Lottie
                        options={{
                          loop: true,
                          autoplay: true,
                          animationData,
                          rendererSettings: {
                            preserveAspectRatio: 'xMidYMid slice',
                          },
                        }}
                        width={'100%'}
                      />
                    )}
                  </div>
                  <div className="flex items-center justify-center">
                    <Pagination
                      currentPage={currPage}
                      layout="pagination"
                      onPageChange={onPageChange}
                      showIcons={true}
                      totalPages={totalPages}
                      previousLabel="Prev"
                      nextLabel="Next"
                    />
                  </div>
                </>
              )}
              {loading && <Loading count={2} />}
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Courses;

Courses.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
