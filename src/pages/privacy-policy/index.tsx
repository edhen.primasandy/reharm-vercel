import React from 'react';

import Breadscrumb from '@/components/elements/breadscrumb';
import { Meta } from '@/layouts/Meta';
import { useActions } from '@/overmind';
import Main from '@/templates/Main';

const Content = () => {
  const { getSettingByKey } = useActions();
  const [data, setData] = React.useState({} as any);
  React.useEffect(() => {
    getSettingByKey({ key: 'privacy_policy' }).then((res: any) => {
      setData(res);
    });

    return () => {};
  }, []);
  return (
    <div>
      <div className="container mx-auto mt-20 flex flex-col pb-5">
        <Meta title="Reharm Music Courses" description="Reharm Music Courses" />
        <Breadscrumb
          homeRoute="/"
          dataItem={[{ label: 'Privacy and policy', route: '' }]}
        />
        <div className="my-10">
          <h1 className="text-6xl font-bold">Privacy policy</h1>
        </div>
        <div className="rounded-lg bg-white bg-opacity-50 p-2">
          <div
            className="leading-relaxed tracking-wide"
            dangerouslySetInnerHTML={{ __html: data.value }}
          />
        </div>
      </div>
    </div>
  );
};

export default Content;

Content.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
