/* eslint-disable no-alert */
/* eslint-disable tailwindcss/migration-from-tailwind-2 */
/* eslint-disable import/no-extraneous-dependencies */
import { Button, TextInput } from 'flowbite-react';
import lodash from 'lodash';
import { useRouter } from 'next/router';
import React from 'react';
import { FaRegTrashAlt } from 'react-icons/fa';
import { toast } from 'react-toastify';

import Breadscrumb from '@/components/elements/breadscrumb';
import { useActions, useState } from '@/overmind';
import Main from '@/templates/Main';
import helper from '@/utils/helper';

declare global {
  interface Window {
    snap: any;
  }
}

const Cart = () => {
  const router = useRouter();
  const {
    checkoutOrder,
    updateOrder,
    getListOrder,
    deleteOrder,
    getUserProfile,
    validateReferral,
  } = useActions();
  const { orders, user } = useState();
  const [summary, setSummary] = React.useState({
    count: 0,
    price: 0,
    discount: 0,
    commission: 0,
    totalPrice: 0,
  });
  const [data, setData] = React.useState({} as any);
  const [loadingCheckout, setLoadingCheckout] = React.useState(false);
  const [activeCart, setActiveCart] = React.useState('course') as any;
  const [countCourse, setCountCourse] = React.useState(0);
  const [countSubs, setCountSubs] = React.useState(0);
  const [referralCode, setReferralCode] = React.useState('');
  const [isLoading, setIsLoading] = React.useState(false);
  const [validationResult, setValidationResult] = React.useState<string | null>(
    null
  );

  const init = (type: any, ordersData: any) => {
    setActiveCart(type);
    const findDraft = ordersData?.items.find((o: any) => {
      return (
        o.status.toLowerCase() === 'draft' &&
        (type === 'course'
          ? o.is_subscription === 'no'
          : o.is_subscription === 'yes')
      );
    });
    if (findDraft) {
      const findHasRef = findDraft?.item_details.find(
        (o: any) => o.referral_code
      );
      if (findHasRef) {
        setReferralCode(findHasRef.referral_code);
      }
      setData(findDraft);
      const newSum = {
        count: 0,
        price: 0,
        discount: 0,
        commission: 0,
        totalPrice: 0,
      };
      findDraft.item_details.forEach((item: any) => {
        newSum.count += 1;
        newSum.price += item.price;
        newSum.commission += item.commission;
        newSum.discount +=
          item.discount_flag === 'yes' ? item.price - item.discount_price : 0;
        newSum.totalPrice = newSum.price - newSum.discount - newSum.commission;
      });
      setSummary({ ...newSum });
    } else {
      setData({});
      setSummary({
        count: 0,
        price: 0,
        discount: 0,
        commission: 0,
        totalPrice: 0,
      });
    }
    const findCourse = ordersData?.items.find((o: any) => {
      return o.status.toLowerCase() === 'draft' && o.is_subscription === 'no';
    });
    if (findCourse) {
      setCountCourse(findCourse.item_details.length);
    }
    const findSubs = ordersData?.items.find((o: any) => {
      return o.status.toLowerCase() === 'draft' && o.is_subscription === 'yes';
    });
    if (findSubs) {
      setCountSubs(findSubs.item_details.length);
    }
  };

  React.useEffect(() => {
    // if (orders !== null) {
    //   if (router.query.id) {
    //     init(router.query.id, orders);
    //   } else {
    //     init(activeCart, orders);
    //   }
    // } else {
    // getUserProfile().then(() => {
    //   getListOrder({ user_id: user?.id }).then((res: any) => {
    //     if (router.query.id) {
    //       init(router.query.id, res);
    //     } else {
    //       init(activeCart, res);
    //     }
    //   });
    // });
    // }
    getUserProfile().then((resuser) => {
      getListOrder({ user_id: resuser?.id }).then((res: any) => {
        if (router.query.id) {
          init(router.query.id, res);
        } else {
          init(activeCart, res);
        }
      });
    });
    return () => {};
  }, []);

  const onChangeCart = (type: any) => {
    init(type, orders);
  };

  const onCheckout = () => {
    // if (validationResult === null && referralCode) {
    //   toast.error('Please validate your referral code');
    //   return;
    // }
    setLoadingCheckout(true);
    checkoutOrder({ id: data.id }).then((res) => {
      window?.snap?.pay(res.snap_token, {
        onClose() {
          /* You may add your own implementation here */
          alert('you closed the popup without finishing the payment');
        },
      });
      setLoadingCheckout(false);
    });
  };
  const onDeleteOrder = (index: number) => {
    if (data.item_details.length > 1) {
      const newItems: any[] = [...data.item_details];
      newItems?.splice(index, 1);
      const payload = {
        ...data,
        item_details: [...newItems],
        total_price: lodash.sumBy(newItems, 'discount_price'),
        updated_at: new Date().toISOString(),
      };
      updateOrder(payload).then(() => {
        getListOrder({ user_id: data.user_id }).then((res) =>
          init(activeCart, res)
        );
        toast.success('Your item is deleted');
      });
    } else {
      deleteOrder({ id: data.id }).then(() => {
        getListOrder({ user_id: data.user_id }).then((res) =>
          init(activeCart, res)
        );
        toast.success('Your order is deleted');
      });
    }
  };

  const validateReferralCode = async () => {
    setIsLoading(true);
    setValidationResult(null);
    validateReferral({ referral_code: referralCode, order_id: data.id })
      .then(() => {
        setValidationResult('Referral code is valid!');
        getListOrder({ user_id: user?.id }).then((res: any) => {
          init(activeCart, res);
        });
      })
      .catch(() => {
        setValidationResult('Invalid referral code.');
      })
      .finally(() => {
        setIsLoading(false);
      });

    // // Simulate an API call to validate the referral code
    // setTimeout(() => {
    //   setIsLoading(false);
    //   // Example validation logic
    //   if (referralCode === 'VALID_CODE') {
    //     setValidationResult('Referral code is valid!');
    //   } else {
    //     setValidationResult('Invalid referral code.');
    //   }
    // }, 2000);
  };

  return (
    <section>
      <div className="container mx-auto mt-20 flex items-center">
        <div className="w-full">
          <Breadscrumb
            homeRoute="/"
            dataItem={[{ label: 'Cart', route: '' }]}
          />
          <h1 className="mb-2 text-4xl  font-semibold">Cart</h1>
          <div className="mb-5 flex flex-row items-center gap-5">
            <div className="relative">
              <button
                onClick={() => onChangeCart('course')}
                className={`group relative mb-2 mr-2 inline-flex items-center justify-center overflow-hidden rounded-lg bg-gradient-to-br from-purple-600 to-blue-500 p-0.5 text-sm font-medium  hover:text-white ${
                  activeCart === 'course' ? 'text-white' : 'text-gray-900'
                } focus:outline-none focus:ring-4 focus:ring-blue-300 group-hover:from-purple-600 group-hover:to-blue-500`}
              >
                <span
                  className={`relative rounded-md bg-white px-5 py-2.5 transition-all duration-75 ease-in ${
                    activeCart === 'course' ? 'bg-opacity-0' : ''
                  }`}
                >
                  Course Cart
                </span>
              </button>
              {countCourse > 0 && (
                <div className="absolute -top-2 -right-1 inline-flex h-5 w-5 items-center justify-center rounded-full  bg-red-600 text-[10px] font-bold text-white">
                  {countCourse}
                </div>
              )}
            </div>
            <div className="relative">
              <button
                onClick={() => onChangeCart('subscription')}
                className={`group relative mb-2 mr-2 inline-flex items-center justify-center overflow-hidden rounded-lg bg-gradient-to-br from-purple-600 to-blue-500 p-0.5 text-sm font-medium  hover:text-white ${
                  activeCart === 'subscription' ? 'text-white' : 'text-gray-900'
                } focus:outline-none focus:ring-4 focus:ring-blue-300 group-hover:from-purple-600 group-hover:to-blue-500`}
              >
                <span
                  className={`relative rounded-md bg-white px-5 py-2.5 transition-all duration-75 ease-in ${
                    activeCart === 'subscription' ? 'bg-opacity-0' : ''
                  }`}
                >
                  Subscriptions Cart
                </span>
              </button>
              {countSubs > 0 && (
                <div className="absolute -top-2 -right-1 inline-flex h-5 w-5 items-center justify-center rounded-full  bg-red-600 text-[10px] font-bold text-white">
                  {countSubs}
                </div>
              )}
            </div>
          </div>
          <div className="mb-5 grid w-full grid-cols-6 gap-6">
            <div className="col-span-4">
              <div className="rounded-2xl bg-white p-4">
                {data?.item_details?.map((item: any, i: number) => (
                  <div className="mb-2 flex flex-col border-b" key={i}>
                    <div className="mb-5">
                      <div className="flex items-start gap-2">
                        <div className="flex flex-1 flex-col">
                          <div className="mb-2 flex flex-col">
                            <span className="text-lg font-semibold">
                              {activeCart === 'course'
                                ? item.course_name
                                : item.name}
                            </span>
                            <span className="text-sm text-gray-600">
                              {item.category_name}
                            </span>
                          </div>
                          <div className="flex flex-row items-center justify-between">
                            <div className="flex gap-3">
                              <img
                                className="h-20 w-20 rounded object-contain"
                                src={
                                  item.thumbnail ??
                                  'https://edhenprimasandy.sirv.com/reharm/no-image.jpg'
                                }
                                alt="Large avatar"
                              ></img>
                              <div className="flex flex-col">
                                <span className="mb-2 text-base">
                                  {item.short_description}
                                </span>
                                <span className="mb-2 text-base text-gray-700">
                                  {activeCart === 'course'
                                    ? item.instructor_name
                                    : 'Subscription plan'}
                                </span>
                                <div className="flex items-center gap-2">
                                  {item.discount_flag === 'yes' && (
                                    <>
                                      <span className="rounded bg-red-500 px-2 py-1 text-white">
                                        {Math.round(
                                          ((item.price - item.discount_price) /
                                            item.price) *
                                            100
                                        )}
                                        %
                                      </span>
                                      <span className="line-through">
                                        {helper.currencyFormat(item.price)}
                                      </span>
                                    </>
                                  )}

                                  <span className="font-semibold">
                                    {helper.currencyFormat(
                                      item.discount_flag === 'yes'
                                        ? item.discount_price
                                        : item.price
                                    )}
                                  </span>
                                </div>
                              </div>
                            </div>
                            <button
                              className="mr-5"
                              onClick={() => onDeleteOrder(i)}
                            >
                              <FaRegTrashAlt size={20} />
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
                {Object.keys(data).length === 0 && (
                  <div className="flex flex-col justify-center">
                    <span className="text-center text-gray-800">
                      There are no orders that can be {'\n'} checked out
                    </span>
                  </div>
                )}
              </div>
            </div>
            <div className="col-span-2">
              <div className="rounded-2xl bg-white px-4 py-2">
                {activeCart === 'course' && (
                  <div className="mb-2">
                    <div className="mb-2 block">
                      <span className="mb-3 text-lg font-bold">
                        Referral code
                      </span>
                    </div>
                    <div className="flex items-center">
                      <TextInput
                        className="eTextinput w-full"
                        required
                        value={referralCode}
                        placeholder="Type referral code here"
                        id="referral"
                        type="text"
                        sizing="md"
                        onChange={(e) => {
                          setValidationResult(null);
                          setReferralCode(e.target.value);
                        }}
                      />
                      <button
                        className="ml-3 rounded-lg bg-green-400 px-4 py-2 text-white hover:bg-green-600 focus:outline-none"
                        onClick={validateReferralCode}
                        disabled={isLoading || !referralCode}
                      >
                        {isLoading ? 'Validating...' : 'Validate'}
                      </button>
                    </div>
                    {validationResult && (
                      <div
                        className={`mt-2 text-sm ${
                          validationResult.includes('valid!')
                            ? 'text-green-500'
                            : 'text-red-500'
                        }`}
                      >
                        {validationResult}
                      </div>
                    )}
                  </div>
                )}

                <span className="mb-3 text-lg font-bold">Details</span>
                <div className="flex flex-row items-center justify-between">
                  <span className="text-base text-gray-500">Total price</span>
                  <span className="text-base text-gray-500">
                    {helper.currencyFormat(summary.price)}
                  </span>
                </div>
                <div className="flex flex-row items-center justify-between">
                  <span className="text-base text-gray-500">
                    Total discount
                  </span>
                  <span className="text-base text-gray-500">
                    {summary.discount ? '-' : ''}{' '}
                    {helper.currencyFormat(summary.discount)}
                  </span>
                </div>
                {activeCart === 'course' && (
                  <div className="flex flex-row items-center justify-between">
                    <span className="text-base text-gray-500">
                      Total referral discount
                    </span>
                    <span className="text-base text-gray-500">
                      {summary.commission ? '-' : ''}{' '}
                      {helper.currencyFormat(summary.commission)}
                    </span>
                  </div>
                )}

                <div className="my-2 border-b border-gray-200" />
                <div className="flex flex-row items-center justify-between">
                  <span className="text-lg font-bold">Total Price</span>
                  <span className="text-lg font-bold">
                    {helper.currencyFormat(summary.totalPrice)}
                  </span>
                </div>
                <div className="my-4 flex flex-col gap-3">
                  {Object.keys(data).length !== 0 && (
                    <Button
                      pill
                      disabled={loadingCheckout}
                      isProcessing={loadingCheckout}
                      gradientDuoTone="purpleToBlue"
                      onClick={() => onCheckout()}
                    >
                      Checkout
                    </Button>
                  )}

                  <Button
                    color="gray"
                    pill={true}
                    onClick={() => {
                      router.push('/courses');
                    }}
                  >
                    Continue shopping
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Cart;

Cart.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
