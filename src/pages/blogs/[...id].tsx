import moment from 'moment';
import { useRouter } from 'next/router';
import React from 'react';

import Breadscrumb from '@/components/elements/breadscrumb';
import { Meta } from '@/layouts/Meta';
import { useActions } from '@/overmind';
import Main from '@/templates/Main';
import helper from '@/utils/helper';

const Blogs = () => {
  const router = useRouter();
  const { getBlog } = useActions();
  const [data, setData] = React.useState({
    id: '',
    author: 0,
    title: '',
    content: '',
    media_type: 'image',
    media_url: '',
    status: '',
    created_at: '',
  } as any);
  const [idVideo, setIdVideo] = React.useState('');
  const [videoLoading, setVideoLoading] = React.useState(true);

  React.useEffect(() => {
    if (router.query.id && !router.query.id?.includes('add')) {
      getBlog({ slug: router.query.id.toString() }).then((res) => {
        setData(res);
        if (res.media_type === 'video') {
          const idV: any = helper.geIdVideoFromUrlYoutube(res?.media_url);
          setIdVideo(idV);
        }
      });
    }

    return () => {};
  }, [router.query.id]);
  const spinner = () => {
    setVideoLoading(!videoLoading);
  };
  return (
    <div className="bg-white">
      <div className="container mx-auto mt-20 flex flex-col">
        <Meta title="Reharm Music Courses" description="Reharm Music Courses" />
        <Breadscrumb
          homeRoute="/"
          dataItem={[
            { label: 'Blogs', route: '/blogs' },
            { label: data?.slug, route: '' },
          ]}
        />
        <div className="my-10">
          <h1 className="text-6xl font-bold">{data.title}</h1>
        </div>
        <div className="mb-10 flex">
          <div className="grid grid-cols-2 gap-5">
            <div className="border-l-2 border-purple-700 py-3 px-5">
              <p className="mb-2 text-lg font-semibold text-purple-700">Date</p>
              <p>{moment(data.created_at).format('DD MMM YYYY')}</p>
            </div>
            <div className="border-l-2 border-purple-700 py-3 px-5">
              <p className="mb-2 text-lg font-semibold text-purple-700">
                Author
              </p>
              <p>
                {data?.user?.first_name} {data?.user?.last_name}
              </p>
            </div>
          </div>
        </div>
        <div className="mb-10">
          {data?.media_type === 'image' && (
            <img
              src={data.media_url}
              className="h-80 w-full object-contain"
              alt=""
            />
          )}
          {data?.media_type === 'video' && (
            <div className="h-[720px]">
              <iframe
                onLoad={spinner}
                loading="lazy"
                width="100%"
                height="100%"
                src={`https://www.youtube.com/embed/${idVideo}?autoplay=1`}
                title="YouTube video player"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowFullScreen
              ></iframe>
            </div>
          )}
        </div>
        <div className="mb-5">
          <div dangerouslySetInnerHTML={{ __html: data.content }} />
        </div>
      </div>
    </div>
  );
};

export default Blogs;

Blogs.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
