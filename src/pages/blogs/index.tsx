import Breadscrumb from '@components/elements/breadscrumb';
import EButton from '@components/elements/button';
import { Pagination } from 'flowbite-react';
import moment from 'moment';
import { useRouter } from 'next/router';
import React from 'react';

import { useActions } from '@/overmind';
import Main from '@/templates/Main';
import helper from '@/utils/helper';

const Blogs = () => {
  const router = useRouter();
  const { getListBlogs } = useActions();
  const [currPage, setCurrPage] = React.useState(1);
  const [totalPages, setTotalPages] = React.useState(1);
  const [filter, setFilter] = React.useState({
    id: 0,
    author: '',
    skip: 0,
    limit: 12,
    sort: '',
    status: 'active',
    sort_type: 'asc',
  });

  const [data, setData] = React.useState([] as any[]);
  const [loading, setLoading] = React.useState(false);

  const init = () => {
    setLoading(true);
    const payload = { ...filter };
    getListBlogs(payload).then((res) => {
      setData(res.items);
      const remainder = res.total % filter.limit;
      let count = res.total / filter.limit;
      if (remainder > 0) {
        count = Math.ceil(count);
      }
      setTotalPages(count);
      setLoading(false);
    });
  };

  React.useEffect(() => {
    init();
    return () => {};
  }, [filter]);

  const onPageChange = (page: number) => {
    if (currPage !== page) {
      setFilter({ ...filter, skip: (page - 1) * filter.limit });
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
    setCurrPage(page);
  };
  const Card = ({ item }: any) => {
    const getUrlThubnail = () => {
      const idV: any = helper.geIdVideoFromUrlYoutube(item.media_url);
      return `https://img.youtube.com/vi/${idV}/0.jpg`;
    };

    return (
      <div className="relative my-3 flex h-auto flex-col rounded-2xl bg-white shadow-md duration-200 ease-in hover:scale-105 hover:shadow-md">
        <div className="h-40">
          {item.media_url ? (
            <img
              src={
                item.media_type === 'image' ? item.media_url : getUrlThubnail()
              }
              className="h-40 w-full rounded-t-2xl object-cover"
              alt=""
            />
          ) : (
            <img
              src="https://edhenprimasandy.sirv.com/reharm/no-image.jpg"
              className="h-40 w-full rounded-t-2xl object-cover"
              alt=""
            />
          )}
        </div>
        <div className="mt-[-20px] rounded-2xl bg-white p-4">
          <div className="border-dashed">
            <span className="text-sm text-gray-700">
              {moment(item.created_at).format('DD MMM YYYY')}
            </span>
            <div>
              <EButton
                onClick={() =>
                  router.push({
                    pathname: 'blogs/[...id]',
                    query: { id: item.slug },
                  })
                }
              >
                <span className="mb-2 text-left text-base font-semibold line-clamp-1 hover:text-blue-600">
                  {item.title}
                </span>
              </EButton>
            </div>
            <div className="mb-2 h-20 pb-2">
              <div className="text-xs line-clamp-3">{item.short_content}</div>
            </div>
          </div>
        </div>
      </div>
    );
  };
  return (
    <section>
      <div className="container mx-auto mt-20 flex items-center">
        <div className="w-full">
          <Breadscrumb
            homeRoute="/"
            dataItem={[{ label: 'Blogs', route: '' }]}
          />
          <div className="py-5">
            <h1 className="mb-2 text-4xl font-semibold">List Blogs</h1>
            {!loading && (
              <div>
                <div className="grid grid-cols-4 gap-5">
                  {data.map((item: any, i: number) => (
                    <div key={i}>
                      <Card item={item} />
                    </div>
                  ))}
                </div>
              </div>
            )}
            <div className="flex items-center justify-center">
              <Pagination
                currentPage={currPage}
                layout="pagination"
                onPageChange={onPageChange}
                showIcons={true}
                totalPages={totalPages}
                previousLabel="Prev"
                nextLabel="Next"
              />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Blogs;

Blogs.getLayout = (page: any) => {
  return <Main>{page}</Main>;
};
