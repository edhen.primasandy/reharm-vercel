import Button from '@components/elements/button';
import { Config } from '@constant/index';
import Logo from '@images/logo-color.png';
import { useActions } from '@overmind/index';
import { useGoogleLogin } from '@react-oauth/google';
import axios from 'axios';
import { getAuth, OAuthProvider, signInWithPopup } from 'firebase/auth';
import { Button as ButtonFB, Checkbox, Label, Modal } from 'flowbite-react';
import Image from 'next/image';
import Link from 'next/link';
import Router from 'next/router';
import React from 'react';
import { Fade } from 'react-awesome-reveal';
import { Cookies } from 'react-cookie';
import ReCAPTCHA from 'react-google-recaptcha';
import { AiOutlineEye, AiOutlineEyeInvisible } from 'react-icons/ai';
import { FaApple } from 'react-icons/fa';
import { FcGoogle } from 'react-icons/fc';
import { IoIosLock } from 'react-icons/io';
import { toast } from 'react-toastify';

import * as firbaseService from '@/services/firebase';
import AuthLayout from '@/templates/Auth';

const Login = () => {
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [showPassword, setShowPassword] = React.useState(false);
  const [remember, setRemember] = React.useState(false);
  const [opernFormForgetPassword, setOpernFormForgetPassword] =
    React.useState(false);
  const [emailForget, setEmailForget] = React.useState('');
  const [captcha, setCaptcha] = React.useState(null as any);

  const cookies = new Cookies();

  const {
    signIn,
    signUp,
    forgotPassword,
    getAppleCredential,
    setAppleCredential,
  } = useActions();

  const inputRef = React.useRef(null as any);
  React.useEffect(() => {
    if (inputRef.current) {
      inputRef.current?.focus();
    }
  }, [emailForget]);
  const onSignIn = () => {
    signIn({ email, password })
      .then((res: any) => {
        if (remember) {
          const user = { email, password };
          cookies.set('_reharm_user', JSON.stringify(user));
        }
        if (res.role === 'admin') {
          Router.push('/admin/dashboard');
        } else if (res.role === 'instructor') {
          Router.push('/admin/courses');
        } else {
          Router.push('/');
        }
      })
      .catch((e) => toast.error(e.message));
  };
  const signInWithGoogle = useGoogleLogin({
    onSuccess: async (codeResponse) => {
      // console.log(codeResponse);
      const { data } = await axios.get(
        'https://www.googleapis.com/oauth2/v3/userinfo',
        { headers: { Authorization: `Bearer ${codeResponse.access_token}` } }
      );

      // console.log(data);
      const payload = {
        email: data?.email,
        password: data?.sub,
      };
      signIn(payload)
        .then((res: any) => {
          if (remember) {
            const user = payload;
            cookies.set('_reharm_user', JSON.stringify(user));
          }
          if (['instructor', 'admin'].includes(res.role)) {
            Router.push('/admin/dashboard');
          } else {
            Router.push('/');
          }
        })
        .catch((e) => {
          if (e.message === 'Data not found.') {
            const newUser = {
              first_name: data?.given_name,
              last_name: data?.family_name,
              email: data?.email,
              password: data?.sub,
              role: 'user',
              is_gmail: 'yes',
              is_apple: 'no',
            };
            signUp(newUser).then(() => {
              signIn(payload).then(() => {
                if (remember) {
                  const user = payload;
                  cookies.set('_reharm_user', JSON.stringify(user));
                }
                Router.push('/');
              });
            });
          } else {
            toast.error(e.message);
          }
        });
    },
    onError: (error) => {
      return toast.error(error.error_description);
    },
  });
  const auth = getAuth(firbaseService.app);
  const signInWithApple = () => {
    const provider = new OAuthProvider('apple.com');
    provider.addScope('name');
    signInWithPopup(auth, provider)
      .then((result) => {
        const userInfo = result.user.providerData.find(
          (o) => o.providerId === 'apple.com'
        );
        const displayName = result.user.displayName?.split(' ');

        if (userInfo !== null) {
          getAppleCredential({ email: userInfo?.email }).then((res) => {
            if (res.data.total === 0) {
              setAppleCredential({
                email: userInfo?.email,
                user_identifier: userInfo?.uid,
                given_name: displayName
                  ? displayName[0]
                  : result.user.displayName,
                family_name: displayName
                  ? displayName[1]
                  : result.user.displayName,
              });
            }
          });
          const payload = {
            email: userInfo?.email,
            password: userInfo?.uid,
          };
          signIn(payload)
            .then((res: any) => {
              if (remember) {
                const user = payload;
                cookies.set('_reharm_user', JSON.stringify(user));
              }
              if (['instructor', 'admin'].includes(res.role)) {
                Router.push('/admin/dashboard');
              } else {
                Router.push('/');
              }
            })
            .catch((e) => {
              if (e.message.includes('Data not found.')) {
                const newUser = {
                  first_name: displayName
                    ? displayName[0]
                    : result.user.displayName,
                  last_name: displayName
                    ? displayName[1]
                    : result.user.displayName,
                  email: userInfo?.email,
                  password: userInfo?.uid,
                  role: 'user',
                  is_gmail: 'no',
                  is_apple: 'yes',
                  is_from_website: 'yes',
                };
                signUp(newUser).then(() => {
                  signIn(payload).then(() => {
                    if (remember) {
                      const user = payload;
                      cookies.set('_reharm_user', JSON.stringify(user));
                    }
                    Router.push('/');
                  });
                });
              } else {
                toast.error(e.message);
              }
            });
        }
      })
      .catch((error) => {
        // Handle Errors here.
        const errorMessage = error.message;
        toast.error(errorMessage);
        // // The email of the user's account used.
        // const email = error.customData.email;
        // // The credential that was used.
        // const credential = OAuthProvider.credentialFromError(error);
        // ...
      });
  };
  const onSubmitforgotPassword = () => {
    forgotPassword({ email: emailForget }).then(() =>
      toast.success('Please check your email')
    );
  };
  return (
    <div className="relative z-10 m-5 flex items-center justify-center rounded-3xl bg-white shadow-lg">
      <Fade duration={600} cascade>
        <div className="flex flex-col items-center justify-center py-5 px-10">
          <div className="mb-1">
            <Image className="w-52" src={Logo} alt={''} />
          </div>
          <span className="mb-5 px-5 text-center text-lg text-gray-500 md:text-base">
            The Biggest Music Family with Community
          </span>

          <div className="mb-3 w-full">
            <label className="mb-2 block text-lg font-medium text-gray-900 dark:text-white md:text-base">
              Email
            </label>
            <div className="relative">
              <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                <svg
                  aria-hidden="true"
                  className="h-5 w-5 text-gray-500 dark:text-gray-400"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                  <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
                </svg>
              </div>
              <input
                type="email"
                id="email-address-icon"
                className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 pl-10 text-lg text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600  dark:bg-gray-700 dark:text-white dark:placeholder:text-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500 md:text-base"
                placeholder="Email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
              />
            </div>
          </div>
          <div className="mb-5 w-full">
            <label className="mb-2 block text-lg font-medium text-gray-900 dark:text-white md:text-base">
              Password
            </label>
            <div className="relative">
              <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                <IoIosLock color="#a0aec0" />
              </div>
              <input
                type={showPassword ? 'text' : 'password'}
                id="password"
                className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 pl-10 text-lg text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600  dark:bg-gray-700 dark:text-white dark:placeholder:text-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500 md:text-base"
                placeholder="Password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
              />
              <div
                onClick={() => setShowPassword(!showPassword)}
                className="absolute inset-y-0 right-0 flex cursor-pointer items-center pr-3"
              >
                {showPassword ? (
                  <AiOutlineEye color="#a0aec0" />
                ) : (
                  <AiOutlineEyeInvisible color="#a0aec0" />
                )}
              </div>
            </div>
            {/* <p className="mt-2 text-base text-red-600 dark:text-red-500">
            <span className="font-medium">Oops!</span> Username already taken!
          </p> */}
          </div>
          <div className="mb-5 flex w-full flex-row justify-between">
            <div className="flex items-center gap-2">
              <Checkbox
                checked={remember}
                id="remember"
                onChange={() => setRemember(!remember)}
              />
              <Label htmlFor="remember">Remember me</Label>
            </div>
          </div>
          <div className="mb-5">
            {Config.recaptchaSiteKey && (
              <ReCAPTCHA
                sitekey={Config.recaptchaSiteKey ?? ''}
                onChange={(token) => setCaptcha(token)}
              />
            )}
          </div>
          <div className="mb-5 w-full justify-center">
            <Button
              onClick={onSignIn}
              isDisabled={captcha === null}
              className="group relative inline-block w-full items-center rounded-full px-5 py-2.5 text-center font-medium text-white disabled:opacity-50"
            >
              <span className="absolute top-0 left-0 h-full w-full rounded-full bg-gradient-to-l from-reharmpink to-reharmpurple opacity-50 blur-sm"></span>
              <span className="absolute inset-0 mt-0.5 ml-0.5 h-full w-full rounded-full bg-gradient-to-l from-reharmpink to-reharmpurple opacity-50 group-active:opacity-0"></span>
              <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-l from-reharmpink to-reharmpurple shadow-xl transition-all duration-200 ease-out group-hover:blur-sm group-active:opacity-0"></span>
              <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-l from-reharmpurple to-reharmpink transition duration-200 ease-out"></span>
              <span className="relative">Sign In</span>
            </Button>
            <div className="mt-2 flex justify-center">
              <Button onClick={() => setOpernFormForgetPassword(true)}>
                <span className="text-xs  text-gray-600 hover:text-gray-700">
                  Forget your password?
                </span>
              </Button>
            </div>
          </div>
          <div className="mb-5 flex w-full flex-row items-center">
            <div className="w-full px-5">
              <hr />
            </div>
            <span>or</span>
            <div className="w-full px-5">
              <hr />
            </div>
          </div>
          <div className="mb-5 w-full justify-center">
            <ButtonFB
              className="w-full"
              color="gray"
              onClick={() => signInWithGoogle()}
            >
              <FcGoogle className="mr-2 h-5 w-5" />
              Sign in with google
            </ButtonFB>
          </div>
          <div className="mb-5 w-full justify-center">
            <ButtonFB
              className="w-full"
              color="dark"
              onClick={() => signInWithApple()}
            >
              <FaApple className="mr-2 h-5 w-5" />
              Sign in with Apple
            </ButtonFB>
          </div>
          <div className="mb-5">
            <span className="text-base">
              New to Reharm?{' '}
              <Link href="/register">
                <span className="text-base font-semibold text-reharmpink hover:text-reharmpurple">
                  Sign up!
                </span>
              </Link>
            </span>
          </div>
          <div className="">
            <span className="text-xs text-gray-500">&copy; Reharm 2023</span>
          </div>
        </div>
      </Fade>
      <Modal
        show={opernFormForgetPassword}
        onClose={() => setOpernFormForgetPassword(false)}
      >
        <Modal.Header>Forget password</Modal.Header>
        <Modal.Body>
          <form>
            <div className="mb-2 block">
              <Label htmlFor="EmailForget" value="Email" />
            </div>
            <div className="relative">
              <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                <svg
                  aria-hidden="true"
                  className="h-5 w-5 text-gray-500 dark:text-gray-400"
                  fill="currentColor"
                  viewBox="0 0 20 20"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path>
                  <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path>
                </svg>
              </div>
              <input
                type="email"
                ref={inputRef}
                id="email-forget"
                className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 pl-10 text-lg text-gray-900 focus:border-blue-500 focus:ring-blue-500 dark:border-gray-600  dark:bg-gray-700 dark:text-white dark:placeholder:text-gray-400 dark:focus:border-blue-500 dark:focus:ring-blue-500 md:text-base"
                placeholder="Email"
                value={emailForget}
                onChange={(event) => setEmailForget(event.target.value)}
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer className="flex justify-end">
          <ButtonFB
            color="success"
            onClick={() => {
              onSubmitforgotPassword();
              setOpernFormForgetPassword(false);
            }}
          >
            Submit
          </ButtonFB>
          <ButtonFB
            color="gray"
            onClick={() => setOpernFormForgetPassword(false)}
          >
            Cancel
          </ButtonFB>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Login;

Login.getLayout = (page: any) => {
  return <AuthLayout>{page}</AuthLayout>;
};
