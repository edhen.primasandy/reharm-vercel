/* eslint-disable no-console */
/* eslint-disable no-param-reassign */
import { InstructorData, StudentsData } from '@data/dummy/index';
import type { Context } from '@overmind/index';
import { Cookies } from 'react-cookie';

const cookies = new Cookies();
export const setAppName = ({ state }: Context, payload: any) => {
  state.title = payload;
};
export const loadApp = async ({ state, actions }: Context) => {
  const jwtToken = cookies.get('_reharm_token');
  let user;
  if (jwtToken) {
    user = await actions.getUserProfile();
    state.auth = {
      accessToken: jwtToken,
      isLogin: true,
    };
    await actions.getListOrder({ user_id: user.id });
  }
  actions.getsSetting({});
  actions.resetFilter();
  return { ...state.auth, ...user };
};
export const resetFilter = async ({ state }: Context) => {
  state.filter = {
    skip: 0,
    limit: 10,
    sort: '',
    sort_type: 'asc',
  };
};
export const changeFilter = async (
  { state }: Context,
  payload: { skip: number; limit: number }
) => {
  state.filter = {
    ...state.filter,
    skip: payload.skip,
    limit: payload.limit,
  };
};

export const signIn = async (
  { state, effects, actions }: Context,
  payload: { email: string | null | unknown; password: string | null | unknown }
) => {
  const { data } = await effects.api.signIn(payload).catch((e) => {
    console.log('[ERROR] sign in : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  cookies.set('_reharm_token', data.data.access_token, {
    maxAge: data.data.expires_in,
    path: '/',
  });
  const userInfo = await actions.getUserProfile();

  state.auth = {
    accessToken: data.data.access_token,
    isLogin: true,
  };
  await actions.getListOrder({ user_id: userInfo.id });
  return userInfo;
};

export const signUp = async (
  { effects }: Context,
  payload: {
    first_name: string | null | unknown;
    last_name: string | null | unknown;
    email: string | null | unknown;
    password: string | null | unknown;
    role: string | null | unknown;
  }
) => {
  const { data } = await effects.api.signUp(payload).catch((e) => {
    console.log('[ERROR] sign up : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });

  return data;
};
export const getUserProfile = async ({ state, effects }: Context) => {
  const { data } = await effects.api.getUserProfile().catch((e) => {
    console.log('[ERROR] get user profile : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  state.user = data.data;
  return data.data;
};
export const getUser = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getUser(payload).catch((e) => {
    console.log('[ERROR] get user : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getUserInstructor = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getUserInstructor(payload).catch((e) => {
    console.log('[ERROR] get user instructor: ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const updateUser = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.updateUser(payload).catch((e) => {
    console.log('[ERROR] update user profile : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const createUser = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.createUser(payload).catch((e) => {
    console.log('[ERROR] create user : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const deleteUser = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.deleteUser(payload).catch((e) => {
    console.log('[ERROR] delete user : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const changePassword = async (
  { effects }: Context,
  payload: {
    old_password: string;
    password: string;
    password_confirmation: string;
  }
) => {
  const { data } = await effects.api.changePassword(payload).catch((e) => {
    console.log('[ERROR] change password : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const uploadPhotoUser = async (
  { effects }: Context,
  payload: { id: any; photo: string }
) => {
  const { data } = await effects.api.uploadPhotoUser(payload).catch((e) => {
    console.log('[ERROR] upload photo user profile : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const updateLessonProgress = async (
  { effects }: Context,
  payload: { id: any; lesson_id: string; progress: string }
) => {
  const { data } = await effects.api
    .updateLessonProgress(payload)
    .catch((e) => {
      console.log('[ERROR] update lesson progress: ', e?.response?.data || e);
      return Promise.reject(e?.response?.data || e);
    });
  return data.data;
};
export const logOut = async ({ state, effects }: Context) => {
  await effects.api.logOut().catch((e) => {
    console.log('[ERROR] logout : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  cookies.remove('_reharm_token', { path: '/' });
  state.auth = {
    accessToken: '',
    isLogin: false,
  };
  state.user = null;
  return '';
  // return data.data;
};
export const getsUsers = async (
  { effects }: Context,
  payload: {
    user_id?: number;
    keyword?: string;
    email?: number;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
    role?: string;
  }
) => {
  const { data } = await effects.api.getsUsers(payload).catch((e) => {
    console.log('[ERROR] gets users : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getCertificate = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getCertificate(payload).catch((e) => {
    console.log('[ERROR] get getCertificate : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const forgotPassword = async (
  { effects }: Context,
  payload: { email: string }
) => {
  const { data } = await effects.api.forgotPassword(payload).catch((e) => {
    console.log('[ERROR] forget password : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getsSetting = async (
  { effects, state }: Context,
  payload: {
    key?: number;
    email?: number;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
  }
) => {
  const { data } = await effects.api.getsSetting(payload).catch((e) => {
    console.log('[ERROR] gets setings : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  state.setting = data.data;
  return data.data;
};
export const getSettingByKey = async (
  { effects }: Context,
  payload: {
    key: any;
  }
) => {
  const { data } = await effects.api.getSettingByKey(payload).catch((e) => {
    console.log('[ERROR] get setting : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const updateSetting = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.updateSetting(payload).catch((e) => {
    console.log('[ERROR] update setting : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};

// ====== Apple Credential Section
export const getAppleCredential = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getAppleCredential(payload).catch((e) => {
    console.log('[ERROR] get credential : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const setAppleCredential = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.setAppleCredential(payload).catch((e) => {
    console.log('[ERROR] set credential : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
// ====== Category Section
export const getsCategories = async ({ effects }: Context) => {
  const { data } = await effects.api.getsCategories().catch((e) => {
    console.log('[ERROR] gets categories : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getCategory = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.getCategory(payload).catch((e) => {
    console.log('[ERROR] get Category : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const createCategory = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.createCategory(payload).catch((e) => {
    console.log('[ERROR] create Category : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const deleteCategory = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.deleteCategory(payload).catch((e) => {
    console.log('[ERROR] delete Category : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const updateCategory = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.updateCategory(payload).catch((e) => {
    console.log('[ERROR] update Category : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};

// ====== Courses Section
export const getsCourses = async (
  { state, effects, actions }: Context,
  payload: {
    title?: string;
    category_id?: number;
    user_id?: number;
    status?: string;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
  }
) => {
  const jwtToken = cookies.get('_reharm_token');
  if (jwtToken) {
    if (state.user !== null) {
      const { data } = await effects.api
        .getsMyCourses({
          ...payload,
          userid: state.user.id,
          user_login_id: state.user.id,
        })
        .catch((e) => {
          console.log('[ERROR] gets courses : ', e?.response?.data || e);
          return Promise.reject(e?.response?.data || e);
        });
      return data.data;
    }
    actions.getUserProfile().then(async (res: any) => {
      const { data } = await effects.api
        .getsMyCourses({ ...payload, userid: res.id })
        .catch((e) => {
          console.log('[ERROR] gets courses : ', e?.response?.data || e);
          return Promise.reject(e?.response?.data || e);
        });
      return data.data;
    });
  }
  const { data } = await effects.api.getsCourses(payload).catch((e) => {
    console.log('[ERROR] gets courses : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getsAdminCourses = async (
  { effects }: Context,
  payload: {
    title?: string;
    category_id?: number;
    user_id?: number;
    status?: string;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
  }
) => {
  const { data } = await effects.api.getsCourses(payload).catch((e) => {
    console.log('[ERROR] gets courses : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getMyCourse = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.getsMyCourse(payload).catch((e) => {
    console.log('[ERROR] get my course : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getCourse = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.getCourse(payload).catch((e) => {
    console.log('[ERROR] get course : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const deleteCourse = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.deleteCourse(payload).catch((e) => {
    console.log('[ERROR] delete course : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const createCourse = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.createCourse(payload).catch((e) => {
    console.log('[ERROR] create Course : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const updateCourse = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.updateCourse(payload).catch((e) => {
    console.log('[ERROR] update Course : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getsTop3Courses = async ({ effects }: Context) => {
  const { data } = await effects.api.getsTop3Courses().catch((e) => {
    console.log('[ERROR] get top 3 Course : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getsRecomendedCourses = async ({ effects }: Context) => {
  const { data } = await effects.api.getsRecomendedCourses().catch((e) => {
    console.log('[ERROR] get recommended Course : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getsPopularCourses = async ({ effects }: Context) => {
  const { data } = await effects.api.getsPopularCourses().catch((e) => {
    console.log('[ERROR] get popular Course : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};

export const setSelectedCourseFromCourses = async (
  { state }: Context,
  payload: {}
) => {
  state.selectedCourse = payload;
};

export const getsCourseSections = async (
  { effects }: Context,
  payload: {
    title?: string;
    course_id?: number;
    user_id?: number;
    status?: string;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
  }
) => {
  const { data } = await effects.api.getsCourseSections(payload).catch((e) => {
    console.log('[ERROR] gets courses section : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getCourseSection = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.getCourseSection(payload).catch((e) => {
    console.log('[ERROR] get course section : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const deleteCourseSection = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.deleteCourseSection(payload).catch((e) => {
    console.log('[ERROR] delete course section: ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const createCourseSection = async (
  { effects }: Context,
  payload: {}
) => {
  const { data } = await effects.api.createCourseSection(payload).catch((e) => {
    console.log('[ERROR] create course section : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const updateCourseSection = async (
  { effects }: Context,
  payload: {}
) => {
  const { data } = await effects.api.updateCourseSection(payload).catch((e) => {
    console.log('[ERROR] update Course section : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const sortCourseSection = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.sortCourseSection(payload).catch((e) => {
    console.log('[ERROR] sort course section : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getsCourseLesson = async (
  { effects }: Context,
  payload: {
    title?: string;
    course_id?: number;
    is_admin?: string;
    section_id?: number;
    user_id?: number;
    status?: string;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
  }
) => {
  const { data } = await effects.api.getsCourseLesson(payload).catch((e) => {
    console.log('[ERROR] gets courses lesson : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getCourseLesson = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.getCourseLesson(payload).catch((e) => {
    console.log('[ERROR] get course lesson : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const deleteCourseLesson = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.deleteCourseLesson(payload).catch((e) => {
    console.log('[ERROR] delete course lesson: ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const createCourseLesson = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.createCourseLesson(payload).catch((e) => {
    console.log('[ERROR] create course lesson : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const updateCourseLesson = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.updateCourseLesson(payload).catch((e) => {
    console.log('[ERROR] update Course lesson : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const sortCourseLesson = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.sortCourseLesson(payload).catch((e) => {
    console.log('[ERROR] sort course lesson : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getSummaryCourses = async ({ effects }: Context) => {
  const { data } = await effects.api.getSummaryCourses().catch((e) => {
    console.log('[ERROR] get summary : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
// ====== Instructor Section
export const getsInstructor = async (
  { state }: Context,
  payload: { skip: number; limit: number }
) => {
  const newData = [...InstructorData];
  console.log(state.filter);
  return {
    data: newData.slice(payload.skip, payload.limit + payload.skip),
    total: InstructorData.length,
  };
};

// ====== Student Section
export const getsStudent = async (
  { state }: Context,
  payload: { skip: number; limit: number }
) => {
  const newData = [...StudentsData];
  console.log(state.filter);
  return {
    data: newData.slice(payload.skip, payload.limit + payload.skip),
    total: StudentsData.length,
  };
};

// ====== Rating Section
export const getListRatings = async (
  { effects }: Context,
  payload: {
    id?: string;
    course_id?: number;
    user_id?: number;
    rating?: number;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
  }
) => {
  const { data } = await effects.api.getListRatings(payload).catch((e) => {
    console.log('[ERROR] gets list rating : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const createRating = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.createRating(payload).catch((e) => {
    console.log('[ERROR] create rating : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};

// ====== Cart Section
export const getListOrder = async (
  { effects, state }: Context,
  payload: {
    id?: string;
    user_id?: number;
    status?: string;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
  }
) => {
  const { data } = await effects.api.getListOrder(payload).catch((e) => {
    console.log('[ERROR] gets list order : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  state.orders = data.data;
  return data.data;
};
export const getOrder = async (
  { effects }: Context,
  payload: {
    order_id: string;
  }
) => {
  const { data } = await effects.api.getOrder(payload).catch((e) => {
    console.log('[ERROR] get order : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};

export const createOrder = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.createOrder(payload).catch((e) => {
    console.log('[ERROR] create order : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const checkoutOrder = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.checkoutOrder(payload).catch((e) => {
    console.log('[ERROR] checkout order : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const deleteOrder = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.deleteOrder(payload).catch((e) => {
    console.log('[ERROR] delete order : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const updateOrder = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.updateOrder(payload).catch((e) => {
    console.log('[ERROR] update order : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const createOrderSubscription = async (
  { effects }: Context,
  payload: {}
) => {
  const { data } = await effects.api
    .createOrderSubscription(payload)
    .catch((e) => {
      console.log('[ERROR] create order : ', e?.response?.data || e);
      return Promise.reject(e?.response?.data || e);
    });
  return data.data;
};
// ====== Enrol Section
export const getListEnrol = async (
  { effects }: Context,
  payload: {
    id?: string;
    user_id?: number;
    course_id?: number;
    order_id?: number;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
  }
) => {
  const { data } = await effects.api.getListEnrol(payload).catch((e) => {
    console.log('[ERROR] gets list enrol : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getEnrol = async (
  { effects }: Context,
  payload: {
    id: string;
  }
) => {
  const { data } = await effects.api.getEnrol(payload).catch((e) => {
    console.log('[ERROR] get enrol : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const createEnrol = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.createEnrol(payload).catch((e) => {
    console.log('[ERROR] create enrol : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const updateEnrol = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.updateEnrol(payload).catch((e) => {
    console.log('[ERROR] update enrol : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const deleteEnrol = async (
  { effects }: Context,
  payload: {
    id: string;
  }
) => {
  const { data } = await effects.api.deleteEnrol(payload).catch((e) => {
    console.log('[ERROR] deletele : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
// ====== Subscribtion Section
export const getListSubscription = async (
  { effects }: Context,
  payload: {
    id?: string;
    user_id?: string;
    name?: string;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
  }
) => {
  const { data } = await effects.api.getListSubscription(payload).catch((e) => {
    console.log('[ERROR] gets list enrol : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getSubscription = async (
  { effects }: Context,
  payload: {
    id: string;
  }
) => {
  const { data } = await effects.api.getSubscription(payload).catch((e) => {
    console.log('[ERROR] get subscription : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const createSubscription = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.createSubscription(payload).catch((e) => {
    console.log('[ERROR] create subscription : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const updateSubscription = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.updateSubscription(payload).catch((e) => {
    console.log('[ERROR] update subscription : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const deleteSubscription = async (
  { effects }: Context,
  payload: {
    id: string;
  }
) => {
  const { data } = await effects.api.deleteSubscription(payload).catch((e) => {
    console.log('[ERROR] delete subscription : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};

// ====== Blogs Section
export const getListBlogs = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getListBlogs(payload).catch((e) => {
    console.log('[ERROR] get blogs : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getBlog = async (
  { effects }: Context,
  payload: {
    slug: string;
  }
) => {
  const { data } = await effects.api.getBlog(payload).catch((e) => {
    console.log('[ERROR] get blog : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const createBlog = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.createBlog(payload).catch((e) => {
    console.log('[ERROR] create blog : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const updateBlog = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.updateBlog(payload).catch((e) => {
    console.log('[ERROR] update blog : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const deleteBlog = async (
  { effects }: Context,
  payload: {
    id: string;
  }
) => {
  const { data } = await effects.api.deleteBlog(payload).catch((e) => {
    console.log('[ERROR] delete blog : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};

// ====== Region Section
export const getListProvince = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getListProvince(payload).catch((e) => {
    console.log('[ERROR] get province : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getListRegency = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getListRegency(payload).catch((e) => {
    console.log('[ERROR] get regency : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getListDistrict = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getListDistrict(payload).catch((e) => {
    console.log('[ERROR] get district : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const getListVilage = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getListVilage(payload).catch((e) => {
    console.log('[ERROR] get vilage : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};

// ====== Enrol subscription Section
export const getListEnrolSubsciption = async (
  { effects }: Context,
  payload: {}
) => {
  const { data } = await effects.api
    .getListEnrolSubsciption(payload)
    .catch((e) => {
      console.log('[ERROR] gets enrol subscription : ', e?.response?.data || e);
      return Promise.reject(e?.response?.data || e);
    });
  return data.data;
};
export const getEnrolSubsciption = async (
  { effects }: Context,
  payload: {}
) => {
  const { data } = await effects.api.getEnrolSubsciption(payload).catch((e) => {
    console.log('[ERROR] get enrol subscription : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const updatetEnrolSubsciption = async (
  { effects }: Context,
  payload: {}
) => {
  const { data } = await effects.api
    .updatetEnrolSubsciption(payload)
    .catch((e) => {
      console.log('[ERROR] update enrol subs : ', e?.response?.data || e);
      return Promise.reject(e?.response?.data || e);
    });
  return data.data;
};

// ====== Dashboard Section
export const getsDashboard = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getsDashboard(payload).catch((e) => {
    console.log('[ERROR] get summary : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const getsReportRevenue = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getsReportRevenue(payload).catch((e) => {
    console.log('[ERROR] get getsReportRevenue : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};

// ====== Instrument Section
export const getInstrument = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.getInstrument(payload).catch((e) => {
    console.log('[ERROR] get instrument : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const getListInstrument = async (
  { effects }: Context,
  payload: {
    id?: string;
    status?: string;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
  }
) => {
  const { data } = await effects.api.getListInstrument(payload).catch((e) => {
    console.log('[ERROR] gets instrument section : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data.data;
};
export const deleteInstrument = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.deleteInstrument(payload).catch((e) => {
    console.log('[ERROR] delete instrument : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const importInstrument = async (
  { effects }: Context,
  payload: FormData
) => {
  const { data } = await effects.api.importInstrument(payload).catch((e) => {
    console.log('[ERROR] import instrument : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const downloadTemplateInstrument = async ({ effects }: Context) => {
  const { data } = await effects.api.downloadTemplateInstrument().catch((e) => {
    console.log(
      '[ERROR] download template instrument : ',
      e?.response?.data || e
    );
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
// ====== Instrument Category Section
export const getInstrumentCategory = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api
    .getInstrumentCategory(payload)
    .catch((e) => {
      console.log('[ERROR] get instrument category : ', e?.response?.data || e);
      return Promise.reject(e?.response?.data || e);
    });
  return data;
};
export const getInstrumentCategories = async (
  { effects }: Context,
  payload: {
    id?: string;
    name?: string;
    status?: string;
    skip?: number;
    limit?: number;
    sort?: string;
    sort_type?: string;
  }
) => {
  const { data } = await effects.api
    .getInstrumentCategories(payload)
    .catch((e) => {
      console.log(
        '[ERROR] gets instrument categories : ',
        e?.response?.data || e
      );
      return Promise.reject(e?.response?.data || e);
    });
  return data.data;
};
export const deleteInstrumentCategory = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api
    .deleteInstrumentCategory(payload)
    .catch((e) => {
      console.log(
        '[ERROR] delete instrument category : ',
        e?.response?.data || e
      );
      return Promise.reject(e?.response?.data || e);
    });
  return data;
};
export const createInstrumentCategory = async (
  { effects }: Context,
  payload: {}
) => {
  const { data } = await effects.api
    .createInstrumentCategory(payload)
    .catch((e) => {
      console.log(
        '[ERROR] create instrument category : ',
        e?.response?.data || e
      );
      return Promise.reject(e?.response?.data || e);
    });
  return data;
};
export const updateInstrumentCategory = async (
  { effects }: Context,
  payload: {}
) => {
  const { data } = await effects.api
    .updateInstrumentCategory(payload)
    .catch((e) => {
      console.log(
        '[ERROR] update instrument category : ',
        e?.response?.data || e
      );
      return Promise.reject(e?.response?.data || e);
    });
  return data;
};

// ====== Notification
export const sendNotification = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.sendNotification(payload).catch((e) => {
    console.log('[ERROR] send notification : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};

// ====== Portofolio
export const getsPortofolio = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getsPortofolio(payload).catch((e) => {
    console.log('[ERROR] getsPortofolio : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const createPortofolio = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.createPortofolio(payload).catch((e) => {
    console.log('[ERROR] createPortofolio : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const deletePortofolio = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.deletePortofolio(payload).catch((e) => {
    console.log('[ERROR] deletePortofolio : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};

// ====== Referral
export const getsReferral = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.getsReferral(payload).catch((e) => {
    console.log('[ERROR] getsReferral : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const getReferral = async (
  { effects }: Context,
  payload: { id: string }
) => {
  const { data } = await effects.api.getReferral(payload).catch((e) => {
    console.log('[ERROR] getReferral : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const createReferral = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.createReferral(payload).catch((e) => {
    console.log('[ERROR] createReferral : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const updateReferral = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.updateReferral(payload).catch((e) => {
    console.log('[ERROR] updateReferral : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const deleteReferral = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.deleteReferral(payload).catch((e) => {
    console.log('[ERROR] deleteReferral : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const validateReferral = async ({ effects }: Context, payload: {}) => {
  const { data } = await effects.api.validateReferral(payload).catch((e) => {
    console.log('[ERROR] validateReferral : ', e?.response?.data || e);
    return Promise.reject(e?.response?.data || e);
  });
  return data;
};
export const changeSidebar = async ({ state }: Context, payload: string) => {
  state.sidebar = payload;
};
