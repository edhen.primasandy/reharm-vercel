type State = {
  title: string;
  auth: {
    isLogin: boolean;
    accessToken: any;
  };
  user: any;
  setting: any;
  filter: {
    skip: number;
    limit: number;
    sort: string;
    sort_type: string;
  };
  selectedCourse: any;
  orders: any;
  sidebar: string;
};
const state: State = {
  title: 'Reharm',
  auth: {
    isLogin: false,
    accessToken: undefined,
  },
  user: null,
  setting: null,
  filter: {
    skip: 0,
    limit: 10,
    sort: '',
    sort_type: 'asc',
  },
  selectedCourse: null,
  orders: null,
  sidebar: 'expanded',
};

export default state;
