import http from '@utils/http';

export const api = {
  // ====== User / Auth Section
  signIn(payload: any) {
    return http.post('/auth/login', payload);
  },
  signUp(payload: any) {
    return http.post('/auth/register', payload);
  },
  logOut() {
    return http.get('/auth/logout');
  },
  forgotPassword(payload: any) {
    return http.post('/auth/forgot-password', payload);
  },
  getUserProfile() {
    return http.get('/auth/user-profile');
  },
  getsUsers(payload: any) {
    return http.get('/user', { params: payload });
  },
  getUser(payload: any) {
    return http.get(`/user/${payload.id}`);
  },
  getUserInstructor(payload: any) {
    return http.get(`/user/${payload.id}/instructor`);
  },
  updateUser(payload: any) {
    return http.put(`/user/${payload.id}`, payload);
  },
  createUser(payload: any) {
    return http.post(`/user`, payload);
  },
  deleteUser(payload: any) {
    return http.delete(`/user/deactivate/${payload.id}`);
  },
  changePassword(payload: any) {
    return http.post(`/auth/change-password`, payload);
  },
  uploadPhotoUser(payload: any) {
    return http.post(`/user/upload-photo/${payload.id}`, payload);
  },
  updateLessonProgress(payload: any) {
    return http.post(`/user/update-progress/${payload.id}`, payload);
  },
  getCertificate(payload: any) {
    return http.post(`/user/certificate`, payload);
  },

  // ===== Apple Credential Section
  getAppleCredential(payload: any) {
    return http.get('/apple-credential', { params: payload });
  },
  setAppleCredential(payload: any) {
    return http.post('/apple-credential', payload);
  },

  // ===== Setting Section
  getsSetting(payload: any) {
    return http.get('/setting', { params: payload });
  },
  getSettingByKey(payload: any) {
    return http.get(`/setting/${payload.key}`);
  },
  updateSetting(payload: any) {
    return http.put('/setting', payload);
  },

  // ===== Categories Section
  getsCategories() {
    return http.get('/category/tree');
  },
  getCategory(payload: any) {
    return http.get('/category/tree', { params: payload });
  },
  createCategory(payload: any) {
    return http.post(`/category`, payload);
  },
  deleteCategory(payload: any) {
    return http.delete(`/category/${payload.id}`);
  },
  updateCategory(payload: any) {
    return http.put(`/category/${payload.id}`, payload);
  },

  // ===== Courses Section
  getsCourses(payload: any) {
    return http.get('/course', { params: payload });
  },
  getsMyCourses(payload: any) {
    return http.get(`/course/my-course/${payload.userid}`, {
      params: payload,
    });
  },
  getsMyCourse(payload: any) {
    return http.get(`/course/my-course/${payload.id}`, {
      params: payload,
    });
  },
  getCourse(payload: any) {
    return http.get(`/course/${payload.id}`);
  },
  deleteCourse(payload: any) {
    return http.delete(`/course/${payload.id}`);
  },
  createCourse(payload: any) {
    return http.post(`/course`, payload);
  },
  updateCourse(payload: any) {
    return http.put(`/course/${payload.id}`, payload);
  },
  getsCourseSections(payload: any) {
    return http.get('/section', { params: payload });
  },
  getCourseSection(payload: any) {
    return http.get(`/section/${payload.id}`);
  },
  deleteCourseSection(payload: any) {
    return http.delete(`/section/${payload.id}`);
  },
  createCourseSection(payload: any) {
    return http.post(`/section`, payload);
  },
  updateCourseSection(payload: any) {
    return http.put(`/section/${payload.id}`, payload);
  },
  sortCourseSection(payload: any) {
    return http.post(`/section/sort`, payload);
  },
  getsCourseLesson(payload: any) {
    return http.get('/lesson', { params: payload });
  },
  getCourseLesson(payload: any) {
    return http.get(`/lesson/${payload.id}`);
  },
  deleteCourseLesson(payload: any) {
    return http.delete(`/lesson/${payload.id}`);
  },
  createCourseLesson(payload: any) {
    return http.post(`/lesson`, payload);
  },
  updateCourseLesson(payload: any) {
    return http.put(`/lesson/${payload.id}`, payload);
  },
  sortCourseLesson(payload: any) {
    return http.post(`/lesson/sort`, payload);
  },
  getsTop3Courses() {
    return http.post('/course/top-3');
  },
  getsRecomendedCourses() {
    return http.post('/course/recommend');
  },
  getsPopularCourses() {
    return http.post('/course/popular');
  },
  getSummaryCourses() {
    return http.post('/course/summary');
  },

  // ===== Rating Section
  getListRatings(payload: any) {
    return http.get('/rating', { params: payload });
  },
  createRating(payload: any) {
    return http.post('/rating', payload);
  },

  // ===== Cart Section
  getListOrder(payload: any) {
    return http.get('/order', { params: payload });
  },
  getOrder(payload: any) {
    // return http.get(`/order/${payload.id}`);
    return http.get(`/order/${payload.order_id}/detail`);
  },
  createOrder(payload: any) {
    return http.post(`/order`, payload);
  },
  checkoutOrder(payload: any) {
    return http.post(`/order/checkout/${payload.id}`);
  },
  deleteOrder(payload: any) {
    return http.delete(`/order/${payload.id}`);
  },
  updateOrder(payload: any) {
    return http.put(`/order/${payload.id}`, payload);
  },
  createOrderSubscription(payload: any) {
    return http.post(`/order/subscription`, payload);
  },

  // ===== Enrol Section
  getListEnrol(payload: any) {
    return http.get('/enrol', { params: payload });
  },
  getEnrol(payload: any) {
    return http.get(`/enrol/${payload.id}`);
  },
  createEnrol(payload: any) {
    return http.post(`/enrol`, payload);
  },
  updateEnrol(payload: any) {
    return http.put(`/enrol/${payload.id}`);
  },
  deleteEnrol(payload: any) {
    return http.delete(`/enrol/${payload.id}`);
  },

  // ===== Subscription Section
  getListSubscription(payload: any) {
    return http.get('/subscription', { params: payload });
  },
  getSubscription(payload: any) {
    return http.get(`/subscription/${payload.id}`);
  },
  createSubscription(payload: any) {
    return http.post(`/subscription`, payload);
  },
  updateSubscription(payload: any) {
    return http.put(`/subscription/${payload.id}`, payload);
  },
  deleteSubscription(payload: any) {
    return http.delete(`/subscription/${payload.id}`);
  },

  // ===== Blogs Section
  getListBlogs(payload: any) {
    return http.get('/article', { params: payload });
  },
  getBlog(payload: any) {
    return http.get(`/article/${payload.slug}`);
  },
  createBlog(payload: any) {
    return http.post(`/article`, payload);
  },
  updateBlog(payload: any) {
    return http.put(`/article/${payload.id}`, payload);
  },
  deleteBlog(payload: any) {
    return http.delete(`/article/${payload.id}`);
  },

  // ===== Region Section
  getListProvince(payload: any) {
    return http.get('/region/province', { params: payload });
  },
  getListRegency(payload: any) {
    return http.get('/region/regency', { params: payload });
  },
  getListDistrict(payload: any) {
    return http.get('/region/district', { params: payload });
  },
  getListVilage(payload: any) {
    return http.get('/region/village', { params: payload });
  },
  // ===== Enrol subscription Section
  getListEnrolSubsciption(payload: any) {
    return http.get('/enrol-subscription', { params: payload });
  },
  getEnrolSubsciption(payload: any) {
    return http.get(`/enrol-subscription/${payload.id}`);
  },
  updatetEnrolSubsciption(payload: any) {
    return http.put(`/enrol-subscription/${payload.id}`, payload);
  },

  // ===== Dashboard Section
  getsDashboard(payload: any) {
    return http.get('/dashboard', { params: payload });
  },
  getsReportRevenue(payload: any) {
    return http.get('/dashboard/revenue', { params: payload });
  },

  // ===== Instruments
  getListInstrument(payload: any) {
    return http.get('/instrument', { params: payload });
  },
  getInstrument(payload: any) {
    return http.get(`/instrument/${payload.id}`);
  },
  deleteInstrument(payload: any) {
    return http.delete(`/instrument/${payload.id}`);
  },
  importInstrument(payload: any) {
    return http.post(`/instrument`, payload, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
  downloadTemplateInstrument() {
    return http.post('/instrument/export');
  },
  // ===== Instruments Category
  getInstrumentCategories(payload: any) {
    return http.get('/instrument-category', { params: payload });
  },
  getInstrumentCategory(payload: any) {
    return http.get(`/instrument-category/${payload.id}`);
  },
  deleteInstrumentCategory(payload: any) {
    return http.delete(`/instrument-category/${payload.id}`);
  },
  createInstrumentCategory(payload: any) {
    return http.post(`/instrument-category`, payload);
  },
  updateInstrumentCategory(payload: any) {
    return http.put(`/instrument-category/${payload.id}`, payload);
  },
  // ===== Notification
  sendNotification(payload: any) {
    return http.post('/notification', payload);
  },

  // ===== Portofolio
  getsPortofolio(payload: any) {
    return http.get('/portofolio', { params: payload });
  },
  deletePortofolio(payload: any) {
    return http.delete(`/portofolio/${payload.id}`);
  },
  createPortofolio(payload: any) {
    return http.post('/portofolio', payload);
  },

  // ====== Referral
  getsReferral(payload: any) {
    return http.get('/referral', { params: payload });
  },
  getReferral(payload: any) {
    return http.get(`/referral/${payload.id}`);
  },
  deleteReferral(payload: any) {
    return http.delete(`/referral/${payload.id}`);
  },
  createReferral(payload: any) {
    return http.post('/referral', payload);
  },
  updateReferral(payload: any) {
    return http.put(`/referral/${payload.id}`, payload);
  },
  validateReferral(payload: any) {
    return http.post('/order/referral', payload);
  },
};
