import config from '@constant/config';
import { useRouter } from 'next/router';
import React from 'react';
import { Cookies } from 'react-cookie';

import { useActions, useState } from '@/overmind';

const AuthProvider = ({ children }: any) => {
  const { loadApp } = useActions();
  const { auth, user } = useState();
  const router = useRouter();
  const cookies = new Cookies();
  const paths = [
    '/admin',
    '/cart',
    '/order',
    '/profile',
    '/wishlist',
    '/my-course',
  ];
  const pathsWhitelist = ['/orders/[...id]'];

  const authRoute = async () => {
    const checkRoute = paths.some((r: any) => router.pathname.indexOf(r) >= 0);
    const checkWL = pathsWhitelist.some(
      (r: any) => router.pathname.indexOf(r) >= 0
    );
    // const checkRoute = paths?.includes(router.pathname);
    if (user?.role === 'user' && checkRoute) {
      if (router.pathname.includes('/admin')) {
        router.push('/');
      }
    }
    if (!cookies.get('_reharm_token') && checkRoute && !checkWL) {
      router.push('/login');
    }
  };
  const load = async () => {
    await loadApp().then(() => authRoute());
  };

  React.useEffect(() => {
    load();
    const snapSrcUrl = config.midtransURL || '';
    const myMidtransClientKey = config.midtransClientKey || '';
    const script = document.createElement('script');
    script.src = snapSrcUrl;
    script.setAttribute('data-client-key', myMidtransClientKey);
    script.async = true;

    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);
  React.useEffect(() => {
    authRoute();
    // if (auth.isLogin) {
    //   console.log('asdasd');
    //   getListOrder({ user_id: user.id });
    // }
    return () => {};
  }, [auth, user, router.pathname]);

  // React.useEffect(() => {
  //   if (auth.isLogin) {
  //     console.log('asdasd');
  //     getListOrder({ user_id: user.id });
  //   }
  //   return () => {};
  // }, [user, auth]);
  return children;
};

export default AuthProvider;
