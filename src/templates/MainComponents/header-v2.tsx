/* eslint-disable tailwindcss/migration-from-tailwind-2 */
/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable tailwindcss/no-custom-classname */
// import Button from '@/components/elements/button';
import Logo from '@images/logo-light.png';
import {
  // Avatar,
  Button as ButtonM,
  Collapse,
  IconButton,
  List,
  ListItem,
  Menu,
  MenuHandler,
  MenuItem,
  MenuList,
  Navbar,
  Typography,
} from '@material-tailwind/react';
import { Avatar } from 'flowbite-react';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { AiOutlineShoppingCart } from 'react-icons/ai';
import { BiArchive, BiHeart, BiListUl, BiLogOut, BiUser } from 'react-icons/bi';
import { BsMusicNoteBeamed } from 'react-icons/bs';
import { FaGuitar } from 'react-icons/fa';
import { GiDrumKit, GiGuitarBassHead } from 'react-icons/gi';
import { HiChevronDown } from 'react-icons/hi';
import { MdClose, MdMenu, MdOutlinePiano } from 'react-icons/md';
import { RiListSettingsLine } from 'react-icons/ri';

import Button from '@/components/elements/button';
import { useActions, useState } from '@/overmind/index';

type PropsMenu = {
  Categories: any;
};
function ProfileMenu() {
  const router = useRouter();
  const { user } = useState();
  const { logOut } = useActions();
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);

  const closeMenu = () => setIsMenuOpen(false);
  const onLogOut = () => {
    logOut().then(() => {
      window.location.reload();
    });
  };
  // profile menu component
  const profileMenuItems = [
    {
      label: 'User profile',
      icon: <BiUser size={20} />,
      type: 'link',
      route: '/profile',
      role: ['user', 'instructor', 'admin'],
    },
    {
      label: 'Orders List',
      icon: <BiListUl size={20} />,
      type: 'link',
      route: '/orders',
      role: ['user', 'instructor', 'admin'],
    },
    {
      label: 'My Wishlist',
      icon: <BiHeart size={20} />,
      type: 'link',
      route: '/wishlist',
      role: ['user', 'instructor', 'admin'],
    },
    {
      label: 'Administration',
      icon: <RiListSettingsLine size={20} />,
      type: 'link',
      route: '/admin',
      role: ['instructor', 'admin'],
    },
    {
      label: 'Sign Out',
      icon: <BiLogOut size={20} className="text-red" color="red" />,
      type: 'action',
      role: ['user', 'instructor', 'admin'],
    },
  ];
  return (
    <Menu
      open={isMenuOpen}
      handler={setIsMenuOpen}
      placement="bottom-end"
      allowHover={true}
    >
      <MenuHandler>
        <ButtonM
          variant="text"
          color="gray"
          className="flex w-[4.5rem] items-center gap-1 rounded-full py-0.5 pr-2 pl-0.5"
        >
          {/* <div className="flex-1">
            <Avatar
              variant="circular"
              size="sm"
              alt="avatar"
              className="border border-white p-0.5"
              src={
                user?.photo
                  ? user?.photo
                  : 'https://flowbite.com/docs/images/people/profile-picture-5.jpg'
              }
            />
          </div> */}
          <Avatar
            img={
              user?.photo
                ? user?.photo
                : 'https://flowbite.com/docs/images/people/profile-picture-5.jpg'
            }
            rounded={true}
            bordered={true}
          />
          <HiChevronDown
            color="white"
            className={`h-3 w-3 transition-transform ${
              isMenuOpen ? 'rotate-180' : ''
            }`}
          />
        </ButtonM>
      </MenuHandler>
      <MenuList className="z-20 p-1">
        <div className="mb-2">
          <span className="px-3 pb-2 text-sm">Hi, {`${user?.first_name}`}</span>
        </div>
        {profileMenuItems
          .filter((o) => o.role.includes(user?.role))
          .map(({ label, icon, type, route }, key) => {
            const isLastItem = key === profileMenuItems.length - 1;
            return (
              <MenuItem
                key={label}
                onClick={() => {
                  if (type === 'link') {
                    router.push(route || '');
                  } else if (label === 'Sign Out') {
                    onLogOut();
                  }
                  closeMenu();
                }}
                className={`flex items-center gap-2 rounded hover:bg-gray-100 ${
                  isLastItem
                    ? 'hover:bg-red-500/10 focus:bg-red-500/10 active:bg-red-500/10'
                    : ''
                }`}
              >
                {icon}
                <Typography
                  as="span"
                  variant="small"
                  className="font-normal"
                  color={isLastItem ? 'red' : 'inherit'}
                >
                  {label}
                </Typography>
              </MenuItem>
            );
          })}
      </MenuList>
    </Menu>
  );
}
const NavListMenu = (props: PropsMenu) => {
  const [isMenuOpen, setIsMenuOpen] = React.useState(false);
  const [isMobileMenuOpen, setIsMobileMenuOpen] = React.useState(false);
  const itemCategory = (cat: string) => {
    switch (cat) {
      case 'Piano':
        return {
          color: 'bg-blue-50 text-blue-500',
          icon: <MdOutlinePiano className="h-6 w-6" />,
        };
      case 'Gitar':
        return {
          color: 'bg-orange-50 text-orange-500',
          icon: <FaGuitar className="h-6 w-6" />,
        };
      case 'Guitar':
        return {
          color: 'bg-orange-50 text-orange-500',
          icon: <FaGuitar className="h-6 w-6" />,
        };
      case 'Bass':
        return {
          color: 'bg-purple-50 text-purple-500',
          icon: <GiGuitarBassHead className="h-6 w-6" />,
        };
      case 'Drum':
        return {
          color: 'bg-green-50 text-green-500',
          icon: <GiDrumKit className="h-6 w-6" />,
        };
      default:
        return {
          color: 'bg-gray-100 text-gray-500',
          icon: <BsMusicNoteBeamed className="h-6 w-6" />,
        };
    }
  };
  const renderItems = () => {
    return (
      <>
        {props.Categories.map((item: any, key: number) => (
          <Link href={`/courses?category=${item.id}`} key={key}>
            <MenuItem className="mt flex w-full items-center gap-3 rounded-lg">
              <div
                className={`rounded-lg p-5 ${
                  itemCategory(item.name).color
                } flex w-full items-center justify-center gap-3`}
              >
                {itemCategory(item.name).icon}
                <div>
                  <Typography
                    variant="h6"
                    color="black"
                    className="flex items-center text-sm"
                  >
                    {item.name}
                  </Typography>
                </div>
              </div>
            </MenuItem>
          </Link>
        ))}
        <Link href={`/courses`}>
          <MenuItem className="w-fullitems-center flex gap-3 rounded-lg">
            <div
              className={`flex w-full items-center justify-center gap-3 rounded-lg bg-pink-50 p-5 text-pink-500`}
            >
              <BiArchive className="h-6 w-6" />
              <div>
                <Typography
                  variant="h6"
                  color="black"
                  className="flex items-center text-sm"
                >
                  All Course
                </Typography>
              </div>
            </div>
          </MenuItem>
        </Link>
      </>
    );
  };
  return (
    <React.Fragment>
      <Menu
        open={isMenuOpen}
        handler={setIsMenuOpen}
        offset={{ mainAxis: 15 }}
        placement="bottom"
        allowHover={true}
      >
        <MenuHandler>
          <Typography
            as="div"
            variant="h6"
            className="font-medium"
            color="white"
          >
            <ListItem
              className="flex items-center gap-2 py-2 pr-4 hover:bg-gray-700 hover:bg-opacity-20"
              selected={isMenuOpen || isMobileMenuOpen}
              onClick={() => setIsMobileMenuOpen((cur) => !cur)}
            >
              Courses
              <HiChevronDown
                className={`hidden h-5 w-5 transition-transform lg:block ${
                  isMenuOpen ? 'rotate-180' : ''
                }`}
              />
              <HiChevronDown
                className={`block h-5 w-5 transition-transform lg:hidden ${
                  isMobileMenuOpen ? 'rotate-180' : ''
                }`}
              />
            </ListItem>
          </Typography>
        </MenuHandler>
        <MenuList className="z-20 hidden max-w-screen-xl rounded-xl lg:block">
          <ul className="grid grid-cols-5 gap-y-2">{renderItems()}</ul>
        </MenuList>
      </Menu>
      <div className="block w-full rounded-md lg:hidden">
        <Collapse open={isMobileMenuOpen}>{renderItems()}</Collapse>
      </div>
    </React.Fragment>
  );
};
const Header = () => {
  const { auth, user, orders } = useState();
  const { getsCategories } = useActions();
  const [totalOrderCart, setTotalOrderCart] = React.useState(0);
  const [categories, setCategories] = React.useState([] as any[]);

  React.useEffect(() => {
    getsCategories().then((res: any) => {
      setCategories(res.items);
    });
    return () => {};
  }, []);
  React.useEffect(() => {
    if (orders !== null) {
      const filterDraft = orders.items.filter(
        (o: any) => o.status.toLowerCase() === 'draft'
      );
      if (filterDraft) {
        let count = 0;
        filterDraft.forEach((o: any) => {
          count += o.item_details.length;
        });
        setTotalOrderCart(count);
      } else {
        setTotalOrderCart(0);
      }
    }
    return () => {};
  }, [orders]);
  const [openNav, setOpenNav] = React.useState(false);

  React.useEffect(() => {
    window.addEventListener(
      'resize',
      () => window.innerWidth >= 960 && setOpenNav(false)
    );
  }, []);

  const NavList = () => {
    return (
      <List className="mt-4 mb-6 items-center p-0 lg:my-0 lg:flex-row lg:p-1">
        <Typography variant="h6" color="white" className="font-medium">
          <Link href="/">
            <ListItem className="flex items-center gap-2 py-2 pr-4 hover:bg-gray-700 hover:bg-opacity-20">
              Home
            </ListItem>
          </Link>
        </Typography>
        <Typography variant="h6" color="white" className="font-medium">
          <Link href="/blogs">
            <ListItem className="flex items-center gap-2 py-2 pr-4 hover:bg-gray-700 hover:bg-opacity-20">
              Blogs
            </ListItem>
          </Link>
        </Typography>
        <Typography variant="h6" color="white" className="font-medium">
          <Link href="/instruments">
            <ListItem className="flex items-center gap-2 py-2 pr-4 hover:bg-gray-700 hover:bg-opacity-20">
              Instruments
            </ListItem>
          </Link>
        </Typography>
        <NavListMenu Categories={categories} />
        {auth.isLogin && (
          <Typography variant="h6" color="white" className="font-medium">
            <Link href="/my-courses">
              <ListItem className="flex items-center gap-2 whitespace-nowrap py-2 pr-4 hover:bg-gray-700 hover:bg-opacity-20">
                My courses
              </ListItem>
            </Link>
          </Typography>
        )}
        <Link href={user !== null ? '/cart' : '/login'}>
          <ListItem className="flex items-center gap-2 py-2 pr-4 hover:bg-gray-700 hover:bg-opacity-20">
            <div className="relative">
              <AiOutlineShoppingCart size={30} color="white" />
              {totalOrderCart > 0 && (
                <div className="absolute -top-2 -right-2 inline-flex h-5 w-5 items-center justify-center rounded-full  bg-red-600 text-[10px] font-bold text-white">
                  {totalOrderCart}
                </div>
              )}
            </div>
          </ListItem>
        </Link>
      </List>
    );
  };
  return (
    <header
      className={`main-header header 'on-top' fixed z-10 w-full shadow-lg`}
    >
      {/* <Navbar className="sticky top-0 z-10 h-max max-w-full rounded-none py-2 px-4 lg:px-8 lg:py-4"> */}
      <Navbar className="container mx-auto rounded-none border-0 bg-transparent bg-opacity-100 py-4 shadow-none">
        <div className="flex items-center justify-between text-gray-900">
          <div className="logo">
            <Button href="/">
              <Image
                className="mr-3 w-[200px] lg:w-[220px]"
                src={Logo}
                alt={''}
              />
            </Button>
          </div>
          <div className="relative flex items-center gap-4">
            <div className="hidden lg:block">
              <NavList />
            </div>
            <div className="flex gap-2">
              <IconButton
                variant="text"
                color="white"
                className="ml-auto mr-2 lg:hidden"
                onClick={() => setOpenNav(!openNav)}
              >
                {openNav ? (
                  <MdClose className="h-6 w-6" />
                ) : (
                  <MdMenu className="h-6 w-6" />
                )}
              </IconButton>
              {auth.isLogin ? (
                <ProfileMenu />
              ) : (
                <div className="hidden lg:block">
                  <Link
                    href="/login"
                    className="group relative inline-block rounded-full px-5 py-2.5 font-medium text-white"
                  >
                    <span className="absolute top-0 left-0 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 opacity-50 blur-sm"></span>
                    <span className="absolute inset-0 mt-0.5 ml-0.5 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 opacity-50 group-active:opacity-0"></span>
                    <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 shadow-xl transition-all duration-200 ease-out group-hover:blur-sm group-active:opacity-0"></span>
                    <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-br from-red-500 to-orange-400 transition duration-200 ease-out"></span>
                    <span className="relative">Login</span>
                  </Link>
                </div>
              )}
            </div>
          </div>
        </div>
        <Collapse open={openNav}>
          <div className="rounded-md bg-slate-500 bg-opacity-20">
            <NavList />
            <div className="flex w-full items-center justify-center gap-2 pb-2 lg:hidden">
              {!auth.isLogin && (
                <Link
                  href="/login"
                  className="group relative inline-block rounded-full px-5 py-2.5 font-medium text-white"
                >
                  <span className="absolute top-0 left-0 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 opacity-50 blur-sm"></span>
                  <span className="absolute inset-0 mt-0.5 ml-0.5 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 opacity-50 group-active:opacity-0"></span>
                  <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 shadow-xl transition-all duration-200 ease-out group-hover:blur-sm group-active:opacity-0"></span>
                  <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-br from-red-500 to-orange-400 transition duration-200 ease-out"></span>
                  <span className="relative">Login</span>
                </Link>
              )}
            </div>
          </div>
        </Collapse>
      </Navbar>
      {/* <nav className="container mx-auto flex items-center justify-between py-4">
        <Fade duration={500} cascade direction="up">
          <div className="flex flex-row itk,mems-center md:gap-10">
            <div className="logo">
              <Button href="/">
                <Image className="logo" src={Logo} alt={''} />
              </Button>
            </div>
          </div>
        </Fade>
      </nav> */}
    </header>
  );
};

export default Header;
