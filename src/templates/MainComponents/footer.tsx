/* eslint-disable tailwindcss/no-custom-classname */
import AppStore from '@images/dowload-apple-store.png';
import GooglePlay from '@images/download-google-pay.png';
import Logo from '@images/logo-light.png';
import moment from 'moment';
import Image from 'next/image';
import Link from 'next/link';
import React from 'react';
import { IoMdPhonePortrait } from 'react-icons/io';
import { IoMail } from 'react-icons/io5';

import Button from '@/components/elements/button';
import { useState } from '@/overmind';

const Footer = () => {
  const { setting } = useState();
  const [constants, setConstants] = React.useState({} as any);
  React.useEffect(() => {
    let mapping = {};
    setting?.items?.forEach((item: any) => {
      mapping = { ...mapping, [item.key]: item.value };
    });
    setConstants(mapping);
    return () => {};
  }, [setting]);

  return (
    <footer className="footer">
      <div className="container mx-auto p-4">
        <div className="items-center py-5">
          <div>
            <Button href="/">
              <Image className="logo" src={Logo} alt={''} />
            </Button>
          </div>
          <div className="flex flex-row py-4">
            <div className="flex flex-1 flex-col">
              <span className="mb-10 text-gray-400">
                {constants.system_title}
              </span>
              <span className="mb-3 text-2xl font-semibold text-white">
                Download Reharm App
              </span>
              <div className="flex items-center gap-3">
                <a
                  href={constants.link_play_store}
                  target="_blank"
                  rel="noreferrer"
                >
                  <Image className="w-[200px]" src={GooglePlay} alt={''} />
                </a>
                <a
                  href={constants.link_app_store}
                  target="_blank"
                  rel="noreferrer"
                >
                  <Image className="w-[200px]" src={AppStore} alt={''} />
                </a>
              </div>
            </div>
            <div className="flex flex-1 ">
              <div className="flex flex-1 flex-col px-2">
                <span className="mb-2 text-xl font-semibold text-white">
                  Useful links
                </span>
                <Link
                  href="/courses"
                  className="text-base text-gray-400 hover:text-white"
                >
                  Courses
                </Link>
                <Link
                  href="/login"
                  className="text-base text-gray-400 hover:text-white"
                >
                  Login
                </Link>
                <Link
                  href="/register"
                  className="text-base text-gray-400 hover:text-white"
                >
                  Register
                </Link>
              </div>
              <div className="flex flex-1 flex-col px-2">
                <span className="mb-2 text-xl font-semibold text-white">
                  Contact with us
                </span>
                <Link
                  href={`tel:${constants.phone}`}
                  className="inline-flex items-center gap-2 text-base text-gray-400 hover:text-white"
                >
                  <IoMdPhonePortrait /> {constants.phone}
                </Link>
                <Link
                  href={`mailto:${constants.system_email}`}
                  className="inline-flex items-center gap-2 text-base text-gray-400 hover:text-white"
                >
                  <IoMail /> {constants.system_email}
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="flex items-center justify-between border-t border-gray-700 py-5">
          <div className="flex flex-row">
            <Link
              href="/terms-condition"
              className="mr-2 text-sm text-gray-400 hover:text-white"
            >
              Terms & Conditions
            </Link>
            <span className="mr-2 text-sm text-gray-400">|</span>
            <Link
              href="/privacy-policy"
              className="mr-2 text-sm text-gray-400 hover:text-white"
            >
              Privacy policy
            </Link>
            <span className="mr-2 text-sm text-gray-400">|</span>
            <Link
              href="/about-us"
              className="text-sm text-gray-400 hover:text-white"
            >
              About us
            </Link>
          </div>
          <span className="txt-footer text-sm">{`© Reharm ${moment().format(
            'YYYY'
          )}`}</span>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
