/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable tailwindcss/no-custom-classname */
// import Button from '@/components/elements/button';
import Logo from '@images/logo-light.png';
import { Avatar, Sidebar } from 'flowbite-react';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { Fade } from 'react-awesome-reveal';
import { AiOutlineShoppingCart } from 'react-icons/ai';
import { BiHeart, BiListUl, BiLogOut, BiUser } from 'react-icons/bi';
import { GiGrandPiano, GiHamburgerMenu } from 'react-icons/gi';
import { HiArchive, HiArrowSmRight, HiChartPie, HiUser } from 'react-icons/hi';
import { MdOutlineArticle } from 'react-icons/md';
import { RiListSettingsLine } from 'react-icons/ri';

import Button from '@/components/elements/button';
import { useActions, useState } from '@/overmind/index';

const Header = () => {
  const router = useRouter();
  const { auth, user, orders } = useState();
  const { logOut, getsCategories } = useActions();
  const [navbar, setNavbar] = React.useState(false);
  const [totalOrderCart, setTotalOrderCart] = React.useState(0);
  const [categories, setCategories] = React.useState([] as any[]);
  const onLogOut = () => {
    logOut().then(() => {
      window.location.reload();
    });
  };
  React.useEffect(() => {
    getsCategories().then((res: any) => {
      setCategories(res.items);
    });
    return () => {};
  }, []);
  React.useEffect(() => {
    if (orders !== null) {
      const filterDraft = orders.items.filter(
        (o: any) => o.status.toLowerCase() === 'draft'
      );
      if (filterDraft) {
        let count = 0;
        filterDraft.forEach((o: any) => {
          count += o.item_details.length;
        });
        setTotalOrderCart(count);
      } else {
        setTotalOrderCart(0);
      }
    }
    return () => {};
  }, [orders]);

  return (
    <header
      className={`main-header header 'on-top' fixed z-10 w-full shadow-lg`}
    >
      <nav className="container mx-auto flex items-center justify-between py-4">
        <Fade duration={500} cascade direction="up">
          <div className="flex flex-row items-center md:gap-10">
            <div className="logo">
              <Button href="/">
                <Image className="logo" src={Logo} alt={''} />
              </Button>
            </div>
          </div>
        </Fade>
        <div className="flex flex-row items-center gap-2 lg:gap-5">
          <div className="hidden md:block">
            <div className="flex flex-row items-center gap-2 lg:gap-5">
              <ul className="mt-4 flex flex-col font-medium md:mt-0 md:flex-row md:space-x-8">
                <li className="py-2">
                  <Button
                    href="/"
                    className="block border-b border-gray-100 py-2 pr-4 pl-3 text-white hover:bg-gray-50 md:border-0 md:p-0 md:hover:bg-transparent md:hover:text-orange-400"
                    aria-current="page"
                  >
                    Home
                  </Button>
                </li>
                <li className="py-2">
                  <Button
                    href="/blogs"
                    className="block border-b border-gray-100 py-2 pr-4 pl-3 text-white hover:bg-gray-50 md:border-0 md:p-0 md:hover:bg-transparent md:hover:text-orange-400"
                    aria-current="page"
                  >
                    Blogs
                  </Button>
                </li>
                <li className="py-2">
                  <Button
                    href="/instruments"
                    className="block border-b border-gray-100 py-2 pr-4 pl-3 text-white hover:bg-gray-50 md:border-0 md:p-0 md:hover:bg-transparent md:hover:text-orange-400"
                    aria-current="page"
                  >
                    Instruments
                  </Button>
                </li>
                <li>
                  <div className="main-dropdowon py-2">
                    <div className="block border-b border-gray-100 py-2 pr-4 pl-3 text-white hover:bg-gray-50 md:border-0 md:p-0 md:hover:bg-transparent md:hover:text-orange-400">
                      Courses
                    </div>
                    <ul className="main-dropdowon-menu rounded-md shadow-lg">
                      {categories.map((item: any) => (
                        <li
                          key={item.id}
                          className="rounded-t-md p-2 text-left hover:bg-zinc-100 hover:text-orange-400"
                        >
                          <Link href={`/courses?category=${item.id}`}>
                            {item.name}
                          </Link>
                        </li>
                      ))}
                      <li className="rounded-b-md p-2 text-left hover:bg-zinc-100 hover:text-orange-400">
                        <Link href="/courses">All courses</Link>
                      </li>
                    </ul>
                  </div>
                </li>
                {auth.isLogin && (
                  <li className="py-2">
                    <Button
                      href="/my-courses"
                      className="block border-b border-gray-100 py-2 pr-4 pl-3 text-white hover:bg-gray-50 md:border-0 md:p-0 md:hover:bg-transparent md:hover:text-orange-400"
                      aria-current="page"
                    >
                      My courses
                    </Button>
                  </li>
                )}
              </ul>
              <Link href={user !== null ? '/cart' : '/login'}>
                <div className="relative">
                  <AiOutlineShoppingCart size={30} color="white" />
                  {totalOrderCart > 0 && (
                    <div className="absolute -top-2 -right-2 inline-flex h-5 w-5 items-center justify-center rounded-full  bg-red-600 text-[10px] font-bold text-white">
                      {totalOrderCart}
                    </div>
                  )}
                </div>
              </Link>
              {auth.isLogin ? (
                <div className="main-dropdowon">
                  <div className="block py-1 pr-4 pl-3">
                    <Avatar
                      img={
                        user?.photo
                          ? user?.photo
                          : 'https://flowbite.com/docs/images/people/profile-picture-5.jpg'
                      }
                      rounded={true}
                      bordered={true}
                    />
                  </div>
                  <ul className="main-dropdowon-menu mt-5 rounded-md shadow-lg">
                    <li className="rounded-t-md p-2 text-left hover:bg-zinc-100 hover:text-orange-400">
                      <span className="text-sm">
                        Hi, {`${user?.first_name}`}
                      </span>
                    </li>
                    <li className="flex rounded-b-md p-2 text-left hover:bg-zinc-100 hover:text-orange-400">
                      <button
                        onClick={() => {
                          router.push('/profile');
                        }}
                        className="flex items-center gap-2 text-sm"
                      >
                        <BiUser size={20} />
                        <span>User profile</span>
                      </button>
                    </li>
                    <li className="flex rounded-b-md p-2 text-left hover:bg-zinc-100 hover:text-orange-400">
                      <button
                        onClick={() => {
                          router.push('/orders');
                        }}
                        className="flex items-center gap-2 text-sm"
                      >
                        <BiListUl size={20} />
                        <span>Orders List</span>
                      </button>
                    </li>
                    {/* <li className="flex rounded-b-md p-2 text-left hover:bg-zinc-100 hover:text-orange-400">
                      <button
                        onClick={() => {
                          router.push('/my-courses');
                        }}
                        className="flex items-center gap-2 text-sm"
                      >
                        <MdQueueMusic size={20} />
                        <span>My Courses</span>
                      </button>
                    </li> */}
                    <li className="flex rounded-b-md p-2 text-left hover:bg-zinc-100 hover:text-orange-400">
                      <button
                        onClick={() => {
                          router.push('/wishlist');
                        }}
                        className="flex items-center gap-2 text-sm"
                      >
                        <BiHeart size={20} />
                        <span>My Wishlist</span>
                      </button>
                    </li>
                    {user?.role === 'admin' && (
                      <li className="flex rounded-b-md p-2 text-left hover:bg-zinc-100 hover:text-orange-400">
                        <button
                          onClick={() => {
                            router.push('/admin');
                          }}
                          className="flex items-center gap-2 text-sm"
                        >
                          <RiListSettingsLine size={20} />
                          <span>Administration</span>
                        </button>
                      </li>
                    )}
                    <li className="flex rounded-b-md p-2 text-left hover:bg-zinc-100 hover:text-orange-400">
                      <button
                        onClick={onLogOut}
                        className="flex items-center gap-2 text-sm"
                      >
                        <BiLogOut size={20} />
                        <span>Sign out</span>
                      </button>
                    </li>
                  </ul>
                </div>
              ) : (
                <Link
                  href="/login"
                  className="group relative inline-block rounded-full px-5 py-2.5 font-medium text-white"
                >
                  <span className="absolute top-0 left-0 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 opacity-50 blur-sm"></span>
                  <span className="absolute inset-0 mt-0.5 ml-0.5 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 opacity-50 group-active:opacity-0"></span>
                  <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 shadow-xl transition-all duration-200 ease-out group-hover:blur-sm group-active:opacity-0"></span>
                  <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-br from-red-500 to-orange-400 transition duration-200 ease-out"></span>
                  <span className="relative">Login</span>
                </Link>
              )}
            </div>
          </div>
          {/* mobile version */}
          <button
            className="block cursor-pointer rounded border border-solid border-transparent bg-transparent px-3 py-1 text-xl leading-none outline-none focus:outline-none md:hidden"
            type="button"
            onClick={() => setNavbar(!navbar)}
          >
            <GiHamburgerMenu color="white" />
          </button>
        </div>
      </nav>
      {/* nav bar mobile */}
      <div
        className={`${
          navbar ? ' block' : ' hidden'
        } max-h-[90vh] overflow-y-auto md:hidden`}
      >
        <Fade duration={300} cascade direction="right">
          <div
            className={`grow items-center bg-white lg:flex lg:opacity-0 lg:shadow-none`}
            id="nav-mobile"
          >
            <div className="w-fit">
              <Sidebar
                aria-label="Sidebar with multi-level dropdown example"
                className="w-full"
              >
                <Sidebar.Items>
                  <Sidebar.ItemGroup>
                    <Sidebar.Item href="/" icon={HiChartPie}>
                      Home
                    </Sidebar.Item>
                    <Sidebar.Item href="/blogs" icon={MdOutlineArticle}>
                      Blogs
                    </Sidebar.Item>
                    <Sidebar.Item href="/blogs" icon={GiGrandPiano}>
                      Instrument
                    </Sidebar.Item>
                    <Sidebar.Collapse icon={HiArchive} label="Courses">
                      {categories.map((item: any) => (
                        <Sidebar.Item
                          key={item.id}
                          href={`/courses?category=${item.id}`}
                        >
                          {item.name}
                        </Sidebar.Item>
                      ))}
                      <Sidebar.Item href="/courses">All courses</Sidebar.Item>
                    </Sidebar.Collapse>
                    {auth.isLogin && (
                      <Sidebar.Collapse icon={HiUser} label="My account">
                        <Sidebar.Item href="/profile">
                          User Profile
                        </Sidebar.Item>
                        <Sidebar.Item href="/orders">Orders List</Sidebar.Item>
                        <Sidebar.Item href="/my-courses">
                          My Courses
                        </Sidebar.Item>
                        <Sidebar.Item href="/wishlist">
                          My Wishlist
                        </Sidebar.Item>
                        {user?.role === 'admin' && (
                          <Sidebar.Item href="/admin">
                            Administration
                          </Sidebar.Item>
                        )}
                      </Sidebar.Collapse>
                    )}
                  </Sidebar.ItemGroup>
                  {auth.isLogin ? (
                    <Sidebar.ItemGroup>
                      <Sidebar.Item onClick={onLogOut} icon={HiArrowSmRight}>
                        Sign Out
                      </Sidebar.Item>
                    </Sidebar.ItemGroup>
                  ) : (
                    <Sidebar.ItemGroup>
                      <Link
                        href="/login"
                        className="group relative inline-block rounded-full px-5 py-2.5 font-medium text-white"
                      >
                        <span className="absolute top-0 left-0 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 opacity-50 blur-sm"></span>
                        <span className="absolute inset-0 mt-0.5 ml-0.5 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 opacity-50 group-active:opacity-0"></span>
                        <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-br from-orange-400 to-red-500 shadow-xl transition-all duration-200 ease-out group-hover:blur-sm group-active:opacity-0"></span>
                        <span className="absolute inset-0 h-full w-full rounded-full bg-gradient-to-br from-red-500 to-orange-400 transition duration-200 ease-out"></span>
                        <span className="relative">Login</span>
                      </Link>
                    </Sidebar.ItemGroup>
                  )}
                </Sidebar.Items>
              </Sidebar>
            </div>
          </div>
        </Fade>
      </div>
    </header>
  );
};

export default Header;
