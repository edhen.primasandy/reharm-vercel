import Button from '@components/elements/button';
import { AdminMenu } from '@data/index';
import Logo from '@images/logo-color.png';
import LogoSM from '@images/logo-color-sm.png';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useRef } from 'react';
import { Fade } from 'react-awesome-reveal';
import { BiArchive, BiCategory, BiPackage } from 'react-icons/bi';
import {
  BsChevronDown,
  BsEyeglasses,
  BsImages,
  BsNewspaper,
} from 'react-icons/bs';
import {
  FaHistory,
  FaNetworkWired,
  FaSignOutAlt,
  FaUsers,
} from 'react-icons/fa';
import { FiSettings } from 'react-icons/fi';
import { GiGrandPiano } from 'react-icons/gi';
import { HiOutlineDocumentReport } from 'react-icons/hi';
import { MdOutlineHome } from 'react-icons/md'; // Import icons for expand/collapse
import { RiMenuFoldFill, RiMenuUnfoldFill } from 'react-icons/ri';

import { useActions, useState } from '@/overmind/index';

type MenuItem = {
  id: string;
  name: string;
  icon: string;
  multilevel: boolean;
  url: string;
  role: string[];
  opensub?: boolean; // Make opensub optional
  child?: MenuItem[]; // Allow child items for multilevel menus
};

const RenderingMenu = ({
  router,
  item,
  onOpenSubMenu,
  user,
  collapsed,
}: any) => {
  const path: string = router.route;
  const getPath = path.replace('/[...id]', '');
  let active = getPath === item.url;

  if (item?.multilevel) {
    item.child.forEach((o: any) => {
      active = getPath === o.url;
    });
  }

  const getIcon = (icon: string) => {
    return (
      <>
        {icon === 'MdOutlineHome' && <MdOutlineHome size={25} />}
        {icon === 'BiCategory' && <BiCategory size={25} />}
        {icon === 'BiArchive' && <BiArchive size={25} />}
        {icon === 'BiPackage' && <BiPackage size={25} />}
        {icon === 'BsEyeglasses' && <BsEyeglasses size={25} />}
        {icon === 'FaUsers' && <FaUsers size={25} />}
        {icon === 'FaNetworkWired' && <FaNetworkWired size={25} />}
        {icon === 'HiOutlineDocumentReport' && (
          <HiOutlineDocumentReport size={25} />
        )}
        {icon === 'BsNewspaper' && <BsNewspaper size={25} />}
        {icon === 'GiGrandPiano' && <GiGrandPiano size={25} />}
        {icon === 'FiSettings' && <FiSettings size={25} />}
        {icon === 'BsImages' && <BsImages size={25} />}
        {icon === 'FaHistory' && <FaHistory size={25} />}
      </>
    );
  };
  return (
    <>
      {item.multilevel ? (
        <>
          <Button
            onClick={() => onOpenSubMenu()}
            className={`my-1 flex w-full items-center rounded-lg p-3 hover:bg-fuchsia-300 hover:text-white ${
              item?.opensub || item.child.find((o: any) => getPath === o.url)
                ? 'bg-fuchsia-600 text-white'
                : 'bg-white'
            }`}
          >
            {getIcon(item.icon)}
            {!collapsed && (
              <span className="ml-4 flex-1 text-left">{item.name}</span>
            )}
            <BsChevronDown />
          </Button>
          {item?.opensub && (
            <Fade duration={300} direction="down">
              <ul
                className={`${
                  collapsed
                    ? 'z-100 absolute left-full w-48 rounded-lg border bg-white p-2'
                    : 'bg-slate-50'
                } transition-all delay-1000`}
              >
                {item.child.map((o: any, i: any) => {
                  if (o.role.includes(user?.role)) {
                    const baseClasses =
                      'flex items-center p-3 hover:rounded-lg hover:bg-fuchsia-300 hover:text-white';
                    const activeClasses =
                      'rounded-lg bg-fuchsia-800 text-white';
                    const collapsedClasses = !collapsed ? 'bg-slate-50' : '';

                    const classes = `${baseClasses} ${
                      getPath === o.url ? activeClasses : collapsedClasses
                    }`;
                    return (
                      <li key={i}>
                        <Link href={o.url} className={classes}>
                          {o.icon}
                          <span className="ml-4">{o.name}</span>
                        </Link>
                      </li>
                    );
                  }
                  return <div key={i}></div>;
                })}
              </ul>
            </Fade>
          )}
        </>
      ) : (
        <Link
          href={item.url}
          className={`my-1 flex items-center rounded-lg p-3 hover:bg-fuchsia-300 hover:text-white ${
            active ? 'bg-fuchsia-800 text-white' : 'bg-white'
          }`}
        >
          {getIcon(item.icon)}
          {!collapsed && <span className="ml-4">{item.name}</span>}
        </Link>
      )}
    </>
  );
};

const Sidebar = () => {
  const router = useRouter();
  const path: string = router.route;
  const [collapsed, setCollapsed] = React.useState(false);
  const [menus, setMenus] = React.useState<MenuItem[]>(AdminMenu as any[]);
  const { user } = useState();
  const { logOut, changeSidebar } = useActions();
  const sidebarRef = useRef<HTMLDivElement>(null);

  const onLogOut = () => {
    logOut();
  };

  // Detect screen size to toggle collapse on md and smaller
  React.useEffect(() => {
    const handleResize = () => {
      if (window.innerWidth < 768) {
        setCollapsed(true);
        changeSidebar('collapsed');
      } else {
        setCollapsed(false);
        changeSidebar('expanded');
      }
    };
    window.addEventListener('resize', handleResize);
    handleResize(); // Initial check

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  // Manual toggle for expanding/collapsing the sidebar
  const toggleSidebar = () => {
    if (!collapsed) {
      changeSidebar('collapsed');
    } else {
      changeSidebar('expanded');
    }
    setCollapsed(!collapsed);
  };

  const handleClickOutside = (event: MouseEvent) => {
    if (
      sidebarRef.current &&
      !sidebarRef.current.contains(event.target as Node)
    ) {
      setMenus((prevMenus) =>
        prevMenus.map((menu) => ({ ...menu, opensub: false }))
      );
    }
  };

  React.useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => document.removeEventListener('mousedown', handleClickOutside);
  }, []);
  return (
    <aside
      className={`${
        collapsed ? 'w-24' : 'w-64'
      } transition-width z-50 hidden border-r border-gray-200 bg-white duration-300 md:block`}
      ref={sidebarRef}
    >
      <div
        className={`${collapsed ? '' : 'w-64'} fixed flex h-screen flex-col`}
      >
        {/* Logo and Collapse/Expand Button beside */}
        <div className="flex items-center justify-between p-2">
          <Button href="/">
            <Image
              className={`${collapsed ? 'w-12' : 'w-52'}`}
              src={collapsed ? LogoSM : Logo}
              alt={''}
            />
          </Button>
          <button
            onClick={toggleSidebar}
            className="rounded-full bg-pink-500 p-2 text-white hover:bg-pink-600"
            style={{ position: 'absolute', right: '-15px' }} // Slightly offside
          >
            {collapsed ? (
              <RiMenuUnfoldFill size={20} />
            ) : (
              <RiMenuFoldFill size={20} />
            )}
          </button>
        </div>

        {!collapsed && (
          <div className="flex w-full flex-col items-center">
            <img
              className="mb-3 h-24 w-24 rounded-full object-cover shadow-lg"
              src={
                user?.photo !== null
                  ? user?.photo
                  : 'https://flowbite.com/docs/images/people/profile-picture-5.jpg'
              }
              alt="user photo"
            />
            <h5 className="mb-1 text-lg font-medium text-gray-900 dark:text-white">
              {user?.first_name} {user?.last_name}
            </h5>
          </div>
        )}

        <div className="custom-scrollbar flex-1 overflow-y-auto p-4 text-sm text-gray-600">
          <ul>
            {menus.map((o, i) => {
              const menu = { ...o };
              if (o.role.includes(user?.role)) {
                const getPath = path.replace('/[...id]', '');
                if (o.multilevel) {
                  o.child?.forEach((oo: any) => {
                    if (getPath === oo.url && !collapsed) {
                      menu.opensub = true;
                    }
                  });
                }
                return (
                  <li key={i}>
                    <RenderingMenu
                      router={router}
                      item={menu}
                      user={user}
                      onOpenSubMenu={() => {
                        const newMenus = menus.map((item: any) => {
                          const obj = item;
                          if (item.multilevel) {
                            if (o.id === item.id) {
                              obj.opensub = !item.opensub;
                            } else {
                              obj.opensub = false;
                            }
                          }
                          return obj;
                        });
                        setMenus([...newMenus]);
                      }}
                      collapsed={collapsed}
                    />
                  </li>
                );
              }
              return <div key={i}></div>;
            })}
          </ul>
        </div>

        <div className="p-4">
          <Button
            onClick={onLogOut}
            className="flex w-full items-center justify-center gap-2 rounded-lg bg-orange-500 py-2 text-white hover:bg-orange-600"
          >
            <FaSignOutAlt />
            {!collapsed && <span>Sign out</span>}
          </Button>
        </div>
      </div>
    </aside>
  );
};

export default Sidebar;
