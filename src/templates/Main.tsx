/* eslint-disable tailwindcss/no-custom-classname */
import type { ReactNode } from 'react';

import { Meta } from '@/layouts/Meta';

import Footer from './MainComponents/footer';
// import Header from './MainComponents/header';
import Header from './MainComponents/header-v2';

type IMainProps = {
  meta?: ReactNode;
  children: ReactNode;
};

const Main = (props: IMainProps) => {
  return (
    <>
      <Meta title="Reharm Music Courses" description="Reharm Music Courses" />
      <div className="flex min-h-screen flex-col">
        <Header />
        <div className="content-body flex-1 bg-slate-100 px-5 lg:px-0">
          {props.children}
        </div>
        <Footer />
      </div>
    </>
  );
};

export default Main;
