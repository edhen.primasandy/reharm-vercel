import React from 'react';

import { Meta } from '@/layouts/Meta';
import { useActions } from '@/overmind';

import Sidebar from './AdminComponents/sidebar';

const Admin = ({ children }: any) => {
  const { loadApp } = useActions();
  const [isLogin, setIsLogin] = React.useState(false);

  React.useEffect(() => {
    loadApp().then((res) => setIsLogin(res.isLogin));

    return () => {};
  }, []);

  return (
    <>
      <Meta title="Reharm Music Courses" description="Reharm Music Courses" />
      {isLogin && (
        <div className="flex min-h-screen w-full flex-row bg-slate-50">
          <Sidebar />
          <section className="flex flex-1 flex-col p-4">
            <div className="flex flex-1">{children}</div>
            <footer>
              <div className="flex items-center justify-center border-t py-3">
                <span className="text-sm">&copy; Reharm 2023</span>
              </div>
            </footer>
          </section>
        </div>
      )}
    </>
  );
};

export default Admin;
