/* eslint-disable tailwindcss/no-custom-classname */
import { Label, Textarea } from 'flowbite-react';
import React from 'react';

type Props = {
  id: string;
  label: string;
  value: string;
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
};

const Layout = (props: Props) => {
  return (
    <div className="mb-2">
      <div className="mb-2 block">
        <Label htmlFor={props.id} value={props.label} />
      </div>
      <Textarea
        className="eTextarea"
        value={props.value}
        id={props.id}
        onChange={props.onChange}
      />
    </div>
  );
};

export default Layout;
