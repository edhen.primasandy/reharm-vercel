/* eslint-disable tailwindcss/no-custom-classname */
import '@yaireo/tagify/dist/tagify.css'; // Tagify CSS

import Tags from '@yaireo/tagify/dist/react.tagify';
import React from 'react';

type Props = {
  keywords: string;
  changeKeyword: (values: string) => void;
};

const Layout = (props: Props) => {
  const tagifyRef = React.useRef();
  const [tags, setTags] = React.useState(props.keywords);

  React.useEffect(() => {
    props.changeKeyword(tags);
    return () => {};
  }, [tags]);

  return (
    <Tags
      tagifyRef={tagifyRef} // optional Ref object for the Tagify instance itself, to get access to  inner-methods
      value={props.keywords}
      settings={{ classNames: { namespace: 'tagify tagify-custom' } }}
      className="tagify-input-custom"
      autoFocus={true}
      // className="block w-full rounded-lg border border-gray-300 bg-gray-50 p-2.5 text-sm text-gray-900 focus:border-blue-500 focus:ring-blue-500"
      onChange={(e) => {
        setTags(
          e.detail.tagify
            .getCleanValue()
            .map((o: any) => o.value)
            .toString()
        );
      }}
    />
  );
};

export default Layout;
