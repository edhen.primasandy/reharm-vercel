import React from 'react';
import { useDropzone } from 'react-dropzone';
import { FaImage } from 'react-icons/fa';
import { MdOutlineCancel } from 'react-icons/md';

import Button from '../button';

type Props = {
  imgSource: string;
  baseUrl?: string;
  onChange: (file: any) => void;
};

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden',
};

const img = {
  display: 'block',
  width: 'auto',
  height: '100%',
};
const ImageDropzone = (props: Props) => {
  const { imgSource, onChange } = props;
  const [files, setFiles] = React.useState([]);
  const [imageView, setImageView] = React.useState(imgSource);
  const { getRootProps, getInputProps } = useDropzone({
    accept: {
      'image/*': [],
    },
    multiple: false,
    onDrop: (acceptedFiles: any) => {
      setFiles(
        acceptedFiles.map((file: any) => {
          setImageView(URL.createObjectURL(file));
          return Object.assign(file, {
            preview: URL.createObjectURL(file),
          });
        })
      );

      acceptedFiles.map((file: any) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          onChange(reader.result);
        };
        return file;
      });
    },
  });
  // const getImgUrl = (id: string) => {
  //   return baseUrl + id;
  // };
  const thumbs = files.map((file: any) => (
    <div
      className="relative flex h-52 w-56 rounded border-[1px] p-2"
      key={file.name}
    >
      <Button
        onClick={() => {
          setFiles([]);
          onChange('');
        }}
        className="absolute right-[-10px] top-[-10px] rounded-full border-[1px] bg-white"
      >
        <MdOutlineCancel className="h-5 w-5" />
      </Button>
      <div style={thumbInner}>
        <img
          src={imageView}
          style={{ objectFit: 'contain' }}
          alt=""
          onLoad={() => {
            URL.revokeObjectURL(file.preview);
          }}
        />
      </div>
    </div>
  ));

  React.useEffect(() => {
    // Make sure to revoke the data uris to avoid memory leaks, will run on unmount
    return () =>
      files.forEach((file: any) => URL.revokeObjectURL(file.preview));
  }, []);

  return (
    <section className="container">
      <div
        {...getRootProps({ className: 'dropzone' })}
        className="flex min-h-min cursor-pointer flex-col items-center justify-center bg-gray-200 py-5"
      >
        <input {...getInputProps()} />
        <FaImage color="#363636" size={50} />
        <p className="mt-4">
          Drag & drop some files here, or click to select files
        </p>
      </div>
      <div>
        <aside className="mt-5 flex flex-row flex-wrap">
          {imgSource ? (
            <>
              <div className="relative flex h-52 w-56 rounded border-[1px] p-2">
                <Button
                  onClick={() => onChange('')}
                  className="absolute right-[-10px] top-[-10px] rounded-full border-[1px] bg-white"
                >
                  <MdOutlineCancel className="h-5 w-5" />
                </Button>
                <div style={thumbInner}>
                  <img src={imgSource} style={img} alt="" />
                </div>
              </div>{' '}
            </>
          ) : (
            <>{thumbs}</>
          )}
        </aside>
      </div>
    </section>
  );
};

export default ImageDropzone;
