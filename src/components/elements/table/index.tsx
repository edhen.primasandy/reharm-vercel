import { useActions, useState } from '@overmind/index';
import { Dropdown, Pagination, Table } from 'flowbite-react';
import React from 'react';
import { HiPencil, HiTrash } from 'react-icons/hi';

import Button from '../button';

interface Iheader {
  title: string;
  field: string;
  type?: string;
}
type Props = {
  data: any[];
  totalData: number;
  header: Iheader[];
  pageCount: number;
  searchText: string;
  haveAction?: boolean;
  customTableBody?: boolean;
  renderTableBody?: any;
};

const Layout = (props: Props) => {
  const { header, data, haveAction, totalData } = props;
  const { filter } = useState();
  const { changeFilter } = useActions();
  const [currPage, setCurrPage] = React.useState(1);
  // const [totalData, setTotalData] = React.useState(47);
  const [totalPages, setTotalPages] = React.useState(1);
  const renderCell = (item: any, key: string, index: number) => {
    switch (key) {
      case 'no':
        return filter.skip + index + 1;
      case 'photo':
        return (
          <div>
            <img
              className="h-6 w-6 rounded-full shadow-lg"
              src={item[key]}
              alt="user photo"
            />
          </div>
        );
      default:
        break;
    }
    return item[key];
  };
  const initPaging = () => {
    const remainder = totalData % filter.limit;
    let count = totalData / filter.limit;
    if (remainder > 0) {
      count = Math.ceil(count);
    }
    setTotalPages(count);
  };
  const changeShowPer = (n: number) => {
    if (filter.limit !== n) {
      changeFilter({ ...filter, limit: n, skip: 0 });
      setCurrPage(1);
    }
  };
  const onPageChange = (page: number) => {
    if (currPage !== page) {
      changeFilter({ ...filter, skip: (page - 1) * filter.limit });
    }
    setCurrPage(page);
  };
  // React.useEffect(() => {
  //   resetFilter();
  //   return () => {};
  // }, []);
  React.useEffect(() => {
    initPaging();
    return () => {};
  }, [filter.limit, totalData]);
  React.useEffect(() => {
    if (filter.skip === 0) {
      setCurrPage(1);
    }
    return () => {};
  }, [filter.skip]);

  return (
    <div className="mb-2 flex flex-col">
      <Table>
        <Table.Head className="bg-gray-300">
          {header.map((item, i) => (
            <Table.HeadCell key={i}>{item.title}</Table.HeadCell>
          ))}
          {haveAction && <Table.HeadCell>Action</Table.HeadCell>}
        </Table.Head>
        {props.customTableBody ? (
          <>{props.renderTableBody}</>
        ) : (
          <Table.Body className="divide-y">
            {data?.map((item, i) => (
              <Table.Row className="bg-white" key={i}>
                {header.map((h, ii) => {
                  if (h.type === 'list') {
                    return (
                      <>
                        <ul>
                          {item[h.field]?.map((o: any, iii: number) => {
                            return <li key={iii}>{o}</li>;
                          })}
                        </ul>
                      </>
                    );
                  }
                  return (
                    <Table.Cell key={ii}>
                      {renderCell(item, h.field, i)}
                    </Table.Cell>
                  );
                })}
                {haveAction && (
                  <Table.Cell width={100}>
                    <Button>
                      <HiPencil color="#2b6cb0" size={20} />
                    </Button>
                    <Button className="ml-3">
                      <HiTrash color="gray" size={20} />
                    </Button>
                  </Table.Cell>
                )}
              </Table.Row>
            ))}
          </Table.Body>
        )}
      </Table>
      <div className="my-3 flex items-center text-center">
        <div className="mt-2">
          Showing {filter.skip + 1} to {filter.skip + filter.limit} of{' '}
          {totalData} entries
        </div>
        <div className="flex-1">
          <Pagination
            currentPage={currPage}
            layout="pagination"
            onPageChange={onPageChange}
            showIcons={true}
            totalPages={totalPages}
            previousLabel="Prev"
            nextLabel="Next"
          />
        </div>
        <div className="mt-2">
          <Dropdown label={filter.limit}>
            <Dropdown.Item onClick={() => changeShowPer(5)}>5</Dropdown.Item>
            <Dropdown.Item onClick={() => changeShowPer(10)}>10</Dropdown.Item>
            <Dropdown.Item onClick={() => changeShowPer(15)}>15</Dropdown.Item>
            <Dropdown.Item onClick={() => changeShowPer(20)}>20</Dropdown.Item>
          </Dropdown>
        </div>
      </div>
    </div>
  );
};

export default Layout;
