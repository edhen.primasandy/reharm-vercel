/* eslint-disable tailwindcss/no-custom-classname */
import { Label, TextInput } from 'flowbite-react';
import React from 'react';

type Props = {
  id: string;
  label: string;
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

const Layout = (props: Props) => {
  return (
    <div className="mb-2">
      <div className="mb-2 block">
        <Label htmlFor={props.id} value={props.label} />
      </div>
      <TextInput
        className="eTextinput"
        value={props.value}
        id={props.id}
        type="text"
        sizing="md"
        onChange={props.onChange}
      />
    </div>
  );
};

export default Layout;
