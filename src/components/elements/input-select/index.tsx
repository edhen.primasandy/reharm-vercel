import { Label } from 'flowbite-react';
import React from 'react';
import Select from 'react-select';

type Props = {
  id?: string;
  data: any;
  value: any;
  onChange: any;
  label: string;
  placeholder?: string;
  required?: boolean;
  isClearable?: boolean;
  onFocus?: React.FocusEventHandler<HTMLInputElement> | undefined;
};

const Layout = (props: Props) => {
  const { id, data, value, onChange, label, placeholder, required } = props;
  let selected: any = null;
  if (value) {
    if (typeof value === 'number' || typeof value === 'string') {
      const find = data.find((o: any) => o.value === value);
      selected = find;
    } else if (typeof value === 'object') {
      selected = value;
    }
  }
  return (
    <div id={id ?? 'select'}>
      <div className="mb-2 block">
        <Label htmlFor={id ?? 'select'} value={label} />
      </div>
      <Select
        {...props}
        required={required}
        classNamePrefix={'eSelect'}
        className="focus:border-cyan-500"
        value={selected}
        onChange={onChange}
        options={data}
        isSearchable
        placeholder={placeholder}
        menuPosition="fixed"
        menuPortalTarget={document.body}
        styles={{ menuPortal: (base) => ({ ...base, zIndex: 9999 }) }}
      />
      {/* <Select
        id={id ?? 'select'}
        required={true}
        value={value}
        onChange={onChange}
      >
        {!value && <option value="">{placeholder ?? 'Select item'}</option>}
        {data.map((item: any, i: number) => {
          if (item.value === 'group') {
            return <optgroup key={i} label={item.text}></optgroup>;
          }
          return (
            <option key={i} value={item.value}>
              {item.text}
            </option>
          );
        })}
      </Select> */}
    </div>
  );
};

export default Layout;
