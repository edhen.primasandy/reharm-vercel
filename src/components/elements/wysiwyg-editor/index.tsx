import 'jodit/build/jodit.min.css';

import JoditEditor from 'jodit-react';
import React from 'react';

type Props = { content: any; setContent: any };
const Layout = (props: Props) => {
  const { content, setContent } = props;
  const editor = React.useRef(null);
  const config = React.useMemo(
    () => ({
      readonly: false,
      toolbar: true,
      autofocus: true,
      cursorAfterAutofocus: 'end',
    }),
    []
  );
  // React.useEffect(() => {
  //   props.setContent(content);
  //   console.log(props.content);

  //   return () => {};
  // }, [content]);

  return (
    <JoditEditor
      ref={editor}
      value={content}
      config={config}
      onBlur={(newContent) => {
        setContent(newContent);
      }}
      // onChange={(newContent) => {
      //   // setContent(newContent);
      //   setContent(newContent);
      // }}
    />
  );
};

export default Layout;
