/* eslint-disable @typescript-eslint/no-shadow */
/* eslint-disable prefer-destructuring */
/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable tailwindcss/no-custom-classname */
import { Button as ButtonComp } from '@components/elements';
import {
  Accordion,
  AccordionBody,
  AccordionHeader,
} from '@material-tailwind/react';
import { Button, Label, Modal, TextInput } from 'flowbite-react';
// import {Button  } from '@components/elements'
import React from 'react';
import { DragDropContext, Draggable } from 'react-beautiful-dnd';
import {
  BsFileImage,
  BsFilePdf,
  BsFileText,
  BsFileWord,
  BsFillPlayBtnFill,
} from 'react-icons/bs';
import {
  HiOutlineDocumentAdd,
  HiPencil,
  HiSortAscending,
  HiTrash,
} from 'react-icons/hi';
import { MdOutlineDelete } from 'react-icons/md';
import { toast } from 'react-toastify';
import Swal from 'sweetalert2';

import { useActions } from '@/overmind';

import ModalFormLesson from './modal-form-lesson';
import DropAbleStrictMode from './strictmode-dropable';

type Props = {
  courseId: any;
  onChangeData: any;
  data: any;
};

const RenderSection = ({
  section,
  lessons,
  index,
  onDeleteSection,
  onEditSection,
  onDeleteLesson,
  onEditLesson,
  onSubmitSortLesson,
}: any) => {
  const [open, setOpen] = React.useState(true);
  const getIcon = (type: string) => {
    switch (type) {
      case 'url':
        return <BsFillPlayBtnFill />;
      case 'pdf':
        return <BsFilePdf />;
      case 'txt':
        return <BsFileText />;
      case 'doc':
        return <BsFileWord />;
      case 'img':
        return <BsFileImage />;
      default:
        return <></>;
    }
  };
  const [showSortModal, setShowSortModal] = React.useState(false);
  const [listDragableLesson, setListDragableLesson] = React.useState([] as any);
  const onDragEnd = (result: any) => {
    if (!result.destination) return;

    const items = Array.from(listDragableLesson);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);
    const mappingOrder = items.map((o: any, i) => {
      const newItem = {
        ...o.item,
        order: i + 1,
      };
      return { ...o, item: newItem };
    });
    setListDragableLesson(mappingOrder);
  };

  const documentBodyRef = React.useRef(null as any);
  React.useEffect(() => {
    documentBodyRef.current = document.body;
  }, []);

  React.useEffect(() => {
    if (showSortModal) {
      const maplesson = lessons.map((o: any, i: number) => {
        return {
          id: `Lesson ${i + 1}`,
          item: o,
        };
      });
      setListDragableLesson(maplesson);
    }
  }, [showSortModal]);

  const onSaveSortingLesson = () => {
    onSubmitSortLesson(
      listDragableLesson.map((o: any) => {
        return o.item.id;
      })
    );
  };
  return (
    <div>
      <Accordion
        open={open}
        className="mb-2 rounded-lg border border-gray-200 bg-slate-50 px-5 py-3 shadow-sm"
      >
        <AccordionHeader onClick={() => setOpen(!open)}>
          <div className="flex flex-1 items-center justify-between">
            Section {index + 1} - {section.title}
            <div className="flex gap-3">
              <Button
                gradientMonochrome="cyan"
                size="xs"
                onClick={(e) => {
                  e.stopPropagation();
                  setShowSortModal(true);
                }}
              >
                <HiSortAscending className="mr-1 h-3 w-3" />
                Sort
              </Button>
              <Button
                gradientMonochrome="cyan"
                size="xs"
                onClick={(e) => {
                  e.stopPropagation();
                  onEditSection();
                }}
              >
                <HiPencil className="mr-1 h-3 w-3" />
                Edit Section
              </Button>
              <Button
                gradientMonochrome="cyan"
                size="xs"
                onClick={(e) => {
                  e.stopPropagation();
                  onDeleteSection();
                }}
              >
                <MdOutlineDelete className="mr-1 h-3 w-3" />
                Delete Section
              </Button>
            </div>
          </div>
        </AccordionHeader>
        <AccordionBody>
          {lessons?.map((item: any, i: number) => {
            return (
              <div
                className="mb-2 flex flex-row items-center justify-between rounded-lg border border-gray-200 bg-white px-5 py-3 shadow-sm"
                key={i}
              >
                <div className="flex items-center gap-3">
                  {getIcon(item.attachment_type)}
                  <span>
                    Lesson {i + 1} - {item.title}
                  </span>
                </div>
                <div className="flex gap-3">
                  <ButtonComp
                    onClick={() => {
                      onEditLesson(item);
                    }}
                  >
                    <HiPencil color="#2b6cb0" size={20} />
                  </ButtonComp>
                  <ButtonComp
                    className="ml-3"
                    onClick={() => {
                      onDeleteLesson(item);
                    }}
                  >
                    <HiTrash color="gray" size={20} />
                  </ButtonComp>
                </div>
              </div>
            );
          })}
        </AccordionBody>
      </Accordion>
      {/* modal sort lesson */}
      <Modal
        root={documentBodyRef.current}
        show={showSortModal}
        onClose={() => setShowSortModal(false)}
      >
        <Modal.Header>
          Sorting Section {index + 1} - {section.title}
        </Modal.Header>
        <Modal.Body>
          <div>
            <DragDropContext onDragEnd={onDragEnd}>
              <DropAbleStrictMode droppableId="listDragableLesson">
                {(provided) => (
                  <ul
                    className="listDragableLesson"
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                  >
                    {listDragableLesson?.map((lesson: any, i: number) => {
                      return (
                        <Draggable
                          key={lesson.id}
                          draggableId={lesson.id}
                          index={i}
                        >
                          {(provided) => (
                            <li
                              className="mb-2 flex flex-row items-center rounded-lg border border-gray-200 bg-white px-5 py-3 shadow-sm"
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                            >
                              <div className="flex items-center gap-3">
                                {getIcon(lesson.item.attachment_type)}
                                <span>
                                  {lesson.id} - {lesson.item.title}
                                </span>
                              </div>
                            </li>
                          )}
                        </Draggable>
                      );
                    })}
                    {provided.placeholder}
                  </ul>
                )}
              </DropAbleStrictMode>
            </DragDropContext>
          </div>
        </Modal.Body>
        <Modal.Footer className="flex justify-end">
          <Button
            color="success"
            onClick={() => {
              onSaveSortingLesson();
              setShowSortModal(false);
            }}
          >
            Submit
          </Button>
          <Button color="gray" onClick={() => setShowSortModal(false)}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};
const Layout = (props: Props) => {
  const { courseId, data, onChangeData } = props;
  const {
    getsCourseSections,
    getsCourseLesson,
    deleteCourseSection,
    createCourseSection,
    updateCourseSection,
    createCourseLesson,
    updateCourseLesson,
    deleteCourseLesson,
    sortCourseSection,
    sortCourseLesson,
  } = useActions();
  const objNewSection = {
    course_id: courseId,
    title: '',
    order: 0,
  };
  const objNewLesson = {
    course_id: courseId,
    section_id: 0,
    title: '',
    duration: '',
    video_type: '',
    video_url: '',
    lesson_type: '',
    summary: '',
    order: 0,
    attachment: '',
    hour: 0,
    minute: 0,
    second: 0,
  };
  const [selectedSection, setSelectedSection] = React.useState({
    ...objNewSection,
  } as any);
  const [selectedLesson, setSelectedLesson] = React.useState({
    ...objNewLesson,
  } as any);

  const [dataSection, setDataSection] = React.useState([] as any);
  const [optionsSection, setOptionsSection] = React.useState([] as any);
  const [dataLesson, setDataLesson] = React.useState([] as any);
  const [showSortModal, setShowSortModal] = React.useState(false);

  const init = () => {
    getsCourseSections({
      course_id: parseInt(`${courseId}`, 10),
      sort_type: 'asc',
      sort: 'order',
    }).then((res: any) => {
      setDataSection(res.items);
      onChangeData({ ...data, sections: res.items.map((o: any) => o.id) });
      const options = res.items.map((item: any) => {
        return { value: item.id, label: item.title };
      });
      setOptionsSection([...options]);
    });
    getsCourseLesson({
      course_id: parseInt(`${courseId}`, 10),
      is_admin: 'yes',
      sort_type: 'asc',
      sort: 'order',
    }).then((res: any) => {
      setDataLesson(res.items);
    });
  };
  const onCreateNewSection = (item: any) => {
    const order = dataSection.length + 1;
    createCourseSection({ ...item, order }).then((res) => {
      init();
      toast.success(res.message);
    });
  };
  const onUpdateSection = (item: any) => {
    updateCourseSection({
      id: item.id,
      title: item.title,
      order: item.order,
    }).then((res) => {
      init();
      toast.success(res.message);
    });
  };
  const onCreateNewLesson = (item: any) => {
    const order = dataLesson.filter(
      (o: any) => o.section_id === item.section_id
    ).length;
    createCourseLesson({
      ...item,
      order,
      duration: `${item.hour}:${item.minute}:${item.second}`,
    }).then((res) => {
      init();
      toast.success(res.message);
    });
  };
  const updateLesson = (item: any) => {
    updateCourseLesson({
      ...item,
      duration: `${item.hour}:${item.minute}:${item.second}`,
    }).then((res) => {
      init();
      toast.success(res.message);
    });
  };
  const onDeteleSection = (item: any) => {
    Swal.fire({
      title: 'Are you sure? ',
      text: 'You want to delete this section',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        deleteCourseSection({ id: item.id }).then((res) => {
          init();
          toast.success(res.message);
        });
      }
    });
  };
  const onDeteleLesson = (item: any) => {
    Swal.fire({
      title: 'Are you sure? ',
      text: 'You want to delete this lesson',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Delete',
      denyButtonText: `Cancel`,
    }).then((result) => {
      if (result.isConfirmed) {
        deleteCourseLesson({ id: item.id }).then((res) => {
          init();
          toast.success(res.message);
        });
      }
    });
  };
  const sortingLesson = (orders: []) => {
    sortCourseLesson({ order: orders }).then(() => {
      init();
    });
  };
  React.useEffect(() => {
    init();
    return () => {};
  }, []);

  const [showFormSection, setShowFormSection] = React.useState(false);
  const [showFormLesson, setShowFormLesson] = React.useState(false);

  const documentBodyRef = React.useRef(null as any);
  React.useEffect(() => {
    documentBodyRef.current = document.body;
  }, []);
  const [listDragableSection, setListDragableSection] = React.useState(
    [] as any
  );
  const onDragEnd = (result: any) => {
    if (!result.destination) return;

    const items = Array.from(listDragableSection);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);
    const mappingOrder = items.map((o: any, i) => {
      const newItem = {
        ...o.item,
        order: i + 1,
      };
      return { ...o, item: newItem };
    });
    setListDragableSection(mappingOrder);
  };

  React.useEffect(() => {
    if (showSortModal) {
      const maplesson = dataSection.map((o: any, i: number) => {
        return {
          id: `Section ${i + 1}`,
          item: o,
        };
      });
      setListDragableSection(maplesson);
    }
  }, [showSortModal]);
  const sortingSection = () => {
    const orders = listDragableSection.map((o: any) => {
      return o.item.id;
    });
    sortCourseSection({ order: orders }).then(() => {
      init();
    });
  };
  return (
    <div>
      <div className="mb-2 flex flex-row justify-end gap-3">
        <Button
          color="gray"
          onClick={() => {
            setShowSortModal(true);
          }}
        >
          <HiSortAscending className="mr-2 h-5 w-5" />
          Sort Section
        </Button>
        <Button
          color="gray"
          onClick={() => {
            setSelectedSection({ ...objNewSection });
            setShowFormSection(true);
          }}
        >
          <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
          Add Section
        </Button>
        <Button
          color="gray"
          onClick={() => {
            setSelectedLesson({ ...objNewLesson });
            setShowFormLesson(true);
          }}
        >
          <HiOutlineDocumentAdd className="mr-2 h-5 w-5" />
          Add Lesson
        </Button>
      </div>
      <div className="mb-2">
        {dataSection.map((item: any, i: number) => {
          const filteredLesson = dataLesson.filter(
            (o: any) => o.section_id === item.id
          );
          return (
            <div key={i}>
              <RenderSection
                section={item}
                lessons={filteredLesson}
                index={i}
                onDeleteSection={() => {
                  onDeteleSection(item);
                }}
                onEditSection={() => {
                  setSelectedSection(item);
                  setShowFormSection(true);
                }}
                onDeleteLesson={() => {
                  onDeteleLesson(item);
                }}
                onEditLesson={(lesson: any) => {
                  const newlesson: any = {
                    ...lesson,
                    hour: 0,
                    minute: 0,
                    second: 0,
                  };
                  if (lesson.duration !== null) {
                    const times = newlesson.duration.split(':');
                    if (times.length === 3) {
                      newlesson.hour = times[0];
                      newlesson.minute = times[1];
                      newlesson.second = times[2];
                    }
                  }
                  setSelectedLesson(newlesson);
                  setShowFormLesson(true);
                }}
                onSubmitSortLesson={(orders: any) => sortingLesson(orders)}
              />
            </div>
          );
        })}
      </div>
      {/* modal sort section */}
      <Modal
        root={documentBodyRef.current}
        show={showSortModal}
        onClose={() => setShowSortModal(false)}
      >
        <Modal.Header>Sorting Section</Modal.Header>
        <Modal.Body>
          <div>
            <DragDropContext onDragEnd={onDragEnd}>
              <DropAbleStrictMode droppableId="listDragableSection">
                {(provided) => (
                  <ul
                    className="listDragableSection"
                    {...provided.droppableProps}
                    ref={provided.innerRef}
                  >
                    {listDragableSection?.map((section: any, i: number) => {
                      return (
                        <Draggable
                          key={section.id}
                          draggableId={section.id}
                          index={i}
                        >
                          {(provided) => (
                            <li
                              className="mb-2 flex flex-row items-center rounded-lg border border-gray-200 bg-white px-5 py-3 shadow-sm"
                              ref={provided.innerRef}
                              {...provided.draggableProps}
                              {...provided.dragHandleProps}
                            >
                              <div className="flex items-center gap-3">
                                <span>
                                  {section.id} - {section.item.title}
                                </span>
                              </div>
                            </li>
                          )}
                        </Draggable>
                      );
                    })}
                    {provided.placeholder}
                  </ul>
                )}
              </DropAbleStrictMode>
            </DragDropContext>
          </div>
        </Modal.Body>
        <Modal.Footer className="flex justify-end">
          <Button
            color="success"
            onClick={() => {
              sortingSection();
              setShowSortModal(false);
            }}
          >
            Submit
          </Button>
          <Button color="gray" onClick={() => setShowSortModal(false)}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
      {/* modal section */}
      <Modal
        root={documentBodyRef.current}
        show={showFormSection}
        onClose={() => setShowFormSection(false)}
      >
        <Modal.Header>
          {selectedSection.title ? 'Edit' : 'Add'} new section
        </Modal.Header>
        <Modal.Body>
          <div>
            <div className="mb-2 block">
              <Label htmlFor="title" value="Title*" />
            </div>
            <TextInput
              className="eTextinput"
              placeholder="Title"
              required
              value={selectedSection.title}
              id="title"
              type="text"
              sizing="md"
              onChange={(e) => {
                setSelectedSection({
                  ...selectedSection,
                  title: e.target.value,
                });
              }}
            />
          </div>
        </Modal.Body>
        <Modal.Footer className="flex justify-end">
          <Button
            color="success"
            onClick={() => {
              if (!selectedSection?.id) {
                onCreateNewSection(selectedSection);
              } else {
                onUpdateSection(selectedSection);
              }
              setShowFormSection(false);
            }}
          >
            Submit
          </Button>
          <Button color="gray" onClick={() => setShowFormSection(false)}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
      {/* modal lesson */}
      <ModalFormLesson
        show={showFormLesson}
        setShow={setShowFormLesson}
        data={selectedLesson}
        optionsSection={optionsSection}
        onChangeData={(item: any) => setSelectedLesson({ ...item })}
        onSubmit={() => {
          if (!selectedLesson?.id) {
            onCreateNewLesson(selectedLesson);
          } else {
            updateLesson(selectedLesson);
          }
          setShowFormLesson(false);
        }}
      />
    </div>
  );
};
export default Layout;
