/* eslint-disable tailwindcss/no-custom-classname */
import { Button, Label, TextInput } from 'flowbite-react';
import React from 'react';
import { HiOutlineMinus, HiOutlinePlus } from 'react-icons/hi';

type Props = {
  data: any;
  onChangeData: (value: any) => void;
};

const Layout = (props: Props) => {
  const { data, onChangeData } = props;
  // const [newReqs, setNewReqs] = React.useState(data.requirements);
  return (
    <div>
      <form className="grid grid-cols-1">
        <div className="mb-2">
          <div className="mb-2 block">
            <Label htmlFor="req" value="Requirements" />
          </div>
          {data.requirements.map((item: string, i: number) => (
            <div className="mb-2 flex flex-row gap-3" key={i}>
              <TextInput
                className="eTextinput basis-1/2"
                placeholder="Provide requirement"
                required
                value={item}
                id="req"
                type="text"
                sizing="md"
                onChange={(e) => {
                  const reqs: any[] = [...data.requirements];
                  reqs[i] = e.target.value;
                  const newdata = { ...data, requirements: reqs };
                  onChangeData(newdata);
                }}
              />
              {i === 0 ? (
                <Button
                  gradientMonochrome="success"
                  onClick={() => {
                    const reqs: any[] = [...data.requirements];
                    reqs.push('');
                    const newdata = { ...data, requirements: reqs };
                    onChangeData(newdata);
                  }}
                >
                  <HiOutlinePlus className="h-6 w-6" />
                </Button>
              ) : (
                <Button
                  gradientMonochrome="failure"
                  onClick={() => {
                    const reqs: any[] = [...data.requirements];
                    reqs.splice(i, 1);
                    const newdata = { ...data, requirements: reqs };
                    onChangeData(newdata);
                  }}
                >
                  <HiOutlineMinus className="h-6 w-6" />
                </Button>
              )}
            </div>
          ))}
        </div>
      </form>
    </div>
  );
};

export default Layout;
