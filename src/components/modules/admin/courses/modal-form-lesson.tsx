/* eslint-disable prefer-destructuring */
/* eslint-disable tailwindcss/no-custom-classname */
import { Button, Label, Modal, Textarea, TextInput } from 'flowbite-react';
import React from 'react';

import { InputSelect } from '@/components/elements';
import { useState } from '@/overmind';
import helper from '@/utils/helper';

type Props = {
  show: boolean;
  setShow: any;
  optionsSection: any;
  data: any;
  onSubmit: any;
  onChangeData: any;
};

const Layout = (props: Props) => {
  const { show, setShow, data, onChangeData, onSubmit, optionsSection } = props;
  const fileInput = React.useRef<HTMLInputElement>(null);
  const [accept, setAccept] = React.useState('');
  const { setting } = useState();
  const optionsLessonType = [
    { value: 'video', label: 'Video', attach: 'url', accept: 'video/*' },
    { value: 'txt', label: 'Text file', attach: 'txt', accept: '.text' },
    { value: 'pdf', label: 'PDF file', attach: 'pdf', accept: '.pdf' },
    {
      value: 'doc',
      label: 'Document file',
      attach: 'doc',
      accept: '.doc,.docx',
    },
    { value: 'img', label: 'Image file', attach: 'img', accept: 'image/*' },
  ];
  const optionsProvider = [
    { value: 'youtube', label: 'Youtube' },
    { value: 'vimeo', label: 'Vimeo' },
    { value: 'html5', label: 'Html5' },
    { value: 'google drive', label: 'Google Drive' },
  ];
  const handleFileinputAttachment = (e: any) => {
    const file = e.target.files[0];
    helper
      .getBase64(file)
      .then((result: any) => {
        const newData = { ...data, attachment: result };
        onChangeData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const handleFileinputThumnail = (e: any) => {
    const file = e.target.files[0];
    helper
      .getBase64(file)
      .then((result: any) => {
        const newData = { ...data, thumbnail: result };
        onChangeData(newData);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // React.useEffect(() => {
  //   getsSetting({}).then((res) => {
  //     setDataSetting(res);
  //   });
  //   return () => {};
  // }, []);
  const documentBodyRef = React.useRef(null as any);

  React.useEffect(() => {
    if (data.video_type === 'youtube') {
      // const urlsplit = data.video_url.split('/');
      const apiKeyObj: any = setting?.items?.find(
        (o: any) => o.key === 'youtube_api_key'
      );
      // if (urlsplit) {
      const id = helper.geIdVideoFromUrlYoutube(data.video_url);
      fetch(
        `https://www.googleapis.com/youtube/v3/videos?id=${id}&part=contentDetails&key=${apiKeyObj?.value}`
      )
        .then((response) => response.json())
        .then((response: any) => {
          if (response.items) {
            const dur: any = response.items[0]?.contentDetails?.duration;
            if (dur) {
              const formattedTime = dur
                .replace('PT', '')
                .replace('H', ':')
                .replace('M', ':')
                .replace('S', '');
              let hh = 0;
              let mm = 0;
              let ss = 0;
              const timesplit = formattedTime.split(':');
              if (timesplit.length === 3) {
                hh = timesplit[0] ? timesplit[0] : 0;
                mm = timesplit[1] ? timesplit[1] : 0;
                ss = timesplit[2] ? timesplit[2] : 0;
              } else if (timesplit.length === 2) {
                mm = timesplit[0] ? timesplit[0] : 0;
                ss = timesplit[1] ? timesplit[1] : 0;
              } else {
                ss = formattedTime;
              }
              const newData = {
                ...data,
                hour: hh,
                minute: mm,
                second: ss,
              };
              onChangeData(newData);
            } else {
              const newData = { ...data, hour: 0, minute: 0, second: 0 };
              onChangeData(newData);
            }
          }
        });
    }
    // }
    return () => {};
  }, [data.video_url]);
  React.useEffect(() => {
    documentBodyRef.current = document.body;
  }, []);
  return (
    <div>
      <Modal
        root={documentBodyRef.current}
        show={show}
        onClose={() => setShow(false)}
      >
        <Modal.Header>{data.title ? 'Edit' : 'Add new'} lesson</Modal.Header>
        <Modal.Body>
          <form>
            <div className="mb-2">
              <div className="mb-2 block">
                <Label htmlFor="title" value="Title*" />
              </div>
              <TextInput
                className="eTextinput"
                placeholder="Title"
                required
                value={data.title}
                id="title"
                type="text"
                sizing="md"
                onChange={(e) => {
                  const newData = { ...data, title: e.target.value };
                  onChangeData(newData);
                }}
              />
            </div>
            <div className="mb-2">
              <InputSelect
                label="Section*"
                data={optionsSection}
                value={data.section_id}
                placeholder="Select section"
                onChange={(e: any) => {
                  const newData = {
                    ...data,
                    section_id: e !== null ? e.value : 0,
                  };
                  onChangeData(newData);
                }}
              />
            </div>
            <div className="mb-2">
              <InputSelect
                label="Lesson type"
                data={optionsLessonType}
                value={
                  data.lesson_type === 'other'
                    ? data.attachment_type
                    : data.lesson_type
                }
                placeholder="Select type"
                onChange={(e: any) => {
                  const newData = {
                    ...data,
                    lesson_type: e !== null ? e.value : '',
                    attachment_type: e !== null ? e.attach : '',
                  };
                  setAccept(e !== null ? e.accept : '');
                  onChangeData(newData);
                }}
              />
            </div>
            {!['video', ''].includes(data.lesson_type) && (
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label htmlFor="attach" value="Attachment" />
                </div>
                <input
                  className="block w-full cursor-pointer rounded-lg border border-gray-300 bg-gray-50 text-sm text-gray-900 focus:outline-none dark:border-gray-600 dark:bg-gray-700 dark:text-gray-400 dark:placeholder:text-gray-400"
                  id="file_input"
                  type="file"
                  accept={accept}
                  ref={fileInput}
                  onChange={handleFileinputAttachment}
                ></input>
                {/* <FileInput id="file" ref={fileInput} /> */}
              </div>
            )}
            {data.lesson_type === 'video' && (
              <>
                <div className="mb-2">
                  <InputSelect
                    label="Lesson provider"
                    data={optionsProvider}
                    value={data.video_type}
                    placeholder="Select provider"
                    onChange={(e: any) => {
                      const newData = {
                        ...data,
                        video_type: e !== null ? e.value : '',
                      };
                      onChangeData(newData);
                    }}
                  />
                </div>
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="videourl" value="Video url" />
                  </div>
                  <TextInput
                    className="eTextinput"
                    placeholder="Video url"
                    required
                    value={data.video_url}
                    id="videourl"
                    type="text"
                    sizing="md"
                    onChange={(e) => {
                      const newData = { ...data, video_url: e.target.value };
                      onChangeData(newData);
                    }}
                  />
                </div>
                {data.video_type === 'html5' && (
                  <div className="mb-2">
                    <div className="mb-2 block">
                      <Label htmlFor="attach" value="Attachment" />
                    </div>
                    <input
                      className="block w-full cursor-pointer rounded-lg border border-gray-300 bg-gray-50 text-sm text-gray-900 focus:outline-none dark:border-gray-600 dark:bg-gray-700 dark:text-gray-400 dark:placeholder:text-gray-400"
                      id="file_input"
                      type="file"
                      accept={accept}
                      ref={fileInput}
                      onChange={handleFileinputThumnail}
                    ></input>
                  </div>
                )}
                <div className="mb-2">
                  <div className="mb-2 block">
                    <Label htmlFor="duration" value="Duration" />
                  </div>
                  <div className="grid grid-cols-3 gap-3">
                    <div>
                      <TextInput
                        className="eTextinput"
                        placeholder="Hour"
                        value={data.hour}
                        id="duration"
                        type="number"
                        sizing="md"
                        onChange={(e) => {
                          const newData = { ...data, hour: e.target.value };
                          onChangeData(newData);
                        }}
                      />
                    </div>
                    <div>
                      <TextInput
                        className="eTextinput"
                        placeholder="Minute"
                        value={data.minute}
                        id="duration"
                        type="number"
                        sizing="md"
                        onChange={(e) => {
                          const newData = { ...data, minute: e.target.value };
                          onChangeData(newData);
                        }}
                      />
                    </div>
                    <div>
                      <TextInput
                        className="eTextinput"
                        placeholder="Second"
                        value={data.second}
                        id="duration"
                        type="number"
                        sizing="md"
                        onChange={(e) => {
                          const newData = { ...data, second: e.target.value };
                          onChangeData(newData);
                        }}
                      />
                    </div>
                  </div>
                  {/* <TextInput
                  className="eTextinput"
                  placeholder="Duration"
                  required
                  value={data.duration}
                  id="duration"
                  type="time"
                  step="1"
                  min="00:00"
                  max="23:59"
                  sizing="md"
                  onChange={(e) => {
                    const newData = { ...data, duration: e.target.value };
                    onChangeData(newData);
                  }}
                /> */}
                </div>
              </>
            )}
            <div className="mb-2">
              <div className="mb-2 block">
                <Label htmlFor="summary" value="Summary" />
              </div>
              <Textarea
                className="eTextarea"
                placeholder="Summary"
                required
                value={data.summary}
                id="summary"
                onChange={(e) => {
                  const newData = { ...data, summary: e.target.value };
                  onChangeData(newData);
                }}
              />
            </div>
          </form>
        </Modal.Body>
        <Modal.Footer className="flex justify-end">
          <Button
            color="success"
            onClick={() => {
              onSubmit();
            }}
          >
            Submit
          </Button>
          <Button color="gray" onClick={() => setShow(false)}>
            Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Layout;
