/* eslint-disable tailwindcss/no-custom-classname */
import { Checkbox, Label, TextInput } from 'flowbite-react';
import React from 'react';

type Props = {
  data: any;
  onChangeData: (value: any) => void;
};

const Layout = (props: Props) => {
  const { data, onChangeData } = props;
  const [isFree, setIsFree] = React.useState(data.is_free_course === 'yes');
  const [isDiscount, setIsDiscount] = React.useState(
    data.discount_flag === 'yes'
  );
  const [discountPercentage, setDiscountPercentage] = React.useState(0);
  React.useEffect(() => {
    let percetage = 0;
    if (data.discounted_price > 0) {
      percetage = ((data.price - data.discounted_price) / data.price) * 100;
    }
    setDiscountPercentage(percetage);
    return () => {};
  }, [data.discounted_price, data.price]);
  return (
    <div>
      <form className="grid grid-cols-2 gap-3">
        <div className="mb-2 flex items-center gap-2">
          <Checkbox
            id="is_free_course"
            checked={isFree}
            onChange={() => {
              const newdata = {
                ...data,
                is_free_course: !isFree ? 'yes' : 'no',
              };
              setIsFree(!isFree);
              onChangeData(newdata);
            }}
          />
          <Label htmlFor="is_free_course">Check if this is a free course</Label>
        </div>
        {!isFree && (
          <>
            <div className="mb-2 flex items-center gap-2">
              <Checkbox
                id="is_free_course"
                checked={isDiscount}
                onChange={() => {
                  const newdata = {
                    ...data,
                    discount_flag: !isDiscount ? 'yes' : 'no',
                  };
                  setIsDiscount(!isDiscount);
                  onChangeData(newdata);
                }}
              />
              <Label htmlFor="is_free_course">
                Check if this course has discount
              </Label>
            </div>
            <div className="mb-2">
              <div className="mb-2 block">
                <Label htmlFor="price" value="Course price (Rp)" />
              </div>
              <TextInput
                className="eTextinput"
                required
                value={data.price}
                id="price"
                type="number"
                sizing="md"
                onChange={(e) => {
                  const newdata = {
                    ...data,
                    price: e.target.value,
                  };
                  onChangeData(newdata);
                }}
              />
            </div>
            {isDiscount && (
              <div className="mb-2">
                <div className="mb-2 block">
                  <Label
                    htmlFor="discounted_price"
                    value="Discounted price (Rp)"
                  />
                </div>
                <TextInput
                  className="eTextinput"
                  required
                  value={data.discounted_price}
                  id="discounted_price"
                  type="number"
                  sizing="md"
                  // helperText={`This course has 40.00% Discount`}
                  onChange={(e) => {
                    const newdata = {
                      ...data,
                      discounted_price: e.target.value,
                    };
                    onChangeData(newdata);
                  }}
                />
                <span className="text-xs italic">
                  his course has{' '}
                  <span className="text-red-700">{discountPercentage}%</span>{' '}
                  Discount
                </span>
              </div>
            )}
          </>
        )}
      </form>
    </div>
  );
};
export default Layout;
