/* eslint-disable tailwindcss/no-custom-classname */
import { Button, Label, TextInput } from 'flowbite-react';
import React from 'react';
import { HiOutlineMinus, HiOutlinePlus } from 'react-icons/hi';

type Props = {
  data: any;
  onChangeData: (value: any) => void;
};

const Layout = (props: Props) => {
  const { data, onChangeData } = props;
  // const [newReqs, setNewReqs] = React.useState(data.outcomes);
  return (
    <div>
      <form className="grid grid-cols-1">
        <div className="mb-2">
          <div className="mb-2 block">
            <Label htmlFor="req" value="Outcomes" />
          </div>
          {data.outcomes.map((item: string, i: number) => (
            <div className="mb-2 flex flex-row gap-3" key={i}>
              <TextInput
                className="eTextinput basis-1/2"
                placeholder="Provide outcomes"
                required
                value={item}
                id="req"
                type="text"
                sizing="md"
                onChange={(e) => {
                  const reqs: any[] = [...data.outcomes];
                  reqs[i] = e.target.value;
                  const newdata = { ...data, outcomes: reqs };
                  onChangeData(newdata);
                }}
              />
              {i === 0 ? (
                <Button
                  gradientMonochrome="success"
                  onClick={() => {
                    const reqs: any[] = [...data.outcomes];
                    reqs.push('');
                    const newdata = { ...data, outcomes: reqs };
                    onChangeData(newdata);
                  }}
                >
                  <HiOutlinePlus className="h-6 w-6" />
                </Button>
              ) : (
                <Button
                  gradientMonochrome="failure"
                  onClick={() => {
                    const reqs: any[] = [...data.outcomes];
                    reqs.splice(i, 1);
                    const newdata = { ...data, outcomes: reqs };
                    onChangeData(newdata);
                  }}
                >
                  <HiOutlineMinus className="h-6 w-6" />
                </Button>
              )}
            </div>
          ))}
        </div>
      </form>
    </div>
  );
};

export default Layout;
