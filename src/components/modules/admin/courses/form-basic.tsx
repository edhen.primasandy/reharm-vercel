/* eslint-disable tailwindcss/no-custom-classname */
import InputSelect from '@components/elements/input-select';
import { Checkbox, Label, TextInput } from 'flowbite-react';
import dynamic from 'next/dynamic';
import React from 'react';

import { useActions } from '@/overmind';

type Props = {
  data: any;
  onChangeData: (value: any) => void;
};
const Editor = dynamic(() => import('@components/elements/wysiwyg-editor'), {
  ssr: false,
});
const Layout = (props: Props) => {
  const { data, onChangeData } = props;
  const { getsCategories } = useActions();
  const [dataCategory, setDataCategory] = React.useState([] as any);
  const [selectedCategory, setSelectedCategory] = React.useState(null);
  const [selectedLevel, setSelectedLevel] = React.useState(null);
  const [selectedLanguage, setSelectedLanguage] = React.useState(null);
  const [selectedStatus, setSelectedStatus] = React.useState(null);

  const dataLevel = [
    { value: 'beginner', label: 'Beginner' },
    { value: 'advanced', label: 'Advanced' },
    { value: 'intermediate', label: 'Intermediate' },
  ];
  const dataLanguage = [
    { value: 'english', label: 'English' },
    { value: 'indonesia', label: 'Indonesia' },
  ];
  const dataStatus = [
    { value: 'active', label: 'Active' },
    { value: 'pending', label: 'Pending' },
  ];

  React.useEffect(() => {
    getsCategories().then((res) => {
      const categories: any[] = [];
      res.items.forEach((o: any) => {
        // categories.push({
        //   value: 'group',
        //   label: o.name,
        // });
        o.childs.forEach((c: any) => {
          categories.push({
            value: c.id,
            label: `[${o.name}] - ${c.name}`,
          });
        });
      });
      const selected = categories.find((o) => o.value === data.category_id);
      if (selected) {
        setSelectedCategory(selected);
      }
      setDataCategory(categories);
    });
    const fetchLevel: any = dataLevel.find((o) => o.value === data.level);
    if (fetchLevel) {
      setSelectedLevel(fetchLevel);
    }
    const fetchLang: any = dataLanguage.find((o) => o.value === data.language);
    if (fetchLang) {
      setSelectedLanguage(fetchLang);
    }
    const fetchStatus: any = dataStatus.find((o) => o.value === data.status);
    if (fetchStatus) {
      setSelectedStatus(fetchStatus);
    }
    return () => {};
  }, [data?.id]);

  return (
    <div>
      <form className="grid grid-cols-2 gap-3">
        <div className="mb-2">
          <div className="mb-2 block">
            <Label htmlFor="title" value="Course title*" />
          </div>
          <TextInput
            className="eTextinput"
            required
            value={data.title}
            id="title"
            type="text"
            sizing="md"
            onChange={(e) => {
              const newdata = { ...data, title: e.target.value };
              onChangeData(newdata);
            }}
          />
        </div>
        <div className="mb-2">
          <div className="mb-2 block">
            <Label htmlFor="short_description" value="Short Description" />
          </div>
          <TextInput
            className="eTextinput"
            required
            value={data.short_description}
            id="short_description"
            type="text"
            sizing="md"
            onChange={(e) => {
              const newdata = { ...data, short_description: e.target.value };
              onChangeData(newdata);
            }}
          />
        </div>
        <div className="col-span-2 mb-2">
          <div className="mb-2 block">
            <Label htmlFor="description" value="Description" />
            <Editor
              content={data.description}
              setContent={(newcontent: any) => {
                const newdata = { ...data, description: newcontent };
                onChangeData(newdata);
              }}
            />
          </div>
        </div>
        <div className="mb-2">
          <InputSelect
            label="Category*"
            data={dataCategory}
            value={selectedCategory}
            placeholder="Select category"
            onChange={(e: any) => {
              const newdata = {
                ...data,
                category_id: e !== null ? e.value : 0,
              };
              setSelectedCategory(e);
              onChangeData(newdata);
            }}
          />
        </div>
        <div className="mb-2">
          <InputSelect
            label="Level"
            data={dataLevel}
            value={selectedLevel}
            placeholder="Select level"
            onChange={(e: any) => {
              const newdata = {
                ...data,
                level: e !== null ? e.value : '',
              };
              setSelectedLevel(e);
              onChangeData(newdata);
            }}
          />
        </div>
        <div className="mb-2">
          <InputSelect
            label="Language made in"
            data={dataLanguage}
            value={selectedLanguage}
            placeholder="Select language"
            onChange={(e: any) => {
              const newdata = {
                ...data,
                language: e !== null ? e.value : '',
              };
              setSelectedLanguage(e);
              onChangeData(newdata);
            }}
          />
        </div>
        <div className="mb-2">
          <InputSelect
            label="Status"
            data={dataStatus}
            value={selectedStatus}
            placeholder="Select Status"
            onChange={(e: any) => {
              const newdata = {
                ...data,
                status: e !== null ? e.value : '',
              };
              setSelectedStatus(e);
              onChangeData(newdata);
            }}
          />
        </div>
        <div className="mb-2 flex items-center gap-2">
          <Checkbox
            id="topcourse"
            checked={data.is_top_course === 'yes'}
            value={data.is_top_course}
            onChange={() => {
              const newdata = {
                ...data,
                is_top_course: data.is_top_course === 'no' ? 'yes' : 'no',
              };
              onChangeData(newdata);
            }}
          />
          <Label htmlFor="topcourse">Check if this course is top course</Label>
        </div>
      </form>
    </div>
  );
};

export default Layout;
