/* eslint-disable tailwindcss/no-custom-classname */
import { Config } from '@constant/index';
import { Label, TextInput } from 'flowbite-react';
import React from 'react';

import { InputSelect } from '@/components/elements';
import ImageDropzone from '@/components/elements/image-dropzone';

type Props = {
  data: any;
  onChangeData: (value: any) => void;
};

const Layout = (props: Props) => {
  const { data, onChangeData } = props;
  const [selectedProvider, setSelectedProvider] = React.useState(null as any);
  const [isBase64, setIsBase64] = React.useState(false);

  const options = [
    { value: 'youtube', label: 'Youtube' },
    { value: 'vimeo', label: 'Vimeo' },
    { value: 'html5', label: 'Html5' },
  ];
  React.useEffect(() => {
    if (data.video_provider) {
      const get = options.find((o) => o.value === data.video_provider);
      setSelectedProvider(get);
    }
    return () => {};
  }, []);

  return (
    <div>
      <form className="grid grid-cols-2 gap-3">
        <div className="mb-2">
          <InputSelect
            label="Course overview provider"
            data={options}
            value={selectedProvider}
            placeholder="Select provider"
            onChange={(e: any) => {
              const newdata = {
                ...data,
                video_provider: e !== null ? e.value : '',
              };
              setSelectedProvider(e);
              onChangeData(newdata);
            }}
          />
        </div>
        <div className="mb-2">
          <div className="mb-2 block">
            <Label htmlFor="video_url" value="Course overview url" />
          </div>
          <TextInput
            className="eTextinput"
            placeholder="E.g: https://www.youtube.com/watch?v=oBtf8Yglw2w"
            required
            value={data.video_url}
            id="video_url"
            type="text"
            sizing="md"
            onChange={(e) => {
              const newdata = { ...data, video_url: e.target.value };
              onChangeData(newdata);
            }}
          />
        </div>
        <div>
          <div className="mb-2 block">
            <Label htmlFor="thumbnail" value="Course thumbnail" />
          </div>
          <ImageDropzone
            imgSource={isBase64 ? '' : data.thumbnail}
            baseUrl={`${Config.assetUrl}/categories/`}
            onChange={(file) => {
              const newdata = { ...data, thumbnail: file };
              onChangeData(newdata);
              if (file) {
                setIsBase64(true);
              }
            }}
          />
        </div>
      </form>
    </div>
  );
};

export default Layout;
