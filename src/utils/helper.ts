/* eslint-disable no-console */
/* eslint-disable import/no-anonymous-default-export */
const currencyFormat = (
  num: number,
  prefix: string = 'Rp. ',
  thousandSeparator: string = '.'
) => {
  return (
    num
      ? [
          prefix,
          num
            .toString()
            .replace(/(\d)(?=(\d{3})+(?!\d))/g, `$1${thousandSeparator}`),
        ].filter((d) => d)
      : [prefix, 0]
  ).join(' ');
};
const getBase64 = (file: any) => {
  return new Promise((resolve) => {
    let fileInfo;
    let baseURL: any = '';
    // Make new FileReader
    const reader = new FileReader();

    // Convert the file to base64 text
    reader.readAsDataURL(file);

    // on reader load somthing...
    reader.onload = () => {
      // Make a fileInfo Object
      baseURL = reader.result;
      resolve(baseURL);
    };
    console.log(fileInfo);
  });
};
const geIdVideoFromUrlYoutube = (url: string) => {
  const videoid = url.match(
    /(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/
  );
  if (videoid != null) {
    return videoid[1];
  }
  return '';
};
export default { currencyFormat, getBase64, geIdVideoFromUrlYoutube };
