/* eslint-disable no-console */
/* eslint-disable no-param-reassign */
import { Config } from '@constant/index';
import axios from 'axios';
import { Cookies } from 'react-cookie';
import { toast } from 'react-toastify';

const http = axios.create({
  baseURL: Config.apiURL,
});
const cookies = new Cookies();

http.interceptors.request.use(
  async (config: any) => {
    const JWT_TOKEN = cookies.get('_reharm_token');
    // console.log('jwt', JWT_TOKEN);
    if (JWT_TOKEN != null) {
      config.headers.Authorization = `Bearer ${JWT_TOKEN}`;
    }
    return config;
  },
  (error) => {
    console.warn('error');
    return Promise.reject(error);
  }
);

http.interceptors.response.use(
  (response) => response,
  (error) => {
    if (
      typeof error.response !== 'undefined' &&
      error.response.status === 500
    ) {
      if (typeof error.response !== 'object') {
        console.log(error.response);
        if (
          error.response.data.indexOf('Mod jwt error:Token error:') > -1 ||
          error.response.data.indexOf(
            'Mod jwt error:unable to get session: Session not found'
          ) > -1 ||
          error.response.data.indexOf('Session ') > -1 ||
          error.response.data.indexOf('token') > -1 ||
          error.response.data.indexOf('not allowed') > -1 ||
          error.response.data.indexOf('Unauthenticated') > -1
        ) {
          cookies.remove('_reharm_token');
        }
      }
    }
    if (error?.response?.data) {
      toast.error(error.response.data);
      if (error.response.data.message.indexOf('Unauthenticated') > -1) {
        cookies.remove('_reharm_token', { path: '/' });
      }
    } else {
      toast.error(error.message);
    }
    return Promise.reject(error);
  }
);

export default http;
